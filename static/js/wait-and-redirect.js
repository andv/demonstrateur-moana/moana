function waitAndRedirect() {
  // We retrieve the URL and the time to wait - thanks to Django `json_script` tag
  const waitSeconds = JSON.parse(document.getElementById('waitSeconds').textContent);
  const urlRedirect = JSON.parse(document.getElementById('urlRedirect').textContent);

  setTimeout(function(){
    window.location.replace(urlRedirect );
  }, waitSeconds * 1000);
}

waitAndRedirect()
