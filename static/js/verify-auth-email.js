var loginForm = document.querySelector("#login-form")

if (loginForm) {
  loginForm.addEventListener("submit", function (event) {
    event.preventDefault()
    verifyAuthByEmail()
  })
}

async function verifyAuthByEmail() {
  var url = new URL(window.location.href)
  var data = new FormData(loginForm)
  var endpoint = `${url.origin}/api/accounts/auth-email-allowed/`
  var response = await fetch(endpoint, {
    method: "post",
    body: data,
  })
  var status = response.status
  if (status === 403) loginForm.submit()
  else if (status === 200) {
    var isAllowed = await response.json()
    if (isAllowed) loginForm.submit()
    else showErrorMesssage()
  }
}

function showErrorMesssage() {
  var messsage = document.querySelector(".message__auth-not-allowed")
  var emailForm = document.querySelector(".email-form")
  messsage.classList.remove("hidden")
  emailForm.classList.add("disabled")
}
