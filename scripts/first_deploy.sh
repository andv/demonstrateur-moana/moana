#!/bin/bash

# This script is used by scalingo and is meant to run on Review Apps.
# When operating the first deployment of the application, it replaces
# the post deploy hook.

echo ">>> Starting the first_deploy hook"
PG_OPTIONS="--clean --if-exists --no-owner --no-privileges --no-comments"
PG_EXCLUDE_SCHEMA="-N 'information_schema' -N '^pg_*'"
pg_dump $PG_OPTIONS $PG_EXCLUDE_SCHEMA --dbname $PARENT_POSTGRESQL_URL --format c --file /tmp/dump.pgsql
sleep 5 # Pause while SGBD loading
pg_restore $PG_OPTIONS --dbname $SCALINGO_POSTGRESQL_URL /tmp/dump.pgsql

python scripts/check_existing_users.py

# We want to include commands from the post deploy hook as well:
bash $HOME/scripts/post_deploy.sh
echo ">>> Leaving the first_deploy hook"
