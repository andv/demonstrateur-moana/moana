#!/bin/bash

if [ "$1" != "--days" ] || [ -z "$2" ]; then
    echo "Error: --days option is required"
    exit 1
fi

DAYS="$2"

echo ">>> Analyzing person list checks for $DAYS days"

python manage.py analyze_crew_imports --days $DAYS
python manage.py analyze_crew_check_actions --days $DAYS
python manage.py analyze_passenger_imports --days $DAYS
python manage.py analyze_passenger_check_actions --days $DAYS
python manage.py analyze_deleted_movements --days $DAYS

echo ">>> Analysis complete"

