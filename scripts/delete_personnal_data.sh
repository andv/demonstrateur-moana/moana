#!/bin/bash
# This script is for deleting all data related to user or passsangers and crew list

echo ">>> Deleting personnal data"

read -p "This will delete : crew and passengers lists, users not in Moana team. Press [Enter] key to start deleting or exit now..."

python manage.py delete_users
python manage.py delete_passengers_list
python manage.py delete_crew_list

echo ">>> Personnal data deleted"
