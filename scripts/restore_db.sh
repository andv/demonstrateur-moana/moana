#!/bin/bash

# This script expects dump files for posgresql and mysql to be
# available in /tmp/dump.pgsql and /tmp/dump.mysql.

# If you need to upload these files in a scalingo one-off container, you can use
# scalingo --app <my-app> run bash --file dump.mysql --file dump.pgsql

echo "Starting database restore"

read -p "This will erase the existing database : Press [Enter] key to start restore or exit now..."

echo ">>> Copy and restore data from prod PosgreSQL"
PG_OPTIONS="--clean --if-exists --no-owner --no-privileges --no-comments"
pg_restore $PG_OPTIONS --dbname $SCALINGO_POSTGRESQL_URL /tmp/uploads/dump.pgsql

echo ">>> Copy and restore data from prod MySQL"
dbclient-fetcher mysql
mysql -u $MYSQL_USER --password=$MYSQL_PASSWORD -h $MYSQL_HOST -P $MYSQL_PORT $MYSQL_DATABASE < /tmp/uploads/dump.mysql

echo ">>> Restore completed"
