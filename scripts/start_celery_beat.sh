#!/bin/bash

# This script is used by scalingo to start the the application
# It run every time the app container starts, for instance,
# after a deployment or when the container is restarted.

echo "Entering start celery beat script"
echo "Using Django settings module: $DJANGO_SETTINGS_MODULE"
celery -A backend.tasks worker --beat --concurrency=${CELERY_BEAT_CONCURRENCY:=4} --loglevel info -n celery_beat@%h
echo "Completed start celery beat script"
