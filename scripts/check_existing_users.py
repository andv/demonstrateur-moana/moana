from django.db import connection, DatabaseError
import sys, os
from colored import Fore, Back, Style


def check_database_initialized():
    try:
        with connection.cursor() as cursor:
            cursor.execute("SELECT COUNT(*) FROM accounts_user;")
            count = cursor.fetchone()[0]

        if count == 0:
            # On utilise plusieurs prints pour que la console imprime les messages dans l'ordre
            print(
                f"{Fore.yellow}{Back.black}/!\ La base de données ne contient aucun utilisateur !{Style.reset}"
            )
            print(
                f"{Fore.yellow}{Back.black} 1) Dans le cas d'un premier lancement, créez manuellement vos utilisateurs (cf. https://docs.djangoproject.com/fr/5.1/topics/auth/default/){Style.reset}"
            )
            print(
                f"{Fore.yellow}{Back.black} 2) Dans le cas d'une App Review, le script '/scripts/first_deploy.sh' a dû être exécuté par Scalingo avant la fin du lancement des bases de données ! Supprimez l'application et relancez le déploiement.{Style.reset}"
            )

    except DatabaseError as e:
        print(f"Une erreur est survenue lors de l'exécution de la requête : {e}")


if __name__ == "__main__":
    # Effectuer la vérification au démarrage que la BDD n'est pas vide !
    check_database_initialized()
