import argparse
import re
import subprocess


def change_version(version, file_path):
    print(f"Changing version in {file_path}")
    with open(file_path, "r") as file:
        content = file.read()
    new_content = re.sub(r"Version \d+\.\d+\.\d+", f"Version {version}", content)
    if new_content == content:
        print("-- No version changed in", file_path)
        return
    with open(file_path, "w") as file:
        file.write(new_content)
    print(f"-- Version updated in {file_path}")


def run_command(command, dry_run=False):
    print(f"Running command: {command}")
    if dry_run:
        return
    try:
        subprocess.run(command, shell=True, check=True)
    except subprocess.CalledProcessError as e:
        print(f"Error: {e}")


def git_tag_and_push(version: str, file_paths, dry_run=False):
    run_command("git checkout main", dry_run)
    for file_path in file_paths:
        run_command(f"git add {file_path}", dry_run)
    cmd = 'git commit -m "Bump version to ' + version + '"'
    run_command(cmd, dry_run)
    cmd = f"git tag {version}"
    run_command(cmd, dry_run)
    cmd = f"git push origin main"
    run_command(cmd, dry_run)
    cmd = f"git push origin {version}"
    run_command(cmd, dry_run)
    print("Git tag created and pushed successfully")


def confirm_action(prompt):
    while True:
        response = input(prompt).lower()
        if response in ["y", "yes"]:
            return True
        elif response in ["n", "no"]:
            return False
        else:
            print("Invalid response. Please enter 'y' or 'n'.")


if __name__ == "__main__":
    import subprocess

    parser = argparse.ArgumentParser(description="Bump version and create Git tag")
    parser.add_argument("version", help="The new version number (e.g., 1.8.2)")
    parser.add_argument("--dry-run", action="store_true", help="Dry-run commands")
    args = parser.parse_args()
    version = args.version
    dry_run = args.dry_run
    file_paths = [
        "frontend/components/AppFooter.vue",
        "templates/moana/blocks/footer.html",
    ]
    for file_path in file_paths:
        change_version(version, file_path)
    if dry_run:
        print("Dry-run mode :")
    else:
        confirm = confirm_action("Create a new Git tag and push it? (y/n) ")
        if not confirm:
            print("Aborting...")
            exit()
    git_tag_and_push(version, file_paths, dry_run)
