#!/bin/bash
# This script is ran by scalingo to start the application

echo ">>> Starting the post_deploy hook"

python manage.py migrate
python manage.py migrate_stats_database

python scripts/check_existing_users.py

echo ">>> Leaving the post_deploy hook"
