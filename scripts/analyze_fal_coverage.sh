#!/bin/bash

if [ "$1" != "--days" ] || [ -z "$2" ]; then
    echo "Error: --days option is required"
    exit 1
fi

DAYS="$2"

echo ">>> Analyzing FAL coverage for $DAYS days"

python manage.py analyze_fal6_coverage --days $DAYS
python manage.py analyze_fal5_coverage --days $DAYS
python manage.py analyze_fal1_coverage --days $DAYS
python manage.py analyze_sip_import --days $DAYS
python manage.py analyze_fal_coverage_deleted --days $DAYS

echo ">>> FAL coverage analysis complete"
