#!/bin/sh
python3 manage.py migrate
python3 manage.py migrate_stats_database
python3 manage.py collectstatic --noinput
python manage.py runserver 0.0.0.0:8000
gunicorn -w 4 -b 0.0.0.0:3000 backend.wsgi:application
