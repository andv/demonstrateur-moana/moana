import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import legacy from '@vitejs/plugin-legacy'
import eslintPlugin from 'vite-plugin-eslint'

import { resolve } from 'path'

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  const env = loadEnv(mode, process.cwd())

  return {
    plugins: [
      vue(),
      legacy({
        targets: ['> 1%, last 2 versions, IE 11, Firefox 52'],
      }),
      eslintPlugin(),
    ],
    resolve: {
      alias: {
        '@': resolve(__dirname, './frontend'),
      },
    },
    server: {
      proxy: {
        '/api': env.VITE_DOCKER_PROXY_URL || 'http://127.0.0.1:8000/',
      },
    },
    build: {
      assetsDir: 'static',
    },
    html: {
      cspNonce: '{{request.csp_nonce}}',
    },
    css: {
      preprocessorOptions: {
        scss: {
          api: 'modern-compiler',
        },
      },
    },
  }
})
