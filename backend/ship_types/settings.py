from backend.ship_types.models import ShipType


# This can be used for ship types fitlering purposes
OTHER_SHIP_TYPES = ShipType.objects.exclude(
        name_simple__in=["Cargo", "Croisière", "Ferry"]
    ).values_list("name_simple", flat=True).distinct()
