from rest_framework import serializers

from backend.ship_types.models import ShipType


class ShipTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShipType
        fields = ("name_reference", "name_fr", "name_simple")
