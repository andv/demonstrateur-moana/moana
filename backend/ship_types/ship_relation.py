from django.db import models

from backend.ship_types.models import ShipTypeMapping


class ShipTypeModelMixin(models.Model):
    def get_ship_type(self):
        mapping = ShipTypeMapping.objects.filter(imo_number=self.imo_number).first()
        if not mapping:
            return None
        return mapping.ship_type

    class Meta:
        abstract = True

    def link_ship_to_ship_type(self):
        raise NotImplementedError

    def save(self, *args, **kwargs):
        self.link_ship_to_ship_type()
        return super().save(*args, **kwargs)


class ShipTypeRelationOnShip(ShipTypeModelMixin, models.Model):
    """
    Base model to be used on ShipMovement with helpers and fields
    to relate a ship to a ship type.
    """

    ship_type = models.ForeignKey(
        to="ship_types.ShipType",
        verbose_name="type de navire",
        related_name="ship_type_ignore_+",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )

    class Meta:
        abstract = True

    def link_ship_to_ship_type(self):
        ship_type = self.get_ship_type()
        if not ship_type:
            return self
        self.ship_type = ship_type
        return self


class WithShipTypeInfo(ShipTypeModelMixin, models.Model):
    """
    Base model to be used on ShipData-like or any ShipMovementReference-like model used
    for logs, tracking and stats.
    This brings helpers and fields to relate the ship movement to a ship type.
    """

    ship_type_reference = models.CharField("type référence", max_length=256)
    ship_type_fr = models.CharField("type français", max_length=256, blank=True)
    ship_type_simple = models.CharField("type simplifié", max_length=256, blank=True)

    class Meta:
        abstract = True

    def link_ship_to_ship_type(self):
        ship_type = self.get_ship_type()
        if not ship_type:
            return self
        self.ship_type_reference = ship_type.name_reference
        self.ship_type_simple = ship_type.name_simple
        self.ship_type_fr = ship_type.name_fr
        return self
