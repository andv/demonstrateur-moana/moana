from django.db import models

from model_utils.models import TimeStampedModel


class ShipType(TimeStampedModel):
    name_reference = models.CharField("nom de référence", max_length=256)
    name_fr = models.CharField("nom en français", max_length=256, blank=True)
    name_simple = models.CharField("nom simplifié ", max_length=256, blank=True)

    class Meta:
        verbose_name = "type de navire"
        verbose_name_plural = "types de navires"

    def __str__(self):
        return f"{self.name_reference} ({self.name_simple})"


class ShipTypeMapping(TimeStampedModel):
    imo_number = models.CharField("IMO number", max_length=256)
    ship_type_name = models.CharField(
        "nom du type de navire", max_length=256, help_text="nom de référence"
    )
    ship_type = models.ForeignKey(
        to="ShipType",
        verbose_name="type de navire",
        related_name="type_mapping_ignore_+",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        help_text="La relation avec le type de navire se fait grâce au nom de référence",
    )

    class Meta:
        verbose_name = "correspondance type de navire"
        verbose_name_plural = "correspondances types de navires"

    def __str__(self):
        return f"{self.imo_number} ({self.ship_type_name})"

    def save(self, *args, **kwargs):
        matching_type = ShipType.objects.filter(
            name_reference=self.ship_type_name
        ).first()
        self.ship_type = matching_type
        super().save(*args, **kwargs)
