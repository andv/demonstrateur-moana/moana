from django.contrib import admin

from backend.ship_types.models import ShipType, ShipTypeMapping


@admin.register(ShipType)
class ShipTypeAdmin(admin.ModelAdmin):
    list_display = ["name_reference", "name_fr", "name_simple", "created", "modified"]
    search_fields = ["name_reference", "name_fr", "name_simple"]
    date_hierarchy = "created"


@admin.register(ShipTypeMapping)
class ShipTypeMappingAdmin(admin.ModelAdmin):
    list_display = ["imo_number", "ship_type_name", "ship_type", "created", "modified"]
    search_fields = ["imo_number", "ship_type_name"]
    readonly_fields = ["ship_type"]
    date_hierarchy = "created"
