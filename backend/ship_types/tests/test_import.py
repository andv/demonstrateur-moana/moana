from os import getcwd, path

from django.conf import settings
from django.core import management

import pytest

from backend.ship_types.models import ShipType, ShipTypeMapping

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


SHIP_TYPE_FILE_PATH = path.join(
    getcwd(), "backend/ship_types/tests/data/ship_types.csv"
)

MAPPING_FILE_PATH = path.join(
    getcwd(), "backend/ship_types/tests/data/ship_types_mapping.csv"
)


def test_import_command_creates_ship_types(client):
    before = ShipType.objects.count()
    management.call_command("import_ship_types", SHIP_TYPE_FILE_PATH)
    after = ShipType.objects.count()
    assert after == before + 4  # test file has 4 entries


def test_import_command_creates_ship_type_mapping(client):
    before = ShipTypeMapping.objects.count()
    management.call_command("import_ship_types_mapping", MAPPING_FILE_PATH)
    after = ShipTypeMapping.objects.count()
    assert after == before + 4  # test file has 4 entries
