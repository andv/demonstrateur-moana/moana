from import_export import fields as import_export_fields
from import_export import resources

from backend.ship_types.models import ShipType, ShipTypeMapping


class ShipTypeResource(resources.ModelResource):
    name_reference = import_export_fields.Field(
        attribute="name_reference", column_name="nom"
    )
    name_fr = import_export_fields.Field(attribute="name_fr", column_name="nom fr")
    name_simple = import_export_fields.Field(
        attribute="name_simple", column_name="nom simplifié"
    )

    class Meta:
        model = ShipType
        import_id_fields = ("name_reference",)
        fields = ("name_reference", "name_fr", "name_simple")


class ShipTypeMappingResource(resources.ModelResource):
    imo_number = import_export_fields.Field(
        attribute="imo_number", column_name="imonumber"
    )
    ship_type_name = import_export_fields.Field(
        attribute="ship_type_name", column_name="type"
    )

    class Meta:
        model = ShipTypeMapping
        import_id_fields = ("imo_number",)
        fields = ("imo_number", "ship_type_name")
        skip_unchanged = True
        report_skipped = False
        use_bulk = False  # We want run the save() method for each instance
        use_transactions = False  # This import can be a long process - avoid locking DB
