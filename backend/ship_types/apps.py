from django.apps import AppConfig


class ShipTypesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.ship_types"
    verbose_name = "4.1 Référentiels - Type navires"
