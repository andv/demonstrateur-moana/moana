from django.core.management.base import BaseCommand

from backend.ship_types.resources import ShipTypeMappingResource
from backend.initialization.data_import import CSVImportMixin


class Command(CSVImportMixin, BaseCommand):
    help = "Import mapping between ship and ship types, from a CSV file"
    resource_class = ShipTypeMappingResource
