from django.core.management.base import BaseCommand

from backend.ship_types.resources import ShipTypeResource
from backend.initialization.data_import import CSVImportMixin


class Command(CSVImportMixin, BaseCommand):
    help = "Import ship types from CSV file"
    resource_class = ShipTypeResource
