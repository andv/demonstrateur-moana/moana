from django.core.management.base import BaseCommand
from django.db.models import Count

from backend.ship_types.models import ShipTypeMapping


class Command(BaseCommand):
    help = "Clean duplicates from the ship type mapping database."

    @staticmethod
    def ask_yes_no_question(question, unanswered_return=None):
        """
        Prompt the given question, waiting for a yes/no response.
        Return a True/False boolean.
        """
        question += " [yes/no]: "
        while True:
            result = input(f"{question}").lower()
            if result in ["y", "n"] or not result:
                return result == "y" or unanswered_return

    def display_and_delete_items(self, to_be_deleted):
        self.stdout.write("\n-------------------")
        self.stdout.write("\nThe following ship types mapping will be deleted:")
        for ship_type_mapping in to_be_deleted:
            self.stdout.write(
                f"IMO {ship_type_mapping.imo_number} / "
                f"ID: {ship_type_mapping.id} / "
                f"Type: {ship_type_mapping.ship_type_name}"
            )
        self.stdout.write("\n-------------------")
        count_items = len(to_be_deleted)
        if self.ask_yes_no_question(f"Deleting {count_items} items, proceed ?"):
            for ship_type_mapping in to_be_deleted:
                ship_type_mapping.delete()
            self.stdout.write(f"Deleted {count_items} items")
        else:
            self.stdout.write("Aborting...")
        self.stdout.write("End cleaning up ship types mapping.")

    def handle(self, *args, **options):
        self.stdout.write("Start cleaning up ship types mapping.")
        duplicates = (
            ShipTypeMapping.objects.values("imo_number")
            .annotate(Count("id"))
            .filter(id__count__gt=1)
        )
        if not duplicates:
            self.stdout.write("No duplicates found, ending clean up script.")
            return
        to_be_deleted = []
        for duplicate_item in duplicates:
            imo = duplicate_item["imo_number"]
            self.stdout.write(f"\n=> Finding duplicates of IMO {imo}:")
            ship_type_mapping_qs = ShipTypeMapping.objects.filter(imo_number=imo)
            # Let's reverse the order, since we want the last item show-up first:
            ship_type_mapping_qs = ship_type_mapping_qs.order_by("-id")
            for counter, ship_type_mapping in enumerate(ship_type_mapping_qs):
                display_message = (
                    f"IMO {ship_type_mapping.imo_number} / "
                    f"ID: {ship_type_mapping.id} / "
                    f"Type: {ship_type_mapping.ship_type_name}"
                )
                if counter == 0:  # First item in the loop
                    display_message += " <-- keeping only this one"
                else:
                    to_be_deleted.append(ship_type_mapping)
                self.stdout.write(display_message)
        self.display_and_delete_items(to_be_deleted)
