from django.core.management.base import BaseCommand

from defusedcsv import csv


class Command(BaseCommand):
    help = "Clean duplicates from the CSV file ship type/imo mapping."

    def add_arguments(self, parser):
        parser.add_argument("file_name", type=str, help="The path to the csv file")

    def handle(self, *args, **options):
        file_name = options["file_name"]
        records = {}
        unique_records = []
        human_review_records = []
        removed_records = []

        self.stdout.write("Read the records")
        with open(file_name, "r") as file:
            reader = csv.DictReader(file)
            for row in reader:
                imo, ship_type = row["imonumber"], row["type"]
                if imo in records:
                    records[imo].append(ship_type)
                    removed_records.append((imo, ship_type))
                else:
                    records[imo] = [ship_type]

        self.stdout.write("Check each record for duplicates or conflicts")
        for imo, types in records.items():
            types_set = set(types)
            if len(types_set) == 1:
                unique_records.append((imo, types[0]))
            else:
                human_review_records.append((imo, types))

        self.stdout.write("Save unique records to a new csv file")
        with open("ship-types-mapping-cleaned.csv", "w", newline="") as csvfile:
            writer = csv.writer(csvfile, lineterminator="\n")
            writer.writerow(["imonumber", "type"])
            for record in unique_records:
                writer.writerow(list(record))

        self.stdout.write("Save removed records to a new file")
        with open("ship-types-mapping-removed.txt", "w") as file:
            file.write("Removed records (IMO number - Ship type):\n")
            for record in removed_records:
                file.write(f"{str(record)}\n")

        self.stdout.write("Save records for human review to a new file")
        with open("ship-types-mapping-cleaned-to-review.txt", "w") as file:
            file.write("Records for human review (IMO number - Ship type):\n")
            for record in human_review_records:
                file.write(f"{str(record)}\n")
