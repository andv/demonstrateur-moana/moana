from django.core.files.base import ContentFile
from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import get_object_or_404

from django_downloadview import VirtualDownloadView

from backend.geo import choices as geo_choices
from backend.logs.actions import ActionLogHandler
from backend.persons import choices as persons_choices
from backend.ports.user_relation import UserPortsViewMixin
from backend.ships.models import CrewList, PassengerList


class DownloadPersonListView(VirtualDownloadView, UserPortsViewMixin, ActionLogHandler):
    """
    Base class for downloading the CSV file.
    """

    PersonListModel = None  # Model class : CrewList or PassengerList
    person_list_type = None  # Choice of "crew" or "passenger"
    file_format = ""  # Choice of "fpr" or "roc"
    person_list = None  # Instance of model : CrewList() or PassengerList()
    download_action_name = "downloaded"
    is_annex = False

    def user_is_authorized(self, request):
        """
        Check if user is authorized to access the list.
        """
        return request.user.access_person_list

    def initialize(self):
        """
        Initialize parameters depending on the file format ROC/FPR and the list type Crew/Passenger.
        """
        self.person_list_type = self.kwargs.get("person_list_type")
        self.file_format = self.kwargs.get("file_format")
        if self.person_list_type == persons_choices.PERSON_LIST_TYPE_CREW:
            self.PersonListModel = CrewList
        if self.person_list_type == persons_choices.PERSON_LIST_TYPE_PASSENGER:
            self.PersonListModel = PassengerList

    def log_download_action(self, file_name):
        self.create_user_action_log(
            action=f"{self.download_action_name} {self.file_format} {self.person_list_type}",
            user=self.request.user,
            object=file_name,
            ship=self.person_list.ship_movement,
        )

    def get_object(self):
        """
        Returns an instance of either `PassengerList` or `CrewList`.
        """
        pk = self.kwargs.get("pk")
        areas_allowed = dict(geo_choices.AREA_ALLOWED_TO_DOWNLOAD_LISTS).keys()
        qs = self.PersonListModel.objects.filter(
            ship_movement__port_of_call_relation__in=self.assigned_ports,
            ship_movement__geographic_area__in=areas_allowed,
        )
        self.person_list = get_object_or_404(qs, pk=pk)
        return self.person_list

    def get_file(self):
        person_list = self.get_object()
        file_name = person_list.get_file_name(
            person_list_type=self.person_list_type,
            file_format=self.file_format,
            is_annex=self.is_annex,
        )
        self.log_download_action(file_name=file_name)
        file_content = person_list.get_file_content(
            file_format=self.file_format, is_annex=self.is_annex
        )
        return ContentFile(file_content, name=file_name)

    def get(self, request, *args, **kwargs):
        """
        Add gzip content encoding header to the response from base class.
        """
        if not self.user_is_authorized(request):
            return HttpResponseForbidden()
        self.initialize()
        response = super().get(request, *args, **kwargs)
        return response


class DownloadPersonListAnnexView(DownloadPersonListView):
    download_action_name = "downloaded annex"
    is_annex = True
