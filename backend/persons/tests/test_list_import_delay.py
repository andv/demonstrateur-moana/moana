from venv import create
from zoneinfo import ZoneInfo

from django.conf import settings
from django.utils.timezone import datetime, timedelta

import pytest

from backend.persons import choices as persons_choices
from backend.ships import settings as ships_settings
from backend.ships.models import CrewList, PassengerList
from backend.unit_tests import factories

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


recent = datetime.now(ZoneInfo("Europe/Paris"))
past = recent - timedelta(days=2)
futur = recent + timedelta(days=2)
tomorrow = recent + timedelta(days=1)


def create_ship_movements(arrival_time):
    saint_malo = "FRSML"
    ship = factories.ShipMovementFactory(
        eta_to_port_of_call=arrival_time,
        way=ships_settings.ARRIVING,
        port_of_call=saint_malo,
        last_port="AZXXX",
    )
    return ship


def test_crew_list_is_marked_as_delayed_if_imported_after_arrival(client):
    ship = create_ship_movements(arrival_time=past)
    crew_list = CrewList.objects.create(ship_movement=ship)
    assert crew_list.delay_type == persons_choices.DELAY_AFTER_ARRIVAL
    assert (
        crew_list.ship_movement.crew_list_delay_type
        == persons_choices.DELAY_AFTER_ARRIVAL
    )


def test_crew_list_is_marked_as_delayed_if_imported_after_24_hours_notice(client):
    ship = create_ship_movements(arrival_time=tomorrow)
    crew_list = CrewList.objects.create(ship_movement=ship)
    assert crew_list.delay_type == persons_choices.DELAY_MISSED_24H_NOTICE
    assert (
        crew_list.ship_movement.crew_list_delay_type
        == persons_choices.DELAY_MISSED_24H_NOTICE
    )


def test_crew_list_is_not_marked_as_delayed_if_imported_before_arrival(client):
    ship = create_ship_movements(arrival_time=futur)
    crew_list = CrewList.objects.create(ship_movement=ship)
    assert crew_list.delay_type != persons_choices.DELAY_AFTER_ARRIVAL
    assert (
        crew_list.ship_movement.crew_list_delay_type
        != persons_choices.DELAY_AFTER_ARRIVAL
    )


def test_passenger_list_is_marked_as_delayed_if_imported_after_arrival(client):
    ship = create_ship_movements(arrival_time=past)
    passenger_list = PassengerList.objects.create(ship_movement=ship)
    assert passenger_list.delay_type == persons_choices.DELAY_AFTER_ARRIVAL
    assert (
        passenger_list.ship_movement.passenger_list_delay_type
        == persons_choices.DELAY_AFTER_ARRIVAL
    )


def test_passenger_list_is_marked_as_delayed_if_imported_after_24_hours_notice(client):
    ship = create_ship_movements(arrival_time=tomorrow)
    passenger_list = PassengerList.objects.create(ship_movement=ship)
    assert passenger_list.delay_type == persons_choices.DELAY_MISSED_24H_NOTICE
    assert (
        passenger_list.ship_movement.passenger_list_delay_type
        == persons_choices.DELAY_MISSED_24H_NOTICE
    )


def test_passenger_list_is_not_marked_as_delayed_if_imported_before_arrival(client):
    ship = create_ship_movements(arrival_time=futur)
    passenger_list = PassengerList.objects.create(ship_movement=ship)
    assert passenger_list.delay_type != persons_choices.DELAY_AFTER_ARRIVAL
    assert (
        passenger_list.ship_movement.passenger_list_delay_type
        != persons_choices.DELAY_AFTER_ARRIVAL
    )
