from django.conf import settings
from django.contrib.auth import get_user_model
from django.urls import reverse

import pytest

from backend.ships import settings as ships_settings
from backend.ships.models import CrewList, PassengerList
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())

User = get_user_model()


def get_download_urls_for_user(client, user):
    ship = factories.ShipMovementFactory(
        way=ships_settings.ARRIVING,
        port_of_call="FRSML",
        last_port="AZXXX",
    )
    CrewList.objects.create(
        ship_movement=ship,
        fpr_file_content=b"test,test,test",
        roc_file_content=b"test,test,test",
        roc_annex_file_content=b"test,test,test",
        fpr_annex_file_content=b"test,test,test",
    )
    PassengerList.objects.create(
        ship_movement=ship,
        fpr_file_content=b"test,test,test",
        roc_file_content=b"test,test,test",
        roc_annex_file_content=b"test,test,test",
        fpr_annex_file_content=b"test,test,test",
    )
    download_urls = [
        ship.crew_list_file_roc,
        ship.crew_list_file_fpr,
        ship.passengers_list_file_fpr,
        ship.passengers_list_file_roc,
        ship.crew_list_annex_file_fpr,
        ship.passengers_list_annex_file_fpr,
        ship.crew_list_annex_file_roc,
        ship.passengers_list_annex_file_roc,
    ]
    return download_urls


def test_user_can_download_person_list_csv_files(client):
    user = utils.login_saint_malo_user(client)
    user.access_person_list = True
    user.save()
    urls_to_test = get_download_urls_for_user(client, user)
    for url in urls_to_test:
        response = client.get(url)
        assert response.status_code == 200
        assert "attachment" in response.get("content-disposition", "")


def test_user_cannot_download_person_list_csv_files_if_unauthorized(client):
    user = utils.login_saint_malo_user(client)
    user.access_person_list = False
    user.save()
    urls_to_test = get_download_urls_for_user(client, user)
    for url in urls_to_test:
        response = client.get(url)
        assert response.status_code == 403
