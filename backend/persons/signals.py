from django.db.models.signals import post_save
from django.dispatch import receiver

from backend.persons import choices as persons_choices
from backend.ships.models import CrewList, PassengerList


@receiver(post_save, sender=PassengerList)
@receiver(post_save, sender=CrewList)
def update_list_arrival_delay(sender, instance, **kwargs):
    """
    Check if the list is delayed and update the delay type field accordingly.
    """
    post_save_ongoing = getattr(instance, "post_save_ongoing", False)
    if post_save_ongoing:
        return None  # Prevent post save looping on list instance
    instance.post_save_ongoing = True
    ship = instance.ship_movement
    if not ship:
        return
    if not ship.is_arriving or not ship.relevant_time:
        return
    delay_type = get_delay_type(instance, ship)
    instance.delay_type = delay_type
    instance.save()
    # Now that we have saved the list, we should update the ship movement to update
    # the delay type field there as well.
    ship_post_save_ongoing = getattr(instance.ship_movement, "post_save_ongoing", False)
    if ship_post_save_ongoing:
        return None  # Prevent post save looping on ship
    instance.ship_movement.post_save_ongoing = True
    instance.ship_movement.save()


def get_delay_type(person_list, ship):
    """
    Verify if the last list received for the ship is too late,
    and if so returns the corresponding type of delay detected.
    """
    if person_list.created > ship.relevant_time:
        return persons_choices.DELAY_AFTER_ARRIVAL
    delay_type = ""
    if ship.is_long_travel():
        list_too_late_24_hours = person_list.created > ship.h24_before_arrival
        if list_too_late_24_hours:
            delay_type = persons_choices.DELAY_MISSED_24H_NOTICE
    return delay_type
