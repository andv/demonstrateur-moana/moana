from django.db import models
from django.urls import reverse

from backend.geo import choices as geo_choices
from backend.persons.person_counters import WithPersonCounters


class PersonOnShip(WithPersonCounters):
    """
    This abstract class is expected to be sub-classed on a ShipMovement-like model.
    It provides crew and passengers helpers.
    """

    class Meta:
        abstract = True

    @property
    def crew_list_file_fpr(self):
        if not hasattr(self, "crew_list"):
            return ""
        if not self.crew_list.fpr_file_content:
            return ""
        return reverse("crew_fpr_file", args=[self.crew_list.pk])

    @property
    def passengers_list_file_fpr(self):
        if not hasattr(self, "passenger_list"):
            return ""
        if not self.passenger_list.fpr_file_content:
            return ""
        return reverse("passengers_fpr_file", args=[self.passenger_list.pk])

    @property
    def crew_list_file_roc(self):
        if not hasattr(self, "crew_list"):
            return ""
        if not self.crew_list.roc_file_content:
            return ""
        return reverse("crew_roc_file", args=[self.crew_list.pk])

    @property
    def passengers_list_file_roc(self):
        if not hasattr(self, "passenger_list"):
            return ""
        if not self.passenger_list.roc_file_content:
            return ""
        return reverse("passengers_roc_file", args=[self.passenger_list.pk])

    @property
    def crew_list_annex_file_fpr(self):
        if not hasattr(self, "crew_list"):
            return ""
        if not self.crew_list.fpr_annex_file_content:
            return ""
        return reverse("crew_fpr_annex_file", args=[self.crew_list.pk])

    @property
    def passengers_list_annex_file_fpr(self):
        if not hasattr(self, "passenger_list"):
            return ""
        if not self.passenger_list.fpr_annex_file_content:
            return ""
        return reverse("passengers_fpr_annex_file", args=[self.passenger_list.pk])

    @property
    def crew_list_annex_file_roc(self):
        if not hasattr(self, "crew_list"):
            return ""
        if not self.crew_list.roc_annex_file_content:
            return ""
        return reverse("crew_roc_annex_file", args=[self.crew_list.pk])

    @property
    def passengers_list_annex_file_roc(self):
        if not hasattr(self, "passenger_list"):
            return ""
        if not self.passenger_list.roc_annex_file_content:
            return ""
        return reverse("passengers_roc_annex_file", args=[self.passenger_list.pk])

    @property
    def can_download_lists(self):
        geographic_area = self.get_geographic_area()
        allowed_areas = dict(geo_choices.AREA_ALLOWED_TO_DOWNLOAD_LISTS).keys()
        return geographic_area in allowed_areas

    @property
    def number_of_crew_parsed(self):
        if not hasattr(self, "crew_list"):
            return 0
        if not self.crew_list.number_of_crew_parsed:
            return 0
        return self.crew_list.number_of_crew_parsed

    @property
    def number_of_passengers_parsed(self):
        if not hasattr(self, "passenger_list"):
            return 0
        if not self.passenger_list.number_of_passengers_parsed:
            return 0
        return self.passenger_list.number_of_passengers_parsed

    @property
    def has_passengers(self):
        return bool(self.number_of_passengers_parsed)

    @property
    def has_crew_members(self):
        return bool(self.number_of_crew_parsed)

    def link_ship_to_crew(self):
        """
        Link the given ship movement entry to it's related crew list.
        """
        ship = self
        related_crew = self.related_crew
        if related_crew and not not related_crew.ship_movement:
            related_crew.ship_movement = ship
            related_crew.save()
        return ship

    def link_ship_to_passengers(self):
        """
        Link the ship movement entry to it's related passenger list.
        """
        ship = self
        related_passenger = self.related_passenger
        if related_passenger and related_passenger.ship_movement:
            related_passenger.ship_movement = ship
            related_passenger.save()
        return ship

    def get_passenger_list_delay_type(self):
        if not hasattr(self, "passenger_list"):
            return ""
        return self.passenger_list.delay_type

    def get_crew_list_delay_type(self):
        if not hasattr(self, "crew_list"):
            return ""
        return self.crew_list.delay_type

    def set_list_delay_type(self):
        ship = self
        ship.passenger_list_delay_type = ship.get_passenger_list_delay_type()
        ship.crew_list_delay_type = ship.get_crew_list_delay_type()
        return ship

    def save(self, *args, **kwargs):
        self.set_list_delay_type()
        result = super().save(*args, **kwargs)
        self.link_ship_to_passengers()
        self.link_ship_to_crew()
        return result
