from django.db import models

from backend.nationalities.nationality_counters import WithNationalityCounters
from backend.persons import choices as persons_choices


class PersonBaseModel(WithNationalityCounters):
    fpr_file_content = models.BinaryField("contenu fichier", blank=True, null=True)
    fpr_annex_file_content = models.BinaryField(
        "fichier annexe fpr", blank=True, null=True
    )
    roc_file_content = models.BinaryField("contenu fichier", blank=True, null=True)
    roc_annex_file_content = models.BinaryField(
        "fichier annexe roc", blank=True, null=True
    )
    delay_type = models.CharField(
        "type de retard",
        max_length=256,
        blank=True,
        choices=persons_choices.DELAY_TYPE_CHOICES,
    )

    class Meta:
        abstract = True


class CrewListBaseModel(PersonBaseModel):
    ship_movement = models.OneToOneField(
        to="ships.ShipMovement",
        verbose_name="mouvement de navire",
        related_name="crew_list",
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )
    roc_annex_file_content = models.BinaryField(
        "fichier annexe roc", blank=True, null=True
    )
    number_of_crew_parsed = models.PositiveIntegerField(
        "nombre de membres d'équipage détectés dans la liste", blank=True, null=True
    )

    class Meta:
        abstract = True

    def __str__(self):
        return f"Equipage Navire {self.ship_name} {self.imo_number}"


class PassengerListBaseModel(PersonBaseModel):
    ship_movement = models.OneToOneField(
        to="ships.ShipMovement",
        verbose_name="mouvement de navire",
        related_name="passenger_list",
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )
    roc_annex_file_content = models.BinaryField(
        "fichier annexe roc", blank=True, null=True
    )
    number_of_passengers_parsed = models.PositiveIntegerField(
        "nombre de passagers détectés dans la liste", blank=True, null=True
    )

    class Meta:
        abstract = True

    def __str__(self):
        return f"Passagers Navire {self.ship_name} {self.imo_number}"
