from django.core.management.base import BaseCommand

from backend.ships.models import PassengerList


class Command(BaseCommand):
    def handle(self, *args, **options):
        passengers_lists = PassengerList.objects.all()
        number_passengers_lists = passengers_lists.count()
        passengers_lists.delete()
        self.stdout.write(
            self.style.SUCCESS(f"{number_passengers_lists} passengers list deleted")
        )
