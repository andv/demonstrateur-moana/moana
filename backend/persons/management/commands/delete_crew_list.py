from django.core.management.base import BaseCommand

from backend.ships.models import CrewList


class Command(BaseCommand):
    def handle(self, *args, **options):
        crew_list = CrewList.objects.all()
        number_crew_list = crew_list.count()
        crew_list.delete()
        self.stdout.write(self.style.SUCCESS(f"{number_crew_list} crew list deleted"))
