PERSON_LIST_TYPE_CREW = "crew"
PERSON_LIST_TYPE_PASSENGER = "passenger"
PERSON_LIST_FILE_FORMAT_FPR = "fpr"
PERSON_LIST_FILE_FORMAT_ROC = "roc"

DELAY_AFTER_ARRIVAL = "after-arrival"
DELAY_MISSED_24H_NOTICE = "missed-24h-notice"
NO_DELAY = "no-delay"

DELAY_TYPE_CHOICES = (
    (DELAY_AFTER_ARRIVAL, "après l'arrivée"),
    (DELAY_MISSED_24H_NOTICE, "délai 24h non respecté"),
    (NO_DELAY, "pas de retard"),
)
