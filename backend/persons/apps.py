from django.apps import AppConfig


class PersonsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.persons"

    def ready(self):
        import backend.persons.signals  # noqa
