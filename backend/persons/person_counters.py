from django.db import models

##################
# Models         #
##################


class WithPersonCounters(models.Model):
    """
    This abstract class is expected to be sub-classed on a ShipMovement-like model.
    It provides crew and passengers counter fields.
    These the declaration of expected numbers of persons.
    """

    number_of_crew = models.PositiveIntegerField(
        "number of crew", blank=True, null=True
    )
    number_of_passengers = models.PositiveIntegerField(
        "number of passengers", blank=True, null=True
    )

    class Meta:
        abstract = True


class WithParsedPersonCounters(models.Model):
    """
    This abstract class is expected to be sub-classed on models that need
    fields related to a person count. This class can be used in models
    defined in the ShipMovement-like models.
    """

    number_of_crew_parsed = models.PositiveIntegerField(
        "nombre de membres d'équipage détectés dans la liste", blank=True, null=True
    )
    number_of_passengers_parsed = models.PositiveIntegerField(
        "nombre de passagers détectés dans la liste", blank=True, null=True
    )

    class Meta:
        abstract = True


class PersonCountersHelper:
    """
    Provides helper methods to related to a the number of persons.
    """

    def get_person_counters_from_ship_movement(self, ship):
        """
        From the given ship movement instance, get the number of persons.
        The resulting dictionary can be used to update the person counters
        fields on models that inherits from `WithParsedPersonCounters` class.
        """
        return {
            "number_of_crew_parsed": ship.number_of_crew_parsed,
            "number_of_passengers_parsed": ship.number_of_passengers_parsed,
        }
