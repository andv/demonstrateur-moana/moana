from django.contrib import admin

from backend.nationalities.admin import NATIONALITY_FIELDSET
from backend.ships import settings as ships_settings

PERSON_LIST_FIELDSET = [
    (
        "Relations et champs auto-renseignés",
        {
            "fields": [
                "ship_movement",
                "delay_type",
            ],
        },
    ),
    (
        "Navire et escale",
        {
            "fields": [
                "ship_name",
                "imo_number",
                "ship_call_id",
                "port_of_call",
                "way",
                "call_sign",
                "flag_state_of_ship",
                "last_port",
                "next_port",
            ],
        },
    ),
    (
        "Horaires",
        {
            "fields": [
                "eta_to_port_of_call",
                "ata_to_port_of_call",
                "etd_from_port_of_call",
                "atd_from_port_of_call",
            ],
        },
    ),
]


class CrewListBaseAdmin:
    list_display = [
        "id",
        "__str__",
        "ship_movement",
        "last_port",
        "next_port",
        "eta_to_port_of_call",
        "etd_from_port_of_call",
        "ship_call_id",
        "delay_type",
        "way",
        "created",
        "modified",
    ]
    list_display_links = ["id", "__str__"]
    search_fields = ships_settings.SHIP_BASE_SEARCH_FIELDS
    list_filter = ["way", "delay_type"]
    readonly_fields = ["ship_movement"]
    fieldsets = (
        PERSON_LIST_FIELDSET
        + NATIONALITY_FIELDSET
        + [
            (
                "Personnes",
                {
                    "fields": [
                        "number_of_crew_parsed",
                    ],
                },
            ),
        ]
    )


class PassengerListBaseAdmin(CrewListBaseAdmin):
    fieldsets = (
        PERSON_LIST_FIELDSET
        + NATIONALITY_FIELDSET
        + [
            (
                "Personnes",
                {
                    "fields": [
                        "number_of_passengers_parsed",
                    ],
                },
            ),
        ]
    )
