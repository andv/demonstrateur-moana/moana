from django.db import models

FIELD_NAME_MAX_LENGTH = 21


def generate_indexes(field_names, prefix):
    """
    Generates a list of Django model index instances with the provided field names and prefix.
    """
    indexes = []
    for field in field_names:
        field_name = field.replace("-", "desc_")
        # We shorten the field name as index name can not be too long.
        field_name = field_name[:FIELD_NAME_MAX_LENGTH]
        name = f"{prefix}{field_name}_index"
        index = models.Index(fields=[field], name=name)
        indexes.append(index)
    return indexes
