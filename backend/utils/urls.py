def remove_params_from_url(url):
    """
    Removes query string parameters from the given URL.
    """
    if not url:
        return url
    return url.split("?")[0]
