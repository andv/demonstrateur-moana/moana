from typing import Any

from django.db import models
from django.utils.html import strip_tags


class SanitizedTextField(models.TextField):
    def pre_save(self, model_instance: models.Model, add: bool):
        value_to_sanitize = getattr(model_instance, self.attname)
        sanitized_value = self.sanitize(value_to_sanitize)
        setattr(model_instance, self.attname, sanitized_value)
        return super().pre_save(model_instance, add)

    def sanitize(self, string):
        sanitized_string = strip_tags(string)
        suspicious_vuejs_char_to_remove = ["{", "}"]
        for char in suspicious_vuejs_char_to_remove:
            sanitized_string = sanitized_string.replace(char, "")
        cleaned_string = " ".join(sanitized_string.split())
        return cleaned_string
