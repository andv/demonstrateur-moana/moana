from django.core.management.base import BaseCommand

import pandas as pd


class Command(BaseCommand):
    help = "Convert parquet file to csv file."

    def add_arguments(self, parser):
        parser.add_argument("--from", type=str, help="The path of the parquet file")
        parser.add_argument("--to", type=str, help="The path of the csv file")

    def handle(self, *args, **options):
        parquet_input = options["from"]
        csv_output = options["to"]

        pd.read_parquet(
            parquet_input,
            dtype_backend="numpy_nullable",
        ).to_csv(csv_output, index=False)
        self.stdout.write(
            self.style.SUCCESS(f"'{parquet_input}' convert to '{csv_output}'")
        )
