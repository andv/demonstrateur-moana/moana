from string import Template

from django.utils import timezone


class DeltaTemplate(Template):
    delimiter = "%"


def breakdown_tdelta(tdelta):
    """
    Return a tuple of days, hours, minutes, seconds for a timedelta object.
    """
    days = tdelta.days
    hours, remainder = divmod(tdelta.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    return (days, hours, minutes, seconds)


def strfdelta(tdelta, fmt):
    days, hours, minutes, seconds = breakdown_tdelta(tdelta)
    d = {}
    d["D"] = days
    d["H"] = f"{hours:01d}"
    d["M"] = f"{minutes:01d}"
    d["S"] = seconds
    t = DeltaTemplate(fmt)
    return t.substitute(**d)


def duration_format_fr(tdelta):

    duration_format = ""
    days, hours, minutes, _ = breakdown_tdelta(tdelta)
    if days > 0:
        duration_format += "%D-j "
    if hours > 0:
        duration_format += "%H-h "
    if minutes > 0:
        duration_format += "%M-min "
    formated_string = strfdelta(tdelta, duration_format)
    formated_string = formated_string.replace("-", "")
    formated_string = formated_string.strip()
    return formated_string
