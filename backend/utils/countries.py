import pycountry


def get_country_name_from_code(code):
    """
    Get the country name from alpha2 code : DE => Germany
    """
    country = pycountry.countries.get(alpha_2=code)
    if not country:
        return code
    return country.name


def get_country_code_from_name(name):
    """
    Get the alpha2 country code from the name : Germany => DE
    """
    code = ""
    try:
        country = pycountry.countries.lookup(name)
        code = country.alpha_2
    except LookupError:
        pass
    return code
