from functools import wraps

from sentry_sdk import capture_exception


def sentry_capture(func):
    """
    Decorator to capture exceptions and log them to the console.
    """

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except Exception as err:
            capture_exception(err)
            self.stderr.write(f"Error in {func.__name__}: {err}")

    return wrapper
