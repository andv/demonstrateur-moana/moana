from django import forms

from django_filters import rest_framework as filters


class NoValidationField(forms.MultipleChoiceField):
    def validate(self, value):
        pass


class NoValidationMultipleChoiceFilter(filters.MultipleChoiceFilter):
    field_class = NoValidationField
