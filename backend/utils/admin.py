class ReadOnlyAdminMixin:
    """
    Model admin class that prevents modifications through the admin.
    """

    actions = None

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
