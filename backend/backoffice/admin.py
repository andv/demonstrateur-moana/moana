from django.contrib import admin

from magicauth.models import MagicToken

admin.site.unregister(MagicToken)
