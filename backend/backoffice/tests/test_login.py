from django.conf import settings
from django.contrib.auth import get_user_model
from django.shortcuts import reverse

import pytest

from backend.unit_tests import factories

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())

User = get_user_model()


ADMIN_USER_DATA = {
    "email": "admin@test.com",
    "password": "test12345",
}


def test_login_to_admin_redirects(client, settings):
    settings.ALLOW_ADMIN_PASSWORD_AUTH = True
    settings.ADMIN_URL = "admin/"
    admin_user = factories.UserFactory(**ADMIN_USER_DATA)
    admin_user.is_staff = True
    admin_user.is_supperuser = True
    admin_user.save()
    url = reverse("admin-login")
    post_data = {"username": "admin@test.com", "password": "test12345"}
    response = client.post(url, post_data)
    assert response.status_code == 302
    assert "admin/" in response.url


def test_login_to_admin_does_not_redirect_to_next(client):
    settings.ADMIN_URL = "admin/"
    settings.ALLOW_ADMIN_PASSWORD_AUTH = True
    User.objects.create_superuser(**ADMIN_USER_DATA)
    url = reverse("admin-login")
    url += "?next=/unwanted/"
    post_data = {"username": "admin@test.com", "password": "test12345"}
    response = client.post(url, post_data)
    assert response.status_code == 302
    assert "/unwanted/" not in response.url
    assert "admin/" in response.url
