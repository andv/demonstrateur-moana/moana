import sys

from django.apps import AppConfig


class BackofficeConfig(AppConfig):
    name = "backend.backoffice"
    verbose_name = "Backoffice"

    def ready(self):
        if "migrate" not in sys.argv:
            import trackman.signals  # noqa
