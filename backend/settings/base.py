from os import path

from import_export.formats.base_formats import CSV

##########
# DIRs   #
##########

SETTINGS_DIR = path.dirname(path.dirname(path.abspath(__file__)))
BASE_DIR = path.dirname(SETTINGS_DIR)


##########
# APP    #
##########

INSTALLED_APPS = [
    ########################
    #  Django Apps         #
    ########################
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    ########################
    # Third Party Apps     #
    ########################
    "rest_framework",
    "model_utils",
    "corsheaders",
    "import_export",
    "django_extensions",
    "magicauth",
    "django_celery_beat",
    "django_filters",
    "tinymce",
    "admin_auto_filters",
    "djangosaml2",
    "solo",
    "mirage",
    "filesify",
    "trackman",
    "drf_yasg",
    "dsfr",
    "rangefilter",
    "watchman",
    "adminsortable2",
    "marathon",
    ########################
    # Projects Apps        #
    ########################
    # Ship movements functionalities
    # ------------------------------
    "backend.ships",
    "backend.persons",
    "backend.alerts",
    "backend.person_checks",
    # References
    # ----------
    "backend.ports",
    "backend.geo",
    "backend.ship_types",
    "backend.nationalities",
    "backend.agents",
    "backend.travel_duration",
    "backend.mmsi",
    # SIP
    "backend.sip",
    "backend.neptune",
    # File handling : FAL, FPR, ROC
    # -----------------------------
    "backend.ship_files",
    "backend.nca",
    "backend.fal",
    "backend.nca_port",
    "backend.fpr",
    # School trips
    # ------------
    "backend.school_trips",
    "backend.school_trips_auth",
    "backend.school_trips_upload",
    # FAQ
    # ----
    "backend.faq",
    # Backoffice and config
    # ---------------------
    "backend.settings",
    "backend.backoffice",
    "backend.accounts",
    "backend.cheops",
    "backend.saml",
    # Tasks and scheduling
    # ---------------------
    "backend.tasks",
    "backend.email_import",
    # Tracking
    # --------
    "backend.logs",
    "backend.issues",
    # Utilities
    # ---------
    "backend.initialization",
    "backend.cleanup",
    "backend.db",
    "backend.utils",
    "backend.current_user",
    "backend.unit_tests",
    # Site
    # ----
    "backend.home",
    "backend.website",
    ########################
    # Data processing      #
    ########################
    "data_processing.fal_coverage",
    "data_processing.person_list",
    "data_processing.ship_references",
    "data_processing.data_anomalies",
    ########################
    # Placed last          #
    ########################
    "django_cleanup.apps.CleanupConfig",
]

BASE_MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "login_required.middleware.LoginRequiredMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "djangosaml2.middleware.SamlSessionMiddleware",
    "backend.saml.saml_entity_id_middleware.SAMLServiceProviderMiddleware",
]

AUTH_USER_MODEL = "accounts.User"

ROOT_URLCONF = "backend.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": ["dist", path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "backend.wsgi.application"

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

LANGUAGE_CODE = "fr-fr"
TIME_ZONE = "Europe/Paris"
USE_I18N = True
USE_L10N = True
USE_TZ = True


############
# SECURITY #
############

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
        "OPTIONS": {
            "min_length": 15,
        },
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
    {
        "NAME": "backend.accounts.password_validation.NoReusePasswordValidator",
    },
]

# Exclude incoming file if its mime type is not whitelisted
SHIP_FILE_MIME_TYPE_WHITELIST = ("application/xml", "text/xml")
CSV_FILE_MIME_TYPE_WHITELIST = ("text/csv", "text/plain")

SHIP_FILE_EXTENSION_WHITELIST = (".xml",)
CSV_FILE_EXTENSION_WHITELIST = (".csv",)
CSV_FILE_MAX_SIZE = 1024 * 1024 * 10  # 10MB


###############
# SSO Auth   #
###############

AUTHENTICATION_BACKENDS = (
    "django.contrib.auth.backends.ModelBackend",
    "djangosaml2.backends.Saml2Backend",
)

SSO_LOGIN_URL = "sso/login/"
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SAML_DJANGO_USER_MAIN_ATTRIBUTE = "email"
SAML_DJANGO_USER_MAIN_ATTRIBUTE_LOOKUP = "__iexact"
SAML_CREATE_UNKNOWN_USER = False
SAML_ATTRIBUTE_MAPPING = {
    "email": ["email"],
}

##########
# STATIC #
##########

STATIC_URL = "/static/"
STATIC_ROOT = path.join(BASE_DIR, "dist", "static")
STATICFILES_DIRS = (path.join(BASE_DIR, "static"),)

MEDIA_ROOT = path.join(BASE_DIR, "media")

##########
# DRF    #
##########

REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "rest_framework.authentication.SessionAuthentication",
    ],
}


##########
# URLs   #
##########

# URLs path
USER_LOGIN_WITH_PASSWORD_URL = "utilisateurs/login/"
LOGOUT_URL = "logout/"
VERIFY_USER_AUTH_WITH_EMAIL_URL = "api/accounts/auth-email-allowed/"

# Django redirects
LOGIN_URL = "/login/"
LOGIN_REDIRECT_URL = "/"
LOGOUT_REDIRECT_URL = "/"

#################
#  Monitoring   #
#################
from watchman import constants as watchman_constants

MONITORING_URL = "watchman/"
WATCHMAN_EXTRA_CHECKS = (
    "backend.monitoring.checks.fal_import",
    "backend.monitoring.checks.app_database",
    "backend.monitoring.checks.stats_database",
)
WATCHMAN_CHECKS = watchman_constants.DEFAULT_CHECKS + WATCHMAN_EXTRA_CHECKS

#################
# School Trip   #
#################
SCHOOL_TRIPS_HOME_URL = "school-trips/"
SCHOOL_TRIPS_LOGIN_URL = "school-trips/login/"
SCHOOL_TRIPS_LOGOUT_URL = "school-trips/logout/"
SCHOOL_TRIPS_EMAIL_SENT_URL = "school-trips/email-sent/"
SCHOOL_TRIPS_UPLOAD_FORM_URL = "school-trips/upload/"
SCHOOL_TRIPS_UPLOAD_SUCCESS_URL = "school-trips/success/"

###############
# Magicauth   #
###############

MAGICAUTH_FROM_EMAIL = "contact@mysite.com"
MAGICAUTH_LOGGED_IN_REDIRECT_URL_NAME = "index"
MAGICAUTH_EMAIL_FIELD = "email"
MAGICAUTH_BASE_URL = "login/"
MAGICAUTH_LOGIN_URL = MAGICAUTH_BASE_URL
MAGICAUTH_EMAIL_SENT_URL = MAGICAUTH_BASE_URL + "email-envoyé/"
MAGICAUTH_WAIT_URL = MAGICAUTH_BASE_URL + "chargement/code/<str:key>/"
MAGICAUTH_VALIDATE_TOKEN_URL = MAGICAUTH_BASE_URL + "code/<str:key>/"
MAGICAUTH_EMAIL_UNKNOWN_MESSAGE = "Utilisateur inconnu."
MAGICAUTH_DEFAULT_AUTHENTICATION_BACKEND = "django.contrib.auth.backends.ModelBackend"
MAGICAUTH_EMAIL_SUBJECT = "Lien de connexion Moana"

############
# Celery   #
############

CELERY_BEAT_SCHEDULER = "django_celery_beat.schedulers:DatabaseScheduler"


###################
# FPR and ROC     #
###################

MAX_LINES_IN_FPR_FILE = 999
MAX_LINES_IN_ROC_FILE = 598


############
# TinyMCE  #
############

TINYMCE_DEFAULT_CONFIG = {
    "theme": "silver",
    "height": 500,
    "menubar": False,
    "plugins": "advlist,autolink,lists,link,image,charmap,print,preview,anchor,"
    "searchreplace,visualblocks,code,fullscreen,insertdatetime,media,table,paste,"
    "code,help,wordcount",
    "toolbar": "undo redo | h2 h3 h4 h5 h6 | bold italic backcolor | bullist | removeformat | help | table",
}


############
# Tracking #
############

TRACKMAN_MODELS = {
    "default": "logs.UserActionLog",
    "data-quality": "logs.DataQualityLog",
}

TRACKMAN_DATABASE_ALIAS = "stats"

# Apps dedicated to stats : usually related to logs, tracking and data processing.
# The models of these apps live in the database referenced with the "stats" alias.
STATS_RELATED_APPS = ["logs", "fal_coverage", "ship_references", "data_anomalies"]


###################
#  Data Analysis  #
###################

DATA_ANALYSIS_PERIODS = {
    "Comité 2023-05 - 2023-09": ("2023-05-01", "2023-09-30"),
    "Comité 2023-10 - 2024-04": ("2023-10-01", "2024-04-30"),
    "Comité 2024-05 - 2024-10": ("2024-05-01", "2024-10-31"),
    "Pérennisation 2024-11": ("2024-11-01", "2099-11-01"),
}


###################
#  Import Export  #
###################

IMPORT_FORMATS = [CSV]


########################
# Email Notification   #
########################

EMAIL_NOTIFICATION_ENABLED = False
SITE_URL = ""


########################
# Statistics   #
########################

METABASE_SITE_URL = ""
METABASE_EMBEDED_KEY = ""
METABASE_DASHBOARD_USAGE_ID = 0
METABASE_DASHBOARD_NATIONALITIES_ID = 0

########################
# SIP  #
########################

MAX_DAYS_IN_PAST_FOR_IMPORT = 90


############
# Email    #
############

TEAM_EMAIL_CONTACT = ""
