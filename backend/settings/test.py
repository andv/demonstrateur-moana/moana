from backend.settings.local import *  # noqa

ENV_NAME = "test"

MEDIA_ROOT = ""  # Avoid issues with `exceptions.SuspiciousFileOperation``

CELERY_BROKER_URL = "memory://"

STATICFILES_DIRS = (path.join(BASE_DIR, "static"),)

TRACKMAN_ENABLED = True

MIRAGE_SECRET_KEY = "ignore-this-LYKmeeEiZtKS24DYPHAqniAmSyiGD08="

SHIPS_API_PAGE_SIZE = 10
SHIPS_API_MAX_PAGE_SIZE = 10

ALERT_SHIP_COUNTRIES = ["LOCODE"]

EMAIL_NOTIFICATION_ENABLED = False

ALLOWED_DOMAIN_NAME_ARRAY = "email.com", "courriel.com"
