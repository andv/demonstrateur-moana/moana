import dj_database_url
import environ
import saml2
import saml2.saml
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from backend.settings.base import *  # noqa
from backend.utils.urls import remove_params_from_url

env = environ.Env()


ENV_NAME = env("ENV_NAME")


############
# App      #
############

TRACKMAN_ENABLED = env.bool("TRACKMAN_ENABLED", default=False)

DATABASES = {"default": dj_database_url.parse(env("SCALINGO_POSTGRESQL_URL"))}

if TRACKMAN_ENABLED:
    DATABASE_ROUTERS = ["trackman.db_routers.TrackingDatabaseRouter"]
    db_url = remove_params_from_url(env("STATS_DATABASE_URL"))
    DATABASES[TRACKMAN_DATABASE_ALIAS] = dj_database_url.parse(db_url)


############
# SECURITY #
############

DEBUG = env.bool("DJANGO_DEBUG", False)

SECRET_KEY = env("DJANGO_SECRET_KEY")

ALLOWED_HOSTS = env.list("DJANGO_ALLOWED_HOSTS", default=["moana.andv.gouv.fr"])

CORS_ALLOWED_ORIGINS = []

MIRAGE_SECRET_KEY = env("MIRAGE_SECRET_KEY")

ENABLE_MONITORING = env.bool("ENABLE_MONITORING", default=False)

############
# SENTRY   #
############

SENTRY_DEBUG = env.bool("SENTRY_DEBUG", default=False)

sentry_sdk.init(
    dsn=env.str("SENTRY_URL"),
    integrations=[DjangoIntegration()],
    environment=ENV_NAME,
    send_default_pii=True,  # associate users to errors
    traces_sample_rate=env.float(
        "SENTRY_TRACES_SAMPLE_RATE", default=0.0
    ),  # performance monitoring.
)


EXTA_SECURITY_MIDDLEWARE = [
    "csp.middleware.CSPMiddleware",
    "django_permissions_policy.PermissionsPolicyMiddleware",
]

MIDDLEWARE = EXTA_SECURITY_MIDDLEWARE + BASE_MIDDLEWARE


# Content-Security-Policy
CSP_INCLUDE_NONCE_IN = ["script-src", "style-src"]
CSP_DEFAULT_SRC = env("CSP_DEFAULT_SRC", default=("'self'",))
CSP_SCRIPT_SRC = env("CSP_SCRIPT_SRC", default=("'self'",))
CSP_FRAME_SRC = env("CSP_FRAME_SRC", default=("'self'",))
CSP_STYLE_SRC = env("CSP_STYLE_SRC", default=("'self'",))
CSP_FONT_SRC = env("CSP_FONT_SRC", default=("'self'", "data:"))
CSP_IMG_SRC = env("CSP_IMG_SRC", default=("'self'", "data:"))
CSP_BASE_URI = env("CSP_BASE_URI", default=("'self'",))
CSP_CONNECT_SRC = env("CSP_CONNECT_SRC", default=("'self'",))
CSP_OBJECT_SRC = env("CSP_CONNECT_SRC", default=("'self'",))
CSP_FORM_ACTION = env("CSP_CONNECT_SRC", default=("'self'",))

# HTTP Security
SECURE_CONTENT_TYPE_NOSNIFF = True
REFERRER_POLICY = "same-origin"

PERMISSIONS_POLICY = {
    "accelerometer": [],
    "autoplay": [],
    "camera": [],
    "display-capture": [],
    "document-domain": [],
    "encrypted-media": [],
    "fullscreen": [],
    "geolocation": [],
    "gyroscope": [],
    "magnetometer": [],
    "microphone": [],
    "midi": [],
    "payment": [],
    "usb": [],
}

SECURE_BROWSER_XSS_FILTER = True
CSRF_COOKIE_SECURE = True
CSRF_COOKIE_SAMESITE = "Lax"  # Works best when using email magic link authentication
CSRF_COOKIE_HTTPONLY = env.bool(
    "CSRF_COOKIE_HTTPONLY", default=False
)  # If True, prevent accessing token value from javascript
SECURE_SSL_REDIRECT = True
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_SAMESITE = "Lax"  # Works best when using email magic link authentication
X_FRAME_OPTIONS = "DENY"


##########
# DRF    #
##########

REST_FRAMEWORK["DEFAULT_PERMISSION_CLASSES"] = [
    "rest_framework.permissions.IsAuthenticated",
    "rest_framework.permissions.AllowAny",
]

SHIPS_API_PAGE_SIZE = env.int("SHIPS_API_PAGE_SIZE", default=300)

##########
# URLs   #
##########

ADMIN_URL = env("ADMIN_URL", default="admin/")
ADMIN_LOGIN_WITH_PASSWORD_URL = ADMIN_URL + "login/"
ALLOW_API_DOC = env("ALLOW_API_DOC", default=False)

##########
# Email  #
##########

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
DEFAULT_FROM_EMAIL = env("DEFAULT_FROM_EMAIL")
EMAIL_HOST = env("EMAIL_HOST")
EMAIL_PORT = env.int("EMAIL_PORT")
EMAIL_HOST_USER = env("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = env("EMAIL_HOST_PASSWORD")
EMAIL_USE_TLS = env.bool("EMAIL_USE_TLS", default=False)
EMAIL_USE_SSL = env.bool("EMAIL_USE_SSL")
ALLOWED_DOMAIN_NAME_ARRAY = env.list("ALLOWED_DOMAIN_NAME_ARRAY", default=[])
TEAM_EMAIL_CONTACT = env("TEAM_EMAIL_CONTACT", default="")

#########################
# File Import by Email  #
#########################

EMAIL_IMPORT_USERNAME = env("EMAIL_IMPORT_USERNAME", default="")
EMAIL_IMPORT_PASSWORD = env("EMAIL_IMPORT_PASSWORD", default="")
EMAIL_IMPORT_HOST = env("EMAIL_IMPORT_HOST", default="")
EMAIL_IMPORT_SENDERS_WHITELIST = env.list("EMAIL_IMPORT_SENDERS_WHITELIST", default=[])
EMAIL_IMPORT_DONE_FOLDER = env("EMAIL_IMPORT_DONE_FOLDER", default="CorbeilleFAL")

###############
# Files       #
###############

SHIP_FILE_MIME_TYPE_WHITELIST = env.list(
    "SHIP_FILE_MIME_TYPE_WHITELIST", default=["application/xml", "text/xml"]
)
SHIP_FILE_EXTENSION_WHITELIST = env.list(
    "SHIP_FILE_EXTENSION_WHITELIST",
    default=[
        ".xml",
    ],
)


###############
# Magicauth   #
###############

MAGICAUTH_FROM_EMAIL = env("DEFAULT_FROM_EMAIL")
MAGICAUTH_TOKEN_DURATION_SECONDS = env.int(
    "MAGICAUTH_TOKEN_DURATION_SECONDS", default=15 * 60
)


############
# Celery   #
############

CELERY_BROKER_URL = env("CELERY_BROKER_URL", default="memory://")


###########
# FPR     #
###########

MAX_LINES_IN_FPR_FILE = env.int("MAX_LINES_IN_FPR_FILE", default=999)
MAX_LINES_IN_ROC_FILE = env.int("MAX_LINES_IN_ROC_FILE", default=598)


###########
# Alerts  #
###########

ALERT_SHIP_COUNTRIES = env.list("ALERT_SHIP_COUNTRIES", default=[])
ALERT_NATIONALITIES = env.list("ALERT_NATIONALITIES", default=[])

############
# SIP      #
############

SIP_API_VIGIESIP_SAINT_MALO = env("SIP_API_VIGIESIP_SAINT_MALO", default=None)
SIP_API_VIGIESIP_SETE = env("SIP_API_VIGIESIP_SETE", default=None)
SIP_API_VIGIESIP_PORT_LA_NOUVELLE = env(
    "SIP_API_VIGIESIP_PORT_LA_NOUVELLE", default=None
)
SIP_API_VIGIESIP_TOULON = env("SIP_API_VIGIESIP_TOULON", default=None)
SIP_API_VIGIESIP_BORDEAUX = env("SIP_API_VIGIESIP_BORDEAUX", default=None)
SIP_API_VIGIESIP_LA_ROCHELLE = env("SIP_API_VIGIESIP_LA_ROCHELLE", default=None)
SIP_API_VIGIESIP_PORT_VENDRES = env("SIP_API_VIGIESIP_PORT_VENDRES", default=None)
SIP_API_VIGIESIP_BAYONNE = env("SIP_API_VIGIESIP_BAYONNE", default=None)

SIP_SWING_LAREUNION_API_USERNAME = env("SIP_SWING_LAREUNION_API_USERNAME", default=None)
SIP_SWING_LAREUNION_API_PASSWORD = env("SIP_SWING_LAREUNION_API_PASSWORD", default=None)
SIP_SWING_API_USERNAME = env("SIP_SWING_API_USERNAME", default=None)
SIP_SWING_API_PASSWORD = env("SIP_SWING_API_PASSWORD", default=None)

SIP_HAROPAPORT_API_USERNAME = env("SIP_HAROPAPORT_API_USERNAME", default=None)
SIP_HAROPAPORT_API_PASSWORD = env("SIP_HAROPAPORT_API_PASSWORD", default=None)

MAX_DAYS_IN_PAST_FOR_IMPORT = env.int("MAX_DAYS_IN_PAST_FOR_IMPORT", default=90)

############
# DATA     #
############

# In therms of days, how old the data should be ? This affects the
# SIP import and also the data remove task.
DATA_MAX_DAYS_OLD = env.int("DATA_MAX_DAYS_OLD", default=60)


############
# SAML     #
############

SAML_CHEOPS_METADATA_FILE_PATH = env("SAML_CHEOPS_METADATA_FILE_PATH")
SAML_PROXYMA_METADATA_FILE_PATH = env("SAML_PROXYMA_METADATA_FILE_PATH")
SAML_DGDDI_METADATA_FILE_PATH = env("SAML_DGDDI_METADATA_FILE_PATH")
SAML_SP_METADATA_URL_PATH = "sso/metadata/"
SAML_ACS_URL_PATH = "sso/acs/"
SAML_METADATA_KEY = env("SAML_METADATA_KEY")
SAML_METADATA_CERT = env("SAML_METADATA_CERT")
SAML_ASSERTION_KEY = env("SAML_ASSERTION_KEY")
SAML_ASSERTION_CERT = env("SAML_ASSERTION_CERT")

SAML_SP_BASE_URL = env("SAML_SP_BASE_URL")
SAML_SP_BASE_URL_ALT = env("SAML_SP_BASE_URL_ALT", default="")
acs_endpoints = [
    (
        path.join(SAML_SP_BASE_URL, SAML_ACS_URL_PATH),
        saml2.BINDING_HTTP_POST,
    ),
]

SAML_CONFIG = {
    "xmlsec_binary": env("SAML_XMLSEC_BINARY", default="/app/.apt/usr/bin/xmlsec1"),
    "entityid": path.join(SAML_SP_BASE_URL, SAML_SP_METADATA_URL_PATH),
    "allow_unknown_attributes": True,
    "service": {
        "sp": {
            "name": "Moana Service Provider",
            "name_id_format": saml2.saml.NAMEID_FORMAT_TRANSIENT,
            "endpoints": {
                "assertion_consumer_service": acs_endpoints,
            },
            "signing_algorithm": saml2.xmldsig.SIG_RSA_SHA256,
            "digest_algorithm": saml2.xmldsig.DIGEST_SHA256,
            "required_attributes": ["email"],
            "want_response_signed": True,
            "authn_requests_signed": True,
            "logout_requests_signed": True,
            "want_assertions_signed": True,
            "only_use_keys_in_metadata": True,
            "allow_unsolicited": False,
        },
    },
    "metadata": {
        "local": [
            SAML_CHEOPS_METADATA_FILE_PATH,
            SAML_PROXYMA_METADATA_FILE_PATH,
            SAML_DGDDI_METADATA_FILE_PATH,
        ],
    },
    "debug": env.int("SAML_DEBUG", default=0),
    # Signing metadata:
    "key_file": SAML_METADATA_KEY,
    "cert_file": SAML_METADATA_CERT,
    # Encryption for assertion:
    "encryption_keypairs": [
        {
            "key_file": SAML_ASSERTION_KEY,
            "cert_file": SAML_ASSERTION_CERT,
        }
    ],
    "contact_person": [
        {
            "company": "ANDV - Moana",
            "email_address": "andv-moana@interieur.gouv.fr",
            "contact_type": "technical",
        },
    ],
}

############
# SSO      #
############

DISPLAY_SSO_CHEOPS = env("DISPLAY_SSO_CHEOPS", default=False)
DISPLAY_SSO_PROXYMA = env("DISPLAY_SSO_PROXYMA", default=False)
DISPLAY_SSO_DGDDI = env("DISPLAY_SSO_DGDDI", default=False)
IDP_CHEOPS_ENTITY_ID = env("IDP_CHEOPS_ENTITY_ID")
IDP_PROXYMA_ENTITY_ID = env("IDP_PROXYMA_ENTITY_ID")
IDP_DGDDI_ENTITY_ID = env("IDP_DGDDI_ENTITY_ID")


#################
# School Trip   #
#################

SCHOOL_TRIPS_ENABLED = env.bool("SCHOOL_TRIPS_ENABLED", default=False)


##########
# Auth   #
##########

ALLOW_USER_PASSWORD_AUTH = env("ALLOW_USER_PASSWORD_AUTH", default=False)
ALLOW_ADMIN_PASSWORD_AUTH = env("ALLOW_ADMIN_PASSWORD_AUTH", default=False)

LOGIN_REQUIRED_IGNORE_PATHS = [
    r"/" + MAGICAUTH_BASE_URL,
    r"/" + LOGOUT_URL,
    r"/" + SSO_LOGIN_URL,
    r"/" + SAML_ACS_URL_PATH,
    r"/" + SAML_SP_METADATA_URL_PATH,
    r"/" + VERIFY_USER_AUTH_WITH_EMAIL_URL,
]

if ALLOW_USER_PASSWORD_AUTH:
    LOGIN_REQUIRED_IGNORE_PATHS.append(r"/" + USER_LOGIN_WITH_PASSWORD_URL)

if ALLOW_ADMIN_PASSWORD_AUTH:
    LOGIN_REQUIRED_IGNORE_PATHS.append(r"/" + ADMIN_LOGIN_WITH_PASSWORD_URL)

if SCHOOL_TRIPS_ENABLED:
    # We use strip("/") to make sure the URL without trailing slash is also allowed.
    LOGIN_REQUIRED_IGNORE_PATHS.append(r"/" + SCHOOL_TRIPS_HOME_URL.strip("/"))
    LOGIN_REQUIRED_IGNORE_PATHS.append(r"/" + SCHOOL_TRIPS_LOGIN_URL.strip("/"))
    LOGIN_REQUIRED_IGNORE_PATHS.append(r"/" + SCHOOL_TRIPS_LOGOUT_URL.strip("/"))
    LOGIN_REQUIRED_IGNORE_PATHS.append(r"/" + SCHOOL_TRIPS_EMAIL_SENT_URL.strip("/"))
    # The upload page handles it's own login requirement with a specific redirect URL.
    LOGIN_REQUIRED_IGNORE_PATHS.append(r"/" + SCHOOL_TRIPS_UPLOAD_FORM_URL.strip("/"))

if ENABLE_MONITORING:
    # We use strip("/") to make sure the URL without trailing slash is also allowed.
    LOGIN_REQUIRED_IGNORE_PATHS.append(r"/" + MONITORING_URL.strip("/"))

SESSION_COOKIE_AGE = env("SESSION_COOKIE_AGE", default=6 * 60 * 60)
SESSION_EXPIRE_AT_BROWSER_CLOSE = env.bool(
    "SESSION_EXPIRE_AT_BROWSER_CLOSE", default=True
)


########################
# Email Notification   #
########################

EMAIL_NOTIFICATION_ENABLED = env.bool("EMAIL_NOTIFICATION_ENABLED", default=False)
SITE_URL = env("SITE_URL")


######################
# FAL Data quality   #
######################

NAME_PARTICLES = env.list("NAME_PARTICLES", default=[])


########################
# Statistics   #
########################

METABASE_SITE_URL = env("METABASE_SITE_URL", default="")
METABASE_EMBEDED_KEY = env("METABASE_EMBEDED_KEY", default="")
METABASE_DASHBOARD_USAGE_ID = env.int("METABASE_DASHBOARD_USAGE_ID", default=0)
METABASE_DASHBOARD_NATIONALITIES_ID = env.int(
    "METABASE_DASHBOARD_NATIONALITIES_ID", default=0
)
