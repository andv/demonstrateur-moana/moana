import environ
import saml2
import saml2.saml
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from backend.settings.base import *

env = environ.Env()
environ.Env.read_env(path.join(BASE_DIR, ".env"))


ENV_NAME = "dev"


##########
# APP    #
##########

SECRET_KEY = env.str(
    "SECRET_KEY", default="ignore-this-BLKAeaEaZ3KS2RDYPHAcciBBSyRGDR8="
)

DEBUG = True

ALLOWED_HOSTS = ["*"]

TRACKMAN_ENABLED = env.bool("TRACKMAN_ENABLED", default=False)

INSTALLED_APPS += ["debug_toolbar"]
INSTALLED_APPS = ["whitenoise.runserver_nostatic"] + INSTALLED_APPS

MIDDLEWARE = BASE_MIDDLEWARE + ["debug_toolbar.middleware.DebugToolbarMiddleware"]

DEBUG_TOOLBAR_CONFIG = {
    "DISABLE_PANELS": ["debug_toolbar.panels.redirects.RedirectsPanel"],
    "SHOW_TEMPLATE_CONTEXT": True,
}

INTERNAL_IPS = ["127.0.0.1"]

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": path.join(BASE_DIR, "db.sqlite3"),
    }
}

if TRACKMAN_ENABLED:
    DATABASE_ROUTERS = ["trackman.db_routers.TrackingDatabaseRouter"]
    DATABASES[TRACKMAN_DATABASE_ALIAS] = {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": path.join(BASE_DIR, "db_stats.sqlite3"),
    }

CORS_ALLOWED_ORIGINS = [
    "http://localhost:8080",
]


############
# SECURITY #
############

MIRAGE_SECRET_KEY = env.str(
    "MIRAGE_SECRET_KEY", default="ignore-this-LYKmeeEiZtKS24DYPHAqniAmSyiGD08="
)

ENABLE_MONITORING = env.bool("ENABLE_MONITORING", default=False)

############
# SENTRY   #
############

SENTRY_DEBUG = env.bool("SENTRY_DEBUG", default=True)

SENTRY_URL = env.str("SENTRY_URL", default=None)

if SENTRY_URL:
    sentry_sdk.init(
        dsn=env.str("SENTRY_URL"),
        integrations=[DjangoIntegration()],
        environment=ENV_NAME,
        send_default_pii=True,  # associate users to errors
        traces_sample_rate=env.float(
            "SENTRY_TRACES_SAMPLE_RATE", default=0.0
        ),  # performance monitoring.
    )

SESSION_COOKIE_SECURE = True


##########
# URLs   #
##########

ADMIN_URL = env("ADMIN_URL", default="admin/")
ADMIN_LOGIN_WITH_PASSWORD_URL = ADMIN_URL + "login/"
ALLOW_API_DOC = env("ALLOW_API_DOC", default=True)


#################
# School Trip   #
#################

SCHOOL_TRIPS_ENABLED = env.bool("SCHOOL_TRIPS_ENABLED", default=True)


##########
# Auth   #
##########
ALLOW_USER_PASSWORD_AUTH = env("ALLOW_USER_PASSWORD_AUTH", default=True)
ALLOW_ADMIN_PASSWORD_AUTH = env("ALLOW_ADMIN_PASSWORD_AUTH", default=True)
LOGIN_REQUIRED_IGNORE_PATHS = [r"/"]


##########
# DRF    #
##########

SHIPS_API_PAGE_SIZE = env.int("SHIPS_API_PAGE_SIZE", default=300)


##########
# Email  #
##########

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
DEFAULT_FROM_EMAIL = "Moana ANDV <moana@demo.com>"
MAGICAUTH_FROM_EMAIL = DEFAULT_FROM_EMAIL
ALLOWED_DOMAIN_NAME_ARRAY = env.list("ALLOWED_DOMAIN_NAME_ARRAY", default=[])
TEAM_EMAIL_CONTACT = env("TEAM_EMAIL_CONTACT", default="")

#########################
# File Import by Email  #
#########################

EMAIL_IMPORT_USERNAME = env("EMAIL_IMPORT_USERNAME", default="")
EMAIL_IMPORT_PASSWORD = env("EMAIL_IMPORT_PASSWORD", default="")
EMAIL_IMPORT_HOST = env("EMAIL_IMPORT_HOST", default="")
EMAIL_IMPORT_SENDERS_WHITELIST = env.list("EMAIL_IMPORT_SENDERS_WHITELIST", default=[])
EMAIL_IMPORT_DONE_FOLDER = env("EMAIL_IMPORT_DONE_FOLDER", default="CorbeilleFAL")

###############
# Files       #
###############

SHIP_FILE_MIME_TYPE_WHITELIST = env.list(
    "SHIP_FILE_MIME_TYPE_WHITELIST", default=["application/xml", "text/xml"]
)
SHIP_FILE_EXTENSION_WHITELIST = env.list(
    "SHIP_FILE_EXTENSION_WHITELIST",
    default=[
        ".xml",
    ],
)

############
# Celery   #
############

CELERY_BROKER_URL = env("CELERY_BROKER_URL", default="memory://")
CELERY_TASK_ALWAYS_EAGER = True
CELERY_TASK_EAGER_PROPAGATES = True

###########
# FPR     #
###########

MAX_LINES_IN_FPR_FILE = env.int("MAX_LINES_IN_FPR_FILE", default=999)
MAX_LINES_IN_ROC_FILE = env.int("MAX_LINES_IN_ROC_FILE", default=598)


###########
# FPR     #
###########
ALERT_SHIP_COUNTRIES = env.list("ALERT_SHIP_COUNTRIES", default=[])
ALERT_NATIONALITIES = env.list("ALERT_NATIONALITIES", default=[])


############
# SIP      #
############

SIP_API_VIGIESIP_SAINT_MALO = env("SIP_API_VIGIESIP_SAINT_MALO", default=None)
SIP_API_VIGIESIP_SETE = env("SIP_API_VIGIESIP_SETE", default=None)
SIP_API_VIGIESIP_PORT_LA_NOUVELLE = env(
    "SIP_API_VIGIESIP_PORT_LA_NOUVELLE", default=None
)

SIP_SWING_LAREUNION_API_USERNAME = env("SIP_SWING_LAREUNION_API_USERNAME", default=None)
SIP_SWING_LAREUNION_API_PASSWORD = env("SIP_SWING_LAREUNION_API_PASSWORD", default=None)
SIP_SWING_API_USERNAME = env("SIP_SWING_API_USERNAME", default=None)
SIP_SWING_API_PASSWORD = env("SIP_SWING_API_PASSWORD", default=None)

SIP_HAROPAPORT_API_USERNAME = env("SIP_HAROPAPORT_API_USERNAME", default=None)
SIP_HAROPAPORT_API_PASSWORD = env("SIP_HAROPAPORT_API_PASSWORD", default=None)

MAX_DAYS_IN_PAST_FOR_IMPORT = env.int("MAX_DAYS_IN_PAST_FOR_IMPORT", default=90)

############
# DATA     #
############

# In number of days, how old the data should be ? This affects the
# SIP import and also the data remove task.
DATA_MAX_DAYS_OLD = env.int("DATA_MAX_DAYS_OLD", default=60)


############
# SAML     #
############


SAML_CHEOPS_METADATA_FILE_PATH = env(
    "SAML_CHEOPS_METADATA_FILE_PATH", default="/tmp/cheops-metadata.xml"
)
SAML_PROXYMA_METADATA_FILE_PATH = env(
    "SAML_PROXYMA_METADATA_FILE_PATH", default="/tmp/proxyma-metadata.xml"
)
SAML_DGDDI_METADATA_FILE_PATH = env(
    "SAML_DGDDI_METADATA_FILE_PATH", default="/tmp/dgddi-metadata.xml"
)
SAML_SP_METADATA_URL_PATH = "sso/metadata/"
SAML_ACS_URL_PATH = "sso/acs/"
SAML_METADATA_KEY = env("SAML_METADATA_KEY", default="/tmp/metadata.key")
SAML_METADATA_CERT = env("SAML_METADATA_CERT", default="/tmp/metadata.cert")
SAML_ASSERTION_KEY = env("SAML_ASSERTION_KEY", default="/tmp/assertion.key")
SAML_ASSERTION_CERT = env("SAML_ASSERTION_CERT", default="/tmp/assertion.cert")

SAML_SP_BASE_URL = env("SAML_SP_BASE_URL", default="http://localhost:8000/")
SAML_SP_BASE_URL_ALT = env("SAML_SP_BASE_URL_ALT", default="")
acs_endpoints = [
    (
        path.join(SAML_SP_BASE_URL, SAML_ACS_URL_PATH),
        saml2.BINDING_HTTP_POST,
    ),
]

SAML_CONFIG = {
    "xmlsec_binary": env("SAML_XMLSEC_BINARY", default="/usr/bin/xmlsec1"),
    "entityid": path.join(SAML_SP_BASE_URL, SAML_SP_METADATA_URL_PATH),
    "allow_unknown_attributes": True,
    "service": {
        "sp": {
            "name": "Moana Service Provider",
            "name_id_format": saml2.saml.NAMEID_FORMAT_TRANSIENT,
            "endpoints": {
                "assertion_consumer_service": acs_endpoints,
            },
            "signing_algorithm": saml2.xmldsig.SIG_RSA_SHA256,
            "digest_algorithm": saml2.xmldsig.DIGEST_SHA256,
            "required_attributes": ["email"],
            "want_response_signed": True,
            "authn_requests_signed": True,
            "logout_requests_signed": True,
            "want_assertions_signed": True,
            "only_use_keys_in_metadata": True,
            "allow_unsolicited": False,
        },
    },
    "metadata": {
        "local": [
            SAML_CHEOPS_METADATA_FILE_PATH,
            SAML_PROXYMA_METADATA_FILE_PATH,
            SAML_DGDDI_METADATA_FILE_PATH,
        ],
    },
    "debug": env.int("SAML_DEBUG", default=1),
    # Signing metadata:
    "key_file": SAML_METADATA_KEY,
    "cert_file": SAML_METADATA_CERT,
    # Encryption for assertion:
    "encryption_keypairs": [
        {
            "key_file": SAML_ASSERTION_KEY,
            "cert_file": SAML_ASSERTION_CERT,
        }
    ],
    "contact_person": [
        {
            "company": "ANDV - Moana",
            "email_address": "andv-moana@interieur.gouv.fr",
            "contact_type": "technical",
        },
    ],
}


############
# SSO      #
############

DISPLAY_SSO_CHEOPS = env("DISPLAY_SSO_CHEOPS", default=False)
DISPLAY_SSO_PROXYMA = env("DISPLAY_SSO_PROXYMA", default=False)
DISPLAY_SSO_DGDDI = env("DISPLAY_SSO_DGDDI", default=False)
IDP_CHEOPS_ENTITY_ID = env("IDP_CHEOPS_ENTITY_ID", default="https://idp.cheops.test")
IDP_PROXYMA_ENTITY_ID = env("IDP_PROXYMA_ENTITY_ID", default="https://idp.proxyma.test")
IDP_DGDDI_ENTITY_ID = env("IDP_DGDDI_ENTITY_ID", default="https://idp.dgddi.test")


########################
# Email Notification   #
########################

EMAIL_NOTIFICATION_ENABLED = env.bool("EMAIL_NOTIFICATION_ENABLED", default=False)
SITE_URL = env("SITE_URL", default="localhost:8000")


######################
# FAL Data quality   #
######################

NAME_PARTICLES = env.list("NAME_PARTICLES", default=[])


########################
# Statistics   #
########################

METABASE_SITE_URL = env("METABASE_SITE_URL", default="")
METABASE_EMBEDED_KEY = env("METABASE_EMBEDED_KEY", default="")
METABASE_DASHBOARD_USAGE_ID = env.int("METABASE_DASHBOARD_USAGE_ID", default=0)
METABASE_DASHBOARD_NATIONALITIES_ID = env.int(
    "METABASE_DASHBOARD_NATIONALITIES_ID", default=0
)
