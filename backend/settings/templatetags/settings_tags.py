from django import template
from django.conf import settings

register = template.Library()


@register.simple_tag
def get_setting_display_sso_cheops():
    return getattr(settings, "DISPLAY_SSO_CHEOPS", False)


@register.simple_tag
def get_setting_display_sso_proxyma():
    return getattr(settings, "DISPLAY_SSO_PROXYMA", False)


@register.simple_tag
def get_setting_display_sso_dgddi():
    return getattr(settings, "DISPLAY_SSO_DGDDI", False)


@register.simple_tag
def get_idp_cheops_entity_id():
    return getattr(settings, "IDP_CHEOPS_ENTITY_ID", "")


@register.simple_tag
def get_idp_proxyma_entity_id():
    return getattr(settings, "IDP_PROXYMA_ENTITY_ID", "")


@register.simple_tag
def get_idp_dgddi_entity_id():
    return getattr(settings, "IDP_DGDDI_ENTITY_ID", "")


@register.simple_tag
def get_env_name():
    return getattr(settings, "ENV_NAME", "")


@register.simple_tag
def get_team_email_contact():
    return getattr(settings, "TEAM_EMAIL_CONTACT", "")
