from django.conf import settings
from django.core.management import call_command
from django.test import override_settings
from django.utils import timezone

import pytest

from backend.ships import settings as ships_settings
from backend.ships.models import CrewList, PassengerList, ShipMovement
from backend.unit_tests import factories

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


@override_settings(DATA_MAX_DAYS_OLD=1)
def test_ship_movements_is_removed_if_ETA_is_old(client):
    today = timezone.now()
    yesterday = today - timezone.timedelta(days=1)
    two_days_ago = today - timezone.timedelta(days=2)
    recent_ship = factories.ShipMovementFactory(
        eta_to_port_of_call=yesterday,
        etd_from_port_of_call=today,
        way=ships_settings.ARRIVING,
    )
    old_ship = factories.ShipMovementFactory(
        eta_to_port_of_call=two_days_ago,
        etd_from_port_of_call=today,
        way=ships_settings.ARRIVING,
    )
    call_command("remove_data")
    remaining_ships = ShipMovement.objects.all()
    assert old_ship not in remaining_ships
    assert recent_ship in remaining_ships


@override_settings(DATA_MAX_DAYS_OLD=2)
def test_ship_movements_is_removed_if_ETA_and_ETD_are_old(client):
    today = timezone.now()
    yesterday = today - timezone.timedelta(days=1)
    two_days_ago = today - timezone.timedelta(days=2)
    last_week = today - timezone.timedelta(days=7)
    recent_ship = factories.ShipMovementFactory(
        eta_to_port_of_call=yesterday,
        etd_from_port_of_call=today,
        way=ships_settings.ARRIVING,
    )
    old_ship = factories.ShipMovementFactory(
        eta_to_port_of_call=last_week,
        etd_from_port_of_call=two_days_ago,
        way=ships_settings.ARRIVING,
    )
    call_command("remove_data")
    remaining_ships = ShipMovement.objects.all()
    assert old_ship not in remaining_ships
    assert recent_ship in remaining_ships


@override_settings(DATA_MAX_DAYS_OLD=2)
def test_crew_list_is_removed_if_ship_ETA_is_old(client):
    today = timezone.now()
    yesterday = today - timezone.timedelta(days=1)
    last_week = today - timezone.timedelta(days=7)
    recent_ship = factories.ShipMovementFactory(
        eta_to_port_of_call=yesterday,
        way=ships_settings.ARRIVING,
    )
    old_ship = factories.ShipMovementFactory(
        eta_to_port_of_call=last_week,
        way=ships_settings.ARRIVING,
    )
    old_crew = CrewList.objects.create(
        eta_to_port_of_call=today, way=ships_settings.ARRIVING, ship_movement=old_ship
    )
    recent_crew = CrewList.objects.create(
        eta_to_port_of_call=today,
        way=ships_settings.ARRIVING,
        ship_movement=recent_ship,
    )
    call_command("remove_data")
    remaining_crew = CrewList.objects.all()
    assert old_crew not in remaining_crew
    assert recent_crew in remaining_crew


@override_settings(DATA_MAX_DAYS_OLD=2)
def test_passenger_list_is_removed_if_ship_ETA_is_old(client):
    today = timezone.now()
    yesterday = today - timezone.timedelta(days=1)
    last_week = today - timezone.timedelta(days=7)
    recent_ship = factories.ShipMovementFactory(
        eta_to_port_of_call=yesterday,
        way=ships_settings.ARRIVING,
    )
    old_ship = factories.ShipMovementFactory(
        eta_to_port_of_call=last_week,
        way=ships_settings.ARRIVING,
    )
    old_passenger = PassengerList.objects.create(
        eta_to_port_of_call=today, way=ships_settings.ARRIVING, ship_movement=old_ship
    )
    recent_passenger = PassengerList.objects.create(
        eta_to_port_of_call=today,
        way=ships_settings.ARRIVING,
        ship_movement=recent_ship,
    )
    call_command("remove_data")
    remaining_passenger = PassengerList.objects.all()
    assert old_passenger not in remaining_passenger
    assert recent_passenger in remaining_passenger


@override_settings(DATA_MAX_DAYS_OLD=7)
def test_ship_movements_is_removed_when_comparing_eta_and_arriving(client):
    today = timezone.now()
    yesterday = today - timezone.timedelta(days=1)
    many_days_ago = today - timezone.timedelta(days=30)
    recent_ship = factories.ShipMovementFactory(
        eta_to_port_of_call=yesterday,
        etd_from_port_of_call=many_days_ago,
        way=ships_settings.ARRIVING,
    )
    old_ship = factories.ShipMovementFactory(
        eta_to_port_of_call=many_days_ago,
        etd_from_port_of_call=yesterday,
        way=ships_settings.ARRIVING,
    )
    call_command("remove_data")
    remaining_ships = ShipMovement.objects.all()
    assert old_ship not in remaining_ships
    assert recent_ship in remaining_ships


@override_settings(DATA_MAX_DAYS_OLD=1)
def test_ship_movements_is_removed_when_comparing_etd_departing(client):
    today = timezone.now()
    yesterday = today - timezone.timedelta(days=1)
    many_days_ago = today - timezone.timedelta(days=30)
    recent_ship = factories.ShipMovementFactory(
        eta_to_port_of_call=many_days_ago,
        etd_from_port_of_call=yesterday,
        way=ships_settings.DEPARTING,
    )
    old_ship = factories.ShipMovementFactory(
        eta_to_port_of_call=today,
        etd_from_port_of_call=many_days_ago,
        way=ships_settings.DEPARTING,
    )
    call_command("remove_data")
    remaining_ships = ShipMovement.objects.all()
    assert old_ship not in remaining_ships
    assert recent_ship in remaining_ships
