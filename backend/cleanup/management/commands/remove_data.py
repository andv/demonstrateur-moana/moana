from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone

from backend.logs.actions import ActionLogHandler
from backend.ships.models import ShipMovement
from backend.utils.sentry import sentry_capture


class Command(ActionLogHandler, BaseCommand):
    help = "Remove old data"

    def add_arguments(self, parser):
        parser.add_argument(
            "-y",
            "--dry-run",
            action="store_true",
            help="Display command result without running it.",
        )

    @sentry_capture
    def handle(self, *args, **options):
        days_old = settings.DATA_MAX_DAYS_OLD
        dry_run = options["dry_run"]
        if dry_run:
            self.stdout.write(self.style.SUCCESS("Running in dry-run mode"))
        self.stdout.write(
            self.style.SUCCESS(f"Removing data older than {days_old} days...")
        )
        cutoff_date = timezone.now() - timezone.timedelta(days=days_old)
        cutoff_date = cutoff_date.replace(hour=0, minute=0, second=0, microsecond=0)
        old_ships = ShipMovement.objects.filter(relevant_time__lt=cutoff_date)
        self.stdout.write(self.style.SUCCESS(f"Removing ships: {old_ships}"))
        if not dry_run:
            for ship in old_ships:
                ship.delete()
                self.create_action_log(
                    action="deleted old movement",
                    actor="moana application",
                    object=f"Ref {ship.ship_call_id}",
                    data="",
                    description="movement is automatically remove",
                    ship=ship,
                    target=ship,
                )
