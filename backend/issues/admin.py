from django.contrib import admin

from import_export.admin import ExportActionMixin
from rangefilter.filters import DateRangeFilterBuilder

from backend.issues.models import ShipIssue
from backend.issues.resources import ShipIssueResource


@admin.register(ShipIssue)
class ShipIssueAdmin(ExportActionMixin, admin.ModelAdmin):
    list_display = (
        "id",
        "issue_type",
        "ship_movement_link",
        "has_crew_list",
        "crew_list_link",
        "has_passenger_list",
        "passengers_list_link",
        "processing_status",
        "user",
        "team",
        "port_of_call_relation",
        "way",
        "ship_call_id",
        "created",
        "modified",
    )
    list_display_links = ("id", "issue_type")
    search_fields = (
        "user__email",
        "user__team__name",
        "user__team__ports__locode",
        "user__team__ports__name",
        "ship_movement__imo_number",
        "ship_movement__ship_name",
        "ship_movement__port_of_call",
        "ship_movement__ship_call_id",
        "notes",
    )
    list_filter = (
        "issue_type",
        "processing_status",
        "ship_movement__port_of_call",
        "ship_movement__way",
        "user__team",
        ("ship_movement__relevant_time", DateRangeFilterBuilder()),
        ("created", DateRangeFilterBuilder()),
    )
    raw_id_fields = ("ship_movement",)
    date_hierarchy = "created"
    readonly_fields = [
        "created",
        "modified",
        "team",
        "id",
        "ship_movement_link",
        "port_of_call_relation",
        "way",
        "ship_call_id",
        "has_crew_list",
        "has_passenger_list",
        "crew_list",
        "crew_list_id",
        "crew_list_created",
        "crew_list_updated",
        "passenger_list",
        "passenger_list_id",
        "passenger_list_created",
        "passenger_list_updated",
        "passengers_list_link",
        "crew_list_link",
    ]
    fieldsets = (
        (
            "Problème signalé",
            {
                "fields": [
                    "id",
                    "issue_type",
                    "processing_status",
                    "user",
                    "team",
                    "details",
                    "notes",
                    "created",
                    "modified",
                ],
            },
        ),
        (
            "Informations sur le mouvement",
            {
                "fields": [
                    "ship_movement",
                    "port_of_call_relation",
                    "way",
                    "ship_call_id",
                ],
            },
        ),
        (
            "Informations sur la liste équipage",
            {
                "fields": [
                    "has_crew_list",
                    "crew_list_link",
                    "crew_list_id",
                    "crew_list_created",
                    "crew_list_updated",
                ],
            },
        ),
        (
            "Informations sur la liste passagers",
            {
                "fields": [
                    "has_passenger_list",
                    "passengers_list_link",
                    "passenger_list_id",
                    "passenger_list_created",
                    "passenger_list_updated",
                ],
            },
        ),
    )
    resource_class = ShipIssueResource
