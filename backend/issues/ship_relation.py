from django.db import models
from django.db.models import Exists, OuterRef, Q

from rest_framework import serializers

from backend.issues.models import ShipIssue
from backend.issues.serializers import ShipIssueSerializer

##################
# Models         #
##################


class IssuesOnShip(models.Model):
    """
    This abstract class is expected to be sub-classed on a ShipMovement-like model.
    It provides helpers for handling issues reported for the ship movement.
    """

    class Meta:
        abstract = True

    def issues(self):
        return Issue.objects.filter(ship_movement=self)


##################
# Serializers    #
##################


class IssuesOnShipSerializerMixin(serializers.Serializer):
    """
    This mixin class is expected to be sub-classed on a serializer for ShipMovement model.
    """

    issues = serializers.SerializerMethodField()

    def get_issues(self, ship):
        if not self.current_user:
            return {}
        if not ship.has_issues:
            return {}
        team_issues = ship.all_issues.filter(user__team=self.current_user.team)
        if not team_issues:
            return {}
        serializer = ShipIssueSerializer(team_issues, many=True)
        return serializer.data


##################
# Views          #
##################


class IssueOnShipViewMixin:
    """
    This mixin class is expected to be used on a API list view that handles ShipMovement.
    This mixin is to be used together with the mixin that provides current user helpers.
    """

    def annotate_has_issues(self, ship_queryset):
        """
        On the given ship movement queryset, we annotate whether or not there a ship
        movement has issues.
        To know if there are issues for a ship movement, we need to take into account
        the team of the current user.
        This `has_issues` annotated field, can then be checked so that we make requests
        to the issues only when it make sense. Retrieving the related issues JSON
        document is done at the serializer level, because here we cannot easily annotate
        an entire list of issues.
        """
        qs_issues = ShipIssue.objects.filter(
            Q(ship_movement=OuterRef("pk")) & (Q(user__team=self.current_team))
        )
        qs = ship_queryset.annotate(has_issues=Exists(qs_issues))
        return qs
