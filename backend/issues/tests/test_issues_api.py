from django.conf import settings
from django.shortcuts import reverse

import pytest
from issues import choices as issues_choices

from backend.issues.models import ShipIssue
from backend.logs.models import UserActionLog
from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def initialize_test(client):
    user = utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ship = ShipMovement.objects.create(
        way=ships_settings.ARRIVING, port_of_call_relation=saint_malo
    )
    return ship, user


def test_issue_is_listed_in_ship_movement_api(client):
    ship, user = initialize_test(client)
    ShipIssue.objects.create(
        user=user,
        ship_movement=ship,
        issue_type=issues_choices.ISSUE_TYPE_CREW_CHAR_ENCODING,
    )
    url = reverse("ships-list")
    response = client.get(url)
    assert issues_choices.ISSUE_TYPE_CREW_CHAR_ENCODING in str(response.content)


def test_issue_is_not_listed_if_user_is_not_team_member(client):
    ship, user = initialize_test(client)
    issue = ShipIssue.objects.create(
        user=user,
        ship_movement=ship,
        issue_type=issues_choices.ISSUE_TYPE_CREW_CHAR_ENCODING,
    )
    another_user = factories.UserFactory()
    another_port = factories.PortFactory(locode="FRLEH")
    another_team = factories.TeamFactory()
    another_team.ports.add(another_port)
    another_user.team = another_team
    another_user.save()
    another_issue = ShipIssue.objects.create(
        user=another_user,
        ship_movement=ship,
        issue_type=issues_choices.ISSUE_TYPE_MOVEMENT_BAD_TIMES,
    )
    url = reverse("ships-list")
    response = client.get(url)
    assert issue.issue_type in str(response.content)
    assert another_issue.issue_type not in str(response.content)


def test_user_can_report_an_issue(client):
    ship, user = initialize_test(client)
    url = reverse("ships-report-issue", args=[ship.pk])
    post_data = {
        "issue_type": issues_choices.ISSUE_TYPE_MOVEMENT_BAD_TIMES,
        "issue_details": "",
    }
    response = client.post(url, data=post_data)
    assert ShipIssue.objects.filter(
        user=user,
        ship_movement=ship,
        issue_type=issues_choices.ISSUE_TYPE_MOVEMENT_BAD_TIMES,
    ).exists()
    assert issues_choices.ISSUE_TYPE_MOVEMENT_BAD_TIMES in str(response.content)


def test_tracking_entry_is_created_when_issue_is_reported(client):
    ship, user = initialize_test(client)
    url = reverse("ships-report-issue", args=[ship.pk])
    post_data = {
        "issue_type": issues_choices.ISSUE_TYPE_MOVEMENT_BAD_TIMES,
        "issue_details": "",
    }
    tracking_count_before = UserActionLog.objects.count()
    client.post(url, data=post_data)
    tracking_count_after = UserActionLog.objects.count()
    assert tracking_count_after == tracking_count_before + 1


def test_no_issue_report_is_created_if_issue_already_exists(client):
    ship, user = initialize_test(client)
    ShipIssue.objects.create(
        user=user,
        ship_movement=ship,
        issue_type=issues_choices.ISSUE_TYPE_MOVEMENT_BAD_TIMES,
        details="",
    )
    url = reverse("ships-report-issue", args=[ship.pk])
    post_data = {
        "issue_type": issues_choices.ISSUE_TYPE_MOVEMENT_BAD_TIMES,
        "issue_details": "",
    }
    tracking_count_before = UserActionLog.objects.count()
    client.post(url, data=post_data)
    tracking_count_after = UserActionLog.objects.count()
    issues_count = ShipIssue.objects.filter(
        user=user,
        ship_movement=ship,
        issue_type=issues_choices.ISSUE_TYPE_MOVEMENT_BAD_TIMES,
    ).count()
    assert tracking_count_after == tracking_count_before
    assert issues_count == 1


def test_create_issue_with_details_field(client):
    ship, user = initialize_test(client)
    details = "I am an issue"
    ShipIssue.objects.create(
        user=user,
        ship_movement=ship,
        issue_type=issues_choices.ISSUE_TYPE_MOVEMENT_BAD_TIMES,
        details=details,
    )
    url = reverse("ships-list")
    response = client.get(url)
    assert details in str(response.content)


def test_create_issue_with_html_in_details_field(client):
    ship, user = initialize_test(client)
    details = "I <em>am</em> an issue"
    details_sanitized = "I am an issue"
    ShipIssue.objects.create(
        user=user,
        ship_movement=ship,
        issue_type=issues_choices.ISSUE_TYPE_MOVEMENT_BAD_TIMES,
        details=details,
    )
    url = reverse("ships-list")
    response = client.get(url)
    assert details_sanitized in str(response.content)
    assert details not in str(response.content)


def test_create_issue_with_vue_characters_in_details_field(client):
    ship, user = initialize_test(client)
    details = str("{{I am vue friendly}}")
    details_sanitized = "I am vue friendly"
    ShipIssue.objects.create(
        user=user,
        ship_movement=ship,
        issue_type=issues_choices.ISSUE_TYPE_MOVEMENT_BAD_TIMES,
        details=details,
    )
    url = reverse("ships-list")
    response = client.get(url)
    assert details_sanitized in str(response.content)
    assert details not in str(response.content)


def test_create_issue_with_html_in_issue_type_field(client):
    ship, user = initialize_test(client)
    ShipIssue.objects.create(
        user=user,
        ship_movement=ship,
        issue_type="<div>" + issues_choices.ISSUE_TYPE_MOVEMENT_BAD_TIMES + "</div>",
    )
    url = reverse("ships-list")
    response = client.get(url)
    assert "<div>" not in str(response.content)
    assert "<div/>" not in str(response.content)
    assert issues_choices.ISSUE_TYPE_MOVEMENT_BAD_TIMES in str(response.content)


def test_create_issue_with_html_in_processing_status_field(client):
    ship, user = initialize_test(client)
    ShipIssue.objects.create(
        user=user,
        ship_movement=ship,
        processing_status="<p>status</p>",
    )
    url = reverse("ships-list")
    response = client.get(url)
    assert "<p>" not in str(response.content)
    assert "<p/>" not in str(response.content)
    assert "status" in str(response.content)
