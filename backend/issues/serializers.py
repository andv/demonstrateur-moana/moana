from rest_framework import serializers

from backend.issues import choices as issues_choices
from backend.issues.models import ShipIssue


class ShipIssueSerializer(serializers.ModelSerializer):
    """
    This is a serializer class for the ShipIssue model.
    """

    processing_status_display = serializers.CharField(
        source="get_processing_status_display"
    )
    issue_type_display = serializers.CharField(source="get_issue_type_display")
    user = serializers.CharField(source="user.email")

    class Meta:
        model = ShipIssue
        fields = (
            "user",
            "processing_status",
            "processing_status_display",
            "issue_type",
            "issue_type_display",
            "notes",
            "details",
        )


class ReportIssueSerializer(serializers.Serializer):
    """
    This is a serializer meant for incoming POST request to creating a issue report.
    """

    issue_type = serializers.ChoiceField(choices=issues_choices.ISSUE_TYPE_CHOICES)
