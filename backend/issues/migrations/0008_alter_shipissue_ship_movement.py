# Generated by Django 4.2.11 on 2024-03-27 09:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("ships", "0029_add_relevant_port_relation"),
        ("issues", "0007_add_user_input_sanitizer"),
    ]

    operations = [
        migrations.AlterField(
            model_name="shipissue",
            name="ship_movement",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="all_issues",
                to="ships.shipmovement",
                verbose_name="mouvement de navire",
            ),
        ),
    ]
