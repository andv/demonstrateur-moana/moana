from rest_framework import decorators, viewsets
from rest_framework.response import Response

from backend.issues import choices as issues_choices
from backend.issues.models import ShipIssue
from backend.issues.serializers import ReportIssueSerializer, ShipIssueSerializer
from backend.logs.actions import ActionLogHandler


class ReportIssueLogHandler(ActionLogHandler):
    def log_report_issue_action(self, ship_movement=None, issue_type="", target=""):
        self.create_user_action_log(
            action=f"reported issue {issue_type}",
            user=self.request.user,
            object=issue_type,
            target=target,
            ship=ship_movement,
        )


class ReportIssueViewMixin(ReportIssueLogHandler):
    """
    View mixin that's expected to be used in a ship movement API view
    and adds functionalities around reporting issues.
    """

    @decorators.action(detail=True, methods=["POST"], url_path="report-issue")
    def report_issue(self, request, pk=None):
        """
        Accept a POST request and create the issue reported by the site user.
        """
        ship = self.get_object()
        input_serializer = ReportIssueSerializer(request.data)
        issue_type = input_serializer.data.get("issue_type")
        details = request.data.get("issue_details")
        reported_issue, created = ShipIssue.objects.get_or_create(
            user=self.request.user,
            ship_movement=ship,
            issue_type=issue_type,
            details=details,
        )
        if created:
            self.log_report_issue_action(ship, issue_type=issue_type, target=ship)
        issue_serializer = ShipIssueSerializer(reported_issue)
        response_data = issue_serializer.data.copy()
        response_data["created"] = created
        return Response(response_data)


class IssuesViewSet(viewsets.ViewSet, ReportIssueLogHandler):
    def list(self, request, pk=None):
        """
        List issues from user's team
        """
        issue_type = request.query_params.get("issue_type")
        team = request.user.team
        queryset = ShipIssue.objects.filter(user__team=team)
        if issue_type:
            queryset = queryset.filter(issue_type=issue_type)
        queryset = queryset.distinct()
        serializer = ShipIssueSerializer(queryset, many=True)
        return Response(serializer.data)

    @decorators.action(detail=False, methods=["POST"], url_path="report-missing-ship")
    def report_missing_ship(self, request, pk=None):
        """
        Accept a POST request and create the missing ship issue reported by the site user.
        """
        issue_type = issues_choices.ISSUE_TYPE_MISSING_SHIP
        details = request.data.get("issue_details", "")
        created_issue = ShipIssue.objects.create(
            user=self.request.user,
            issue_type=issue_type,
            details=details,
        )
        self.log_report_issue_action(
            ship_movement=None, issue_type=issue_type, target=""
        )
        issue_serializer = ShipIssueSerializer(created_issue)
        response_data = issue_serializer.data.copy()
        return Response(response_data)
