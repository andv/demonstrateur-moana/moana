import re

from django.contrib import admin
from django.db import models
from django.urls import reverse
from django.utils.html import format_html

from model_utils.models import TimeStampedModel
from tinymce.models import HTMLField

from backend.issues import choices as issues_choices
from backend.utils.fields import SanitizedTextField


class ShipIssue(TimeStampedModel):
    user = models.ForeignKey(
        to="accounts.User",
        verbose_name="utilisateur",
        related_name="issues",
        help_text=("L'utilisateur qui a signalé le problème."),
        on_delete=models.CASCADE,
    )
    ship_movement = models.ForeignKey(
        to="ships.ShipMovement",
        verbose_name="mouvement de navire",
        related_name="all_issues",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    issue_type = SanitizedTextField(
        "type de problème",
        max_length=256,
        choices=issues_choices.ISSUE_TYPE_CHOICES,
    )
    processing_status = SanitizedTextField(
        "status de traitement",
        max_length=256,
        choices=issues_choices.ISSUE_PROCESSING_STATUS_CHOICES,
        default=issues_choices.ISSUE_PROCESSING_STATUS_DEFAULT,
    )
    details = SanitizedTextField(verbose_name="précisions de l'utilisateur", blank=True)
    notes = HTMLField(verbose_name="notes", blank=True)

    class Meta:
        verbose_name = "problème signalé"
        verbose_name_plural = "problèmes signalés"
        constraints = [
            models.UniqueConstraint(
                fields=["user", "ship_movement", "issue_type", "details"],
                name="unique issue reported",
            )
        ]

    def __str__(self):
        return (
            f"{self.get_issue_type_display()} - {self.get_processing_status_display()} - "
            f"{self.user} - {self.ship_movement}"
        )

    def format_datetime(self, date_obj):
        """
        Utility method to format a datetime object as a string.
        """
        if not date_obj:
            return ""
        return date_obj.strftime("le %d/%m/%Y à %H:%M:%S")

    def get_admin_page_link(self, reverse_name, obj):
        """
        Utility method to reverse the URL base on the given reverse name and return an HTML snippet with a link.
        reverse name is in the form : `admin:{app_label}_{model_name}_change`
        """
        if not obj:
            return ""
        url = reverse(
            reverse_name,
            args=[obj.id],
        )
        name = str(obj)
        return format_html(f"<a href='{url}'>{name}</a>")

    @property
    def team(self):
        return self.user.team

    @property
    def port_of_call_relation(self):
        return self.ship_movement.port_of_call_relation if self.ship_movement else ""

    @property
    def relevant_time(self):
        return self.ship_movement.relevant_time if self.ship_movement else ""

    @property
    def way(self):
        return self.ship_movement.way if self.ship_movement else ""

    @property
    def ship_call_id(self):
        return self.ship_movement.ship_call_id if self.ship_movement else ""

    @property
    def crew_list(self):
        """
        Returns the crew list there is one, else None.
        When there is no ship movement, returns None.
        """
        if not self.ship_movement:
            return None
        return getattr(self.ship_movement, "crew_list", None)

    @property
    def passenger_list(self):
        """
        Returns the passenger list there is one, else None.
        When there is no ship movement, returns None.
        """
        if not self.ship_movement:
            return None
        return getattr(self.ship_movement, "passenger_list", None)

    @admin.display(boolean=True, description="liste équipage reçue")
    def has_crew_list(self):
        # When there is no ship movement, returns None to indicate that we don't know.
        return bool(self.crew_list) if self.ship_movement else None

    @admin.display(boolean=True, description="liste passagers reçue")
    def has_passenger_list(self):
        # When there is no ship movement, returns None to indicate that we don't know.
        return bool(self.passenger_list) if self.ship_movement else None

    @admin.display(description="identifiant")
    def crew_list_id(self):
        return self.crew_list.id if self.crew_list else ""

    @admin.display(description="création")
    def crew_list_created(self):
        if not self.crew_list:
            return ""
        return self.format_datetime(self.crew_list.created)

    @admin.display(description="modification")
    def crew_list_updated(self):
        if not self.crew_list:
            return ""
        return self.format_datetime(self.crew_list.modified)

    @admin.display(description="identifiant")
    def passenger_list_id(self):
        return self.passenger_list.id if self.passenger_list else ""

    @admin.display(description="création")
    def passenger_list_created(self):
        if not self.passenger_list:
            return ""
        return self.format_datetime(self.passenger_list.created)

    @admin.display(description="modification")
    def passenger_list_updated(self):
        if not self.passenger_list:
            return ""
        return self.format_datetime(self.passenger_list.modified)

    @admin.display(description="liste passagers")
    def passengers_list_link(self):
        reverse_name = "admin:ships_passengerlist_change"
        return self.get_admin_page_link(reverse_name, self.passenger_list)

    @admin.display(description="list équipage")
    def crew_list_link(self):
        reverse_name = "admin:ships_crewlist_change"
        return self.get_admin_page_link(reverse_name, self.crew_list)

    @admin.display(description="mouvement de navire")
    def ship_movement_link(self):
        reverse_name = "admin:ships_shipmovement_change"
        return self.get_admin_page_link(reverse_name, self.ship_movement)
