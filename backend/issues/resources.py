from import_export import fields as import_export_fields
from import_export import resources

from backend.issues.models import ShipIssue


class ShipIssueResource(resources.ModelResource):
    team_name = import_export_fields.Field(attribute="team", column_name="Nom équipe")
    user_email = import_export_fields.Field(
        attribute="user__email", column_name="Email utilisateur"
    )
    issue_type_display = import_export_fields.Field(
        attribute="get_issue_type_display", column_name="Type"
    )
    processing_status_display = import_export_fields.Field(
        attribute="get_processing_status_display", column_name="Statut"
    )
    created = import_export_fields.Field(attribute="created", column_name="Créé")
    modified = import_export_fields.Field(attribute="created", column_name="Modifié")
    ship_name = import_export_fields.Field(
        attribute="ship_movement__ship_name", column_name="Nom mouvement"
    )
    port_of_call_relation = import_export_fields.Field(
        attribute="port_of_call_relation", column_name="Port"
    )
    way = import_export_fields.Field(
        attribute="ship_movement__way", column_name="Direction"
    )
    ship_call_id = import_export_fields.Field(
        attribute="ship_call_id", column_name="Ship call id"
    )
    has_passenger_list = import_export_fields.Field(
        attribute="has_passenger_list", column_name="Liste passagers reçue"
    )
    has_crew_list = import_export_fields.Field(
        attribute="has_crew_list", column_name="Liste équipage reçue"
    )

    class Meta:
        model = ShipIssue
        fields = (
            "team_name",
            "user_email",
            "issue_type_display",
            "processing_status_display",
            "created",
            "modified",
            "ship_name",
            "port_of_call_relation",
            "ship_call_id",
            "way",
            "has_passenger_list",
            "has_crew_list",
        )
