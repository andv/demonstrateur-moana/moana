ISSUE_TYPE_MOVEMENT_BAD_TIMES = "issue-movement-bad-times"
ISSUE_TYPE_MOVEMENT_BAD_SHIP_TYPE = "issue-movement-bad-ship-type"
ISSUE_TYPE_MOVEMENT_BAD_LAST_PORT = "issue-movement-bad-last-port"
ISSUE_TYPE_MOVEMENT_BAD_NEXT_PORT = "issue-movement-bad-next-port"
ISSUE_TYPE_MOVEMENT_BAD_ANAIS_LINK = "issue-movement-bad-anais-link"
ISSUE_TYPE_CREW_CHAR_ENCODING = "issue-crew-char-encoding"
ISSUE_TYPE_CREW_BAD_DOB = "issue-crew-bad-date-of-birth"
ISSUE_TYPE_CREW_NAME_SWITCHED = "issue-crew-name-switched"
ISSUE_TYPE_CREW_EMPTY_FIELDS = "issue-crew-empty-fields"
ISSUE_TYPE_CREW_NOT_PRESENT = "issue-crew-not-present"
ISSUE_TYPE_CREW_IDENTITY_DOC = "issue-crew-identity-document"
ISSUE_TYPE_CREW_RECEIVED_LATE = "issue-crew-received-late"
ISSUE_TYPE_PASSENGER_CHAR_ENCODING = "issue-passengers-char-encoding"
ISSUE_TYPE_PASSENGERS_BAD_DOB = "issue-passengers-bad-date-of-birth"
ISSUE_TYPE_PASSENGERS_NAME_SWITCHED = "issue-passengers-name-switched"
ISSUE_TYPE_PASSENGERS_EMPTY_FIELDS = "issue-passengers-empty-fields"
ISSUE_TYPE_PASSENGERS_NOT_PRESENT = "issue-passengers-not-present"
ISSUE_TYPE_PASSENGERS_IDENTITY_DOC = "issue-passengers-identity-document"
ISSUE_TYPE_PASSENGERS_RECEIVED_LATE = "issue-passengers-received-late"
ISSUE_TYPE_OTHER = "issue-other"
ISSUE_TYPE_MISSING_SHIP = "issue-missing-ship"

ISSUE_TYPE_CHOICES = (
    (ISSUE_TYPE_MOVEMENT_BAD_TIMES, "Mouvement : Problème horaires"),
    (ISSUE_TYPE_MOVEMENT_BAD_SHIP_TYPE, "Mouvement : Problème type de navire"),
    (ISSUE_TYPE_MOVEMENT_BAD_LAST_PORT, "Mouvement : Problème port de provenance"),
    (ISSUE_TYPE_MOVEMENT_BAD_NEXT_PORT, "Mouvement : Problème port de destination"),
    (
        ISSUE_TYPE_MOVEMENT_BAD_ANAIS_LINK,
        "Mouvement : Lien ANAIS incorrect ou manquant",
    ),
    (ISSUE_TYPE_CREW_CHAR_ENCODING, "Équipage : Problème encodage caractères"),
    (ISSUE_TYPE_CREW_BAD_DOB, "Équipage : Problème date de naissance"),
    (ISSUE_TYPE_CREW_NAME_SWITCHED, "Équipage : Problème nom/prénom"),
    (ISSUE_TYPE_CREW_EMPTY_FIELDS, "Équipage : Champs vides"),
    (ISSUE_TYPE_CREW_NOT_PRESENT, "Équipage : Liste non présente"),
    (ISSUE_TYPE_CREW_RECEIVED_LATE, "Équipage : Liste reçue en retard"),
    (ISSUE_TYPE_CREW_IDENTITY_DOC, "Équipage : Problème sur les documents d'identité"),
    (ISSUE_TYPE_PASSENGER_CHAR_ENCODING, "Passagers : Problème encodage caractères"),
    (ISSUE_TYPE_PASSENGERS_BAD_DOB, "Passagers : Problème date de naissance"),
    (ISSUE_TYPE_PASSENGERS_NAME_SWITCHED, "Passagers : Problème nom/prénom"),
    (ISSUE_TYPE_PASSENGERS_EMPTY_FIELDS, "Passagers : Champs vides"),
    (ISSUE_TYPE_PASSENGERS_NOT_PRESENT, "Passagers : Liste non présente"),
    (ISSUE_TYPE_PASSENGERS_RECEIVED_LATE, "Passagers : Liste reçue en retard"),
    (
        ISSUE_TYPE_PASSENGERS_IDENTITY_DOC,
        "Passagers : Problème sur les documents d'identité",
    ),
    (ISSUE_TYPE_OTHER, "Autre problème"),
    (ISSUE_TYPE_MISSING_SHIP, "Mouvement non présent"),
)


ISSUE_PROCESSING_STATUS_SENT = "sent"
ISSUE_PROCESSING_STATUS_IN_PROGRESS = "in-progress"
ISSUE_PROCESSING_STATUS_RESOLVED = "resolved"
ISSUE_PROCESSING_STATUS_CLOSED_WITHOUT_RESOLUTION = "closed-without-resolution"
ISSUE_PROCESSING_STATUS_CHOICES = (
    (ISSUE_PROCESSING_STATUS_SENT, "Envoyé"),
    (ISSUE_PROCESSING_STATUS_IN_PROGRESS, "En cours"),
    (ISSUE_PROCESSING_STATUS_RESOLVED, "Résolu"),
    (ISSUE_PROCESSING_STATUS_CLOSED_WITHOUT_RESOLUTION, "Fermé sans résolution"),
)
ISSUE_PROCESSING_STATUS_DEFAULT = ISSUE_PROCESSING_STATUS_SENT
