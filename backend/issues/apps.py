from django.apps import AppConfig


class ShipIssuesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.issues"
    verbose_name = "3.2 Mouvements - Problèmes signalés"
