from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.cache import never_cache
from django.views.generic import TemplateView


class IndexView(LoginRequiredMixin, TemplateView):
    """
    Serve Vue Application
    """

    template_name = "index.html"
    redirect_field_name = None


index_view = never_cache(IndexView.as_view())
