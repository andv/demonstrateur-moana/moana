import tempfile

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from defusedxml import ElementTree as ET
from imap_tools import OR, MailBox
from sentry_sdk import capture_exception, capture_message, push_scope

from backend.logs.actions import ActionLogHandler
from backend.ship_files import settings as ship_files_settings
from backend.ship_files.models import ShipFile as Fal
from backend.ship_files.validations import ShipFileValidationMixin
from backend.utils.sentry import sentry_capture
from marathon.commands import SingleRunnerCommand

LOCK_NAME_EMAIL_IMPORT_FAL = "email-import-fal"


class Command(
    ShipFileValidationMixin, ActionLogHandler, SingleRunnerCommand, BaseCommand
):
    lock_name = LOCK_NAME_EMAIL_IMPORT_FAL
    help = "Import FAL files from mailbox"
    mailbox = None  # The imap-tools MailBox instance
    file_type = None  # File type nca_fal1, 5, or 6 for instance

    def get_sender_search_criteria(self):
        """
        Build the search criteria base on imap-tools syntax.
        ```
        fetch(
            criteria=OR(
                OR(from_="someone@test.com"),
                OR(from_="someone.else@test.com"),
            )
        )
        ```
        """
        sender_search_criteria = []
        for sender in settings.EMAIL_IMPORT_SENDERS_WHITELIST:
            sender_search_criteria.append(OR(from_=sender))
        criteria = OR(*sender_search_criteria)
        return criteria

    def initialize_mailbox(self):
        mailbox = MailBox(settings.EMAIL_IMPORT_HOST).login(
            settings.EMAIL_IMPORT_USERNAME, settings.EMAIL_IMPORT_PASSWORD
        )
        self.mailbox = mailbox
        return self.mailbox

    def verify_mailbox_folder(self):
        if not self.mailbox:
            self.initialize_mailbox()
        self.done_folder = settings.EMAIL_IMPORT_DONE_FOLDER
        if not self.done_folder:
            raise CommandError("Mailbox folder settings is missing")
        if not self.mailbox.folder.exists(self.done_folder):
            raise CommandError("Mailbox folder is missing on remote mailbox")
        return self.done_folder

    def fetch_email_messages(self):
        if not self.mailbox:
            self.initialize_mailbox()
        if not settings.EMAIL_IMPORT_SENDERS_WHITELIST:
            self.log_error(
                "Ending import email because senders whitelist is not defined.",
            )
            return None
        messages = self.mailbox.fetch(
            criteria=self.get_sender_search_criteria(), mark_seen=False
        )
        return messages

    def get_email_details(self):
        data = {
            "email_uid": self.email_message.uid,
            "email_to": self.email_message.to,
            "email_from": self.email_message.from_,
            "email_subject": self.email_message.subject,
            "email_date": self.email_message.date_str,
            "email_size": self.email_message.size,
        }
        return data

    def log_error(self, display_message, raw_exception=None):
        self.stdout.write(display_message, self.style.ERROR)
        if raw_exception:
            with push_scope() as scope:
                scope.set_context("email details", self.get_email_details())
                capture_exception(raw_exception)
        else:
            capture_message(display_message)

    def log_action(self, import_success, description="", ship=None):
        """
        Log actions in database for tracking and statistics.
        """
        if import_success:
            action = ship_files_settings.EMAIL_IMPORT_LOG_ACTIONS[self.file_type]
        else:
            action = "email imported fal error"
        ref = ""
        if ship:
            ref = f"Ref {ship.ship_call_id}"
        self.create_action_log(
            action=action,
            actor=self.email_message.from_,
            object=ref,
            data=self.get_email_details(),
            description=description,
            ship=ship,
        )

    def xml_format_is_valid(self, xml_content):
        try:
            ET.fromstring(xml_content)
        except ET.ParseError as e:
            self.log_error(f"Email import FAL: XML format error: {e}", raw_exception=e)
            self.log_action(import_success=False, description=e)
            return False
        return True

    def validate_attachement_file(self, attachement):
        """
        Returns a file-object if the attachement is a valid FAL.
        """
        if not self.xml_format_is_valid(attachement.payload):
            return None
        filename = attachement.filename
        mime_type = attachement.content_type
        f = tempfile.NamedTemporaryFile()
        f.name = filename
        f.write(attachement.payload)
        if not self.file_extension_is_valid(f):
            self.log_error(f"Email import FAL: Ignore invalid extension: {filename}")
            return None
        if not self.is_mime_type_valid(mime_type):
            self.log_error(
                f"Email import FAL: Ignore wrong file type: {filename}: {mime_type}"
            )
            return None
        return f

    def import_attachement_file(self, attachement):
        f = self.validate_attachement_file(attachement)
        if not f:
            return None
        self.stdout.write(f"Email import FAL: Trying to import attachement: '{f.name}'")
        fal = Fal()
        try:
            save_model_instance_on_file_save = True
            fal.ship_file.save(f.name, f, save=save_model_instance_on_file_save)
            self.file_type = fal.file_type
            self.log_action(import_success=True, ship=fal.get_related_ship())
        except Exception as e:
            self.log_error(
                f"Email import FAL: Error when importing the file : {e}",
                raw_exception=e,
            )
            self.log_action(import_success=False, description=e)
        f.close()
        if fal.ship_file:
            fal.ship_file.delete(save=False)
        fal.delete()
        return f

    @sentry_capture
    def run_task(self, *args, **options):
        self.stdout.write("Entering import from email command")
        self.initialize_mailbox()
        done_folder = self.verify_mailbox_folder()
        messages = self.fetch_email_messages()
        checked_email_uids = []
        for msg in messages:
            self.email_message = msg
            self.mailbox.move(msg.uid, done_folder)  # Move now to not process it again.
            self.stdout.write(
                f"Checking email ID {msg.uid} from '{msg.from_}': '{msg.subject}'"
            )
            for attachement in msg.attachments:
                self.import_attachement_file(attachement)
            checked_email_uids.append(msg.uid)
        if checked_email_uids:
            count_processed_email = len(checked_email_uids)
            self.stdout.write(f"Number of email processed: {count_processed_email}")
        self.stdout.write("Ending import from email command")
