from import_export.formats import base_formats


class CSVImportMixin:
    """
    This is a mixin for Django management commands.
    It's an helper class for importing a CSV file based on an import-export resource class.
    This could be used, for instance, to import ship types or ship type mappings.
    """

    resource_class = None  # The resource class must be defined in the child class.

    def add_arguments(self, parser):
        parser.add_argument("file_path")

    def log_error(self, message):
        self.stdout.write(message, self.style.ERROR)

    def show_import_errors(self, import_result):
        self.log_error("Import errors:")
        for error in import_result.base_errors:
            self.log_error(error.error)
        for line, errors in import_result.row_errors():
            for error in errors:
                self.log_error(f"Line number: {line} - {error.error}")

    def handle(self, *args, **options):
        file_path = options["file_path"]
        with open(file_path, encoding="utf-8") as import_file:
            data = import_file.read()
            dataset_csv_format = base_formats.CSV()
            dataset = dataset_csv_format.create_dataset(data)
        resource = self.resource_class()
        result = resource.import_data(dataset, dry_run=True)
        if result.has_errors():
            self.show_import_errors(result)
            return None
        result = resource.import_data(dataset, dry_run=False)
        self.stdout.write(f"Successfully imported {file_path}", self.style.SUCCESS)
