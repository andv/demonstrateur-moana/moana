from django.conf import settings
from django.db import models
from django.db.models import Exists, OuterRef, Q

from rest_framework import serializers

from backend.alerts import choices as alerts_choices
from backend.alerts.mixins import AlertMixin
from backend.alerts.models import ShipAlert, ShipWatchlist
from backend.alerts.serializers import ShipAlertSerializer
from backend.current_user.mixins import CurrentUserSerializerMixin

##################
# Models         #
##################


class AlertsOnShip(AlertMixin, models.Model):
    """
    This abstract class is expected to be sub-classed on a ShipMovement-like model.
    It provides alerts fields and helpers.
    """

    alert_types = models.ManyToManyField(
        "alerts.AlertType", through="alerts.ShipAlert", related_name="ships"
    )

    class Meta:
        abstract = True

    def check_alerts_for_ship_country(self):
        """
        Looking at the FlagStateOfShip, we check if the country is listed
        as an alert country and we create an alert accordingly.
        """
        has_alert = any(
            country.lower() in self.flag_state_of_ship.lower()
            for country in settings.ALERT_SHIP_COUNTRIES
        )
        if has_alert:
            self.create_alert(
                alert_type_id=alerts_choices.SHIP_COUNTRY_ALERT, ship_movement=self
            )
        return self

    def check_alerts_on_ship_watchlist(self):
        qs_watchlist = ShipWatchlist.objects.filter(imo_list__icontains=self.imo_number)
        if not qs_watchlist:
            return
        for watchlist in qs_watchlist:
            port_of_call = (
                self.port_of_call_relation.locode
                if self.port_of_call_relation
                else self.port_of_call
            )
            watchlist_contains_imo = self.imo_number in watchlist.get_imo_list()
            team_contains_port_of_call = port_of_call in watchlist.get_ports_list()
            if watchlist_contains_imo and team_contains_port_of_call:
                self.create_alert(
                    alert_type_id=alerts_choices.SHIP_WATCHLIST_ALERT,
                    ship_movement=self,
                    team=watchlist.team,
                )

    def save(self, *args, **kwargs):
        response = super().save(*args, **kwargs)
        # Before checking on alerts, we need first to save ship object
        self.check_alerts_for_ship_country()
        self.check_alerts_on_ship_watchlist()
        return response


##################
# Serializers    #
##################


class AlertsOnShipSerializerMixin(CurrentUserSerializerMixin, serializers.Serializer):
    """
    This mixin class is expected to be sub-classed on a serializer for ShipMovement model.
    """

    alerts = serializers.SerializerMethodField()

    def get_alerts(self, ship):
        if not self.current_user:
            return {}
        if not ship.has_alerts:
            return {}
        alerts_qs = ship.alerts.filter(
            Q(team=self.current_user.team) | Q(team__isnull=True)
        ).exclude(hide_for_teams__name=self.current_user.team)
        if not alerts_qs:
            return {}
        serializer = ShipAlertSerializer(alerts_qs, many=True)
        return serializer.data


##################
# Views          #
##################


class AlertOnShipViewMixin:
    """
    This mixin class is expected to be used on a API list view that handles ShipMovement.
    This mixin is to be used together with the mixin that provides current user helpers.
    """

    def annotate_has_alerts(self, ship_queryset):
        """
        On the given ship movement queryset, we annotate whether or not there a ship
        movement has alerts.
        To know if there are alerts for a ship movement, we need to take into account
        the team of the current user.
        This `has_alerts` annotated field, can then be checked so that we make requests
        to the alerts only when it make sense. Retrieving the related alerts JSON
        document is done at the serializer level, because here we cannot easily annotate
        an entire list of alerts.
        """
        qs_alerts = ShipAlert.objects.filter(
            Q(ship_movement=OuterRef("pk"))
            & (Q(team=self.current_team) | Q(team__isnull=True))
        ).exclude(hide_for_teams__name=self.current_team)
        qs = ship_queryset.annotate(has_alerts=Exists(qs_alerts))
        return qs

    def annotate_has_movement_related_alerts(self, ship_queryset):
        """
        On the given ship movement queryset, we annotate whether or not there a ship
        movement has movement alerts.
        """
        list_alerts_type_names = dict(alerts_choices.MOVEMENT_RELATED_ALERTS).keys()
        qs_movement_related_alerts = ShipAlert.objects.filter(
            Q(ship_movement=OuterRef("pk"))
            & (
                Q(alert_type__alert_type__in=list_alerts_type_names)
                & (Q(team=self.current_team) | Q(team__isnull=True))
            )
        ).exclude(hide_for_teams__name=self.current_team)
        qs = ship_queryset.annotate(
            has_movement_related_alerts=Exists(qs_movement_related_alerts)
        )
        return qs
