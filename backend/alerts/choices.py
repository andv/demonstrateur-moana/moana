SHIP_COUNTRY_ALERT = "ship-country"
SHIP_WATCHLIST_ALERT = "ship-watchlist"
PASSENGER_NATIONALITY_ALERT = "passenger-nationality"
CREW_NATIONALITY_ALERT = "crew-nationality"
NEW_CREW_LIST_ALERT = "new-crew-list"
NEW_PASSENGER_LIST_ALERT = "new-passenger-list"


MOVEMENT_RELATED_ALERTS = (
    (SHIP_COUNTRY_ALERT, "Pavillon navire"),
    (SHIP_WATCHLIST_ALERT, "Navire sensible"),
)

PERSON_LIST_RELATED_ALERTS = (
    (NEW_CREW_LIST_ALERT, "Liste équipage"),
    (NEW_PASSENGER_LIST_ALERT, "Liste passagers"),
    (PASSENGER_NATIONALITY_ALERT, "Nationnalité passager"),
    (CREW_NATIONALITY_ALERT, "Nationnalité membre d'équipage"),
)

ALERT_TYPE_CHOICES = MOVEMENT_RELATED_ALERTS + PERSON_LIST_RELATED_ALERTS
