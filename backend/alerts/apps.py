from django.apps import AppConfig


class AlertsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.alerts"
    verbose_name = "1.2 Utilisateurs - Alertes"
