from django.conf import settings
from django.shortcuts import reverse

import pytest

from backend.alerts import choices as alerts_choices
from backend.alerts.models import ShipAlert, ShipWatchlist
from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


SENSITIVE_IMO = "12345"
SHIP_CALL_ID = "ABCD"


def test_alert_is_created_if_imo_in_watchlist(client):
    saint_malo = factories.PortFactory(locode="FRSML")
    user = utils.login_saint_malo_user(client)
    ship = ShipMovement.objects.create(
        way=ships_settings.ARRIVING,
        port_of_call="FRSML",
        imo_number=SENSITIVE_IMO,
        ship_call_id=SHIP_CALL_ID,
    )
    count_alerts_before = ShipAlert.objects.count()
    sensitive_imo_config = f"{SENSITIVE_IMO} 56789"
    watchlist = ShipWatchlist.objects.create(
        team=user.team, imo_list=sensitive_imo_config
    )
    ship.save()
    count_alerts_after = ShipAlert.objects.count()
    assert count_alerts_after == count_alerts_before + 1
    return (user, ship, saint_malo)


def test_alert_is_not_created_if_ship_port_of_call_is_not_in_team_ports(
    client,
):
    user = utils.login_saint_malo_user(client)
    sensitive_imo = "1234567"
    ShipWatchlist.objects.create(team=user.team, imo_list=sensitive_imo)
    count_alert_before = ShipAlert.objects.count()
    ShipMovement.objects.create(
        port_of_call="FRMRS",
        imo_number=sensitive_imo,
    )
    count_alert_after = ShipAlert.objects.count()
    assert count_alert_before == count_alert_after


def test_alert_is_listed_in_ship_movement_api(client):
    user, ship, saint_malo = test_alert_is_created_if_imo_in_watchlist(client)
    url = reverse("ships-list")
    response = client.get(url)
    assert SHIP_CALL_ID in str(response.content)
    assert SENSITIVE_IMO in str(response.content)
    assert alerts_choices.SHIP_WATCHLIST_ALERT in str(response.content)


def test_alert_is_not_listed_in_ship_movement_api_if_user_team_does_not_match(client):
    user, ship, saint_malo = test_alert_is_created_if_imo_in_watchlist(client)
    another_user = factories.UserFactory(email="test@test.com")
    # Let's suppose that for Saint Malo, we have a 2nd team
    team_saint_malo_2 = factories.TeamFactory(ports=[saint_malo])
    another_user.team = team_saint_malo_2
    another_user.save()
    utils.login(client, login_user=another_user)
    url = reverse("ships-list")
    response = client.get(url)
    assert SHIP_CALL_ID in str(response.content)
    assert SENSITIVE_IMO in str(response.content)
    # Event if there are alerts raised for this ship, we dont expect the 2nd team
    # to see the alert of the initial team
    assert ShipAlert.objects.filter(ship_movement=ship).exists()
    assert alerts_choices.SHIP_WATCHLIST_ALERT not in str(response.content)


def test_api_returns_imo_watchlist_if_exists_for_user_team(client):
    user, ship, saint_malo = test_alert_is_created_if_imo_in_watchlist(client)
    utils.login(client, login_user=user)
    url = reverse("alerts-watchlist")
    response = client.get(url)
    assert '"status":200' in str(response.content)
    assert '"watchlist":["12345","56789"]' in str(response.content)


def test_api_returns_empty_imo_watchlist_if_not_exists_for_user_team(client):
    another_user = factories.UserFactory(email="test@test.com")
    saint_malo = factories.PortFactory(locode="FRSML")
    another_team = factories.TeamFactory(ports=[saint_malo])
    another_user.team = another_team
    another_user.save()
    utils.login(client, login_user=another_user)
    url = reverse("alerts-watchlist")
    response = client.get(url)
    assert '"status":204' in str(response.content)
    assert '"watchlist":[]' in str(response.content)
