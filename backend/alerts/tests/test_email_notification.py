from django.conf import settings
from django.utils import timezone

import pytest

from backend.accounts.models import Team
from backend.alerts.models import ShipWatchlist
from backend.alerts.tests import settings as alerts_test_settings
from backend.logs.models import UserActionLog
from backend.ship_files.models import ShipFile as Fal
from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


############
#  Setup   #
############


SENSITIVE_IMO = "12345"
NOTIFICATION_EMAIL_LOG_NAME = "send alert notification"
SHIP_WATCHLIST_LOG_NAME = "alert ship watchlist"
EMAIL_NOTIFICATION = "email@notification.on"
TODAY = timezone.now()


def setup_setting():
    settings.EMAIL_NOTIFICATION_ENABLED = True


def setup_team(team):
    team.notification_email = EMAIL_NOTIFICATION
    team.save()


def setup_watchlist(team):
    sensitive_imo_config = f"{SENSITIVE_IMO} 56789"
    watchlist = ShipWatchlist.objects.create(team=team, imo_list=sensitive_imo_config)
    watchlist.save()
    return watchlist


def create_movement():
    tomorrow = TODAY + timezone.timedelta(days=1)
    ship = ShipMovement.objects.create(
        way=ships_settings.ARRIVING,
        port_of_call="FRSML",
        imo_number=SENSITIVE_IMO,
        eta_to_port_of_call=tomorrow,
    )
    return ship


################
# Test Setup   #
################


def test_email_notification_are_on():
    setup_setting()
    assert settings.EMAIL_NOTIFICATION_ENABLED == True


def test_team_has_email_notification_on(client):
    user = utils.login_saint_malo_user(client)
    team = user.team
    team_notification_before = team.notification_email
    setup_team(team)
    team_notification_after = team.notification_email
    assert team_notification_before == ""
    assert team_notification_before != team_notification_after
    assert team_notification_after == EMAIL_NOTIFICATION


def test_watchlist(client):
    user = utils.login_saint_malo_user(client)
    watchlist = setup_watchlist(user.team)
    assert watchlist.team == user.team
    assert SENSITIVE_IMO in watchlist.imo_list


def test_movement_is_future_and_arriving_and_in_team_port(client):
    user = utils.login_saint_malo_user(client)
    ship = create_movement()
    team_with_port_of_call = Team.objects.filter(
        name=user.team.name, ports__locode=ship.port_of_call
    )
    assert ship.relevant_time > TODAY
    assert ship.way == ships_settings.ARRIVING
    assert team_with_port_of_call.exists()


###########
# Tests   #
###########


def test_notification_email_log_is_created_with_fal1(
    client,
):
    user = utils.login_saint_malo_user(client)
    team = user.team
    setup_setting()
    setup_team(team)
    setup_watchlist(team)
    count_send_email_log_before = UserActionLog.objects.filter(
        action=NOTIFICATION_EMAIL_LOG_NAME, object="Ref FAL1-Test-Ship"
    ).count()
    fal = Fal(ship_file=alerts_test_settings.FAL1_WATCHLIST_PATH)
    fal.save()
    count_send_email_log_after = UserActionLog.objects.filter(
        action=NOTIFICATION_EMAIL_LOG_NAME,
        object="Ref FAL1-Test-Ship",
        target=team.name,
    ).count()
    assert count_send_email_log_before == 0
    assert count_send_email_log_after == 1


def test_notification_email_log_is_created_with_fal5(
    client,
):
    user = utils.login_saint_malo_user(client)
    team = user.team
    setup_setting()
    setup_team(team)
    setup_watchlist(team)
    count_send_email_log_before = UserActionLog.objects.filter(
        action=NOTIFICATION_EMAIL_LOG_NAME, object="Ref FAL5-Test-Ship"
    ).count()
    fal = Fal(ship_file=alerts_test_settings.FAL5_WATCHLIST_PATH)
    fal.save()
    count_send_email_log_after = UserActionLog.objects.filter(
        action=NOTIFICATION_EMAIL_LOG_NAME,
        object="Ref FAL5-Test-Ship",
        target=team.name,
    ).count()
    assert count_send_email_log_before == 0
    assert count_send_email_log_after == 1


def test_notification_email_log_is_created_with_fal6(
    client,
):
    user = utils.login_saint_malo_user(client)
    team = user.team
    setup_setting()
    setup_team(team)
    setup_watchlist(team)
    count_send_email_log_before = UserActionLog.objects.filter(
        action=NOTIFICATION_EMAIL_LOG_NAME, object="Ref FAL6-Test-Ship"
    ).count()
    fal = Fal(ship_file=alerts_test_settings.FAL6_WATCHLIST_PATH)
    fal.save()
    count_send_email_log_after = UserActionLog.objects.filter(
        action=NOTIFICATION_EMAIL_LOG_NAME,
        object="Ref FAL6-Test-Ship",
        target=team.name,
    ).count()
    assert count_send_email_log_before == 0
    assert count_send_email_log_after == 1


def test_multiple_notification_email_logs_are_created_if_multiple_teams_whatch_same_imo(
    client,
):
    setup_setting()
    user = utils.login_saint_malo_user(client)
    team = user.team
    setup_team(team)
    setup_watchlist(team)
    saint_malo = factories.PortFactory(locode="FRSML")
    second_team_saint_malo = factories.TeamFactory(ports=[saint_malo])
    setup_team(second_team_saint_malo)
    setup_watchlist(second_team_saint_malo)
    count_send_email_log_before = UserActionLog.objects.filter(
        action=NOTIFICATION_EMAIL_LOG_NAME, object="Ref FAL1-Test-Ship"
    ).count()
    fal = Fal(ship_file=alerts_test_settings.FAL1_WATCHLIST_PATH)
    fal.save()
    count_send_email_log_after = UserActionLog.objects.filter(
        action=NOTIFICATION_EMAIL_LOG_NAME, object="Ref FAL1-Test-Ship"
    ).count()
    assert count_send_email_log_before == 0
    assert count_send_email_log_after == 2
