from os import getcwd, path

FAL5_PATH = path.join(getcwd(), "backend/alerts/tests/data/NCA_FAL5.xml")
FAL6_PATH = path.join(getcwd(), "backend/alerts/tests/data/NCA_FAL6.xml")
FAL1_WATCHLIST_PATH = path.join(
    getcwd(), "backend/alerts/tests/data/NCA_FAL1-ShipWatchlist.xml"
)
FAL5_WATCHLIST_PATH = path.join(
    getcwd(), "backend/alerts/tests/data/NCA_FAL5-ShipWatchlist.xml"
)
FAL6_WATCHLIST_PATH = path.join(
    getcwd(), "backend/alerts/tests/data/NCA_FAL6-ShipWatchlist.xml"
)
