from django.conf import settings

import pytest

from backend.alerts.models import ShipAlert
from backend.ships.models import ShipMovement
from backend.unit_tests import utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_alert_is_created_if_ship_country_is_watch(client):
    utils.login_saint_malo_user(client)
    count_alerts_before = ShipAlert.objects.count()
    ShipMovement.objects.create(flag_state_of_ship="LOCODE")
    count_alerts_after = ShipAlert.objects.count()
    assert count_alerts_after == count_alerts_before + 1


def test_alert_is_not_created_if_ship_country_is_not_watch(client):
    utils.login_saint_malo_user(client)
    count_alerts_before = ShipAlert.objects.count()
    ShipMovement.objects.create(flag_state_of_ship="")
    ShipMovement.objects.create(flag_state_of_ship="FR")
    count_alerts_after = ShipAlert.objects.count()
    assert count_alerts_before == count_alerts_after
