from django.conf import settings
from django.shortcuts import reverse

import pytest

from backend.alerts import choices as alerts_choices
from backend.alerts.models import ShipAlert
from backend.fal.tests import settings as fal_settings
from backend.ship_files.models import ShipFile as Fal
from backend.ships.models import ShipMovement
from backend.unit_tests import utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_alert_is_created_when_new_fal5_is_received():
    first_fal = Fal(ship_file=fal_settings.FAL5_PATH)
    first_fal.save()
    count_ship_mouvement_before = ShipMovement.objects.count()
    count_alerts_before = ShipAlert.objects.count()
    second_fal = Fal(ship_file=fal_settings.FAL5_PATH)
    second_fal.save()
    count_ship_mouvement_after = ShipMovement.objects.count()
    count_alerts_after = ShipAlert.objects.count()
    assert count_alerts_after == count_alerts_before + 1
    assert count_ship_mouvement_before == count_ship_mouvement_after


def test_alert_is_created_when_new_fal6_is_received():
    first_fal = Fal(ship_file=fal_settings.FAL6_PATH)
    first_fal.save()
    count_ship_mouvement_before = ShipMovement.objects.count()
    count_alerts_before = ShipAlert.objects.count()
    second_fal = Fal(ship_file=fal_settings.FAL6_PATH)
    second_fal.save()
    count_ship_mouvement_after = ShipMovement.objects.count()
    count_alerts_after = ShipAlert.objects.count()
    assert count_alerts_after == count_alerts_before + 1
    assert count_ship_mouvement_before == count_ship_mouvement_after


def test_no_alert_created_if_already_existing():
    first_fal = Fal(ship_file=fal_settings.FAL5_PATH)
    first_fal.save()
    second_fal = Fal(ship_file=fal_settings.FAL5_PATH)
    second_fal.save()
    count_alerts_before = ShipAlert.objects.count()
    third_fal = Fal(ship_file=fal_settings.FAL5_PATH)
    third_fal.save()
    count_alerts_after = ShipAlert.objects.count()
    assert count_alerts_after == count_alerts_before


def test_alert_is_not_returning_if_team_marked_as_hide(client):
    # First FAL5 received
    first_fal = Fal(ship_file=fal_settings.FAL5_PATH)
    first_fal.save()
    # New FAL5 received to trigger alert creation
    new_fal = Fal(ship_file=fal_settings.FAL5_PATH)
    new_fal.save()
    ship_movement = ShipMovement.objects.all()[0]
    # User login
    user_a = utils.login_saint_malo_user(client)
    alerts_before = ShipAlert.objects.filter(
        ship_movement=ship_movement,
        alert_type__alert_type=alerts_choices.NEW_CREW_LIST_ALERT,
    ).exclude(hide_for_teams__name=user_a.team.name)
    number_alerts_before = alerts_before.count()
    # User hide notification for his team
    alert = alerts_before[0]
    url = reverse("alerts-stop-showing", args=[alert.pk])
    client.post(url)
    # Alert is not returning
    alerts_after = ShipAlert.objects.filter(
        ship_movement=ship_movement,
        alert_type__alert_type=alerts_choices.NEW_CREW_LIST_ALERT,
    ).exclude(hide_for_teams__name=user_a.team.name)
    number_alerts = alerts_after.count()
    assert number_alerts < number_alerts_before


def test_show_alert_if_new_list_recieved_after_hide(client):
    # First FAL5 received
    first_fal = Fal(ship_file=fal_settings.FAL5_PATH)
    first_fal.save()
    # New FAL5 received to trigger alert creation
    second_fal = Fal(ship_file=fal_settings.FAL5_PATH)
    second_fal.save()
    ship_movement = ShipMovement.objects.all()[0]
    # User hide notification for his team
    user_a = utils.login_saint_malo_user(client)
    alerts = ShipAlert.objects.filter(
        ship_movement=ship_movement,
        alert_type__alert_type=alerts_choices.NEW_CREW_LIST_ALERT,
    ).exclude(hide_for_teams__name=user_a.team.name)
    alert = alerts[0]
    url = reverse("alerts-stop-showing", args=[alert.pk])
    client.post(url)
    # Alert is not returning
    alerts_before = ShipAlert.objects.filter(
        ship_movement=ship_movement,
        alert_type__alert_type=alerts_choices.NEW_CREW_LIST_ALERT,
    ).exclude(hide_for_teams__name=user_a.team.name)
    number_alerts_before = alerts_before.count()
    # New FAL5 received to trigger alert creation
    third_fal = Fal(ship_file=fal_settings.FAL5_PATH)
    third_fal.save()
    # Alert is now returning
    alerts_after = ShipAlert.objects.filter(
        ship_movement=ship_movement,
        alert_type__alert_type=alerts_choices.NEW_CREW_LIST_ALERT,
    ).exclude(hide_for_teams__name=user_a.team.name)
    number_alerts_after = alerts_after.count()
    assert number_alerts_after > number_alerts_before


def test_alert_passengers_and_crew_list_are_separated_from_hide_option(client):
    # First FAL5 received
    first_fal = Fal(ship_file=fal_settings.FAL5_PATH)
    first_fal.save()
    # New FAL5 received to trigger alert creation
    second_fal = Fal(ship_file=fal_settings.FAL5_PATH)
    second_fal.save()
    # First FAL6 received
    first_fal = Fal(ship_file=fal_settings.FAL6_PATH)
    first_fal.save()
    # New FAL6 received to trigger alert creation
    second_fal = Fal(ship_file=fal_settings.FAL6_PATH)
    second_fal.save()
    ship_movement = ShipMovement.objects.all()[0]
    # Passenger list is returning
    user_a = utils.login_saint_malo_user(client)
    passenger_alerts_before = ShipAlert.objects.filter(
        ship_movement=ship_movement,
        alert_type__alert_type=alerts_choices.NEW_PASSENGER_LIST_ALERT,
    ).exclude(hide_for_teams__name=user_a.team.name)
    number_passenger_alerts_before = passenger_alerts_before.count()
    # User hide notification for first alert
    crew_alerts_before = ShipAlert.objects.filter(
        ship_movement=ship_movement,
        alert_type__alert_type=alerts_choices.NEW_CREW_LIST_ALERT,
    ).exclude(hide_for_teams__name=user_a.team.name)
    alert = crew_alerts_before[0]
    url = reverse("alerts-stop-showing", args=[alert.pk])
    client.post(url)
    # Passenger list is returning
    passenger_alerts_after = ShipAlert.objects.filter(
        ship_movement=ship_movement,
        alert_type__alert_type=alerts_choices.NEW_PASSENGER_LIST_ALERT,
    ).exclude(hide_for_teams__name=user_a.team.name)
    number_passenger_alerts_after = passenger_alerts_after.count()
    assert number_passenger_alerts_after == number_passenger_alerts_before
