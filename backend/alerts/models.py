from django.db import models

from model_utils.models import TimeStampedModel

from backend.alerts import choices as alerts_choices


class AlertType(models.Model):
    alert_type = models.CharField(
        "type d'alerte",
        max_length=256,
        choices=alerts_choices.ALERT_TYPE_CHOICES,
        unique=True,
    )

    class Meta:
        verbose_name = "type d'alerte"
        verbose_name_plural = "types d'alertes"

    def __str__(self):
        return self.get_alert_type_display()


class ShipAlert(TimeStampedModel):
    ship_movement = models.ForeignKey(
        "ships.ShipMovement",
        verbose_name="mouvement de navire",
        related_name="alerts",
        on_delete=models.CASCADE,
    )
    alert_type = models.ForeignKey(
        "AlertType",
        verbose_name="type d'alerte",
        related_name="alerts",
        on_delete=models.CASCADE,
    )
    team = models.ForeignKey(
        "accounts.Team",
        verbose_name="équipe",
        blank=True,
        null=True,
        help_text=("L'équipe à laquelle est rattachée cette alerte."),
        on_delete=models.CASCADE,
        related_name="alerts",
    )
    comment = models.CharField("commentaire", max_length=256, blank=True, null=True)
    hide_for_teams = models.ManyToManyField(
        "accounts.Team",
        verbose_name="Équipes à exclure",
        related_name="alerts_hide",
        help_text=("Équipes qui ne verront pas l'alerte."),
        blank=True,
    )

    class Meta:
        verbose_name = "alerte déclenchée"
        verbose_name_plural = "alertes déclenchées"

    def __str__(self):
        return (
            f"Alerte '{self.alert_type}' sur '{self.ship_movement} équipe {self.team}'"
        )

    @property
    def name(self):
        return self.alert_type.get_alert_type_display()

    @property
    def code(self):
        return self.alert_type.alert_type


class ShipWatchlist(models.Model):
    team = models.ForeignKey(
        "accounts.Team",
        verbose_name="équipe",
        help_text=("L'équipe à laquelle est rattachée cette alerte."),
        related_name="ship_watch_list",
        on_delete=models.CASCADE,
    )
    imo_list = models.TextField(
        "list IMO", help_text="Liste séparée par des espaces ou des retours à la ligne"
    )

    class Meta:
        verbose_name = "navires sensibles"
        verbose_name_plural = "navires sensibles"

    def __str__(self):
        return f"navires sensibles pour {self.team}"

    def get_imo_list(self):
        return self.imo_list.split()

    def get_ports_list(self):
        return self.team.ports.values_list("locode", flat=True)
