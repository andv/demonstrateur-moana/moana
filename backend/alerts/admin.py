from django.contrib import admin

from backend.alerts.models import AlertType, ShipAlert, ShipWatchlist


@admin.register(ShipAlert)
class ShipAlertAdmin(admin.ModelAdmin):
    list_display = [
        "ship_movement",
        "alert_type",
        "team",
        "comment",
        "created",
        "modified",
    ]
    raw_id_fields = ["ship_movement"]
    date_hierarchy = "created"
    search_fields = (
        "ship_movement__imo_number",
        "ship_movement__ship_name",
        "ship_movement__port_of_call",
        "ship_movement__ship_call_id",
    )
    fieldsets = (
        (
            "Informations :",
            {
                "fields": (
                    "ship_movement",
                    "alert_type",
                    "team",
                    "comment",
                    "hide_for_teams",
                )
            },
        ),
    )
    autocomplete_fields = ["hide_for_teams"]


@admin.register(ShipWatchlist)
class ShipWatchlistAdmin(admin.ModelAdmin):
    list_display_links = ("id", "team")
    list_display = [
        "id",
        "team",
    ]


@admin.register(AlertType)
class AlertTypeAdmin(admin.ModelAdmin):
    list_display_links = ("id", "alert_type")
    list_display = [
        "id",
        "alert_type",
    ]
