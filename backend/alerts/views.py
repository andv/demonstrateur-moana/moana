from rest_framework import decorators, status, viewsets
from rest_framework.response import Response

from backend.accounts.models import Team
from backend.alerts.models import ShipAlert, ShipWatchlist
from backend.logs.actions import ActionLogHandler


class AlertsViewSet(viewsets.ViewSet, ActionLogHandler):

    @decorators.action(detail=True, methods=["POST"], url_path="stop-showing")
    def stop_showing(self, request, pk=None):
        """
        Save team preference to not see alert anymore
        """
        alert = ShipAlert.objects.get(pk=pk)
        team = Team.objects.get(name=request.user.team)
        alert.hide_for_teams.add(team)
        self.create_user_action_log(
            action="hide alert",
            user=self.request.user,
            object=alert.name,
            target=alert,
            ship=alert.ship_movement,
        )
        response_data = {
            "alert": alert.id,
            "status": status.HTTP_204_NO_CONTENT,
        }
        return Response(response_data)

    @decorators.action(
        detail=False, methods=["GET"], url_path="watchlist", url_name="watchlist"
    )
    def get_watchlist(self, request, pk=None):
        """
        List imo of user team watchlist
        """
        imo_list = []
        has_watchlist = ShipWatchlist.objects.filter(
            team_id=request.user.team.id
        ).exists()
        if has_watchlist:
            watchlist = ShipWatchlist.objects.get(team_id=request.user.team.id)
            imo_list = watchlist.get_imo_list()
            response_data = {
                "status": status.HTTP_200_OK,
                "watchlist": imo_list,
            }
        else:
            response_data = {
                "status": status.HTTP_204_NO_CONTENT,
                "watchlist": imo_list,
            }
        return Response(response_data)
