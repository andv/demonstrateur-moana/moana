from rest_framework import serializers

from backend.alerts.models import ShipAlert


class ShipAlertSerializer(serializers.ModelSerializer):
    """
    This is a serializer class for the ShipAlert model.
    """

    class Meta:
        model = ShipAlert
        fields = ("code", "name", "id", "created", "modified")
