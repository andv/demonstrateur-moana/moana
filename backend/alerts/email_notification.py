from zoneinfo import ZoneInfo

from django.conf import settings
from django.template.loader import render_to_string
from django.utils.timezone import datetime

from sentry_sdk import capture_exception

from backend.alerts import choices as alerts_choices
from backend.ships import settings as ships_settings
from backend.tasks import tasks


class EmailNotificationHandler:
    alerts_created = []
    current_alert = None

    @property
    def ship_movement(self):
        return self.current_alert["ship_movement"]

    @property
    def alert_id(self):
        return self.current_alert["alert_type_id"]

    @property
    def team(self):
        return self.current_alert["team"]

    @property
    def alert_name(self):
        dict_movement_related_alerts = dict(alerts_choices.MOVEMENT_RELATED_ALERTS)
        alert_name = dict_movement_related_alerts[self.alert_id]
        return alert_name

    @property
    def relevant_time_in_team_timezone(self):
        ship_relevant_time = self.ship_movement.relevant_time
        team_timezone_name = ZoneInfo(self.team.timezone)
        return ship_relevant_time.astimezone(tz=team_timezone_name)

    def check_notification_are_enabled(self):
        return settings.EMAIL_NOTIFICATION_ENABLED

    def check_alert_is_for_watchlist(self):
        return self.alert_id == alerts_choices.SHIP_WATCHLIST_ALERT

    def check_movement_is_arriving(self):
        return self.ship_movement.way == ships_settings.ARRIVING

    def check_team_has_notification_on(self):
        return self.team.notification_email != ""

    def check_movement_is_in_future(self):
        now_timezone = datetime.now(ZoneInfo(self.team.timezone))
        if not self.ship_movement.relevant_time:
            capture_exception(
                f"EmailNotificationHandler : no relevant_time for ship {self.ship_movement.ship_call_id}"
            )
            return False
        else:
            return self.relevant_time_in_team_timezone > now_timezone

    def get_html_message(self):
        context = {
            "name": self.ship_movement.ship_name,
            "imo": self.ship_movement.imo_number,
            "date": self.relevant_time_in_team_timezone,
            "port": self.ship_movement.port_of_call_relation,
            "alert": self.alert_name,
            "url": settings.SITE_URL,
        }
        html_message = render_to_string("emails/alert_ship_movement.html", context)
        return html_message

    def send_alert_notification(self):
        html_message = self.get_html_message()
        tasks.task_send_alert_notification(
            self,
            html_message=html_message,
            team=self.team,
            ship=self.ship_movement,
            alert=self.alert_name,
        )

    def notify_by_email(self):
        if not self.check_notification_are_enabled():
            return
        for alert in self.alerts_created:
            # Save alert to use it in calculated properties
            self.current_alert = alert
            if not self.check_alert_is_for_watchlist():
                continue
            if not self.check_movement_is_arriving():
                continue
            if not self.check_team_has_notification_on():
                continue
            if not self.check_movement_is_in_future():
                continue
            self.send_alert_notification()
        self.reset_alerts()

    def save_alert_to_notify(self, ship_movement, team, alert_type_id):
        """
        We save alerts now and send notification once the related ship movement is fully saved
        to avoid missing field like relevant_time or port_of_call displayed in email.
        """
        self.alerts_created.append(
            {
                "alert_type_id": alert_type_id,
                "team": team,
                "ship_movement": ship_movement,
            }
        )

    def reset_alerts(self):
        self.alerts_created.clear()
        self.current_alert = None
