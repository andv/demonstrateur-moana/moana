from backend.alerts.email_notification import EmailNotificationHandler
from backend.alerts.models import AlertType, ShipAlert
from backend.logs.actions import ActionLogHandler


class AlertMixin(ActionLogHandler, EmailNotificationHandler):

    def log_alert(self, alert):
        action_name = alert.alert_type.alert_type.replace("-", " ")
        action_name = f"alert {action_name}"
        self.create_action_log(
            action=action_name,
            actor="moana-application",
            description=alert.alert_type.get_alert_type_display(),
            ship=alert.ship_movement,
            object=f"Ref {alert.ship_movement.ship_call_id}",
            target=alert.ship_movement,
        )
        self.alert_exist_already = True

    def create_alert(
        self,
        alert_type_id,
        ship_movement,
        team=None,
        reset_hide_for_teams=False,
    ):
        """
        Create an alert of the given type.
        """
        # If the AlertType entry is not already there, let's create it.
        alert_type, _ = AlertType.objects.get_or_create(alert_type=alert_type_id)
        alert, alert_was_created = ShipAlert.objects.get_or_create(
            ship_movement=ship_movement,
            alert_type=alert_type,
            team=team,
        )
        if alert and reset_hide_for_teams:
            alert.hide_for_teams.clear()
        if alert_was_created:
            self.log_alert(alert)
            self.save_alert_to_notify(ship_movement, team, alert_type_id)
