# Generated by Django 3.2.18 on 2023-03-06 13:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20221220_1007'),
        ('alerts', '0007_remove_shipwatchlist_port'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shipwatchlist',
            name='team',
            field=models.ForeignKey(help_text="L'équipe à laquelle est ratachée cette alerte.", on_delete=django.db.models.deletion.CASCADE, related_name='ship_watch_list', to='accounts.team', verbose_name='équipe'),
        ),
    ]
