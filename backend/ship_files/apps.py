from django.apps import AppConfig


class ShipFilesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.ship_files"
