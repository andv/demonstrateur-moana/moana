from os import path

from django.conf import settings

import magic


class ShipFileValidationMixin:
    """
    Handles ShipFile validations.
    """

    def file_extension_is_valid(self, ship_file):
        file_extension = path.splitext(ship_file.name)[1]
        if file_extension.lower() not in settings.SHIP_FILE_EXTENSION_WHITELIST:
            return False
        return True

    def is_mime_type_valid(self, mime_type):
        if mime_type.lower() not in settings.SHIP_FILE_MIME_TYPE_WHITELIST:
            return False
        return True

    def file_mime_type_is_valid(self, ship_file):
        mime_type = magic.from_buffer(ship_file.read(2048), mime=True)
        return self.is_mime_type_valid(mime_type)
