from django.http import HttpResponseBadRequest, HttpResponseForbidden

from rest_framework import status, viewsets
from rest_framework.response import Response
from sentry_sdk import capture_exception

from backend.logs.actions import ActionLogHandler
from backend.ship_files import settings as ship_files_settings
from backend.ship_files.models import ShipFile
from backend.ship_files.serializers import ShipFileUploadSerializer
from backend.ship_files.validations import ShipFileValidationMixin


class ShipFileUploadViewSet(
    ShipFileValidationMixin, viewsets.ViewSet, ActionLogHandler
):
    def log_action(self, uploaded_file, file_type, ship):
        action_name = ship_files_settings.FILE_UPLOAD_LOG_ACTIONS[file_type]
        self.create_user_action_log(
            action=action_name,
            user=self.request.user,
            object=f"Ref {ship.ship_call_id}",
            ship=ship,
        )

    def user_is_authorized(self, request):
        """
        Check if user is authorized to upload files.
        """
        return request.user.manual_upload_fal

    def create(self, request):
        if not self.user_is_authorized(request):
            return HttpResponseForbidden()
        serializer_class = ShipFileUploadSerializer(data=request.data)
        if "file" not in request.FILES or not serializer_class.is_valid():
            return HttpResponseBadRequest()
        else:
            uploaded_files = []
            for f in request.FILES.getlist("file"):
                if not self.file_extension_is_valid(f):
                    return HttpResponseForbidden("Extension de fichier non autorisée")
                if not self.file_mime_type_is_valid(f):
                    return HttpResponseForbidden("Type de fichier non autorisé")
                uploaded_files.append(f.name)
                ship_file = ShipFile()
                try:
                    save_model_instance_on_file_save = True
                    ship_file.ship_file.save(
                        f.name, f, save=save_model_instance_on_file_save
                    )
                except Exception as e:
                    capture_exception(e)
                    return HttpResponseBadRequest(f"Problème lié au fichier : {e}")
                self.log_action(
                    uploaded_file=f.name,
                    file_type=ship_file.file_type,
                    ship=ship_file.get_related_ship(),
                )
                f.close()
                if ship_file.ship_file:
                    ship_file.ship_file.delete(save=False)
                ship_file.delete()
            return Response(status=status.HTTP_201_CREATED)
