NCA_FAL1 = "nca_fal1"
NCA_FAL5 = "nca_fal5"
NCA_FAL6 = "nca_fal6"
NCA_PORT = "nca_port"

# For NCA file types, the dictionary key here is mapped to the type declaration inside the XML file.
SHIP_FILE_TYPES = {
    "NCA_FAL1": NCA_FAL1,
    "NCA_FAL5": NCA_FAL5,
    "NCA_FAL6": NCA_FAL6,
    "NCA_Port": NCA_PORT,
}


# Maps the upload log action name with the file type
FAL1_UPLOAD_LOG_ACTION = "uploaded fal 1"
FAL5_UPLOAD_LOG_ACTION = "uploaded fal 5"
FAL6_UPLOAD_LOG_ACTION = "uploaded fal 6"
FILE_UPLOAD_LOG_ACTIONS = {
    NCA_FAL1: FAL1_UPLOAD_LOG_ACTION,
    NCA_FAL5: FAL5_UPLOAD_LOG_ACTION,
    NCA_FAL6: FAL6_UPLOAD_LOG_ACTION,
}

# Maps the email import log action name with the file type
FAL1_EMAIL_IMPORT_LOG_ACTION = "email imported fal 1"
FAL5_EMAIL_IMPORT_LOG_ACTION = "email imported fal 5"
FAL6_EMAIL_IMPORT_LOG_ACTION = "email imported fal 6"
NCA_FAL_EMAIL_IMPORT_LOG_ACTION = "email imported nca port"
EMAIL_IMPORT_LOG_ACTIONS = {
    NCA_FAL1: FAL1_EMAIL_IMPORT_LOG_ACTION,
    NCA_FAL5: FAL5_EMAIL_IMPORT_LOG_ACTION,
    NCA_FAL6: FAL6_EMAIL_IMPORT_LOG_ACTION,
    NCA_PORT: NCA_FAL_EMAIL_IMPORT_LOG_ACTION,
}


# Maps the import command with the file type
FAL1_IMPORT_COMMAND = "import_fal1"
FAL5_IMPORT_COMMAND = "import_fal5"
FAL6_IMPORT_COMMAND = "import_fal6"
NCA_PORT_IMPORT_COMMAND = "import_nca_port"
FILE_IMPORT_COMMANDS = {
    NCA_FAL1: FAL1_IMPORT_COMMAND,
    NCA_FAL5: FAL5_IMPORT_COMMAND,
    NCA_FAL6: FAL6_IMPORT_COMMAND,
    NCA_PORT: NCA_PORT_IMPORT_COMMAND,
}

# Maps the parser with the file type
FAL1_PARSER = "backend.fal.parsers.fal1.Fal1Parser"
FAL5_PARSER = "backend.fal.parsers.fal5.Fal5Parser"
FAL6_PARSER = "backend.fal.parsers.fal6.Fal6Parser"
NCA_PORT_PARSER = "backend.nca_port.parsers.NcaPortParser"
FILE_PARSERS = {
    NCA_FAL1: FAL1_PARSER,
    NCA_FAL5: FAL5_PARSER,
    NCA_FAL6: FAL6_PARSER,
    NCA_PORT: NCA_PORT_PARSER,
}
