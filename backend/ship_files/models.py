from django.core.management import call_command
from django.db import models
from django.utils.module_loading import import_string

from model_utils.models import TimeStampedModel

from backend.nca.parsers import NcaParser
from backend.ship_files import settings as ship_files_settings
from backend.ships.models import ShipMovement


class ShipFile(TimeStampedModel):
    ship_file = models.FileField(upload_to="ship-files")
    file_type = models.CharField(max_length=255, blank=True, null=True)

    parser = None

    class Meta:
        verbose_name = "Fichier"
        verbose_name_plural = "Fichiers"

    def __str__(self):
        return f"Fichier #{self.id}"

    def get_absolute_url(self):
        return self.ship_file.url

    def get_related_ship(self):
        if not self.parser:
            return None
        parsed_data = self.parser.get_parsed_data()
        if "ship_call_id" not in parsed_data or "way" not in parsed_data:
            return None
        ship = ShipMovement.objects.filter(
            ship_call_id=parsed_data["ship_call_id"],
            way=parsed_data["way"],
        ).first()
        return ship

    def set_parser(self, file_type):
        self.file_type = file_type
        parser_dot_path = ship_files_settings.FILE_PARSERS[file_type]
        parser_class = import_string(parser_dot_path)
        self.parser = parser_class(self.ship_file.path)
        return parser_class

    def call_import_command(self):
        command_name = ship_files_settings.FILE_IMPORT_COMMANDS[self.file_type]
        call_command(command_name, self.ship_file.path)

    def save(self, *args, **kwargs):
        file_type = NcaParser(self.ship_file.path).file_type
        self.set_parser(file_type)
        super().save(*args, **kwargs)
        self.call_import_command()
