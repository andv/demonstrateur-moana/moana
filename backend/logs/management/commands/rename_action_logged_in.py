from django.core.management.base import BaseCommand

from backend.logs.models import UserActionLog


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("Start rename action logged in in logs.")
        UserActionLog.objects.filter(action="logged in").update(
            action="logged in email"
        )
        self.stdout.write("End rename action logged in in logs.")
