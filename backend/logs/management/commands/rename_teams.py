from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand

from backend.logs.models import UserActionLog

RENAME_MAP_CONFIG = {
    "BASTIA": "PAF Bastia",
    "Bordeaux": "BSE Bordeaux",
    "BSE douanes Marseille": "BSE Marseille",
    "Cherbourg": "PAF Cherbourg",
    "Douanes Bordeaux": "BSE Bordeaux",
    "Douanes Fos sur mer": "BSE Fos sur mer",
    "Douanes Nantes": "BSE Nantes",
    "Douanes Toulon": "BSE Toulon",
    "Fos sur mer": "BSE Fos sur mer",
    "Le Havre": "PAF Le Havre",
    "Marseille": "PAF Marseille",
    "Nantes": "BSE Nantes",
    "Port-la-Nouvelle": "PAF Port-la-Nouvelle",
    "Rouen": "BSE Rouen - Honfleur",
    "Saint Malo": "PAF Saint Malo",
    "SETE": "PAF Sete",
    "Toulon": "BSE Toulon",
    "BSE BREST": "BSE Brest",
    "DNGCD": "CODM Meditérrannée",
    "DNGCD Méditerranée": "CODM Meditérrannée",
    "FOS MARSEILLE": "MOANA",
}


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("Start rename teams in logs.")
        for old_name, new_name in RENAME_MAP_CONFIG.items():
            count = UserActionLog.objects.filter(team=old_name).update(team=new_name)
            self.stdout.write(f"Renamed {old_name} -> {new_name} : {count} changes")
        self.stdout.write("End rename teams in logs.")
