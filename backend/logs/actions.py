from trackman.tracking import TrackingHandler

from backend.logs.models import DataQualityShipData, ShipData
from backend.nationalities.nationality_counters import NationalityCounterHelper


class ActionLogHandler(NationalityCounterHelper, TrackingHandler):
    def create_user_action_log(
        self, action, user, object="", target="", description="", data="", ship=None
    ):
        """
        Log an action for the given site user - with his team information.
        """
        action_details = {
            "actor": user.email,
            "team": user.team or "",
            "action": action,
            "object": object,
            "target": target,
            "description": description,
            "data": data,
        }
        action_log = self.track_action(action_details)
        self.add_ship_data_to_action_log(action_log, ship)
        return action_log

    def create_action_log(
        self,
        action,
        actor="",
        object="",
        target="",
        description="",
        data="",
        ship=None,
    ):
        """
        Log a standard action where the actor is not required
        """
        action_details = {
            "action": action,
            "actor": actor,
            "object": object,
            "target": target,
            "description": description,
            "data": data,
        }
        action_log = self.track_action(action_details)
        self.add_ship_data_to_action_log(action_log, ship)
        return action_log

    def add_ship_data_to_action_log(self, action_log, ship):
        """
        This helper will add the ship data log to the given action log instance.
        `ship` is expected to be an instance of a ShipMovement model.
        """
        if not action_log:
            return None
        if not ship:
            return action_log
        ship_type = ship.ship_type
        ship_data_details = {
            "action_log": action_log,
            "imo_number": ship.imo_number,
            "ship_call_id": ship.ship_call_id,
            "port_of_call": ship.port_of_call,
            "way": ship.way,
            "ship_name": ship.ship_name,
            "call_sign": ship.call_sign,
            "flag_state_of_ship": ship.flag_state_of_ship,
            "last_port": ship.last_port,
            "next_port": ship.next_port,
            "eta_to_port_of_call": ship.eta_to_port_of_call,
            "etd_from_port_of_call": ship.etd_from_port_of_call,
            "ata_to_port_of_call": ship.ata_to_port_of_call,
            "atd_from_port_of_call": ship.atd_from_port_of_call,
            "number_of_crew_parsed": ship.number_of_crew_parsed,
            "number_of_passengers_parsed": ship.number_of_passengers_parsed,
            "ship_type_reference": ship_type.name_reference if ship_type else "",
            "ship_type_fr": ship_type.name_fr if ship_type else "",
            "ship_type_simple": ship_type.name_simple if ship_type else "",
            "agent_name": ship.agent_name,
            "agent_phone": ship.agent_phone,
            "agent_email": ship.agent_email,
            "position_in_port_of_call": ship.position_in_port_of_call,
            "sip_fal5_available": ship.sip_fal5_available,
            "sip_fal6_available": ship.sip_fal6_available,
            "gross_tonnage": ship.gross_tonnage,
            "crew_list_delay_type": ship.crew_list_delay_type,
            "passenger_list_delay_type": ship.passenger_list_delay_type,
            "travel_duration": ship.travel_duration,
            "h24_before_arrival": ship.h24_before_arrival,
        }
        self.add_nationality_counters_to_ship_data(ship, ship_data_details)
        ship_data = ShipData.objects.create(**ship_data_details)
        return ship_data

    def add_nationality_counters_to_ship_data(self, ship, ship_data_details):
        """
        Updates the given ship data dictionary with nationality counters.
        It's the sum of both lists passengers and crew.
        """
        counter_data = self.get_nationality_counters_from_ship_movement(ship)
        ship_data_details.update(counter_data)
        return ship_data_details


class DataQualityLogLogHandler(TrackingHandler):
    def create_data_quality_log(
        self,
        action,
        actor="",
        object="",
        target="",
        description="",
        data="",
        ship=None,
    ):
        """
        Log a standard action where the actor is not required
        """
        action_details = {
            "action": action,
            "actor": actor,
            "object": object,
            "target": target,
            "description": description,
            "data": data,
        }
        action_log = self.track_action(
            action_details=action_details, model_alias="data-quality"
        )
        self.add_ship_data_to_data_quality_log(action_log, ship)
        return action_log

    def add_ship_data_to_data_quality_log(self, action_log, ship):
        """
        This helper will add the ship data log to the given data quality action log instance.
        `ship` is expected to be a dictionary, similar to what is sometimes called `fal_data`.
        """
        if not action_log:
            return None
        if not ship:
            return action_log
        ship_data_details = {
            "action_log": action_log,
            "imo_number": ship["imo_number"],
            "ship_call_id": ship["ship_call_id"],
            "port_of_call": ship.get("port_of_call", ""),
            "way": ship["way"],
            "ship_name": ship.get("ship_name", ""),
            "call_sign": ship.get("call_sign", ""),
            "flag_state_of_ship": ship.get("flag_state_of_ship", ""),
            "last_port": ship.get("last_port", ""),
            "next_port": ship.get("next_port", ""),
            "eta_to_port_of_call": ship.get("eta_to_port_of_call"),
            "etd_from_port_of_call": ship.get("etd_from_port_of_call"),
            "agent_name": ship.get("agent_name", ""),
        }
        ship_data = DataQualityShipData.objects.create(**ship_data_details)
        return ship_data
