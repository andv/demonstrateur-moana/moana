from django.conf import settings
from django.contrib import admin

from trackman.admin import MODEL_ADMIN_SEARCH_FIELDS, TrackingModelAdminMixin

from backend.geo.admin import GeoModelAdmin
from backend.logs.models import (
    DataQualityLog,
    DataQualityShipData,
    NcaPortShipData,
    PersonListAnalyze,
    ShipData,
    UserActionLog,
)


class ShipDataInline(GeoModelAdmin, admin.TabularInline):
    model = ShipData


class DataQualityShipDataInline(GeoModelAdmin, admin.TabularInline):
    model = DataQualityShipData


ACTION_LOG_SEARCH_FIELDS = MODEL_ADMIN_SEARCH_FIELDS + [
    "ship_data__imo_number",
    "ship_data__ship_call_id",
    "ship_data__port_of_call",
    "ship_data__way",
    "ship_data__ship_name",
    "ship_data__call_sign",
    "ship_data__last_port",
    "ship_data__next_port",
    "ship_data__ship_type_reference",
    "ship_data__ship_type_fr",
    "ship_data__ship_type_simple",
]


class UserActionLogAdmin(TrackingModelAdminMixin, admin.ModelAdmin):
    list_display = [
        "id",
        "user",
        "team",
        "action",
        "object",
        "target",
        "description",
        "created",
    ]
    search_fields = ["user", "team"] + ACTION_LOG_SEARCH_FIELDS
    list_filter = ["team", "action"]
    inlines = [ShipDataInline]


class DataQualityLogAdmin(TrackingModelAdminMixin, admin.ModelAdmin):
    list_display = ["id", "actor", "action", "object", "target", "created"]
    search_fields = ["actor"] + ACTION_LOG_SEARCH_FIELDS
    list_filter = ["action"]
    inlines = [DataQualityShipDataInline]


SHIP_DATA_SEARCH_FIELDS = [
    "imo_number",
    "ship_call_id",
    "port_of_call",
    "way",
    "ship_name",
    "call_sign",
    "last_port",
    "next_port",
    "ship_type_reference",
    "ship_type_fr",
    "ship_type_simple",
]


class ShipDataAdmin(TrackingModelAdminMixin, admin.ModelAdmin):
    list_display = [
        "id",
        "imo_number",
        "ship_call_id",
        "port_of_call",
        "way",
        "ship_name",
        "last_port",
        "next_port",
        "relevant_port",
        "eta_to_port_of_call",
        "etd_from_port_of_call",
        "ship_type_reference",
        "position_in_port_of_call",
    ]
    search_fields = SHIP_DATA_SEARCH_FIELDS + [
        "action_log__user",
        "action_log__team",
        "action_log__action",
        "action_log__object",
        "action_log__target",
        "action_log__description",
        "action_log__data",
    ]


class NcaPortShipDataAdmin(GeoModelAdmin, ShipDataAdmin):
    list_display = ShipDataAdmin.list_display + ["created"]
    search_fields = SHIP_DATA_SEARCH_FIELDS


class DataQualityShipDataAdmin(ShipDataAdmin):
    search_fields = SHIP_DATA_SEARCH_FIELDS + [
        "action_log__action",
        "action_log__object",
        "action_log__target",
        "action_log__description",
        "action_log__data",
    ]


class PersonListAnalyzeAdmin(ShipDataAdmin):
    list_display = [
        "analysis_period",
        "team",
        "ship_call_id",
        "way",
        "person_list_type",
        "list_analyzed",
        "date_imported",
        "date_analyzed",
        "imo_number",
        "port_of_call",
        "ship_name",
        "last_port",
        "next_port",
        "eta_to_port_of_call",
        "etd_from_port_of_call",
        "ship_type_reference",
        "position_in_port_of_call",
        "deleted",
    ]
    search_fields = SHIP_DATA_SEARCH_FIELDS + ["analysis_period"]
    list_filter = [
        "analysis_period",
        "deleted",
        "person_list_type",
        "list_analyzed",
        "team",
    ]
    list_display_links = ["analysis_period", "team"]


if settings.TRACKMAN_ENABLED:
    admin.site.register(UserActionLog, UserActionLogAdmin)
    admin.site.register(DataQualityLog, DataQualityLogAdmin)
    admin.site.register(DataQualityShipData, DataQualityShipDataAdmin)
    admin.site.register(ShipData, ShipDataAdmin)
    admin.site.register(NcaPortShipData, NcaPortShipDataAdmin)
    admin.site.register(PersonListAnalyze, PersonListAnalyzeAdmin)
