from rest_framework import viewsets
from trackman.api import TrackingViewSetMixin


class ActionTrackingViewSet(TrackingViewSetMixin, viewsets.ViewSet):
    model_alias = "default"

    def clean_action_details(self, action_details):
        """
        This will get actor and team from logged in user and replace it
        in the given action_details.
        """
        cleaned_data = action_details.copy()
        if hasattr(self.request.user, "team"):
            cleaned_data["team"] = self.request.user.team.name
        cleaned_data["actor"] = self.request.user.email
        return cleaned_data
