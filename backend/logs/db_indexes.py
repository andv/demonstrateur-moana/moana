SHIP_DATA_INDEX_FIELDS = [
    "port_of_call",
    "way",
    "eta_to_port_of_call",
    "etd_from_port_of_call",
    "ata_to_port_of_call",
    "atd_from_port_of_call",
    "ship_type_simple",
    "geographic_area",
    "ship_call_id",
]

PERSON_LIST_ANALYZE_INDEX_FIELDS = [
    "analysis_period",
    "team",
    "person_list_type",
    "list_analyzed",
    "date_imported",
    "-date_imported",
    "date_analyzed",
    "-date_analyzed",
    "deleted",
]
