# Generated by Django 4.2.16 on 2024-11-07 17:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("logs", "0038_alter_shipdata_number_of_crew_parsed_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="dataqualityshipdata",
            name="arrival_time",
            field=models.DateTimeField(
                blank=True,
                help_text="Le champ 'arrival_time' correspond à l'ATA si disponible, sinon à l'ETA",
                null=True,
                verbose_name="arrival time",
            ),
        ),
        migrations.AlterField(
            model_name="dataqualityshipdata",
            name="departure_time",
            field=models.DateTimeField(
                blank=True,
                help_text="Le champ 'departure_time' correspond à l'ATD si disponible, sinon à l'ETD",
                null=True,
                verbose_name="departure time",
            ),
        ),
        migrations.AlterField(
            model_name="dataqualityshipdata",
            name="relevant_port",
            field=models.CharField(
                blank=True,
                help_text="Correspond au dernier port pour un mouvement en arrivée, ou au prochain port pour un mouvement au départ",
                max_length=256,
                verbose_name="relevant port",
            ),
        ),
        migrations.AlterField(
            model_name="dataqualityshipdata",
            name="relevant_time",
            field=models.DateTimeField(
                blank=True,
                help_text="Le champ 'relevant_time' correspond soit au champ 'arrival_time', soit au champ 'departure time'",
                null=True,
                verbose_name="relevant time",
            ),
        ),
        migrations.AlterField(
            model_name="ncaportshipdata",
            name="arrival_time",
            field=models.DateTimeField(
                blank=True,
                help_text="Le champ 'arrival_time' correspond à l'ATA si disponible, sinon à l'ETA",
                null=True,
                verbose_name="arrival time",
            ),
        ),
        migrations.AlterField(
            model_name="ncaportshipdata",
            name="departure_time",
            field=models.DateTimeField(
                blank=True,
                help_text="Le champ 'departure_time' correspond à l'ATD si disponible, sinon à l'ETD",
                null=True,
                verbose_name="departure time",
            ),
        ),
        migrations.AlterField(
            model_name="ncaportshipdata",
            name="relevant_port",
            field=models.CharField(
                blank=True,
                help_text="Correspond au dernier port pour un mouvement en arrivée, ou au prochain port pour un mouvement au départ",
                max_length=256,
                verbose_name="relevant port",
            ),
        ),
        migrations.AlterField(
            model_name="ncaportshipdata",
            name="relevant_time",
            field=models.DateTimeField(
                blank=True,
                help_text="Le champ 'relevant_time' correspond soit au champ 'arrival_time', soit au champ 'departure time'",
                null=True,
                verbose_name="relevant time",
            ),
        ),
        migrations.AlterField(
            model_name="personlistanalyze",
            name="arrival_time",
            field=models.DateTimeField(
                blank=True,
                help_text="Le champ 'arrival_time' correspond à l'ATA si disponible, sinon à l'ETA",
                null=True,
                verbose_name="arrival time",
            ),
        ),
        migrations.AlterField(
            model_name="personlistanalyze",
            name="departure_time",
            field=models.DateTimeField(
                blank=True,
                help_text="Le champ 'departure_time' correspond à l'ATD si disponible, sinon à l'ETD",
                null=True,
                verbose_name="departure time",
            ),
        ),
        migrations.AlterField(
            model_name="personlistanalyze",
            name="relevant_port",
            field=models.CharField(
                blank=True,
                help_text="Correspond au dernier port pour un mouvement en arrivée, ou au prochain port pour un mouvement au départ",
                max_length=256,
                verbose_name="relevant port",
            ),
        ),
        migrations.AlterField(
            model_name="personlistanalyze",
            name="relevant_time",
            field=models.DateTimeField(
                blank=True,
                help_text="Le champ 'relevant_time' correspond soit au champ 'arrival_time', soit au champ 'departure time'",
                null=True,
                verbose_name="relevant time",
            ),
        ),
        migrations.AlterField(
            model_name="shipdata",
            name="arrival_time",
            field=models.DateTimeField(
                blank=True,
                help_text="Le champ 'arrival_time' correspond à l'ATA si disponible, sinon à l'ETA",
                null=True,
                verbose_name="arrival time",
            ),
        ),
        migrations.AlterField(
            model_name="shipdata",
            name="departure_time",
            field=models.DateTimeField(
                blank=True,
                help_text="Le champ 'departure_time' correspond à l'ATD si disponible, sinon à l'ETD",
                null=True,
                verbose_name="departure time",
            ),
        ),
        migrations.AlterField(
            model_name="shipdata",
            name="relevant_port",
            field=models.CharField(
                blank=True,
                help_text="Correspond au dernier port pour un mouvement en arrivée, ou au prochain port pour un mouvement au départ",
                max_length=256,
                verbose_name="relevant port",
            ),
        ),
        migrations.AlterField(
            model_name="shipdata",
            name="relevant_time",
            field=models.DateTimeField(
                blank=True,
                help_text="Le champ 'relevant_time' correspond soit au champ 'arrival_time', soit au champ 'departure time'",
                null=True,
                verbose_name="relevant time",
            ),
        ),
    ]
