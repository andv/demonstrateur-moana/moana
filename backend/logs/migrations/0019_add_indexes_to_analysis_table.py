# Generated by Django 3.2.21 on 2023-11-02 13:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('logs', '0018_personlistanalyze'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='personlistanalyze',
            index=models.Index(fields=['team'], name='i1_team_index'),
        ),
        migrations.AddIndex(
            model_name='personlistanalyze',
            index=models.Index(fields=['person_list_type'], name='i1_person_list_type_index'),
        ),
        migrations.AddIndex(
            model_name='personlistanalyze',
            index=models.Index(fields=['list_analyzed'], name='i1_list_analyzed_index'),
        ),
        migrations.AddIndex(
            model_name='personlistanalyze',
            index=models.Index(fields=['date_imported'], name='i1_date_imported_index'),
        ),
        migrations.AddIndex(
            model_name='personlistanalyze',
            index=models.Index(fields=['-date_imported'], name='i1_desc_date_imported_index'),
        ),
        migrations.AddIndex(
            model_name='personlistanalyze',
            index=models.Index(fields=['date_analyzed'], name='i1_date_analyzed_index'),
        ),
        migrations.AddIndex(
            model_name='personlistanalyze',
            index=models.Index(fields=['-date_analyzed'], name='i1_desc_date_analyzed_index'),
        ),
        migrations.AddIndex(
            model_name='personlistanalyze',
            index=models.Index(fields=['port_of_call'], name='i1_port_of_call_index'),
        ),
        migrations.AddIndex(
            model_name='personlistanalyze',
            index=models.Index(fields=['way'], name='i1_way_index'),
        ),
        migrations.AddIndex(
            model_name='personlistanalyze',
            index=models.Index(fields=['eta_to_port_of_call'], name='i1_eta_to_port_of_call_index'),
        ),
        migrations.AddIndex(
            model_name='personlistanalyze',
            index=models.Index(fields=['etd_from_port_of_call'], name='i1_etd_from_port_of_call_index'),
        ),
        migrations.AddIndex(
            model_name='personlistanalyze',
            index=models.Index(fields=['ata_to_port_of_call'], name='i1_ata_to_port_of_call_index'),
        ),
        migrations.AddIndex(
            model_name='personlistanalyze',
            index=models.Index(fields=['atd_from_port_of_call'], name='i1_atd_from_port_of_call_index'),
        ),
        migrations.AddIndex(
            model_name='personlistanalyze',
            index=models.Index(fields=['ship_type_simple'], name='i1_ship_type_simple_index'),
        ),
        migrations.AddIndex(
            model_name='personlistanalyze',
            index=models.Index(fields=['geographic_area'], name='i1_geographic_area_index'),
        ),
        migrations.AddIndex(
            model_name='personlistanalyze',
            index=models.Index(fields=['ship_call_id'], name='i1_ship_call_id_index'),
        ),
    ]
