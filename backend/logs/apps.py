from django.apps import AppConfig


class LogsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.logs"
    verbose_name = "2.1 Traitement des données - Données et traçabilité"
