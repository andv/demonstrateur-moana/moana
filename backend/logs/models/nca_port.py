from model_utils.models import TimeStampedModel

from backend.ship_types.ship_relation import WithShipTypeInfo
from backend.ships.base_models import ShipMovementBaseModel


class NcaPortShipData(ShipMovementBaseModel, WithShipTypeInfo, TimeStampedModel):
    """
    Ship data from NCA Port files.
    """

    class Meta:
        verbose_name = "données navire NCA Port"
        verbose_name_plural = "données navire NCA Port"
