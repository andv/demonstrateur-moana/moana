from django.db import models

from trackman.models import TrackingActionModel

from backend.ship_types.ship_relation import WithShipTypeInfo
from backend.ships.base_models import ShipMovementBaseModel


class DataQualityLog(TrackingActionModel):
    """
    A log entry related to data quality.
    """

    class Meta:
        verbose_name = "log qualité des données"
        verbose_name_plural = "logs qualité des données"

    def __str__(self):
        return f"{self.actor} {self.action} {self.created}"


class DataQualityShipData(ShipMovementBaseModel, WithShipTypeInfo):
    """
    Ship data associated to a data quality log event.
    """

    action_log = models.OneToOneField(
        to="DataQualityLog",
        verbose_name="log qualité de données",
        related_name="ship_data",
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = "données navire pour la qualité de données"
        verbose_name_plural = "données navire pour la qualité de données"
        indexes = ShipMovementBaseModel._meta.indexes
