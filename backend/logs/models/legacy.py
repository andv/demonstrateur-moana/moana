from data_processing.person_list.models import PersonListAnalyzeBaseModel


class PersonListAnalyze(PersonListAnalyzeBaseModel):
    """
    This model is used to store the results of the person list analysis.
    It's placed here for legacy reasons, the code lives in the person list app.
    """

    pass
