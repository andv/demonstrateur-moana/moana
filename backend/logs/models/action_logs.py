from django.db import models

from trackman.models import TrackingActionModel

from backend.logs import db_indexes as logs_db_indexes
from backend.nationalities.nationality_counters import WithNationalityCounters
from backend.persons.person_counters import WithParsedPersonCounters
from backend.ship_types.ship_relation import WithShipTypeInfo
from backend.ships.base_models import ShipMovementBaseModel
from backend.utils.db_indexes import generate_indexes


class UserActionLog(TrackingActionModel):
    user = models.CharField("utilisateur", max_length=256, blank=True)
    team = models.CharField("équipe", max_length=256, blank=True)

    class Meta:
        verbose_name = "log d'action"
        verbose_name_plural = "logs d'actions"
        indexes = [
            models.Index(fields=["team"], name="team_index"),
            models.Index(fields=["action"], name="action_index"),
            models.Index(fields=["created"], name="created_index"),
            models.Index(fields=["-created"], name="created_desc_index"),
        ]

    def __str__(self):
        return f"{self.user} {self.action} {self.created}"

    def save(self, *args, **kwargs):
        self.user = self.actor
        return super().save(*args, **kwargs)


class ShipData(
    ShipMovementBaseModel,
    WithShipTypeInfo,
    WithNationalityCounters,
    WithParsedPersonCounters,
):
    """
    Ship data associated to an action log event.
    """

    action_log = models.OneToOneField(
        to="UserActionLog",
        verbose_name="Log action utilisateur",
        related_name="ship_data",
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = "données navire des logs"
        verbose_name_plural = "données navire des logs"
        _ship_data_indexes = generate_indexes(
            field_names=logs_db_indexes.SHIP_DATA_INDEX_FIELDS, prefix=""
        )
        indexes = _ship_data_indexes + [
            models.Index(fields=["action_log"], name="action_log_index"),
        ]
