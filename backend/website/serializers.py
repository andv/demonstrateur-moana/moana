from rest_framework import serializers

from backend.website.models import Pages


class PagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pages
        fields = [
            "page",
            "title",
            "content",
        ]
