from django.db import models

from tinymce.models import HTMLField

from backend.website import choices as website_choices


# Create your models here.
class Pages(models.Model):
    page = models.CharField(
        "page", max_length=256, choices=website_choices.PAGES_CHOICES, unique=True
    )
    title = models.CharField("titre", max_length=256, blank=True)
    content = HTMLField()

    class Meta:
        verbose_name = "Page du site"
        verbose_name_plural = "Pages du site"
