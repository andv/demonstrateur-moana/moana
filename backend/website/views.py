from rest_framework import viewsets

from backend.website.models import Pages
from backend.website.serializers import PagesSerializer


class WebsiteViewSet(viewsets.ReadOnlyModelViewSet):
    """
    View website pages content
    """

    serializer_class = PagesSerializer
    queryset = Pages.objects.all()
    lookup_field = "page"
