from django.apps import AppConfig


class WebsiteConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.website"
    verbose_name = "6.1 Site internet - pages"
