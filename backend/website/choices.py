PAGE_LEGALS = 'mentions-legales'
PAGE_TOS = 'conditions-generales-utilisation'
PAGE_A11Y = 'accessibilite'
PAGE_VERSIONS = 'version'

PAGES_CHOICES = (
    (PAGE_LEGALS, "Mentions Légales"),
    (PAGE_TOS, "Conditions Générales d'utilisation"),
    (PAGE_A11Y, "Accessibilité"),
    (PAGE_VERSIONS, "Versions"),
)
