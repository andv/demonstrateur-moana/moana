from django.db import models


class ShipMovementQueryset(models.QuerySet):
    def display(self):
        return self.filter(display=True)


ShipMovementManager = models.Manager.from_queryset(ShipMovementQueryset)
