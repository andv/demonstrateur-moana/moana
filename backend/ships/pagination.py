from rest_framework.pagination import PageNumberPagination

from backend.ships import settings as ships_settings


class ShipMovementPagination(PageNumberPagination):
    page_size = ships_settings.SHIPS_API_PAGE_SIZE
    max_page_size = ships_settings.SHIPS_API_MAX_PAGE_SIZE

    def get_paginated_response(self, data):
        response = super().get_paginated_response(data)
        response.data["count_pages"] = self.page.paginator.num_pages
        response.data.move_to_end("count_pages", last=False)
        # Retrieves the alerts counts that was set at the view level.
        response.data["count_ships_alerts"] = self.count_ships_alerts
        response.data.move_to_end("count_ships_alerts", last=False)
        return response
