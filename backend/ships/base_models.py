from django.db import models

from backend.geo.ship_relation import GeoOnShip
from backend.mmsi.ship_relation import MmsiOnShip
from backend.persons import choices as persons_choices
from backend.ports.ship_relation import PortOnShip
from backend.ships import settings as ships_settings
from backend.travel_duration.ship_relation import WithTravelDuration


class ShipBaseModel(models.Model):
    imo_number = models.CharField("IMO number", max_length=256, blank=True)
    ship_call_id = models.CharField("ship call id", max_length=256, blank=True)
    way = models.CharField("way", max_length=256, blank=True)
    ship_name = models.CharField("ship name", max_length=256, blank=True)
    call_sign = models.CharField("call sign", max_length=256, blank=True)
    flag_state_of_ship = models.CharField("state", max_length=256, blank=True)
    eta_to_port_of_call = models.DateTimeField("ETA", blank=True, null=True)
    etd_from_port_of_call = models.DateTimeField("ETD", blank=True, null=True)
    ata_to_port_of_call = models.DateTimeField("ATA", blank=True, null=True)
    atd_from_port_of_call = models.DateTimeField("ATD", blank=True, null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return f"Navire {self.ship_name} {self.imo_number} - {self.way}"

    @property
    def is_arriving(self):
        return self.way == ships_settings.ARRIVING

    @property
    def is_departing(self):
        return self.way == ships_settings.DEPARTING


class ShipMovementBaseModel(
    PortOnShip, GeoOnShip, MmsiOnShip, WithTravelDuration, ShipBaseModel
):
    agent_name = models.CharField("agent name", max_length=256, blank=True)
    agent_phone = models.CharField("agent phone", max_length=256, blank=True)
    agent_email = models.CharField("agent email", max_length=256, blank=True)
    agent_fax = models.CharField("agent fax", max_length=256, blank=True)
    name_of_master = models.CharField("name of master", max_length=256, blank=True)
    registry_location_name = models.CharField(
        "registry location name", max_length=256, blank=True
    )
    registry_location_code = models.CharField(
        "registry location code", max_length=256, blank=True
    )
    brief_particulars_of_voyage = models.CharField(
        "brief particulars of voyage", max_length=256, blank=True
    )
    position_in_port_of_call = models.CharField(
        "position in port of call", max_length=256, blank=True
    )
    gross_tonnage = models.FloatField("gross tonnage", blank=True, null=True)
    net_tonnage = models.FloatField("net tonnage", blank=True, null=True)
    arrival_time = models.DateTimeField(
        "arrival time",
        blank=True,
        null=True,
        help_text="Le champ 'arrival_time' correspond à l'ATA si disponible, sinon à l'ETA",
    )
    departure_time = models.DateTimeField(
        "departure time",
        blank=True,
        null=True,
        help_text="Le champ 'departure_time' correspond à l'ATD si disponible, sinon à l'ETD",
    )
    relevant_time = models.DateTimeField(
        "relevant time",
        blank=True,
        null=True,
        help_text="Le champ 'relevant_time' correspond soit au champ 'arrival_time', soit au champ 'departure time'",
    )
    fal5_counter = models.PositiveSmallIntegerField(
        "Nombre de FAL 5 reçus", null=False, blank=False, default=0
    )
    fal6_counter = models.PositiveSmallIntegerField(
        "Nombre de FAL 6 reçus", null=False, blank=False, default=0
    )
    swing_crew_list_id = models.CharField(
        "swing crew list id", max_length=256, blank=True
    )
    swing_passengers_list_id = models.CharField(
        "passengers crew list id", max_length=256, blank=True
    )
    sip_fal5_available = models.BooleanField(
        "Liste équipage disponible dans le SIP",
        default=False,
    )
    sip_fal6_available = models.BooleanField(
        "Liste passagers disponible dans le SIP",
        default=False,
    )
    passenger_list_delay_type = models.CharField(
        "type de retard liste passagers",
        max_length=256,
        blank=True,
        choices=persons_choices.DELAY_TYPE_CHOICES,
    )
    crew_list_delay_type = models.CharField(
        "type de retard liste équipage",
        max_length=256,
        blank=True,
        choices=persons_choices.DELAY_TYPE_CHOICES,
    )

    class Meta:
        abstract = True

    def set_arrival_time(self):
        """
        Sets the arrival time, either the ATA if available, or the ETA.
        """
        self.arrival_time = (
            self.ata_to_port_of_call
            if self.ata_to_port_of_call
            else self.eta_to_port_of_call
        )
        return self.arrival_time

    def set_departure_time(self):
        """
        Sets the departure time, either the ATD if available, or the ETD.
        """
        self.departure_time = (
            self.atd_from_port_of_call
            if self.atd_from_port_of_call
            else self.etd_from_port_of_call
        )
        return self.departure_time

    def set_relevant_time(self):
        """
        Sets the relevant time, depending of the ship way.
        """
        if self.is_arriving:
            self.relevant_time = self.arrival_time
        if self.is_departing:
            self.relevant_time = self.departure_time
        return self.relevant_time

    def save(self, *args, **kwargs):
        self.set_arrival_time()
        self.set_departure_time()
        self.set_relevant_time()
        return super().save(*args, **kwargs)
