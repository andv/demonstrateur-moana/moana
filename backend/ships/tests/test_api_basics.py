from django.conf import settings
from django.shortcuts import reverse
from django.utils import timezone

import pytest

from backend.ships import settings as ships_settings
from backend.ships.models import CrewList, ShipMovement
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())

before_two_weeks = (
    timezone.now() + timezone.timedelta(weeks=2) - timezone.timedelta(days=1)
)
after_two_weeks = (
    timezone.now() + timezone.timedelta(weeks=2) + timezone.timedelta(days=1)
)


def test_ship_movements_api_loads(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipMovement.objects.create(
        way=ships_settings.ARRIVING, port_of_call_relation=saint_malo
    )
    url = reverse("ships-list")
    response = client.get(url)
    assert "FRSML" in str(response.content)


def test_ship_movement_api_has_csv_file(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipMovement.objects.create(
        way=ships_settings.ARRIVING, port_of_call_relation=saint_malo
    )
    CrewList.objects.create(ship_call_id="test-id", way=ships_settings.ARRIVING)
    url = reverse("ships-list")
    response = client.get(url)
    assert "crew_list_file_fpr" in str(response.content)
    assert "crew_list_file_roc" in str(response.content)
    assert "passengers_list_file_fpr" in str(response.content)
    assert "passengers_list_file_roc" in str(response.content)


def test_user_can_only_see_movements_from_his_associated_ports(client):
    utils.login_saint_malo_user(client)
    marseille = factories.PortFactory(locode="FRMRS")
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipMovement.objects.create(
        port_of_call_relation=marseille, way=ships_settings.ARRIVING
    )
    ShipMovement.objects.create(
        port_of_call_relation=saint_malo, way=ships_settings.ARRIVING
    )
    url = reverse("ships-list")
    response = client.get(url)
    assert "FRSML" in str(response.content)
    assert "FRMRS" not in str(response.content)


def test_user_without_assigned_team_has_nothing(client):
    utils.login(client)
    marseille = factories.PortFactory(locode="FRMRS")
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipMovement.objects.create(port_of_call_relation=marseille)
    ShipMovement.objects.create(port_of_call_relation=saint_malo)
    url = reverse("ships-list")
    response = client.get(url)
    assert "FRSML" not in str(response.content)
    assert "FRMRS" not in str(response.content)


def test_user_cannot_see_movements_with_arriving_ship_with_eta_over_2_weeks(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_name="recent planned ship",
        way=ships_settings.ARRIVING,
        eta_to_port_of_call=before_two_weeks,
    )
    ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_name="far in the future",
        way=ships_settings.ARRIVING,
        eta_to_port_of_call=after_two_weeks,
    )
    url = reverse("ships-list")
    response = client.get(url)
    assert "recent planned ship" in str(response.content)
    assert "far in the future" not in str(response.content)


def test_user_cannot_see_movements_with_departing_ship_with_etd_over_2_weeks(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_name="recent planned ship",
        way=ships_settings.DEPARTING,
        etd_from_port_of_call=before_two_weeks,
    )
    ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_name="far in the future",
        way=ships_settings.DEPARTING,
        etd_from_port_of_call=after_two_weeks,
    )
    url = reverse("ships-list")
    response = client.get(url)
    assert "recent planned ship" in str(response.content)
    assert "far in the future" not in str(response.content)


def test_metropole_user_can_see_france_metropolitan_movements(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_name="in france metropole",
        way=ships_settings.DEPARTING,
        next_port="FRFOS",  # France
        port_of_call="FRSML",  # France
    )
    ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_name="in schengen",
        way=ships_settings.DEPARTING,
        next_port="ESSPC",  # Spain
        port_of_call="FRSML",  # France
    )
    url = reverse("ships-list")
    response = client.get(url)
    assert "in schengen" in str(response.content)
    assert "in france metropole" in str(response.content)


def test_overseas_user_can_see_local_oversease_movements(client):
    utils.login_martinique_user(client)
    martinique = factories.PortFactory(locode="MQFDF")
    ShipMovement.objects.create(
        port_of_call_relation=martinique,
        ship_name="in same overseas departement",
        way=ships_settings.DEPARTING,
        next_port="MQSPI",  # St Pierre, Martinique
        port_of_call="MQFDF",  # Fort De France, Martinique
    )
    ShipMovement.objects.create(
        port_of_call_relation=martinique,
        ship_name="between metropole and overseas",
        way=ships_settings.DEPARTING,
        next_port="FRSML",  # St Malo
        port_of_call="MQFDF",  # Fort De France, Martinique
    )
    url = reverse("ships-list")
    response = client.get(url)
    assert "between metropole and overseas" in str(response.content)
    assert "in same overseas departement" in str(response.content)


def test_stop_duration_inferior_to_a_day_is_format_in_hours(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    today = timezone.now()
    today_in_three_hours = today + timezone.timedelta(hours=3)

    ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_name="ship departing with stop duration equal to 3h",
        ship_call_id="FR123456",
        way=ships_settings.ARRIVING,
        eta_to_port_of_call=today,
        next_port="FRFOS",
        port_of_call="FRSML",
    )
    ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_name="ship arriving with stop duration equal to 3h",
        ship_call_id="FR123456",
        way=ships_settings.DEPARTING,
        etd_from_port_of_call=today_in_three_hours,
        next_port="FRFOS",
        port_of_call="FRSML",
    )
    url = reverse("ships-list")
    response = client.get(url)

    assert '"stop_duration":"3h"' in str(response.content)


def test_stop_duration_superior_to_a_day_is_format_in_days(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    today = timezone.now()
    two_days_after = today + timezone.timedelta(days=2)

    ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_name="ship departing with stop duration equal to 2 days",
        ship_call_id="FR234567",
        way=ships_settings.ARRIVING,
        eta_to_port_of_call=today,
        next_port="FRFOS",
        port_of_call="FRSML",
    )
    ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_name="ship arriving with stop duration equal to 2 days",
        ship_call_id="FR234567",
        way=ships_settings.DEPARTING,
        etd_from_port_of_call=two_days_after,
        next_port="FRFOS",
        port_of_call="FRSML",
    )
    url = reverse("ships-list")
    response = client.get(url)

    assert '"stop_duration":"2j"' in str(response.content)


def test_stop_duration_with_hours_and_minutes_precision_is_format_with_hours_and_minutes_precision(
    client,
):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    today = timezone.now()
    two_days_after = (
        today
        + timezone.timedelta(days=2)
        + timezone.timedelta(hours=12)
        + timezone.timedelta(minutes=37)
    )

    ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_name="ship departing with stop duration equal to 2 days, 12 hours and 37 minutes",
        ship_call_id="FR234567",
        way=ships_settings.ARRIVING,
        eta_to_port_of_call=today,
        next_port="FRFOS",
        port_of_call="FRSML",
    )
    ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_name="ship arriving with stop duration equal to 2 days, 12 hours and 37 minutes",
        ship_call_id="FR234567",
        way=ships_settings.DEPARTING,
        etd_from_port_of_call=two_days_after,
        next_port="FRFOS",
        port_of_call="FRSML",
    )
    url = reverse("ships-list")
    response = client.get(url)

    assert '"stop_duration":"2j 12h 37min"' in str(response.content)
