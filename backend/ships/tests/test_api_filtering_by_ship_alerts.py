from django.conf import settings
from django.shortcuts import reverse

import pytest

from backend.alerts.models import ShipWatchlist
from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_filtering_watchlist_alerts_only_return_ship_with_alerts(client):
    user = utils.login_multiports_user(client)
    ShipWatchlist.objects.create(team=user.team, imo_list="12345")
    ShipMovement.objects.create(
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        ship_name="ship alert imo",
        imo_number="12345",
    )
    ShipMovement.objects.create(
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        ship_name="ship no alert",
        imo_number="67891",
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?alerts=ship-watchlist")
    assert "ship alert imo" in str(response.content)
    assert "ship no alert" not in str(response.content)


def test_filtering_watchlist_and_country_alerts_only_return_ship_with_those_alerts(
    client,
):
    user = utils.login_multiports_user(client)
    settings.ALERT_SHIP_COUNTRIES = ["AA"]
    ShipWatchlist.objects.create(team=user.team, imo_list="12345")
    ShipMovement.objects.create(
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        ship_name="ship alert imo",
        imo_number="12345",
    )
    ShipMovement.objects.create(
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        ship_name="ship alert country",
        imo_number="67891",
        flag_state_of_ship="AA",
    )
    ShipMovement.objects.create(
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        ship_name="ship no alert",
        imo_number="ABCDE",
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?alerts=ship-watchlist&alerts=ship-country")
    assert "ship alert imo" in str(response.content)
    assert "ship alert country" in str(response.content)
    assert "ship no alert" not in str(response.content)


def test_filtering_by_watchlist_alerts_return_ships_from_the_same_team(client):
    user = utils.login_multiports_user(client)
    factories.PortFactory(locode="FRMRS")
    factories.TeamFactory(ports=["FRMRS"])
    ShipWatchlist.objects.create(team=user.team, imo_list="12345")
    ShipMovement.objects.create(
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        ship_name="ship with alert and in same team",
        imo_number="12345",
    )
    ShipMovement.objects.create(
        way=ships_settings.DEPARTING,
        port_of_call="FRMRS",
        ship_name="ship with alert but not same team",
        imo_number="12345",
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?alerts=ship-watchlist")
    assert "ship with alert and in same team" in str(response.content)
    assert "ship with alert but not same team" not in str(response.content)
