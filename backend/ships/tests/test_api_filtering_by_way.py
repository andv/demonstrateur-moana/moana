from django.conf import settings
from django.shortcuts import reverse

import pytest

from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_can_filter_arriving_ship_movements(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipMovement.objects.create(
        ship_name="departing ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
    )
    ShipMovement.objects.create(
        ship_name="arriving ship",
        way=ships_settings.ARRIVING,
        port_of_call_relation=saint_malo,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?way=arriving")
    assert "arriving ship" in str(response.content)
    assert "departing ship" not in str(response.content)


def test_can_filter_departing_ship_movements(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipMovement.objects.create(
        ship_name="departing ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
    )
    ShipMovement.objects.create(
        ship_name="arriving ship",
        way=ships_settings.ARRIVING,
        port_of_call_relation=saint_malo,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?way=departing")
    assert "arriving ship" not in str(response.content)
    assert "departing ship" in str(response.content)


def test_filtering_without_way_returns_all_ships(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipMovement.objects.create(
        ship_name="departing ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
    )
    ShipMovement.objects.create(
        ship_name="arriving ship",
        way=ships_settings.ARRIVING,
        port_of_call_relation=saint_malo,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}")
    assert "arriving ship" in str(response.content)
    assert "departing ship" in str(response.content)
