from django.conf import settings

import pytest

from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import factories

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_incoming_departing_movement_is_linked_to_exiting_arriving_movement(client):
    saint_malo = factories.PortFactory(locode="FRSML")
    arriving = ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_call_id="12345",
        way=ships_settings.ARRIVING,
    )
    assert not arriving.related_movement
    departing = ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_call_id="12345",
        way=ships_settings.DEPARTING,
    )
    departing = ShipMovement.objects.get(
        ship_call_id="12345", way=ships_settings.DEPARTING
    )
    assert departing.related_movement == arriving


def test_existing_arriving_movement_is_linked_to_incoming_departing_movement(client):
    saint_malo = factories.PortFactory(locode="FRSML")
    arriving = ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_call_id="12345",
        way=ships_settings.ARRIVING,
    )
    assert not arriving.related_movement
    departing = ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_call_id="12345",
        way=ships_settings.DEPARTING,
    )
    arriving = ShipMovement.objects.get(
        ship_call_id="12345", way=ships_settings.ARRIVING
    )
    assert arriving.related_movement == departing
