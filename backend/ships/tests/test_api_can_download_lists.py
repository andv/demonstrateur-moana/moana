from django.conf import settings
from django.shortcuts import reverse

import pytest

from backend.fal.tests import settings as fal_settings
from backend.ship_files.models import ShipFile as Fal
from backend.ships.models import ShipMovement
from backend.unit_tests import utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


##################################
# Test setup                     #
##################################


def create_ship_without_relevant_time(ship_file):
    list = Fal(ship_file=ship_file)
    list.save()
    # We remove relevant_time to not be exclude by historic initial filter
    ship_movement = ShipMovement.objects.all()[0]
    ship_movement.eta_to_port_of_call = None
    ship_movement.ata_to_port_of_call = None
    ship_movement.etd_from_port_of_call = None
    ship_movement.atd_from_port_of_call = None
    ship_movement.save()


##################################
# Tests                          #
##################################


def test_metro_user_cannot_see_crew_list_for_metro_movement(client):
    utils.login_saint_malo_user(client)
    create_ship_without_relevant_time(ship_file=fal_settings.FAL5_FRANCE_METRO)
    url = reverse("ships-list")
    response = client.get(url)
    assert '"can_download_lists":false' in str(response.content)


def test_metro_user_cannot_see_passengers_list_for_metro_movement(client):
    utils.login_saint_malo_user(client)
    create_ship_without_relevant_time(ship_file=fal_settings.FAL6_FRANCE_METRO)
    url = reverse("ships-list")
    response = client.get(url)
    assert '"can_download_lists":false' in str(response.content)


def test_metro_user_cannot_see_crew_list_annex_for_metro_movement(client):
    utils.login_saint_malo_user(client)
    create_ship_without_relevant_time(ship_file=fal_settings.FAL5_FRANCE_METRO)
    url = reverse("ships-list")
    response = client.get(url)
    assert '"can_download_lists":false' in str(response.content)


def test_metro_user_cannot_see_passengers_list_annex_for_metro_movement(client):
    utils.login_saint_malo_user(client)
    create_ship_without_relevant_time(ship_file=fal_settings.FAL6_FRANCE_METRO)
    url = reverse("ships-list")
    response = client.get(url)
    assert '"can_download_lists":false' in str(response.content)


def test_overseas_user_cannot_see_crew_list_for_local_oversea_movement(client):
    utils.login_martinique_user(client)
    create_ship_without_relevant_time(ship_file=fal_settings.FAL5_LOCAL_OVERSEAS)
    url = reverse("ships-list")
    response = client.get(url)
    assert '"can_download_lists":false' in str(response.content)


def test_overseas_user_cannot_see_passengers_list_for_local_oversea_movement(client):
    utils.login_martinique_user(client)
    create_ship_without_relevant_time(ship_file=fal_settings.FAL6_LOCAL_OVERSEAS)
    url = reverse("ships-list")
    response = client.get(url)
    assert '"can_download_lists":false' in str(response.content)


def test_overseas_user_cannot_see_crew_list_annex_for_local_oversea_movement(client):
    utils.login_martinique_user(client)
    create_ship_without_relevant_time(ship_file=fal_settings.FAL5_LOCAL_OVERSEAS)
    url = reverse("ships-list")
    response = client.get(url)
    assert '"can_download_lists":false' in str(response.content)


def test_overseas_user_cannot_see_passengers_list_annex_for_local_oversea_movement(
    client,
):
    utils.login_martinique_user(client)
    create_ship_without_relevant_time(ship_file=fal_settings.FAL6_LOCAL_OVERSEAS)
    url = reverse("ships-list")
    response = client.get(url)
    assert '"can_download_lists":false' in str(response.content)
