from django.conf import settings
from django.shortcuts import reverse
from django.utils import timezone

import pytest

from backend.alerts.models import ShipAlert, ShipWatchlist
from backend.ship_types.models import ShipType
from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_counter_return_zero_if_no_ships_with_alerts(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipMovement.objects.create(
        ship_name="ship no alert",
        port_of_call_relation=saint_malo,
        way=ships_settings.DEPARTING,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}")
    assert '"count_ships_alerts":0' in str(response.content)


def test_counter_filter_ships_alerts_for_my_team(client):
    user = utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipWatchlist.objects.create(team=user.team, imo_list="12345")
    marseille = factories.PortFactory(locode="FRMRS")
    team_marseille = factories.TeamFactory(ports=[marseille])
    ShipWatchlist.objects.create(team=team_marseille, imo_list="678910")
    ShipMovement.objects.create(
        ship_name="ship alert country my team",
        port_of_call_relation=saint_malo,
        imo_number="12345",
        way=ships_settings.DEPARTING,
    )
    ShipMovement.objects.create(
        ship_name="ship alert country other team",
        port_of_call_relation=marseille,
        imo_number="678910",
        way=ships_settings.DEPARTING,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}")
    assert '"count_ships_alerts":1' in str(response.content)


def test_counter_return_number_of_unique_ships(client):
    user = utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipWatchlist.objects.create(team=user.team, imo_list="12345")
    ShipMovement.objects.create(
        ship_name="ship multiple alerts",
        port_of_call_relation=saint_malo,
        imo_number="12345",
        flag_state_of_ship="LOCODE",
        way=ships_settings.DEPARTING,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}")
    count_alerts = ShipAlert.objects.count()
    assert '"count_ships_alerts":1' in str(response.content)
    assert count_alerts == 2


def test_counter_return_only_today_ships(client):
    user = utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipWatchlist.objects.create(team=user.team, imo_list="12345")
    today = timezone.now()
    yesterday = today - timezone.timedelta(days=1)
    ShipMovement.objects.create(
        ship_name="today ship",
        port_of_call_relation=saint_malo,
        etd_from_port_of_call=today,
        imo_number="12345",
        way=ships_settings.DEPARTING,
    )
    ShipMovement.objects.create(
        ship_name="yesterday ship",
        port_of_call_relation=saint_malo,
        etd_from_port_of_call=yesterday,
        imo_number="12345",
        way=ships_settings.DEPARTING,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?date_before={today.date()}&date_after={today.date()}")
    assert '"count_ships_alerts":1' in str(response.content)


def test_counter_return_ships_corresponding_to_filters(client):
    user = utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipWatchlist.objects.create(team=user.team, imo_list="12345")
    cargo = ShipType.objects.create(name_simple="Cargo")
    ferry = ShipType.objects.create(name_simple="Ferry")
    ShipMovement.objects.create(
        ship_name="cargo ship",
        port_of_call_relation=saint_malo,
        imo_number="12345",
        way=ships_settings.DEPARTING,
        ship_type=cargo,
    )
    ShipMovement.objects.create(
        ship_name="ferry ship",
        port_of_call_relation=saint_malo,
        imo_number="12345",
        way=ships_settings.DEPARTING,
        ship_type=ferry,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?ship_types=cargo")
    assert '"count_ships_alerts":1' in str(response.content)


def test_counter_return_number_ships_total_not_paginated(client):
    user = utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipWatchlist.objects.create(team=user.team, imo_list="12345")
    for x in range(0, settings.SHIPS_API_PAGE_SIZE):
        ship_number = x + 1
        ShipMovement.objects.create(
            ship_name=f"first page ship {ship_number}",
            port_of_call_relation=saint_malo,
            way=ships_settings.DEPARTING,
        )
    ShipMovement.objects.create(
        ship_name="second page ship with alert",
        port_of_call_relation=saint_malo,
        imo_number="12345",
        way=ships_settings.DEPARTING,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}")
    assert '"count_ships_alerts":1' in str(response.content)
    assert '"count":11' in str(response.content)
    assert "first page ship 1" in str(response.content)
    assert "first page ship 10" in str(response.content)
    assert "second page ship with alert" not in str(response.content)
