from django.conf import settings
from django.shortcuts import reverse

import pytest

from backend.ship_types.models import ShipType
from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_everything_is_returned_if_empty_last_port_params(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    italia = factories.PortFactory(locode="IT888")
    ferry = ShipType.objects.create(name_simple="Ferry")
    ShipMovement.objects.create(
        ship_name="arriving ship",
        way=ships_settings.ARRIVING,
        port_of_call_relation=saint_malo,
        last_port_relation=italia,
        ship_type=ferry,
    )
    ShipMovement.objects.create(
        ship_name="departing ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        next_port_relation=italia,
        ship_type=ferry,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}")
    assert "arriving ship" in str(response.content)
    assert "departing ship" in str(response.content)


def test_filtering_last_port_only_return_ships_arriving_from_port_in_country(
    client,
):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    italia = factories.PortFactory(locode="IT888")
    spain = factories.PortFactory(locode="ES888")
    ShipMovement.objects.create(
        ship_name="arriving ship from country",
        way=ships_settings.ARRIVING,
        port_of_call_relation=saint_malo,
        last_port_relation=italia,
        ship_call_id="ship_call_id",
    )
    ShipMovement.objects.create(
        ship_name="arriving ship from different country",
        way=ships_settings.ARRIVING,
        port_of_call_relation=saint_malo,
        last_port_relation=spain,
        ship_call_id="ship_call_id",
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?last_port=IT")
    assert "arriving ship from country" in str(response.content)
    assert "arriving ship from different country" not in str(response.content)


def test_filtering_last_port_doesnt_return_ships_departing(
    client,
):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    italia = factories.PortFactory(locode="IT888")
    ShipMovement.objects.create(
        ship_name="departing ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        next_port_relation=italia,
    )
    ShipMovement.objects.create(
        ship_name="empty last port departing ship",
        way=ships_settings.ARRIVING,
        port_of_call_relation=saint_malo,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?last_port=IT")
    assert "departing ship" not in str(response.content)
    assert "empty last port departing ship" not in str(response.content)


def test_filtering_last_port_with_multiple_values_return_all_ships_from_one_of_the_countries(
    client,
):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    italia = factories.PortFactory(locode="IT888")
    spain = factories.PortFactory(locode="ES888")
    ShipMovement.objects.create(
        ship_name="arriving ship from italia",
        way=ships_settings.ARRIVING,
        port_of_call_relation=saint_malo,
        last_port_relation=italia,
        ship_call_id="ship_call_id",
    )
    ShipMovement.objects.create(
        ship_name="arriving ship from spain",
        way=ships_settings.ARRIVING,
        port_of_call_relation=saint_malo,
        last_port_relation=spain,
        ship_call_id="ship_call_id-2",
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?last_port=IT&last_port=ES")
    assert "arriving ship from italia" in str(response.content)
    assert "arriving ship from spain" in str(response.content)
