from zoneinfo import ZoneInfo

from django.conf import settings
from django.shortcuts import reverse
from django.utils import timezone
from django.utils.timezone import datetime

import pytest

from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_filtering_by_date_return_arriving_ship_with_relevant_time_in_range(client):
    utils.login_saint_malo_user(client)
    today = timezone.now()
    yesterday = today - timezone.timedelta(days=1)
    day_before_yesterday = yesterday - timezone.timedelta(days=1)
    tomorrow = today + timezone.timedelta(days=1)
    day_after_tomorrow = tomorrow + timezone.timedelta(days=1)
    ShipMovement.objects.create(
        ship_name="today arriving ship",
        way=ships_settings.ARRIVING,
        port_of_call="FRSML",
        eta_to_port_of_call=today,
    )
    ShipMovement.objects.create(
        ship_name="before yesterday arriving ship",
        way=ships_settings.ARRIVING,
        port_of_call="FRSML",
        eta_to_port_of_call=day_before_yesterday,
    )
    ShipMovement.objects.create(
        ship_name="after tomorrow arriving ship",
        way=ships_settings.ARRIVING,
        port_of_call="FRSML",
        eta_to_port_of_call=day_after_tomorrow,
    )
    url = reverse("ships-list")
    response = client.get(
        f"{url}?date_before={tomorrow.date()}&date_after={yesterday.date()}"
    )
    assert "today arriving ship" in str(response.content)
    assert "before yesterday arriving ship" not in str(response.content)
    assert "after tomorrow arriving ship" not in str(response.content)


def test_filtering_by_date_return_departing_ship_with_relevant_time_in_range(client):
    utils.login_saint_malo_user(client)
    today = timezone.now()
    yesterday = today - timezone.timedelta(days=1)
    day_before_yesterday = yesterday - timezone.timedelta(days=1)
    tomorrow = today + timezone.timedelta(days=1)
    day_after_tomorrow = tomorrow + timezone.timedelta(days=1)
    ShipMovement.objects.create(
        ship_name="today departing ship",
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        etd_from_port_of_call=today,
    )
    ShipMovement.objects.create(
        ship_name="before yesterday departing ship",
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        etd_from_port_of_call=day_before_yesterday,
    )
    ShipMovement.objects.create(
        ship_name="after tomorrow departing ship",
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        etd_from_port_of_call=day_after_tomorrow,
    )
    url = reverse("ships-list")
    response = client.get(
        f"{url}?date_before={tomorrow.date()}&date_after={yesterday.date()}"
    )
    assert "today departing ship" in str(response.content)
    assert "before yesterday departing ship" not in str(response.content)
    assert "after tomorrow departing ship" not in str(response.content)


def test_filtering_by_dates_returns_ships_in_same_day_no_matter_what_time_it_is(client):
    utils.login_saint_malo_user(client)
    today = datetime.now(ZoneInfo("Europe/Paris"))
    yesterday = today - timezone.timedelta(days=1)
    tomorrow = today + timezone.timedelta(days=1)
    today_beginning = today.replace(hour=00, minute=00, second=00)
    tomorrow_beginning = tomorrow.replace(hour=00, minute=00, second=00)
    today_end = today.replace(hour=23, minute=59, second=59)
    yesterday_end = yesterday.replace(hour=23, minute=59, second=59)
    ShipMovement.objects.create(
        ship_name="ship beginning today",
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        etd_from_port_of_call=today_beginning,
    )
    ShipMovement.objects.create(
        ship_name="ship today",
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        etd_from_port_of_call=today,
    )
    ShipMovement.objects.create(
        ship_name="ship end today",
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        etd_from_port_of_call=today_end,
    )
    ShipMovement.objects.create(
        ship_name="ship beginning tomorrow",
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        etd_from_port_of_call=tomorrow_beginning,
    )
    ShipMovement.objects.create(
        ship_name="ship end yesterday",
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        etd_from_port_of_call=yesterday_end,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?date_before={today.date()}&date_after={today.date()}")
    assert "ship today" in str(response.content)
    assert "ship beginning today" in str(response.content)
    assert "ship end today" in str(response.content)
    assert "ship beginning tomorrow" not in str(response.content)
    assert "ship end yesterday" not in str(response.content)


def test_filtering_by_dates_returns_ship_in_same_day_regarding_timezone_datetime(
    client,
):
    """
    Europe/Paris timezone offsets : UTC+1 or UTC+2
    America/Martinique timezone offset : UTC-4
    """
    utils.login_martinique_user(client)

    today_in_paris = datetime.now(ZoneInfo("Europe/Paris"))
    yesterday_in_paris = today_in_paris - timezone.timedelta(days=1)
    tomorrow_in_paris = today_in_paris + timezone.timedelta(days=1)
    today_beginning_in_paris = today_in_paris.replace(hour=00, minute=00, second=00)
    tomorrow_beginning_in_paris = tomorrow_in_paris.replace(
        hour=00, minute=00, second=00
    )
    today_end_in_paris = today_in_paris.replace(hour=23, minute=59, second=59)
    yesterday_end_in_paris = yesterday_in_paris.replace(hour=23, minute=59, second=59)

    today_in_fort_de_france = datetime.now(ZoneInfo("America/Martinique"))
    yesterday_in_fort_de_france = today_in_fort_de_france - timezone.timedelta(days=1)
    tomorrow_in_fort_de_france = today_in_fort_de_france + timezone.timedelta(days=1)
    today_beginning_in_fort_de_france = today_in_fort_de_france.replace(
        hour=00, minute=00, second=00
    )
    tomorrow_beginning_in_fort_de_france = tomorrow_in_fort_de_france.replace(
        hour=00, minute=00, second=00
    )
    today_end_in_fort_de_france = today_in_fort_de_france.replace(
        hour=23, minute=59, second=59
    )
    yesterday_end_in_fort_de_france = yesterday_in_fort_de_france.replace(
        hour=23, minute=59, second=59
    )

    ShipMovement.objects.create(
        ship_name="ship beginning today in paris",
        way=ships_settings.DEPARTING,
        port_of_call="MQFDF",
        etd_from_port_of_call=today_beginning_in_paris,
    )
    ShipMovement.objects.create(
        ship_name="ship end today in paris",
        way=ships_settings.DEPARTING,
        port_of_call="MQFDF",
        etd_from_port_of_call=today_end_in_paris,
    )
    ShipMovement.objects.create(
        ship_name="ship beginning tomorrow in paris",
        way=ships_settings.DEPARTING,
        port_of_call="MQFDF",
        etd_from_port_of_call=tomorrow_beginning_in_paris,
    )
    ShipMovement.objects.create(
        ship_name="ship end yesterday in paris",
        way=ships_settings.DEPARTING,
        port_of_call="MQFDF",
        etd_from_port_of_call=yesterday_end_in_paris,
    )

    ShipMovement.objects.create(
        ship_name="ship beginning today in Fort-de-France",
        way=ships_settings.DEPARTING,
        port_of_call="MQFDF",
        etd_from_port_of_call=today_beginning_in_fort_de_france,
    )
    ShipMovement.objects.create(
        ship_name="ship end today in Fort-de-France",
        way=ships_settings.DEPARTING,
        port_of_call="MQFDF",
        etd_from_port_of_call=today_end_in_fort_de_france,
    )
    ShipMovement.objects.create(
        ship_name="ship beginning tomorrow in Fort-de-France",
        way=ships_settings.DEPARTING,
        port_of_call="MQFDF",
        etd_from_port_of_call=tomorrow_beginning_in_fort_de_france,
    )
    ShipMovement.objects.create(
        ship_name="ship end yesterday in Fort-de-France",
        way=ships_settings.DEPARTING,
        port_of_call="MQFDF",
        etd_from_port_of_call=yesterday_end_in_fort_de_france,
    )

    url = reverse("ships-list")
    response = client.get(
        f"{url}?date_before={today_beginning_in_fort_de_france.date()}&date_after={today_beginning_in_fort_de_france.date()}"
    )
    assert "ship beginning today in Fort-de-France" in str(response.content)
    assert "ship end today in Fort-de-France" in str(response.content)
    assert "ship beginning tomorrow in Fort-de-France" not in str(response.content)
    assert "ship end yesterday in Fort-de-France" not in str(response.content)

    assert "ship beginning today in paris" not in str(response.content)
    assert "ship end today in paris" in str(response.content)
    assert "ship beginning tomorrow in paris" in str(response.content)
    assert "ship end yesterday in paris" not in str(response.content)
