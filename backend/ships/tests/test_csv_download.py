from django.conf import settings
from django.shortcuts import reverse

import pytest

from backend.fal.tests import settings as fal_settings
from backend.ship_files.models import ShipFile as Fal
from backend.ships.models import CrewList, PassengerList
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())

#############
# View      #
#############


def test_fpr_passengers_csv_download_view_loads(client):
    utils.login_saint_malo_user(client)
    fal1 = Fal(ship_file=fal_settings.FAL1_EXTRA_SCHENGEN)
    fal1.save()
    fal5 = Fal(ship_file=fal_settings.FAL6_PATH)
    fal5.save()
    passengers = PassengerList.objects.last()
    response = client.get(passengers.ship_movement.passengers_list_file_fpr)
    assert response.status_code == 200


def test_roc_passengers_csv_download_view_loads(client):
    utils.login_saint_malo_user(client)
    fal1 = Fal(ship_file=fal_settings.FAL1_EXTRA_SCHENGEN)
    fal1.save()
    fal5 = Fal(ship_file=fal_settings.FAL6_PATH)
    fal5.save()
    passengers = PassengerList.objects.last()
    response = client.get(passengers.ship_movement.passengers_list_file_roc)
    assert response.status_code == 200


def test_fpr_crew_csv_download_view_loads(client):
    utils.login_saint_malo_user(client)
    fal1 = Fal(ship_file=fal_settings.FAL1_EXTRA_SCHENGEN)
    fal1.save()
    fal5 = Fal(ship_file=fal_settings.FAL5_PATH)
    fal5.save()
    crew = CrewList.objects.last()
    response = client.get(crew.ship_movement.crew_list_file_fpr)
    assert response.status_code == 200


def test_roc_crew_csv_download_view_loads(client):
    utils.login_saint_malo_user(client)
    fal1 = Fal(ship_file=fal_settings.FAL1_EXTRA_SCHENGEN)
    fal1.save()
    fal5 = Fal(ship_file=fal_settings.FAL5_PATH)
    fal5.save()
    crew = CrewList.objects.last()
    response = client.get(crew.ship_movement.crew_list_file_roc)
    assert response.status_code == 200


#########################
# Team restriction      #
#########################


def test_fpr_passengers_csv_download_is_restricted_if_port_does_not_match(client):
    user = utils.login(client)
    fal1 = Fal(ship_file=fal_settings.FAL1_EXTRA_SCHENGEN)
    fal1.save()
    fal5 = Fal(ship_file=fal_settings.FAL6_PATH)
    fal5.save()
    passengers = PassengerList.objects.last()
    marseille = factories.PortFactory(locode="FRMRS")
    team_marseille = factories.TeamFactory(ports=[marseille])
    user.team = team_marseille
    user.save()
    # The user's port is Marseille, which does not match the imported FAL data.
    response = client.get(passengers.ship_movement.passengers_list_file_fpr)
    assert response.status_code == 404


def test_roc_passengers_csv_download_is_restricted_if_port_does_not_match(client):
    user = utils.login(client)
    fal1 = Fal(ship_file=fal_settings.FAL1_EXTRA_SCHENGEN)
    fal1.save()
    fal5 = Fal(ship_file=fal_settings.FAL6_PATH)
    fal5.save()
    passengers = PassengerList.objects.last()
    marseille = factories.PortFactory(locode="FRMRS")
    team_marseille = factories.TeamFactory(ports=[marseille])
    user.team = team_marseille
    user.save()
    # The user's port is Marseille, which does not match the imported FAL data.
    response = client.get(passengers.ship_movement.passengers_list_file_roc)
    assert response.status_code == 404


def test_fpr_crew_csv_download_is_restricted_if_port_does_not_match(client):
    user = utils.login(client)
    fal1 = Fal(ship_file=fal_settings.FAL1_EXTRA_SCHENGEN)
    fal1.save()
    fal5 = Fal(ship_file=fal_settings.FAL5_PATH)
    fal5.save()
    crew = CrewList.objects.last()
    marseille = factories.PortFactory(locode="FRMRS")
    team_marseille = factories.TeamFactory(ports=[marseille])
    user.team = team_marseille
    user.save()
    # The user's port is Marseille, which does not match the imported FAL data.
    response = client.get(crew.ship_movement.crew_list_file_fpr)
    assert response.status_code == 404


def test_roc_crew_csv_download_is_restricted_if_port_does_not_match(client):
    user = utils.login(client)
    fal1 = Fal(ship_file=fal_settings.FAL1_EXTRA_SCHENGEN)
    fal1.save()
    fal5 = Fal(ship_file=fal_settings.FAL5_PATH)
    fal5.save()
    crew = CrewList.objects.last()
    marseille = factories.PortFactory(locode="FRMRS")
    team_marseille = factories.TeamFactory(ports=[marseille])
    user.team = team_marseille
    user.save()
    # The user's port is Marseille, which does not match the imported FAL data.
    response = client.get(crew.ship_movement.crew_list_file_roc)
    assert response.status_code == 404


#####################################
# Geographic area restriction       #
#####################################


def test_user_cannot_download_crew_lists_for_local_oversea_movement(client):
    utils.login_martinique_user(client)
    overseas_list = Fal(ship_file=fal_settings.FAL5_LOCAL_OVERSEAS)
    overseas_list.save()
    crew = CrewList.objects.last()
    response_fpr = client.get(crew.ship_movement.crew_list_file_fpr)
    response_fpr_annnexes = client.get(crew.ship_movement.crew_list_annex_file_fpr)
    response_roc = client.get(crew.ship_movement.crew_list_file_roc)
    response_roc_annexes = client.get(crew.ship_movement.crew_list_annex_file_roc)
    assert response_fpr.status_code == 404
    assert response_fpr_annnexes.status_code == 404
    assert response_roc.status_code == 404
    assert response_roc_annexes.status_code == 404


def test_user_cannot_download_crew_lists_for_france_metro_movement(client):
    utils.login_saint_malo_user(client)
    overseas_list = Fal(ship_file=fal_settings.FAL5_FRANCE_METRO)
    overseas_list.save()
    crew = CrewList.objects.last()
    response_fpr = client.get(crew.ship_movement.crew_list_file_fpr)
    response_fpr_annnexes = client.get(crew.ship_movement.crew_list_annex_file_fpr)
    response_roc = client.get(crew.ship_movement.crew_list_file_roc)
    response_roc_annexes = client.get(crew.ship_movement.crew_list_annex_file_roc)
    assert response_fpr.status_code == 404
    assert response_fpr_annnexes.status_code == 404
    assert response_roc.status_code == 404
    assert response_roc_annexes.status_code == 404


def test_user_cannot_download_passenger_lists_for_local_oversea_movement(client):
    utils.login_martinique_user(client)
    overseas_list = Fal(ship_file=fal_settings.FAL6_LOCAL_OVERSEAS)
    overseas_list.save()
    passenger = PassengerList.objects.last()
    response_fpr = client.get(passenger.ship_movement.passengers_list_file_fpr)
    response_fpr_annnexes = client.get(
        passenger.ship_movement.passengers_list_annex_file_fpr
    )
    response_roc = client.get(passenger.ship_movement.passengers_list_file_roc)
    response_roc_annexes = client.get(
        passenger.ship_movement.passengers_list_annex_file_roc
    )
    assert response_fpr.status_code == 404
    assert response_fpr_annnexes.status_code == 404
    assert response_roc.status_code == 404
    assert response_roc_annexes.status_code == 404


def test_user_cannot_download_passenger_lists_for_france_metro_movement(client):
    utils.login_saint_malo_user(client)
    overseas_list = Fal(ship_file=fal_settings.FAL6_FRANCE_METRO)
    overseas_list.save()
    passenger = PassengerList.objects.last()
    response_fpr = client.get(passenger.ship_movement.passengers_list_file_fpr)
    response_fpr_annnexes = client.get(
        passenger.ship_movement.passengers_list_annex_file_fpr
    )
    response_roc = client.get(passenger.ship_movement.passengers_list_file_roc)
    response_roc_annexes = client.get(
        passenger.ship_movement.passengers_list_annex_file_roc
    )
    assert response_fpr.status_code == 404
    assert response_fpr_annnexes.status_code == 404
    assert response_roc.status_code == 404
    assert response_roc_annexes.status_code == 404
