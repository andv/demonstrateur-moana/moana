from django.conf import settings
from django.shortcuts import reverse

import pytest

from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_filtering_with_local_movements_for_user_in_france_returns_france_metropolitan_ships(
    client,
):
    utils.login_saint_malo_user(client)
    ShipMovement.objects.create(
        ship_name="departing ship france",
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        next_port="FRMRS",
    )
    ShipMovement.objects.create(
        ship_name="arriving ship france",
        way=ships_settings.ARRIVING,
        port_of_call="FRSML",
        last_port="FRMRS",
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?local_movements=1")
    assert "departing ship france" in str(response.content)
    assert "arriving ship france" in str(response.content)


def test_filtering_with_no_local_movements_for_user_in_france_does_not_returns_france_metropolitan_ships(
    client,
):
    utils.login_saint_malo_user(client)
    ShipMovement.objects.create(
        ship_name="departing ship france",
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        next_port="FRMRS",
    )
    ShipMovement.objects.create(
        ship_name="arriving ship france",
        way=ships_settings.ARRIVING,
        port_of_call="FRSML",
        last_port="FRMRS",
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?local_movements=0")
    assert "departing ship france" not in str(response.content)
    assert "arriving ship france" not in str(response.content)


def test_filtering_with_empty_local_movements_for_user_in_france_returns_france_metropolitan_ships(
    client,
):
    utils.login_saint_malo_user(client)
    ShipMovement.objects.create(
        ship_name="departing ship france",
        way=ships_settings.DEPARTING,
        port_of_call="FRSML",
        next_port="FRMRS",
    )
    ShipMovement.objects.create(
        ship_name="arriving ship france",
        way=ships_settings.ARRIVING,
        port_of_call="FRSML",
        last_port="FRMRS",
    )
    url = reverse("ships-list")
    response = client.get(f"{url}")
    assert "departing ship france" in str(response.content)
    assert "arriving ship france" in str(response.content)


def test_filtering_with_local_movements_for_user_in_drom_returns_local_oversease_ships(
    client,
):
    utils.login_martinique_user(client)
    ShipMovement.objects.create(
        ship_name="departing ship martinique",
        way=ships_settings.DEPARTING,
        port_of_call="MQFDF",
        next_port="MQSTA",
    )
    ShipMovement.objects.create(
        ship_name="arriving ship martinique",
        way=ships_settings.ARRIVING,
        port_of_call="MQFDF",
        last_port="MQSTA",
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?local_movements=1")
    assert "departing ship martinique" in str(response.content)
    assert "arriving ship martinique" in str(response.content)


def test_filtering_with_no_local_movements_for_user_in_drom_does_not_returns_local_oversease_ships(
    client,
):
    utils.login_martinique_user(client)
    ShipMovement.objects.create(
        ship_name="departing ship martinique",
        way=ships_settings.DEPARTING,
        port_of_call="MQFDF",
        next_port="MQSTA",
    )
    ShipMovement.objects.create(
        ship_name="arriving ship martinique",
        way=ships_settings.ARRIVING,
        port_of_call="MQFDF",
        last_port="MQSTA",
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?local_movements=0")
    assert "departing ship martinique" not in str(response.content)
    assert "arriving ship martinique" not in str(response.content)


def test_filtering_with_empty_local_movements_for_user_in_drom_returns_local_oversease_ships(
    client,
):
    utils.login_martinique_user(client)
    ShipMovement.objects.create(
        ship_name="departing ship martinique",
        way=ships_settings.DEPARTING,
        port_of_call="MQFDF",
        next_port="MQSTA",
    )
    ShipMovement.objects.create(
        ship_name="arriving ship martinique",
        way=ships_settings.ARRIVING,
        port_of_call="MQFDF",
        last_port="MQSTA",
    )
    url = reverse("ships-list")
    response = client.get(f"{url}")
    assert "departing ship martinique" in str(response.content)
    assert "arriving ship martinique" in str(response.content)
