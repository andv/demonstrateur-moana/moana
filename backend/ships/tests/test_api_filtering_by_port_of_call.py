from django.conf import settings
from django.shortcuts import reverse

import pytest

from backend.ship_types.models import ShipType
from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_filtering_by_port_of_call_only_return_same_port_of_call(client):
    utils.login_multiports_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    le_havre = factories.PortFactory(locode="FRLEH")
    ferry = ShipType.objects.create(name_simple="Ferry")
    ShipMovement.objects.create(
        ship_name="saint malo ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=ferry,
    )
    ShipMovement.objects.create(
        ship_name="le havre ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=le_havre,
        ship_type=ferry,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?ports_of_call=FRSML")
    assert "saint malo ship" in str(response.content)
    assert "le havre ship" not in str(response.content)


def test_filtering_by_port_of_call_return_same_port_of_call_if_multiple(client):
    utils.login_multiports_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    le_havre = factories.PortFactory(locode="FRLEH")
    marseille = factories.PortFactory(locode="FRMRS")
    ferry = ShipType.objects.create(name_simple="Ferry")
    ShipMovement.objects.create(
        ship_name="saint malo ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=ferry,
    )
    ShipMovement.objects.create(
        ship_name="le havre ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=le_havre,
        ship_type=ferry,
    )
    ShipMovement.objects.create(
        ship_name="marseille ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=marseille,
        ship_type=ferry,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?ports_of_call=FRSML&ports_of_call=FRLEH")
    assert "saint malo ship" in str(response.content)
    assert "le havre ship" in str(response.content)
    assert "marseille ship" not in str(response.content)


def test_filtering_by_multi_port_of_call_only_return_same_port_of_team(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    le_havre = factories.PortFactory(locode="FRLEH")
    ferry = ShipType.objects.create(name_simple="Ferry")
    ShipMovement.objects.create(
        ship_name="saint malo ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=ferry,
    )
    ShipMovement.objects.create(
        ship_name="le havre ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=le_havre,
        ship_type=ferry,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?ports_of_call=FRSML&ports_of_call=FRLEH")
    assert "saint malo ship" in str(response.content)
    assert "le havre ship" not in str(response.content)


def test_everything_is_returned_if_empty_port_of_call_params(client):
    utils.login_multiports_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    bordeaux = factories.PortFactory(locode="FRBOD")
    ferry = ShipType.objects.create(name_simple="Ferry")
    ShipMovement.objects.create(
        ship_name="saint malo ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=ferry,
    )
    ShipMovement.objects.create(
        ship_name="bordeaux ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=bordeaux,
        ship_type=ferry,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}")
    assert "bordeaux ship" in str(response.content)
    assert "saint malo ship" in str(response.content)
