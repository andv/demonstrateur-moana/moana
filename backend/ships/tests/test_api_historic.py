from zoneinfo import ZoneInfo

from django.conf import settings
from django.shortcuts import reverse
from django.utils.timezone import datetime, timedelta

import pytest

from backend.accounts import settings as accounts_settings
from backend.ships import settings as ships_settings
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


##################################
# Test setup                     #
##################################

# Let's assume the default historic duration is 7 days. Here we extend it to 8 days for these tests.
HISTORIC_DURATION_EXTENDED = accounts_settings.HISTORIC_DURATION_DEFAULT_DAYS + 1


def create_ship_movements(past_time, recent_time):
    """
    This helper function sets initial data for tests.
    It created past and recent ship movements, using the given datetime parameters.
    It creates ship movements with both estimated times ETA/ATA and actual ATA/ATD
    times.
    """
    saint_malo = "FRSML"
    #
    # Past movements with estimated times
    #
    # When set to None the actual time will not take precedence
    value_for_disqualifying_time = None
    factories.ShipMovementFactory(
        ship_name="past-estimated-time",
        eta_to_port_of_call=past_time,
        ata_to_port_of_call=value_for_disqualifying_time,
        way=ships_settings.ARRIVING,
        port_of_call=saint_malo,
    )
    factories.ShipMovementFactory(
        ship_name="past-estimated-time",
        etd_from_port_of_call=past_time,
        atd_from_port_of_call=value_for_disqualifying_time,
        way=ships_settings.DEPARTING,
        port_of_call=saint_malo,
    )
    #
    # Recent movements with estimated times
    #
    # When set to None the actual time will not take precedence
    value_for_disqualifying_time = None
    factories.ShipMovementFactory(
        ship_name="recent-estimated-time",
        eta_to_port_of_call=recent_time,
        ata_to_port_of_call=value_for_disqualifying_time,
        way=ships_settings.ARRIVING,
        port_of_call=saint_malo,
    )
    factories.ShipMovementFactory(
        ship_name="recent-estimated-time",
        etd_from_port_of_call=recent_time,
        atd_from_port_of_call=value_for_disqualifying_time,
        way=ships_settings.DEPARTING,
        port_of_call=saint_malo,
    )
    #
    # Past movements with actual times
    #
    value_for_disqualifying_time = recent_time  # disqualify past with recent time
    factories.ShipMovementFactory(
        ship_name="past-actual-time",
        eta_to_port_of_call=value_for_disqualifying_time,
        ata_to_port_of_call=past_time,
        way=ships_settings.ARRIVING,
        port_of_call=saint_malo,
    )
    factories.ShipMovementFactory(
        ship_name="past-actual-time",
        etd_from_port_of_call=value_for_disqualifying_time,
        atd_from_port_of_call=past_time,
        way=ships_settings.DEPARTING,
        port_of_call=saint_malo,
    )
    #
    # Recent movements with actual times
    #
    value_for_disqualifying_time = past_time  # disqualify recent with past time
    factories.ShipMovementFactory(
        ship_name="recent-actual-time",
        eta_to_port_of_call=value_for_disqualifying_time,
        ata_to_port_of_call=recent_time,
        way=ships_settings.ARRIVING,
        port_of_call=saint_malo,
    )
    factories.ShipMovementFactory(
        ship_name="recent-actual-time",
        etd_from_port_of_call=value_for_disqualifying_time,
        atd_from_port_of_call=recent_time,
        way=ships_settings.DEPARTING,
        port_of_call=saint_malo,
    )


def create_ship_movements_for_testing_short_historic_duration_config():
    recent_time = datetime.now(ZoneInfo("Europe/Paris"))
    past_time = recent_time - timedelta(days=2)
    create_ship_movements(past_time=past_time, recent_time=recent_time)


def create_ship_movements_for_testing_extended_historic_duration_config():
    recent_time = datetime.now(ZoneInfo("Europe/Paris"))
    # By default, let's assume that 7 days old and older movements are filtered out.
    # By creating 8 days old movements, we expect them to be filtered out by default.
    # But we except them to be listed, if the team config extends to historic duration
    # config over 8 days.
    past_time = recent_time - timedelta(days=HISTORIC_DURATION_EXTENDED)
    create_ship_movements(past_time=past_time, recent_time=recent_time)


##################################
# Team historic duration config  #
##################################


def test_filter_with_estimated_times_when_team_configuration_shorten_results(
    client,
):
    create_ship_movements_for_testing_short_historic_duration_config()
    user = utils.login_saint_malo_user(client)
    url = reverse("ships-list")
    response = client.get(f"{url}")
    assert "recent-estimated-time" in str(response.content)
    assert "past-estimated-time" in str(response.content)
    team = user.team
    team.historic_duration = 1  # That's so short that it should cutoff the result list
    team.save()
    response = client.get(f"{url}")
    assert "recent-estimated-time" in str(response.content)
    assert "past-estimated-time" not in str(response.content)


def test_filter_with_estimated_times_when_team_configuration_extend_results(
    client,
):
    create_ship_movements_for_testing_extended_historic_duration_config()
    user = utils.login_saint_malo_user(client)
    url = reverse("ships-list")
    response = client.get(f"{url}")
    assert "recent-estimated-time" in str(response.content)
    assert "past-estimated-time" not in str(response.content)
    team = user.team
    # Let's assume that by default, the results are shorten to 7 days old movements.
    # For this test, we will create 8 days old movements. In that case, we will set
    # the historic duration to 9 days and we will expect the results to be extended
    # and include these 8 days old past movements.
    team.historic_duration = HISTORIC_DURATION_EXTENDED + 1  # That's 8+1=9
    team.save()
    response = client.get(f"{url}")
    assert "recent-estimated-time" in str(response.content)
    assert "past-estimated-time" in str(response.content)


def test_filter_with_actual_times_when_team_configuration_shorten_results(
    client,
):
    create_ship_movements_for_testing_short_historic_duration_config()
    user = utils.login_saint_malo_user(client)
    url = reverse("ships-list")
    response = client.get(f"{url}")
    assert "recent-actual-time" in str(response.content)
    assert "past-actual-time" in str(response.content)
    team = user.team
    team.historic_duration = 1  # That's so short that it should cutoff the result list
    team.save()
    response = client.get(f"{url}")
    assert "recent-actual-time" in str(response.content)
    assert "past-actual-time" not in str(response.content)


def test_filter_with_actual_times_when_team_configuration_extend_results(
    client,
):
    create_ship_movements_for_testing_extended_historic_duration_config()
    user = utils.login_saint_malo_user(client)
    url = reverse("ships-list")
    response = client.get(f"{url}")
    assert "recent-actual-time" in str(response.content)
    assert "past-actual-time" not in str(response.content)
    team = user.team
    # Let's assume that by default, the results are shorten to 7 days old movements.
    # For this test, we will create 8 days old movements. In that case, we will set
    # the historic duration to 9 days and we will expect the results to be extended
    # and include these 8 days old past movements.
    team.historic_duration = HISTORIC_DURATION_EXTENDED + 1  # That's 8+1=9
    team.save()
    response = client.get(f"{url}")
    assert "recent-actual-time" in str(response.content)
    assert "past-actual-time" in str(response.content)
