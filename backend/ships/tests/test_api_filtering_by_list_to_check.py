from zoneinfo import ZoneInfo

from django.conf import settings
from django.shortcuts import reverse
from django.utils.timezone import datetime

import pytest

from backend.person_checks import choices as person_checks_choices
from backend.person_checks.models import PersonCheck
from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_filtering_by_list_to_check_returns_movement_with_list_to_check(client):
    user = utils.login_saint_malo_user(client)
    saint_malo_team = user.team
    today = datetime.now(ZoneInfo("Europe/Paris"))

    # Defining a ship_movement with lists
    ship_movement_name = "movement with lists to check"
    movement_with_lists_to_check = utils.initialize_ship_movement_with_list(
        ship_movement_name
    )

    number_of_list_checked_for_the_ship_movement = PersonCheck.objects.filter(
        ship_movement=movement_with_lists_to_check
    ).count()

    url = reverse("ships-list")
    response = client.get(f"{url}?list_to_check_type=fpr")
    assert number_of_list_checked_for_the_ship_movement == 0
    assert movement_with_lists_to_check.ship_name in str(response.content)


def test_filtering_by_list_to_check_exclude_movement_with_all_list_checked(client):
    user = utils.login_saint_malo_user(client)
    saint_malo_team = user.team
    today = datetime.now(ZoneInfo("Europe/Paris"))

    # Defining a ship_movement with lists
    ship_movement_name = "movement with lists checked"
    movement_with_lists_checked = utils.initialize_ship_movement_with_list(
        ship_movement_name
    )

    # Checking the lists for the ship_movement
    PersonCheck.objects.create(
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
        ship_movement=movement_with_lists_checked,
        team=saint_malo_team,
    )
    PersonCheck.objects.create(
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
        ship_movement=movement_with_lists_checked,
        team=saint_malo_team,
    )

    number_of_list_checked_for_the_ship_movement = PersonCheck.objects.filter(
        ship_movement=movement_with_lists_checked
    ).count()

    url = reverse("ships-list")
    response = client.get(f"{url}?list_to_check_type=fpr")
    assert number_of_list_checked_for_the_ship_movement == 2
    assert movement_with_lists_checked.ship_name not in str(response.content)


def test_filtering_by_list_to_check_display_movement_with_list_checked_by_other_team(
    client,
):
    user = utils.login_saint_malo_user(client)
    saint_malo_team = user.team
    other_team = factories.TeamFactory(name="other team")
    today = datetime.now(ZoneInfo("Europe/Paris"))

    # Defining a ship_movement with lists
    ship_movement_name = "movement with lists checked by other team"
    movement_with_lists_checked_by_other_team = (
        utils.initialize_ship_movement_with_list(ship_movement_name)
    )

    # Checking the lists for the ship_movement with an other team
    PersonCheck.objects.create(
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
        ship_movement=movement_with_lists_checked_by_other_team,
        team=other_team,
    )
    PersonCheck.objects.create(
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
        ship_movement=movement_with_lists_checked_by_other_team,
        team=other_team,
    )

    number_of_list_checked_by_saint_malo_team_for_the_ship_movement = (
        PersonCheck.objects.filter(
            ship_movement=movement_with_lists_checked_by_other_team,
            team=saint_malo_team,
        ).count()
    )

    number_of_list_checked_by_other_team_for_the_ship_movement = (
        PersonCheck.objects.filter(
            ship_movement=movement_with_lists_checked_by_other_team, team=other_team
        ).count()
    )

    url = reverse("ships-list")
    response = client.get(f"{url}?list_to_check_type=fpr")
    assert number_of_list_checked_by_saint_malo_team_for_the_ship_movement == 0
    assert number_of_list_checked_by_other_team_for_the_ship_movement == 2
    assert movement_with_lists_checked_by_other_team.ship_name in str(response.content)


def test_filtering_by_list_to_check_exclude_movement_with_no_list(
    client,
):
    user = utils.login_saint_malo_user(client)
    saint_malo_team = user.team
    saint_malo = factories.PortFactory(locode="FRSML")
    today = datetime.now(ZoneInfo("Europe/Paris"))

    # Defining a ship_movement with lists
    movement_with_no_list_available = ShipMovement.objects.create(
        ship_name="movement with no list available",
        eta_to_port_of_call=today,
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
    )

    number_of_list_checked_for_movement_with_no_list_available = (
        PersonCheck.objects.filter(
            ship_movement=movement_with_no_list_available,
            team=saint_malo_team,
        ).count()
    )

    url = reverse("ships-list")
    response = client.get(f"{url}?list_to_check_type=fpr")
    assert number_of_list_checked_for_movement_with_no_list_available == 0
    assert "movement with no list available" not in str(response.content)


def test_filtering_by_list_to_check_fpr_display_movement_even_if_list_are_checked_in_roc(
    client,
):
    user = utils.login_saint_malo_user(client)
    saint_malo_team = user.team
    today = datetime.now(ZoneInfo("Europe/Paris"))

    # Defining a ship_movement with lists
    ship_movement_name = "movement with lists to check in fpr type"
    movement_with_lists_to_check_in_fpr_type = utils.initialize_ship_movement_with_list(
        ship_movement_name
    )

    # Checking the lists for the ship_movement in ROC type
    PersonCheck.objects.create(
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
        check_type=person_checks_choices.CHECK_TYPE_ROC,
        ship_movement=movement_with_lists_to_check_in_fpr_type,
        team=saint_malo_team,
    )
    PersonCheck.objects.create(
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
        check_type=person_checks_choices.CHECK_TYPE_ROC,
        ship_movement=movement_with_lists_to_check_in_fpr_type,
        team=saint_malo_team,
    )

    number_of_list_checked_in_fpr_for_movement = PersonCheck.objects.filter(
        ship_movement=movement_with_lists_to_check_in_fpr_type,
        team=saint_malo_team,
        check_type="fpr",
    ).count()

    number_of_list_checked_in_roc_for_movement = PersonCheck.objects.filter(
        ship_movement=movement_with_lists_to_check_in_fpr_type,
        team=saint_malo_team,
        check_type="roc",
    ).count()

    url = reverse("ships-list")
    response = client.get(f"{url}?list_to_check_type=fpr")
    assert number_of_list_checked_in_fpr_for_movement == 0
    assert number_of_list_checked_in_roc_for_movement == 2
    assert movement_with_lists_to_check_in_fpr_type.ship_name in str(response.content)


def test_filtering_by_list_to_check_roc_display_movement_even_if_list_are_checked_in_fpr(
    client,
):
    user = utils.login_saint_malo_user(client)
    saint_malo_team = user.team
    today = datetime.now(ZoneInfo("Europe/Paris"))

    # Defining a ship_movement with lists
    ship_movement_name = "movement with lists to check in roc type"
    movement_with_lists_to_check_in_roc_type = utils.initialize_ship_movement_with_list(
        ship_movement_name
    )

    # Checking the lists for the ship_movement in FPR type
    PersonCheck.objects.create(
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
        ship_movement=movement_with_lists_to_check_in_roc_type,
        team=saint_malo_team,
    )
    PersonCheck.objects.create(
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
        ship_movement=movement_with_lists_to_check_in_roc_type,
        team=saint_malo_team,
    )

    number_of_list_checked_in_roc_for_movement = PersonCheck.objects.filter(
        ship_movement=movement_with_lists_to_check_in_roc_type,
        team=saint_malo_team,
        check_type="roc",
    ).count()

    number_of_list_checked_in_fpr_for_movement = PersonCheck.objects.filter(
        ship_movement=movement_with_lists_to_check_in_roc_type,
        team=saint_malo_team,
        check_type="fpr",
    ).count()

    url = reverse("ships-list")
    response = client.get(f"{url}?list_to_check_type=roc")
    assert number_of_list_checked_in_roc_for_movement == 0
    assert number_of_list_checked_in_fpr_for_movement == 2
    assert movement_with_lists_to_check_in_roc_type.ship_name in str(response.content)


def test_filtering_by_list_to_check_display_movement_with_at_least_one_list_to_check_in_the_select_type(
    client,
):
    user = utils.login_saint_malo_user(client)
    saint_malo_team = user.team
    ship_movement_name = "movement with at least one list to check in the select type"
    movement_with_at_least_one_list_to_check_in_the_select_type = (
        utils.initialize_ship_movement_with_list(ship_movement_name)
    )

    # Checking the lists for the ship_movement in ROC type and one in FPR type
    PersonCheck.objects.create(
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
        check_type=person_checks_choices.CHECK_TYPE_ROC,
        ship_movement=movement_with_at_least_one_list_to_check_in_the_select_type,
        team=saint_malo_team,
    )
    PersonCheck.objects.create(
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
        check_type=person_checks_choices.CHECK_TYPE_ROC,
        ship_movement=movement_with_at_least_one_list_to_check_in_the_select_type,
        team=saint_malo_team,
    )
    PersonCheck.objects.create(
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
        ship_movement=movement_with_at_least_one_list_to_check_in_the_select_type,
        team=saint_malo_team,
    )

    number_of_list_checked_in_fpr_for_movement = PersonCheck.objects.filter(
        ship_movement=movement_with_at_least_one_list_to_check_in_the_select_type,
        team=saint_malo_team,
        check_type="fpr",
    ).count()
    number_of_list_checked_in_roc_for_movement = PersonCheck.objects.filter(
        ship_movement=movement_with_at_least_one_list_to_check_in_the_select_type,
        team=saint_malo_team,
        check_type="roc",
    ).count()

    url = reverse("ships-list")
    response = client.get(f"{url}?list_to_check_type=fpr")
    assert number_of_list_checked_in_fpr_for_movement == 1
    assert number_of_list_checked_in_roc_for_movement == 2
    assert movement_with_at_least_one_list_to_check_in_the_select_type.ship_name in str(
        response.content
    )
