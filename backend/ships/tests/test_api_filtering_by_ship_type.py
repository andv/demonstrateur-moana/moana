from django.conf import settings
from django.shortcuts import reverse

import pytest

from backend.ship_types.models import ShipType
from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_can_filter_ship_types_cargo(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    cargo = ShipType.objects.create(name_simple="Cargo")
    ferry = ShipType.objects.create(name_simple="Ferry")
    ShipMovement.objects.create(
        ship_name="cargo ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=cargo,
    )
    ShipMovement.objects.create(
        ship_name="ferry ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=ferry,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?ship_types=cargo")
    assert "cargo ship" in str(response.content)
    assert "ferry ship" not in str(response.content)


def test_can_filter_cargo_and_ferry(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    cargo = ShipType.objects.create(name_simple="Cargo")
    ferry = ShipType.objects.create(name_simple="Ferry")
    cruise = ShipType.objects.create(name_simple="Croisière")
    ShipMovement.objects.create(
        ship_name="cargo ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=cargo,
    )
    ShipMovement.objects.create(
        ship_name="ferry ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=ferry,
    )
    ShipMovement.objects.create(
        ship_name="cruise ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=cruise,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?ship_types=cargo&ship_types=ferry")
    assert "cargo ship" in str(response.content)
    assert "ferry ship" in str(response.content)
    assert "cruise ship" not in str(response.content)


def test_can_filter_ship_types_cruise(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    cruise = ShipType.objects.create(name_simple="Croisière")
    cargo = ShipType.objects.create(name_simple="Cargo")
    ShipMovement.objects.create(
        ship_name="cruise ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=cruise,
    )
    ShipMovement.objects.create(
        ship_name="cargo ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=cargo,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?ship_types=cruise")
    assert "cruise ship" in str(response.content)
    assert "cargo ship" not in str(response.content)


def test_can_filter_ship_types_other(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    other1 = ShipType.objects.create(name_simple="Spécial")
    other2 = ShipType.objects.create(name_simple="Plaisance")
    other3 = ShipType.objects.create(name_simple="Nouveau type")
    cargo = ShipType.objects.create(name_simple="Cargo")
    ShipMovement.objects.create(
        ship_name="other ship 1",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=other1,
    )
    ShipMovement.objects.create(
        ship_name="other ship 2",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=other2,
    )
    ShipMovement.objects.create(
        ship_name="other ship 3",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=other3,
    )
    ShipMovement.objects.create(
        ship_name="cargo ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=cargo,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?ship_types=other")
    assert "other ship 1" in str(response.content)
    assert "other ship 2" in str(response.content)
    assert "other ship 3" in str(response.content)
    assert "cargo ship" not in str(response.content)


def test_filtering__by_other_ship_type_return_ship_without_ship_type(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    cargo = ShipType.objects.create(name_simple="Cargo")
    ShipMovement.objects.create(
        ship_name="Ship with empty ship type",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=None,
    )
    ShipMovement.objects.create(
        ship_name="cargo ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=cargo,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?ship_types=other")
    assert "Ship with empty ship type" in str(response.content)
    assert "cargo ship" not in str(response.content)


def test_filtering_by_cargo_should_not_return_ship_without_ship_type(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    cargo = ShipType.objects.create(name_simple="Cargo")
    ShipMovement.objects.create(
        ship_name="Ship with empty ship type",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=None,
    )
    ShipMovement.objects.create(
        ship_name="cargo ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=cargo,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?ship_types=cargo")
    assert "Ship with empty ship type" not in str(response.content)
    assert "cargo ship" in str(response.content)


def test_filtering_by_cruise_should_not_return_ship_without_ship_type(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    cruise = ShipType.objects.create(name_simple="Croisière")
    ShipMovement.objects.create(
        ship_name="Ship with empty ship type",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=None,
    )
    ShipMovement.objects.create(
        ship_name="cruise ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=cruise,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?ship_types=cruise")
    assert "Ship with empty ship type" not in str(response.content)
    assert "cruise ship" in str(response.content)


def test_filtering_by_ferry_should_not_return_ship_without_ship_type(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ferry = ShipType.objects.create(name_simple="Ferry")
    ShipMovement.objects.create(
        ship_name="Ship with empty ship type",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=None,
    )
    ShipMovement.objects.create(
        ship_name="ferry ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=ferry,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?ship_types=ferry")
    assert "Ship with empty ship type" not in str(response.content)
    assert "ferry ship" in str(response.content)


def test_filtering_without_ship_types_returns_all_ships(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ferry = ShipType.objects.create(name_simple="Ferry")
    cruise = ShipType.objects.create(name_simple="Croisière")
    cargo = ShipType.objects.create(name_simple="Cargo")
    other = ShipType.objects.create(name_simple="Spécial")
    ShipMovement.objects.create(
        ship_name="empty ship-type ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=None,
    )
    ShipMovement.objects.create(
        ship_name="ferry ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=ferry,
    )
    ShipMovement.objects.create(
        ship_name="cruise ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=cruise,
    )
    ShipMovement.objects.create(
        ship_name="cargo ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=cargo,
    )
    ShipMovement.objects.create(
        ship_name="other ship",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
        ship_type=other,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}")
    assert "empty ship-type ship" in str(response.content)
    assert "ferry ship" in str(response.content)
    assert "cruise ship" in str(response.content)
    assert "cargo ship" in str(response.content)
    assert "other ship" in str(response.content)
