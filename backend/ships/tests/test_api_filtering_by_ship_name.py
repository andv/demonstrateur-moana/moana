from django.conf import settings
from django.shortcuts import reverse

import pytest

from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_can_search_on_ship_name(client):
    utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ShipMovement.objects.create(
        ship_name="a good name",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
    )
    ShipMovement.objects.create(
        ship_name="a bad name",
        way=ships_settings.DEPARTING,
        port_of_call_relation=saint_malo,
    )
    url = reverse("ships-list")
    response = client.get(f"{url}?search=good")
    assert "a good name" in str(response.content)
    assert "a bad name" not in str(response.content)
