from zoneinfo import ZoneInfo

from django.db.models import Q
from django.utils import timezone
from django.utils.timezone import datetime, timedelta

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.filters import SearchFilter

from backend.alerts.ship_relation import AlertOnShipViewMixin
from backend.current_user.mixins import CurrentUserViewMixin
from backend.geo import choices as geo_choices
from backend.issues.serializers import ReportIssueSerializer
from backend.issues.ship_relation import IssueOnShipViewMixin
from backend.issues.views import ReportIssueViewMixin
from backend.person_checks.serializers import MarkAsCheckedSerializer
from backend.person_checks.ship_relation import PersonCheckOnShipViewMixin
from backend.person_checks.views import PersonCheckViewMixin
from backend.ports.user_relation import UserPortsViewMixin
from backend.ships import settings as ships_settings
from backend.ships.filters import ShipMovementFilter
from backend.ships.models import ShipMovement
from backend.ships.pagination import ShipMovementPagination
from backend.ships.serializers import ShipMovementSerializer


class ShipMovementViewMixin(
    UserPortsViewMixin,
    CurrentUserViewMixin,
    AlertOnShipViewMixin,
    IssueOnShipViewMixin,
    PersonCheckOnShipViewMixin,
):
    """
    Helper class for handling ship movement.
    """

    def initial_filter_keep_only_assigned_ports(self, qs):
        qs = qs.filter(port_of_call_relation__in=self.assigned_ports)
        return qs

    def initial_filter_exclude_too_far_in_future(self, qs):
        two_weeks = timezone.now() + timezone.timedelta(weeks=2)
        qs = qs.filter(Q(relevant_time__isnull=True) | Q(relevant_time__lt=two_weeks))
        return qs

    def initial_filter_exclude_too_far_in_past(self, qs):
        today = datetime.now(ZoneInfo("Europe/Paris"))
        if not self.request.user.team:
            return qs
        historic_duration = self.request.user.team.historic_duration
        historic_day = today - timedelta(days=historic_duration)
        # Historic does not depend of current time
        historic_beginning_day = historic_day.replace(
            hour=00, minute=00, second=00, microsecond=00
        )
        qs = qs.filter(
            Q(relevant_time__isnull=True) | Q(relevant_time__gt=historic_beginning_day)
        )
        return qs

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        qs = ShipMovement.objects.display()
        qs = qs.prefetch_related(
            "alerts",
            "passenger_list",
            "crew_list",
            "port_of_call_relation",
            "port_of_call_relation__sip_swing",
            "port_of_call_relation__sip_vigiesip",
            "last_port_relation",
            "last_port_relation__sip_swing",
            "last_port_relation__sip_vigiesip",
            "next_port_relation",
            "next_port_relation__sip_swing",
            "next_port_relation__sip_vigiesip",
            "ship_type",
            "related_movement",
            "person_checks",
        )
        qs = self.initial_filter_keep_only_assigned_ports(qs)
        qs = self.initial_filter_exclude_too_far_in_future(qs)
        qs = self.initial_filter_exclude_too_far_in_past(qs)
        qs = self.annotate_fpr_checks(qs)
        qs = self.annotate_roc_checks(qs)
        qs = self.annotate_has_alerts(qs)
        qs = self.annotate_has_movement_related_alerts(qs)
        qs = self.annotate_has_issues(qs)
        return qs.order_by("relevant_time")


class ShipMovementViewSet(ShipMovementViewMixin, viewsets.ReadOnlyModelViewSet):
    """
    Read Ship Movements data.
    """

    serializer_class = ShipMovementSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter)
    filterset_class = ShipMovementFilter
    search_fields = ships_settings.SHIP_ADVANCED_SEARCH_FIELDS
    pagination_class = ShipMovementPagination

    def get_paginated_response(self, data):
        """
        The generic get_paginated_response() returns a paginated style `Response`
        object for the given output data.
        This overrides the generic method to add a count of sensitive alerts using the
        annotated flag that instruct if the ship movement has any sensitive alerts.
        """
        queryset = self.get_queryset()
        queryset = self.filter_queryset(queryset)
        count_ship_alerts = queryset.filter(has_movement_related_alerts=True).count()
        self.paginator.count_ships_alerts = count_ship_alerts
        return self.paginator.get_paginated_response(data)


class ShipMovementActionsViewSet(
    ShipMovementViewMixin,
    ReportIssueViewMixin,
    PersonCheckViewMixin,
    viewsets.GenericViewSet,
):
    """
    Actions on Ship Movement data.
    """

    # Some actions, like POST actions, need a specific serializer. We maintain a global
    # inventory of these serializers in order to dynamically render them using
    # `get_serializer_class()`
    action_serializers = {
        "mark_as_checked": MarkAsCheckedSerializer,
        "report_issue": ReportIssueSerializer,
    }

    def get_serializer_class(self):
        return self.action_serializers.get(self.action)
