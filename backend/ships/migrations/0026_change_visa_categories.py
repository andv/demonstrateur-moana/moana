# Generated by Django 4.2.10 on 2024-02-26 11:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("ships", "0025_fix_geographic_area_metropolitan_typo"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="crewlist",
            name="nationalities_extra_eu",
        ),
        migrations.RemoveField(
            model_name="crewlist",
            name="nationalities_visa_eu",
        ),
        migrations.RemoveField(
            model_name="crewlist",
            name="nationalities_visa_fr",
        ),
        migrations.RemoveField(
            model_name="passengerlist",
            name="nationalities_extra_eu",
        ),
        migrations.RemoveField(
            model_name="passengerlist",
            name="nationalities_visa_eu",
        ),
        migrations.RemoveField(
            model_name="passengerlist",
            name="nationalities_visa_fr",
        ),
        migrations.AddField(
            model_name="crewlist",
            name="nationalities_no_visa",
            field=models.PositiveIntegerField(
                blank=True, null=True, verbose_name="# nationalité nous soumis visa"
            ),
        ),
        migrations.AddField(
            model_name="crewlist",
            name="nationalities_visa",
            field=models.PositiveIntegerField(
                blank=True, null=True, verbose_name="# nationalité avec visa"
            ),
        ),
        migrations.AddField(
            model_name="crewlist",
            name="nationalities_visa_transit",
            field=models.PositiveIntegerField(
                blank=True, null=True, verbose_name="# nationalité avec visa transit"
            ),
        ),
        migrations.AddField(
            model_name="passengerlist",
            name="nationalities_no_visa",
            field=models.PositiveIntegerField(
                blank=True, null=True, verbose_name="# nationalité nous soumis visa"
            ),
        ),
        migrations.AddField(
            model_name="passengerlist",
            name="nationalities_visa",
            field=models.PositiveIntegerField(
                blank=True, null=True, verbose_name="# nationalité avec visa"
            ),
        ),
        migrations.AddField(
            model_name="passengerlist",
            name="nationalities_visa_transit",
            field=models.PositiveIntegerField(
                blank=True, null=True, verbose_name="# nationalité avec visa transit"
            ),
        ),
    ]
