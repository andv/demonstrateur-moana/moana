# Generated by Django 4.2.10 on 2024-02-15 09:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("ships", "0022_add_overseas_geographic_area_choices"),
    ]

    operations = [
        migrations.AlterField(
            model_name="shipmovement",
            name="geographic_area",
            field=models.CharField(
                blank=True,
                choices=[
                    ("france-metropolitain", "FR Métropole"),
                    ("france-overseas", "FR Outre-mer"),
                    ("intra-schengen", "Intra Schengen"),
                    ("extra-schengen", "Extra Schengen"),
                    ("local-overseas", "Local Outre-mer"),
                    ("inter-overseas", "Inter Outre-mer"),
                    ("metropolitan-x-overseas", "Métropole x Outre-mer"),
                    ("unknown", "Inconnu"),
                ],
                max_length=255,
                verbose_name="zone géographique",
            ),
        ),
    ]
