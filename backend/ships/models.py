from django.db import models

from model_utils.models import TimeStampedModel

from backend.alerts.ship_relation import AlertsOnShip
from backend.fpr.ship_relation import FileDownloadOnShip
from backend.issues.ship_relation import IssuesOnShip
from backend.persons.models import CrewListBaseModel, PassengerListBaseModel
from backend.persons.ship_relation import PersonOnShip
from backend.ports.honfleur_correction import HonfleurPortCorrectionMixin
from backend.ports.ship_relation import PortOnShip, PortRelationOnShip
from backend.ship_types.ship_relation import ShipTypeRelationOnShip
from backend.ships import settings as ships_settings
from backend.ships.base_models import ShipBaseModel, ShipMovementBaseModel
from backend.ships.managers import ShipMovementManager
from backend.travel_duration.ship_relation import TravelDurationModelHelper
from backend.utils.date_format import duration_format_fr


class ShipMovement(
    ShipMovementBaseModel,
    AlertsOnShip,
    PortRelationOnShip,
    ShipTypeRelationOnShip,
    PersonOnShip,
    IssuesOnShip,
    TravelDurationModelHelper,
    HonfleurPortCorrectionMixin,
    TimeStampedModel,
):
    related_movement = models.OneToOneField(
        to="self",
        verbose_name="mouvement lié",
        related_name="+",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    display = models.BooleanField(
        "afficher", default=True, help_text="Afficher dans l'interface"
    )

    objects = ShipMovementManager()

    class Meta:
        verbose_name = "Mouvement de navire"
        verbose_name_plural = "Mouvements de navires"

    @property
    def related_crew(self):
        return CrewList.objects.filter(
            ship_call_id=self.ship_call_id, way=self.way
        ).first()

    @property
    def related_passenger(self):
        return PassengerList.objects.filter(
            ship_call_id=self.ship_call_id, way=self.way
        ).first()

    @property
    def arrival_stopover_time(self):
        """
        For a departing ship, the relevant arrival time of it's stopover is not directly available :
        we have to look at the related movement. That's the related arrival movement associated to
        the departing ship.
        Note that if the actual ship is an arrival, we simply look at it's own time information - no
        need to check the related movement.
        """
        ship_to_get_time_from = self
        if self.is_departing and self.related_movement:
            ship_to_get_time_from = self.related_movement
        return ship_to_get_time_from.relevant_time

    @property
    def departure_stopover_time(self):
        """
        For a arriving ship, the relevant departure time of it's stopover is not directly available :
        we have to look at the related movement. That's the related departure movement associated to
        the arriving ship.
        Note that if the actual ship is an departure, we simply look at it's own time information - no
        need to check the related movement.
        """
        ship_to_get_time_from = self
        if self.is_arriving and self.related_movement:
            ship_to_get_time_from = self.related_movement
        return ship_to_get_time_from.relevant_time

    @property
    def stop_duration(self):
        """
        Will return the duration of the stop.
        """
        if not self.arrival_stopover_time:
            return None
        if not self.departure_stopover_time:
            return None
        duration = self.departure_stopover_time - self.arrival_stopover_time
        duration_string = duration_format_fr(duration)
        return duration_string

    def get_related_movement(self):
        """
        For a departing ship, this will return the related arriving movement.
        And for an arrival, we seek and return the related departing movement.
        """
        if self.related_movement:
            return self.related_movement
        movement = None
        if self.way == "arriving":
            movement = ShipMovement.objects.filter(
                ship_call_id=self.ship_call_id, way=ships_settings.DEPARTING
            ).first()
        if self.way == "departing":
            movement = ShipMovement.objects.filter(
                ship_call_id=self.ship_call_id, way=ships_settings.ARRIVING
            ).first()
        return movement


class CrewList(
    PortOnShip, FileDownloadOnShip, CrewListBaseModel, ShipBaseModel, TimeStampedModel
):
    class Meta:
        verbose_name = "Liste équipage"
        verbose_name_plural = "Listes équipages"


class PassengerList(
    PortOnShip,
    FileDownloadOnShip,
    PassengerListBaseModel,
    ShipBaseModel,
    TimeStampedModel,
):
    class Meta:
        verbose_name = "Liste passagers"
        verbose_name_plural = "Listes passagers"
