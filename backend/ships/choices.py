from backend.ships import settings as ships_settings

TIMELINE_RECENT = "recent"
TIMELINE_PAST = "past"

TIMELINE_CHOICES = (
    (TIMELINE_RECENT, "Recent"),
    (TIMELINE_PAST, "Past"),
)

TYPE_CRUISE = "cruise"
TYPE_FERRY = "ferry"
TYPE_CARGO = "cargo"
TYPE_OTHER = "other"

TYPES_CHOICES = (
    (TYPE_CRUISE, "Croisère"),
    (TYPE_FERRY, "Ferry"),
    (TYPE_CARGO, "Cargo"),
    (TYPE_OTHER, "Autres"),
)

WAY_ARRIVING = ships_settings.ARRIVING
WAY_DEPARTING = ships_settings.DEPARTING

WAY_CHOICES = (
    (WAY_ARRIVING, "Arrivée"),
    (WAY_DEPARTING, "Départ"),
)
