from django.contrib import admin

from backend.persons.admin import CrewListBaseAdmin, PassengerListBaseAdmin
from backend.ships import admin_filters
from backend.ships import settings as ships_settings
from backend.ships.models import CrewList, PassengerList, ShipMovement


class ShipMovementBaseAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "__str__",
        "last_port",
        "next_port",
        "port_of_call",
        "relevant_port",
        "ship_call_id",
        "eta_to_port_of_call",
        "ata_to_port_of_call",
        "etd_from_port_of_call",
        "atd_from_port_of_call",
        "arrival_time",
        "departure_time",
        "relevant_time",
        "passenger_list_delay_type",
        "crew_list_delay_type",
        "way",
        "geographic_area",
        "created",
        "modified",
    ]
    list_display_links = ["id", "__str__"]
    search_fields = ships_settings.SHIP_BASE_SEARCH_FIELDS
    list_filter = [
        "way",
        "geographic_area",
        "passenger_list_delay_type",
        "crew_list_delay_type",
    ]
    readonly_fields = [
        "arrival_time",
        "departure_time",
        "relevant_time",
        "number_of_crew_parsed",
        "number_of_passengers_parsed",
    ]
    fieldsets = [
        (
            "Navire et escale",
            {
                "fields": [
                    "ship_name",
                    "imo_number",
                    "ship_call_id",
                    "port_of_call",
                    "way",
                    "call_sign",
                    "flag_state_of_ship",
                    "last_port",
                    "next_port",
                    "name_of_master",
                    "registry_location_name",
                    "registry_location_code",
                    "agent_name",
                    "agent_phone",
                    "agent_fax",
                    "agent_email",
                    "gross_tonnage",
                    "net_tonnage",
                    "position_in_port_of_call",
                    "brief_particulars_of_voyage",
                    "number_of_crew",
                    "number_of_passengers",
                    "number_of_crew_parsed",
                    "number_of_passengers_parsed",
                ],
            },
        ),
        (
            "Horaires",
            {
                "fields": [
                    "eta_to_port_of_call",
                    "ata_to_port_of_call",
                    "arrival_time",
                    "etd_from_port_of_call",
                    "atd_from_port_of_call",
                    "departure_time",
                    "relevant_time",
                ],
            },
        ),
        (
            "SIP",
            {
                "fields": [
                    "sip_fal5_available",
                    "sip_fal6_available",
                    "swing_crew_list_id",
                    "swing_passengers_list_id",
                ],
            },
        ),
        (
            "Champs renseignées automatiquement",
            {
                "fields": [
                    "relevant_port",
                    "geographic_area",
                    "passenger_list_delay_type",
                    "crew_list_delay_type",
                    "travel_duration",
                    "h24_before_arrival",
                    "mmsi_number",
                ],
            },
        ),
    ]


@admin.register(ShipMovement)
class ShipMovementAdmin(ShipMovementBaseAdmin):
    list_display = ShipMovementBaseAdmin.list_display + ["display"]
    list_filter = ShipMovementBaseAdmin.list_filter + [
        "display",
        admin_filters.PortOfCallFilter,
        admin_filters.LastPortFilter,
        admin_filters.NextPortFilter,
    ]
    actions = ["set_display_on", "set_display_off"]
    search_fields = ships_settings.SHIP_ADVANCED_SEARCH_FIELDS
    raw_id_fields = [
        "port_of_call_relation",
        "last_port_relation",
        "next_port_relation",
        "relevant_port_relation",
        "ship_type",
        "related_movement",
    ]
    fieldsets = ShipMovementBaseAdmin.fieldsets + [
        (
            "Relations renseignées automatiquement",
            {
                "fields": [
                    "port_of_call_relation",
                    "last_port_relation",
                    "next_port_relation",
                    "relevant_port_relation",
                    "ship_type",
                    "related_movement",
                ],
            },
        ),
        (
            "Affichage",
            {
                "fields": [
                    "display",
                ],
            },
        ),
    ]

    @admin.action(description="Afficher les mouvements sélectionnés")
    def set_display_on(self, request, queryset):
        queryset.update(display=True)

    @admin.action(description="Ne pas afficher les mouvements sélectionnés")
    def set_display_off(self, request, queryset):
        queryset.update(display=False)


@admin.register(CrewList)
class CrewListAdmin(CrewListBaseAdmin, admin.ModelAdmin):
    pass


@admin.register(PassengerList)
class PassengerListAdmin(PassengerListBaseAdmin, admin.ModelAdmin):
    pass
