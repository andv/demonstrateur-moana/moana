from admin_auto_filters.filters import AutocompleteFilter


class PortOfCallFilter(AutocompleteFilter):
    title = "Port of Call"
    field_name = "port_of_call_relation"


class LastPortFilter(AutocompleteFilter):
    title = "Provenance"
    field_name = "last_port_relation"


class NextPortFilter(AutocompleteFilter):
    title = "Destination"
    field_name = "next_port_relation"
