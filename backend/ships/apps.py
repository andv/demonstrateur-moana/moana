from django.apps import AppConfig


class ShipsConfig(AppConfig):
    name = "backend.ships"
    verbose_name = "3.1 Mouvements"

    def ready(self):
        import backend.ships.signals  # noqa
        import data_processing.ship_references.signals  # noqa
