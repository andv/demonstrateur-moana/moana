from rest_framework import serializers

from backend.alerts.ship_relation import AlertsOnShipSerializerMixin
from backend.issues.ship_relation import IssuesOnShipSerializerMixin
from backend.person_checks.ship_relation import PersonCheckOnShipSerializerMixin
from backend.ports.serializers import PortSerializer
from backend.ship_types.serializers import ShipTypeSerializer
from backend.ships.models import ShipMovement
from backend.ships.settings import SHIP_MOVEMENT_API_FIELDS


class ShipMovementSerializer(
    serializers.ModelSerializer,
    AlertsOnShipSerializerMixin,
    IssuesOnShipSerializerMixin,
    PersonCheckOnShipSerializerMixin,
):
    way = serializers.CharField()
    crew_list_file_fpr = serializers.CharField()
    passengers_list_file_fpr = serializers.CharField()
    crew_list_file_roc = serializers.CharField()
    passengers_list_annex_file_roc = serializers.CharField()
    crew_list_annex_file_fpr = serializers.CharField()
    passengers_list_annex_file_fpr = serializers.CharField()
    crew_list_annex_file_roc = serializers.CharField()
    passengers_list_file_roc = serializers.CharField()
    number_of_crew_parsed = serializers.IntegerField()
    number_of_passengers_parsed = serializers.IntegerField()
    has_crew_members = serializers.BooleanField()
    has_passengers = serializers.BooleanField()
    port_of_call_relation = PortSerializer()
    last_port_relation = PortSerializer()
    next_port_relation = PortSerializer()
    ship_type = ShipTypeSerializer()
    geographic_area = serializers.CharField(source="get_geographic_area_display")
    last_port_geographic_area = serializers.CharField(
        source="get_last_port_geographic_area_display"
    )
    next_port_geographic_area = serializers.CharField(
        source="get_next_port_geographic_area_display"
    )
    next_port_name = serializers.CharField()
    port_of_call_name = serializers.CharField()
    last_port_name = serializers.CharField()
    relevant_port_name = serializers.CharField()
    departure_stopover_time = serializers.DateTimeField()
    arrival_stopover_time = serializers.DateTimeField()
    stop_duration = serializers.CharField()
    mmsi_number = serializers.CharField()

    class Meta:
        model = ShipMovement
        fields = SHIP_MOVEMENT_API_FIELDS
