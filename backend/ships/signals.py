from django.db.models.signals import post_save
from django.dispatch import receiver

from backend.ships.models import ShipMovement


def update_ports(ship_movement, related_movement):
    """
    When we know the related movement, we can then update the last/next port information.
    """
    if ship_movement.is_arriving and related_movement:
        ship_movement.next_port = related_movement.next_port
    if ship_movement.is_departing and related_movement:
        ship_movement.last_port = related_movement.last_port
    return ship_movement


@receiver(post_save, sender=ShipMovement)
def link_related_movement(sender, instance, **kwargs):
    """
    A ship movement has a related movement : for a departure that's the corresponding arrival.
    We will link both ways : For a departure, we find the related arrival and links it.
    And for that related arrival, we also have to set it's `related_movement` field to that
    departure.
    """
    post_save_ongoing = getattr(instance, "post_save_ongoing", False)
    if post_save_ongoing:
        return None  # Prevent post save looping on ship instance
    related = instance.get_related_movement()
    if not related:
        return None
    instance.related_movement = related
    instance.post_save_ongoing = True
    instance = update_ports(ship_movement=instance, related_movement=related)
    instance.save()
    related.related_movement = instance
    related.post_save_ongoing = True
    related = update_ports(ship_movement=related, related_movement=instance)
    related.save()
