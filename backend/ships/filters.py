from datetime import datetime, time
from zoneinfo import ZoneInfo

from django.db.models import Q, Count, Case, When
from django.utils.timezone import make_aware

from django_filters import rest_framework as filters
from ships import choices as ships_choices

from backend.alerts import choices as alerts_choices
from backend.person_checks import choices as person_checks_choices
from backend.geo import choices as geo_choices
from backend.ship_types.settings import OTHER_SHIP_TYPES
from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.utils.choice_filters import NoValidationMultipleChoiceFilter


class ShipMovementFilter(filters.FilterSet):
    way = filters.ChoiceFilter(
        label="way",
        choices=ships_choices.WAY_CHOICES,
        method="filter_by_way",
    )
    ship_types = filters.MultipleChoiceFilter(
        label="type",
        choices=ships_choices.TYPES_CHOICES,
        method="filter_by_types",
    )
    ports_of_call = NoValidationMultipleChoiceFilter(
        label="port",
        method="filter_by_ports",
    )
    alerts = filters.MultipleChoiceFilter(
        label="alerts",
        choices=alerts_choices.ALERT_TYPE_CHOICES,
        method="filter_by_alerts",
    )
    last_port = NoValidationMultipleChoiceFilter(
        label="last_port", method="filter_by_last_port"
    )
    local_movements = filters.BooleanFilter(
        label="local_movements", method="filter_local_movements"
    )
    list_to_check_type = filters.MultipleChoiceFilter(
        label="list_to_check_type",
        choices=person_checks_choices.CHECK_TYPE_CHOICES,
        method="filter_by_list_to_check",
    )
    date = filters.DateFromToRangeFilter(label="date", method="filter_by_date")

    class Meta:
        model = ShipMovement
        fields = []

    def filter_by_way(self, queryset, *args, **kwargs):
        """
        Filter by arriving/departing.
        """
        way_param = self.form.cleaned_data["way"]
        queryset = queryset.filter(Q(way=way_param))
        return queryset

    def filter_by_types(self, queryset, *args, **kwargs):
        """
        Filter by ship types.
        """
        ship_types_params = self.form.cleaned_data["ship_types"]
        ship_types = []
        allow_ship_type_null = False
        if ships_choices.TYPE_CARGO in ship_types_params:
            ship_types.append("Cargo")
        if ships_choices.TYPE_FERRY in ship_types_params:
            ship_types.append("Ferry")
        if ships_choices.TYPE_CRUISE in ship_types_params:
            ship_types.append("Croisière")
        if ships_choices.TYPE_OTHER in ship_types_params:
            ship_types.extend(OTHER_SHIP_TYPES)
            allow_ship_type_null = True
        if allow_ship_type_null:
            lookup_parameters = Q(
                Q(ship_type__name_simple__in=ship_types) | Q(ship_type__isnull=True)
            )
        else:
            lookup_parameters = Q(ship_type__name_simple__in=ship_types)
        queryset = queryset.filter(lookup_parameters)
        return queryset

    def filter_by_ports(self, queryset, *args, **kwargs):
        """
        Filter by ports of call.
        """
        ports_of_call_params = self.form.cleaned_data["ports_of_call"]
        ports_list = []
        for port in ports_of_call_params:
            ports_list.append(port)
        ports_of_team = self.request.user.team.ports.filter(locode__in=ports_list)
        queryset = queryset.filter(Q(port_of_call_relation__locode__in=ports_of_team))
        return queryset

    def filter_by_alerts(self, queryset, *args, **kwargs):
        """
        Filter by ship alerts
        """
        alerts_params = self.form.cleaned_data["alerts"]
        queryset = queryset.filter(alert_types__alert_type__in=alerts_params)
        queryset = queryset.filter(
            Q(alerts__team=self.request.user.team)
            | Q(
                alerts__team__isnull=True,
                alert_types__alert_type=alerts_choices.SHIP_COUNTRY_ALERT,
            )
        )
        queryset = queryset.distinct()
        return queryset

    def filter_by_last_port(self, queryset, *args, **kwargs):
        """
        Filter by last port
        """
        last_ports_params = self.form.cleaned_data["last_port"]
        # We creating a list of all the countries locode with an "or" separator
        list_countries_prefixes = "|".join(last_ports_params)
        # So we can create a regex to find all the matching locodes :
        # ^ : starting
        # ({list_countries_prefixes}) : with any of the country in the list
        regex = rf"^({list_countries_prefixes})"
        queryset = queryset.filter(
            last_port_relation__locode__iregex=regex, way=ships_settings.ARRIVING
        )
        return queryset

    def filter_local_movements(self, queryset, *args, **kwargs):
        """
        Filter to exclude local movements
        """
        local_movements_param = self.form.cleaned_data["local_movements"]
        should_exclude_local_movements = local_movements_param is False
        if should_exclude_local_movements:
            area_choices = geo_choices.AREA_ALLOWED_TO_DOWNLOAD_LISTS
            area_choices_keys = dict(area_choices).keys()
            queryset = queryset.filter(geographic_area__in=area_choices_keys)
        return queryset

    def filter_by_date(self, queryset, *args, **kwargs):
        """
        Filter between date range
        """
        user_timezone = self.request.user.team.timezone
        date = self.form.cleaned_data["date"]

        # To handle user's timezone for relevant_time__range,
        # we reconvert dates in naive datetime, combine with time.min or time.max,
        # then make them aware with user's timezone
        start_date = date.start.strftime("%Y-%m-%d")
        start_date = datetime.strptime(start_date, "%Y-%m-%d")
        start_date = datetime.combine(start_date, time.min)
        start_date = make_aware(start_date, timezone=ZoneInfo(user_timezone))

        stop_date = date.stop.strftime("%Y-%m-%d")
        stop_date = datetime.strptime(stop_date, "%Y-%m-%d")
        stop_date = datetime.combine(stop_date, time.max)
        stop_date = make_aware(stop_date, timezone=ZoneInfo(user_timezone))

        queryset = queryset.filter(relevant_time__range=(start_date, stop_date))
        return queryset

    def filter_by_list_to_check(self, queryset, *args, **kwargs):
        """
        Filter by person list of a specific type (FPR and/or ROC) to check

        the number_person_checks__lt filter depends on :
        - how many type of check the user choose (number_of_check_type) and
        - how many list are available for each movement (1 or 2)
        """
        list_to_check_type_params = self.form.cleaned_data["list_to_check_type"]

        if len(list_to_check_type_params) > 1:
            number_of_check_type = 2
        else:
            number_of_check_type = 1

        # To exclude local movements
        area_choices = geo_choices.AREA_ALLOWED_TO_DOWNLOAD_LISTS
        area_choices_keys = dict(area_choices).keys()

        queryset = (
            queryset.annotate(
                number_person_checks=Count(
                    "person_checks",
                    filter=Q(
                        Q(person_checks__check_type__in=list_to_check_type_params)
                        & Q(person_checks__team=self.request.user.team)
                    ),
                )
            )
            .filter(
                number_person_checks__lt=Case(
                    When(
                        Q(passenger_list__isnull=False) & Q(crew_list__isnull=False),
                        then=2 * number_of_check_type,
                    ),
                    When(
                        Q(passenger_list__isnull=True) | Q(crew_list__isnull=True),
                        then=1 * number_of_check_type,
                    ),
                )
            )
            .filter(Q(Q(passenger_list__isnull=False) | Q(crew_list__isnull=False)))
            .filter(geographic_area__in=area_choices_keys)
            .distinct()
        )
        queryset = queryset.distinct()
        return queryset
