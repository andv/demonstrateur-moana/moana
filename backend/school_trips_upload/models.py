from django.core.exceptions import ValidationError
from django.db import models

from backend.file_utils.validations import CSVFileValidationMixin


class UploadFile(models.Model, CSVFileValidationMixin):

    file = models.FileField(upload_to="school-trips-files")

    def clean(self):
        file_obj = self.file
        if not self.file_extension_is_valid(file_obj):
            raise ValidationError("Wrong file extension")
        if not self.file_mime_type_is_valid(file_obj):
            raise ValidationError("Bad mime type")
        if not self.file_size_is_valid(file_obj):
            raise ValidationError("File size is not allowed")

    def __str__(self):
        return self.file.name

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)
