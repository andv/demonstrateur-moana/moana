from django import forms

from backend.school_trips.models import SchoolTrip


class SchoolTripUploadForm(forms.ModelForm):
    file = forms.FileField(required=True)
    check_is_school_trip = forms.BooleanField(required=True)

    class Meta:
        model = SchoolTrip
        fields = [
            "registration_plate",
            "bus_operator",
            "trip_date",
            "contact_info",
            "file",
        ]

        widgets = {
            "trip_date": forms.DateTimeInput(
                attrs={"type": "datetime-local", "class": "fr-input", "required": True}
            ),
            "registration_plate": forms.TextInput(attrs={"class": "fr-input"}),
            "bus_operator": forms.TextInput(attrs={"class": "fr-input"}),
            "contact_info": forms.TextInput(attrs={"class": "fr-input"}),
            "file": forms.FileInput(attrs={"class": "fr-upload"}),
        }
