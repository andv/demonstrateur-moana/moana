from pathlib import Path

parent_dir = Path(__file__).parent

CSV_FILE = f"{parent_dir}/data/school-trip.csv"
CSV_FILE_SUSPICIOUS_CHARS = f"{parent_dir}/data/school-trip-suspicious-characters.csv"
CSV_FILE_BAD_EXTENSION = f"{parent_dir}/data/school-trip.bad"
