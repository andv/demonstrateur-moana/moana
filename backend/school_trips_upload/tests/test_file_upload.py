from django.conf import settings
from django.urls import reverse

import pytest

from backend.school_trips.models import SchoolTrip
from backend.school_trips_upload.tests import settings as school_trips_upload_settings
from backend.unit_tests import utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


@pytest.fixture
def csv_file():
    return open(school_trips_upload_settings.CSV_FILE, "rb")


@pytest.fixture
def csv_file_suspicious_chars():
    return open(school_trips_upload_settings.CSV_FILE_SUSPICIOUS_CHARS, "rb")


@pytest.fixture
def csv_file_bad_extension():
    return open(school_trips_upload_settings.CSV_FILE_BAD_EXTENSION, "rb")


VALID_TRIP_DATA = {
    "registration_plate": "ABC123",
    "bus_operator": "Test Bus Company",
    "trip_date": "2023-03-01T12:00",
    "check_is_school_trip": True,
}


def test_response_is_redirected_on_success(client, csv_file):
    utils.login_school_trips_upload_user(client)
    url = reverse("school-trips-upload")
    post_data = VALID_TRIP_DATA.copy()
    post_data["file"] = csv_file
    response = client.post(url, data=post_data)
    assert response.status_code == 302


def test_can_create_school_trip(client, csv_file):
    utils.login_school_trips_upload_user(client)
    url = reverse("school-trips-upload")
    post_data = VALID_TRIP_DATA.copy()
    post_data["file"] = csv_file
    count_before = SchoolTrip.objects.count()
    client.post(url, data=post_data)
    count_after = SchoolTrip.objects.count()
    assert count_after == count_before + 1


def test_upload_fails_if_not_logged_in(client, csv_file):
    url = reverse("school-trips-upload")
    post_data = VALID_TRIP_DATA.copy()
    post_data["file"] = csv_file
    response = client.post(url, data=post_data)
    assert response.status_code == 302
    assert SchoolTrip.objects.count() == 0


def test_get_error_if_form_missing_file(client):
    utils.login_school_trips_upload_user(client)
    url = reverse("school-trips-upload")
    post_data = VALID_TRIP_DATA.copy()
    post_data["file"] = ""
    response = client.post(url, data=post_data)
    # expecting the form to be displayed again
    assert "form" in str(response.content)


def test_can_not_access_file_upload_if_permission_removed(client, csv_file):
    utils.login_user_without_access_to_school_trips_list_page(client)
    url = reverse("school-trips-upload")
    response = client.post(url, data={"file": csv_file})
    assert response.status_code == 403
    assert SchoolTrip.objects.count() == 0


def test_cells_with_suspicious_chars_are_prefixed(client, csv_file_suspicious_chars):
    utils.login_school_trips_upload_user(client)
    post_data = VALID_TRIP_DATA.copy()
    post_data["file"] = csv_file_suspicious_chars
    url = reverse("school-trips-upload")
    client.post(url, data=post_data)
    school_trip = SchoolTrip.objects.first()
    assert "'@Martin" in str(school_trip.file_content)
    assert "'+Garcia" in str(school_trip.file_content)
    assert "'-Johnson" in str(school_trip.file_content)
    assert "'=Johnson" in str(school_trip.file_content)
    assert "'%Johnson" in str(school_trip.file_content)


def test_file_with_bad_extension_does_not_create_school_trip(
    client, csv_file_bad_extension
):
    utils.login_school_trips_upload_user(client)
    url = reverse("school-trips-upload")
    post_data = VALID_TRIP_DATA.copy()
    post_data["file"] = csv_file_bad_extension
    count_before = SchoolTrip.objects.count()
    client.post(url, data=post_data)
    count_after = SchoolTrip.objects.count()
    assert count_after == count_before
