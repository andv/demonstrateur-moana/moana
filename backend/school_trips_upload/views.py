from io import BytesIO, StringIO

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.exceptions import ValidationError
from django.urls import reverse_lazy
from django.views.generic import FormView, TemplateView

from defusedcsv import csv
from sentry_sdk import capture_exception

from backend.logs.actions import ActionLogHandler
from backend.school_trips.models import SchoolTrip
from backend.school_trips_upload import settings as school_trips_upload_settings
from backend.school_trips_upload.forms import SchoolTripUploadForm
from backend.school_trips_upload.models import UploadFile


class SchoolTripUploadView(
    UserPassesTestMixin, LoginRequiredMixin, ActionLogHandler, FormView
):
    template_name = "school_trips_upload/upload.html"
    form_class = SchoolTripUploadForm
    success_url = reverse_lazy("school-trips-success")
    login_url = reverse_lazy("school-trips-login")

    def test_func(self):
        team = getattr(self.request.user, "team", None)
        return team and team.access_school_trips_upload_page

    def log_action(self, action, user, object, data=""):
        self.create_user_action_log(action=action, user=user, object=object, data=data)

    def get_csv_file_content(self, file_obj):
        """
        Clean and secure CSV file received
        """
        file_bytes = file_obj.read()
        file_content = str(file_bytes, encoding="utf-8")
        file_rows = file_content.split("\n")
        csv_file = StringIO()
        writer = csv.writer(
            csv_file
        )  # To secure the uploaded file, we need to copy its data to a new csv file created with defusedcsv
        for index, row in enumerate(file_rows):
            if index != 0:  # Strip the header row
                data = row.split(",")
                writer.writerow(data)
        csv_content = csv_file.getvalue()
        # The file content is expected as bytes-like object, so we convert it here:
        file_content = BytesIO()
        file_content.write(csv_content.encode("utf-8"))
        return file_content.getvalue()

    def form_valid(self, form):
        file_obj = form.cleaned_data["file"]
        try:
            upload_file = UploadFile(file=file_obj)
            upload_file.save()
        except ValidationError as e:
            capture_exception(e)
            for msg in e.messages:
                messages.error(self.request, msg)
            return super().form_invalid(form)
        stripped_content = self.get_csv_file_content(file_obj=upload_file.file)
        trip = SchoolTrip.objects.create(
            registration_plate=form.cleaned_data.get("registration_plate", ""),
            bus_operator=form.cleaned_data.get("bus_operator", ""),
            contact_info=form.cleaned_data.get("contact_info", ""),
            trip_date=form.cleaned_data.get("trip_date", None),
        )
        trip.file_content = stripped_content
        trip.save()
        upload_file.delete()
        log_name = school_trips_upload_settings.SCHOOL_TRIPS_FILE_UPLOAD_LOG_ACTION
        log_data = {
            "registration_plate": trip.registration_plate,
            "bus_operator": trip.bus_operator,
            "contact_info": trip.contact_info,
            "trip_date": trip.trip_date.isoformat(),
        }
        self.log_action(
            action=log_name,
            user=self.request.user,
            object=trip.registration_plate,
            data=log_data,
        )
        return super().form_valid(form)


class SchoolTripSuccessView(TemplateView):
    template_name = "school_trips_upload/success.html"


success_view = SchoolTripSuccessView.as_view()
upload_view = SchoolTripUploadView.as_view()
