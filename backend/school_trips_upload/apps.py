from django.apps import AppConfig


class SchoolTripsUploadConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.school_trips_upload"
    verbose_name = "Voyages scolaires formulaire"
