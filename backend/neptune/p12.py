import os
from pathlib import Path

from cryptography import x509
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.serialization import pkcs12


def p12_to_pem(password, p12_path, key_file_path=None, cert_file_path=None):
    """
    Convert a .p12 PKCS#12 file to PEM format.
    Given the following args:
        password: The password for the p12 file.
        p12_path: The path to p12 file.
        key_file_path: Optional path to save the private key file.
        cert_file_path: Optional path to save the certificate file.

    """
    p12_dir = Path(p12_path).parent
    with open(p12_path, "rb") as f:
        key, cert, additional_certificates = pkcs12.load_key_and_certificates(
            f.read(), password.encode("utf-8")
        )
    # Generate private key file
    private_key_pem = key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.PKCS8,
        encryption_algorithm=serialization.NoEncryption(),
    )
    if not key_file_path:
        key_file_path = p12_dir / "key.pem"
    with open(key_file_path, "wb") as key_file:
        key_file.write(private_key_pem)
    # Generate public key file
    public_key_pem = key.public_key().public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo,
    )
    public_key_file_path = Path(key_file_path).with_suffix(".pub")
    with open(public_key_file_path, "wb") as public_key_file:
        public_key_file.write(public_key_pem)
    # Generate certificate file
    cert_pem = cert.public_bytes(encoding=serialization.Encoding.PEM)
    if not cert_file_path:
        cert_file_path = p12_dir / "cert.pem"
    with open(cert_file_path, "wb") as cert_file:
        cert_file.write(cert_pem)
