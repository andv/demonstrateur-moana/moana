from django.contrib import admin

from filesify.admin import FilesifyAdmin

from backend.neptune.models import NeptuneCertificate, NeptunePrivateKey


@admin.register(NeptunePrivateKey)
class NeptunePrivateKeyAdmin(FilesifyAdmin):
    pass


@admin.register(NeptuneCertificate)
class NeptuneCertificate(FilesifyAdmin):
    pass


NEPTUNE_API_ADMIN_FIELDSETS = (
    ("Accès API Neptune", {"fields": ["private_key", "certificate"]}),
    ("Requête à l'API Neptune", {"fields": ["days_before", "days_ahead"]}),
)
