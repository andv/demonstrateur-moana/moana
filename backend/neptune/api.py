import logging
from zoneinfo import ZoneInfo

from django.utils import timezone

import requests
from dateutil import parser
from defusedxml import ElementTree as ET

logger = logging.getLogger(__name__)


class NeptuneAPIClient:
    def __init__(self, cert_path, key_path, base_url):
        self.cert_path = cert_path
        self.key_path = key_path
        self.base_url = base_url
        self.ns = {
            "soap": "http://schemas.xmlsoap.org/soap/envelope/",
            "ns2": "http://neptune.marseille.port.fr/ws/escale",
        }
        self.session = self.create_session()

    def create_session(self):
        session = requests.Session()
        session.cert = (self.cert_path, self.key_path)
        session.verify = True
        return session

    def send_request(self, request_data):
        url = f"{self.base_url}"
        headers = {
            "Content-Type": "text/xml; charset=utf-8",
        }
        response = self.session.post(url, data=request_data, headers=headers)
        return response

    def wrap_in_envelope(self, wrapped_data):
        data = f"""
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:esc="http://neptune.marseille.port.fr/ws/escale">
        <soapenv:Body>
            {wrapped_data}
        </soapenv:Body>
        </soapenv:Envelope>"""
        return data

    def format_datetime(self, datetime_str):
        """
        Return a timezone-aware datetime object from the given datetime string.
        """
        if not datetime_str:
            return None
        dt_obj = parser.parse(datetime_str)
        dt_paris = timezone.make_aware(dt_obj, timezone=ZoneInfo("Europe/Paris"))
        return dt_paris

    def get_element(self, raw_data, element_name):
        """
        Helper function to safely find an element and return its text content.
        If the element is not found, it returns an empty string.
        """
        found_element = raw_data.find(element_name, self.ns)
        if found_element is not None and found_element.text is not None:
            return found_element.text
        return ""

    def get_date_element(self, raw_data, element_name):
        """
        Find an element and apply date formatting.
        """
        found_element = self.get_element(raw_data, element_name)
        if found_element:
            return self.format_datetime(found_element)
        return None

    def get_list_escales(self, search_type, start_date, end_date):
        method_data = f"""
                <getListEscalesRequest>
                    <typeRecherche>{search_type}</typeRecherche>
                    <dateDebut>{start_date}</dateDebut>
                    <dateFin>{end_date}</dateFin>
                </getListEscalesRequest>
        """
        request_data = self.wrap_in_envelope(method_data)
        response = self.send_request(request_data)
        return self.parse_list_escales(response.text)

    def parse_list_escales(self, response):
        root = ET.fromstring(response)
        body = root.find("soap:Body", self.ns)
        api_response = body.find("ns2:getListeEscalesResponse", self.ns)
        if not api_response:
            logger.error("`getListeEscalesResponse` not found in Neptune API response")
            return []
        escales = api_response.find("listeEscales")
        if not escales:
            logger.error("`listeEscales` not fond in Neptune API response")
            return []
        parsed_escales = []
        for escale in escales.findall("Escale"):
            parsed_escale = self.parse_escale(escale)
            parsed_escales.append(parsed_escale)
        return parsed_escales

    def parse_escale(self, escale):
        parsed = {}
        root_element_data = self.get_root_elements(escale)
        parsed.update(root_element_data)
        root_date_elements_data = self.get_root_date_elements(escale)
        parsed.update(root_date_elements_data)
        porteur_data = self.get_porteur(escale)
        parsed.update(porteur_data)
        sejours_data = self.get_sejours(escale)
        parsed.update(sejours_data)
        return parsed

    def get_root_elements(self, escale):
        data = {}
        elements_to_parse = [
            "numeroAnnonce",
            "numeroEscale",
            "statutEscale",
            "numeroEscaleFacturation",
            "installationPortuaire",
            "libelleTiersAgent",
            "locodeProvenance",
            "locodeDestination",
            "locodeArrivee",
        ]

        for element in elements_to_parse:
            data[element] = self.get_element(escale, element)
        return data

    def get_root_date_elements(self, escale):
        data = {}
        date_elements_to_parse = [
            "dateArrivee",
            "dateDepart",
            "dateETA",
            "dateETD",
        ]
        for element in date_elements_to_parse:
            data[element] = self.get_date_element(escale, element)
        return data

    def get_porteur(self, escale):
        porteur = escale.find("Porteur", self.ns)
        if not porteur:
            return {}
        data = self.parse_porteur(porteur)
        return data

    def parse_porteur(self, porteur):
        elements_to_parse = [
            "codeOMI",
            "nom",
            "pavillon",
            "categorie",
        ]
        parsed = {}
        for element in elements_to_parse:
            parsed[element] = self.get_element(porteur, element)
        return parsed

    def get_sejours(self, escale):
        sejours = escale.findall("Sejour", self.ns)
        if not sejours:
            return {}
        data = self.parse_sejours(sejours)
        return data

    def parse_sejours(self, sejours):
        elements_to_parse = [
            "id",
            "paqAttribue",
            "nomTerminal",
        ]
        sejour_list = []
        for sejour in sejours:
            status = self.get_element(sejour, "statutSejour")
            if status == "INVALIDE":
                continue
            parsed = {}
            parsed["statutSejour"] = status
            for element in elements_to_parse:
                parsed[element] = self.get_element(sejour, element)
                parsed["dateAppareillage"] = self.get_date_element(
                    sejour, "dateAppareillage"
                )
                parsed["dateAccostage"] = self.get_date_element(sejour, "dateAccostage")
            sejour_list.append(parsed)
        data = {"sejours": sejour_list}
        return data
