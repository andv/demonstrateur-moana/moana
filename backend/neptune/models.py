from django.db import models

from filesify.models import CryptoFilesify


class NeptunePrivateKey(CryptoFilesify):
    class Meta:
        verbose_name = "Neptune clé privée"
        verbose_name_plural = "Neptune clés privées"


class NeptuneCertificate(CryptoFilesify):
    class Meta:
        verbose_name = "Neptune certificat PEM"
        verbose_name_plural = "Neptune certificats PEM"


class NeptuneBaseModel(models.Model):
    url = models.CharField("url", max_length=256)
    days_before = models.PositiveIntegerField(
        verbose_name="Jours avant",
        blank=True,
        null=True,
        default=1,
        help_text=(
            "Combien de jours avant aujourd'hui faut il considérer pour "
            "les départs/arrivées ? Par défaut 1."
        ),
    )
    days_ahead = models.PositiveIntegerField(
        verbose_name="Jours après",
        blank=True,
        null=True,
        default=1,
        help_text=(
            "Combien de jours après aujourd'hui faut-il considérer pour "
            "les départs/arrivées ? Par défaut 1."
        ),
    )
    private_key = models.ForeignKey(
        to="neptune.NeptunePrivateKey",
        verbose_name="clé privée",
        related_name="sip_neptune",
        on_delete=models.CASCADE,
    )
    certificate = models.ForeignKey(
        to="neptune.NeptuneCertificate",
        verbose_name="certificat pem",
        related_name="sip_neptune",
        on_delete=models.CASCADE,
    )

    class Meta:
        abstract = True
