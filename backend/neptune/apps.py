from django.apps import AppConfig

from filesify.mixins import FilesifyPostMigrateMixin


class NeptuneConfig(FilesifyPostMigrateMixin, AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.neptune"
    verbose_name = "5.2 SIP - Neptune Connexion API"
    filesify_limit_to_models = (
        "neptune.NeptunePrivateKey",
        "neptune.NeptuneCertificate",
    )
