from django.conf import settings
from django.shortcuts import reverse

import pytest

from backend.person_checks import choices as person_checks_choices
from backend.person_checks.models import PersonCheck
from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def initialize_test(client, person_list_type, check_type):
    user = utils.login_saint_malo_user(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    ship = ShipMovement.objects.create(
        way=ships_settings.ARRIVING, port_of_call_relation=saint_malo
    )
    assert not PersonCheck.objects.filter(
        ship_movement=ship,
        team=user.team,
        person_list_type=person_list_type,
        check_type=check_type,
    ).exists()
    return ship, user


##############
# Passengers #
##############


def test_can_mark_passengers_has_checked_for_fpr(client):
    ship, user = initialize_test(
        client,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    )
    url = reverse("ships-mark-as-checked", args=[ship.pk])
    client.post(
        url,
        data={
            "check_type": person_checks_choices.CHECK_TYPE_FPR,
            "passengers_checked": True,
            "passengers_check_result": person_checks_choices.CHECK_RESULT_HIT,
        },
    )
    assert PersonCheck.objects.filter(
        ship_movement=ship,
        team=user.team,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    ).exists()


def test_can_mark_passengers_has_checked_with_hit_for_fpr(client):
    ship, user = initialize_test(
        client,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    )
    url = reverse("ships-mark-as-checked", args=[ship.pk])
    post_data = {
        "passengers_checked": True,
        "passengers_check_result": person_checks_choices.CHECK_RESULT_HIT,
        "check_type": person_checks_choices.CHECK_TYPE_FPR,
    }
    client.post(url, data=post_data)
    person_check = PersonCheck.objects.get(
        ship_movement=ship,
        team=user.team,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    )
    assert person_check.check_result == person_checks_choices.CHECK_RESULT_HIT


def test_passengers_check_result_is_cleared_if_marked_as_unchecked_for_fpr(client):
    ship, user = initialize_test(
        client,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    )
    url = reverse("ships-mark-as-checked", args=[ship.pk])
    post_data = {
        "passengers_checked": False,
        "check_type": person_checks_choices.CHECK_TYPE_FPR,
        "passengers_fpr_result": "ignore-this",
    }
    client.post(url, data=post_data)
    assert not PersonCheck.objects.filter(
        ship_movement=ship,
        team=user.team,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    ).exists()


def test_can_mark_passengers_as_not_checked_for_fpr(client):
    ship, user = initialize_test(
        client,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    )
    url = reverse("ships-mark-as-checked", args=[ship.pk])
    post_data = {
        "check_type": person_checks_choices.CHECK_TYPE_FPR,
        "passengers_checked": True,
        "passengers_check_result": person_checks_choices.CHECK_RESULT_HIT,
    }
    client.post(url, data=post_data)
    assert PersonCheck.objects.filter(
        ship_movement=ship,
        team=user.team,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    ).exists()
    url = reverse("ships-mark-as-checked", args=[ship.pk])
    client.post(
        url,
        data={
            "check_type": person_checks_choices.CHECK_TYPE_FPR,
            "passengers_checked": False,
        },
    )
    assert not PersonCheck.objects.filter(
        ship_movement=ship,
        team=user.team,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    ).exists()


def test_passengers_checked_marker_is_unchanged_if_no_data_for_fpr(client):
    ship, user = initialize_test(
        client,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    )
    url = reverse("ships-mark-as-checked", args=[ship.pk])
    count_before = PersonCheck.objects.all().count()
    client.post(url, data={})
    count_after = PersonCheck.objects.all().count()
    assert count_before == count_after


########
# Crew #
########


def test_can_mark_crew_has_checked_for_fpr(client):
    ship, user = initialize_test(
        client,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    )
    url = reverse("ships-mark-as-checked", args=[ship.pk])
    client.post(
        url,
        data={
            "check_type": person_checks_choices.CHECK_TYPE_FPR,
            "crew_checked": True,
            "crew_check_result": person_checks_choices.CHECK_RESULT_NO_HIT,
        },
    )
    assert PersonCheck.objects.filter(
        ship_movement=ship,
        team=user.team,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    ).exists()


def test_can_mark_crew_has_checked_with_hit_for_fpr(client):
    ship, user = initialize_test(
        client,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    )
    url = reverse("ships-mark-as-checked", args=[ship.pk])
    post_data = {
        "check_type": person_checks_choices.CHECK_TYPE_FPR,
        "crew_checked": True,
        "crew_check_result": person_checks_choices.CHECK_RESULT_HIT,
    }
    client.post(url, data=post_data)
    person_check = PersonCheck.objects.get(
        ship_movement=ship,
        team=user.team,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    )
    assert person_check.check_result == person_checks_choices.CHECK_RESULT_HIT


def test_crew_check_result_is_cleared_if_marked_as_unchecked_for_fpr(client):
    ship, user = initialize_test(
        client,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    )
    url = reverse("ships-mark-as-checked", args=[ship.pk])
    post_data = {
        "check_type": person_checks_choices.CHECK_TYPE_FPR,
        "crew_checked": False,
        "crew_fpr_result": "ignore-this",
    }
    client.post(url, data=post_data)
    assert not PersonCheck.objects.filter(
        ship_movement=ship,
        team=user.team,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    ).exists()


def test_can_mark_crew_as_not_checked_for_fpr(client):
    ship, user = initialize_test(
        client,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    )
    url = reverse("ships-mark-as-checked", args=[ship.pk])
    post_data = {
        "crew_checked": True,
        "crew_check_result": person_checks_choices.CHECK_RESULT_HIT,
        "check_type": person_checks_choices.CHECK_TYPE_FPR,
    }
    client.post(url, data=post_data)
    assert PersonCheck.objects.filter(
        ship_movement=ship,
        team=user.team,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    ).exists()
    url = reverse("ships-mark-as-checked", args=[ship.pk])
    client.post(
        url,
        data={
            "check_type": person_checks_choices.CHECK_TYPE_FPR,
            "crew_checked": False,
        },
    )
    assert not PersonCheck.objects.filter(
        ship_movement=ship,
        team=user.team,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    ).exists()


def test_crew_checked_marker_is_unchanged_if_no_data_for_fpr(client):
    ship, user = initialize_test(
        client,
        person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
        check_type=person_checks_choices.CHECK_TYPE_FPR,
    )
    url = reverse("ships-mark-as-checked", args=[ship.pk])
    count_before = PersonCheck.objects.all().count()
    client.post(url, data={})
    count_after = PersonCheck.objects.all().count()
    assert count_before == count_after
