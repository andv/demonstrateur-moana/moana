from django.db import models

from model_utils.models import TimeStampedModel

from backend.person_checks import choices as person_checks_choices


class PersonCheck(TimeStampedModel):
    team = models.ForeignKey(
        to="accounts.Team",
        verbose_name="équipe",
        related_name="person_checks",
        help_text=("L'équipe pour laquelle la vérification a été faite."),
        on_delete=models.CASCADE,
    )
    ship_movement = models.ForeignKey(
        to="ships.ShipMovement",
        verbose_name="mouvement de navire",
        related_name="person_checks",
        on_delete=models.CASCADE,
    )
    check_type = models.CharField(
        "type de vérification",
        max_length=256,
        choices=person_checks_choices.CHECK_TYPE_CHOICES,
    )
    person_list_type = models.CharField(
        "type de liste",
        max_length=256,
        choices=person_checks_choices.PERSON_LIST_TYPE_CHOICES,
    )
    check_result = models.CharField(
        "résultat de la vérification",
        max_length=256,
        choices=person_checks_choices.CHECK_RESULT_CHOICES,
    )
    details = models.TextField(
        verbose_name="commentaire de l'utilisateur sur le traitement effectué",
        blank=True,
    )

    class Meta:
        verbose_name = "vérification des listes"
        verbose_name_plural = "vérifications des listes"
        constraints = [
            models.UniqueConstraint(
                fields=["team", "ship_movement", "check_type", "person_list_type"],
                name="unique person check",
            )
        ]

    def __str__(self):
        return (
            f"{self.get_check_type_display()} {self.get_person_list_type_display()} "
            f"{self.get_check_result_display()} "
            f"- équipe {self.team} - {self.ship_movement}"
        )
