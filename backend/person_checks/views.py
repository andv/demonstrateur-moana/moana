from rest_framework import decorators, status
from rest_framework.response import Response

from backend.logs.actions import ActionLogHandler
from backend.person_checks import choices as person_checks_choices
from backend.person_checks.models import PersonCheck
from backend.person_checks.serializers import MarkAsCheckedSerializer


class PersonCheckViewMixin(ActionLogHandler):
    """
    View mixin that's expected to be used in a ship movement API view
    and adds functionalities around FPR/ROC person checks.
    """

    def log_check_action(
        self, ship_movement, action_type, list_type, check_type, details
    ):
        self.create_user_action_log(
            action=f"marked {check_type} {list_type} as {action_type}",
            user=self.request.user,
            object=list_type,
            target=ship_movement,
            ship=ship_movement,
            data=details,
        )

    def handle_check_action(
        self, ship, list_type, check_type, is_checked, check_result, details
    ):
        action_type = "checked" if is_checked else "unchecked"
        lookup_parameters = {
            "ship_movement": ship,
            "team": self.request.user.team,
            "person_list_type": list_type,
            "check_type": check_type,
        }
        if is_checked is False:
            PersonCheck.objects.filter(**lookup_parameters).delete()
        else:
            person_check, created = PersonCheck.objects.get_or_create(
                **lookup_parameters,
                defaults={"check_result": check_result, "details": details},
            )
            if not created:
                person_check.check_result = check_result
                person_check.details = details
                person_check.save()
        self.log_check_action(
            ship,
            action_type=action_type,
            list_type=list_type,
            check_type=check_type,
            details=details,
        )
        if check_result:
            self.log_check_action(
                ship,
                action_type=check_result,
                list_type=list_type,
                check_type=check_type,
                details=details,
            )

    @decorators.action(detail=True, methods=["POST"], url_path="mark-as-checked")
    def mark_as_checked(self, request, pk=None):
        """
        Mark passengers or crew list as checked
        """
        ship = self.get_object()
        serializer = MarkAsCheckedSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        check_type = serializer.data.get("check_type")
        passengers_checked = serializer.data.get("passengers_checked")
        passengers_check_result = serializer.data.get("passengers_check_result", "")
        passengers_details = request.data.get("passengers_details", "")
        crew_checked = serializer.data.get("crew_checked")
        crew_check_result = serializer.data.get("crew_check_result", "")
        crew_details = request.data.get("crew_details", "")
        if passengers_checked is not None:
            if passengers_checked and not passengers_check_result:
                return Response(
                    "Missing parameter passengers_check_result",
                    status=status.HTTP_400_BAD_REQUEST,
                )
            self.handle_check_action(
                ship=ship,
                list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
                check_type=check_type,
                is_checked=passengers_checked,
                check_result=passengers_check_result,
                details=passengers_details,
            )
        if crew_checked is not None:
            if crew_checked and not crew_check_result:
                return Response(
                    "Missing parameter crew_check_result",
                    status=status.HTTP_400_BAD_REQUEST,
                )
            self.handle_check_action(
                ship=ship,
                list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
                check_type=check_type,
                is_checked=crew_checked,
                check_result=crew_check_result,
                details=crew_details,
            )
        response_data = {
            "passengers_checked": passengers_checked,
            "passengers_check_result": passengers_check_result,
            "passengers_details": passengers_details,
            "crew_checked": crew_checked,
            "crew_check_result": crew_check_result,
            "crew_details": crew_details,
        }
        return Response(response_data)
