from django.contrib import admin

from backend.person_checks.models import PersonCheck


@admin.register(PersonCheck)
class PersonCheckAdmin(admin.ModelAdmin):
    list_display = [
        "team",
        "ship_movement",
        "check_type",
        "person_list_type",
        "check_result",
        "created",
        "modified",
    ]
    search_fields = (
        "team__name",
        "team__ports__locode",
        "team__ports__name",
        "ship_movement__imo_number",
        "ship_movement__ship_name",
        "ship_movement__port_of_call",
        "ship_movement__ship_call_id",
    )
    raw_id_fields = ("ship_movement",)
    list_filter = ("team", "check_type", "person_list_type", "check_result")
    date_hierarchy = "created"
