from django.db.models import Exists, OuterRef, Subquery

from rest_framework import serializers

from backend.person_checks import choices as person_checks_choices
from backend.person_checks.models import PersonCheck

##################
# Serializers    #
##################


class PersonCheckOnShipSerializerMixin(serializers.Serializer):
    """
    This mixin class is expected to be sub-classed on a serializer for ShipMovement model.
    """

    fpr_passengers_checked = serializers.BooleanField()
    fpr_crew_checked = serializers.BooleanField()
    fpr_passengers_check_result = serializers.CharField()
    fpr_crew_check_result = serializers.CharField()
    roc_passengers_checked = serializers.BooleanField()
    roc_crew_checked = serializers.BooleanField()
    roc_passengers_check_result = serializers.CharField()
    roc_crew_check_result = serializers.CharField()
    fpr_crew_details = serializers.CharField()
    fpr_passengers_details = serializers.CharField()
    roc_crew_details = serializers.CharField()
    roc_passengers_details = serializers.CharField()


##################
# Views          #
##################


class PersonCheckOnShipViewMixin:
    """
    This mixin class is expected to be used on a API list view that handles ShipMovement.
    This mixin is to be used together with the mixin that provides current user helpers.
    """

    def annotate_fpr_checks(self, ship_queryset):
        """
        On the given ship movement queryset, we annotate some fields that tells
        concerning FPR checks:
        - Whether the checkboxes where checked or not.
        - The result of the checks: Hit / No Hit.
        """
        qs_fpr_passengers = PersonCheck.objects.filter(
            ship_movement=OuterRef("pk"),
            team=self.current_team,
            person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
            check_type=person_checks_choices.CHECK_TYPE_FPR,
        )
        qs_fpr_crew = PersonCheck.objects.filter(
            ship_movement=OuterRef("pk"),
            team=self.current_team,
            person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
            check_type=person_checks_choices.CHECK_TYPE_FPR,
        )
        qs = ship_queryset.annotate(
            fpr_passengers_checked=Exists(qs_fpr_passengers),
            fpr_crew_checked=Exists(qs_fpr_crew),
            fpr_passengers_check_result=Subquery(
                qs_fpr_passengers.values("check_result")[:1]
            ),
            fpr_crew_check_result=Subquery(qs_fpr_crew.values("check_result")[:1]),
            fpr_passengers_details=Subquery(qs_fpr_passengers.values("details")[:1]),
            fpr_crew_details=Subquery(qs_fpr_crew.values("details")[:1]),
        )
        return qs

    def annotate_roc_checks(self, ship_queryset):
        """
        On the given ship movement queryset, we annotate some fields that tells
        concerning ROC checks:
        - Whether the checkboxes where checked or not.
        - The result of the checks: Hit / No Hit.
        """
        qs_roc_passengers = PersonCheck.objects.filter(
            ship_movement=OuterRef("pk"),
            team=self.current_team,
            person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
            check_type=person_checks_choices.CHECK_TYPE_ROC,
        )
        qs_roc_crew = PersonCheck.objects.filter(
            ship_movement=OuterRef("pk"),
            team=self.current_team,
            person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
            check_type=person_checks_choices.CHECK_TYPE_ROC,
        )
        qs = ship_queryset.annotate(
            roc_passengers_checked=Exists(qs_roc_passengers),
            roc_crew_checked=Exists(qs_roc_crew),
            roc_passengers_check_result=Subquery(
                qs_roc_passengers.values("check_result")[:1]
            ),
            roc_crew_check_result=Subquery(qs_roc_crew.values("check_result")[:1]),
            roc_passengers_details=Subquery(qs_roc_passengers.values("details")[:1]),
            roc_crew_details=Subquery(qs_roc_crew.values("details")[:1]),
        )
        return qs
