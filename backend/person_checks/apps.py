from django.apps import AppConfig


class PersonChecksConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.person_checks"
    verbose_name = "3.3 Mouvements - Vérification des listes"
