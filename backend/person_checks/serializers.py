from rest_framework import serializers

from backend.person_checks import choices as person_checks_choices


class MarkAsCheckedSerializer(serializers.Serializer):
    passengers_check_result = serializers.ChoiceField(
        choices=person_checks_choices.CHECK_RESULT_CHOICES,
        required=False,
        allow_null=True,
    )
    crew_check_result = serializers.ChoiceField(
        choices=person_checks_choices.CHECK_RESULT_CHOICES,
        required=False,
        allow_null=True,
    )
    passengers_checked = serializers.BooleanField(required=False)
    crew_checked = serializers.BooleanField(required=False)
    check_type = serializers.ChoiceField(
        choices=person_checks_choices.CHECK_TYPE_CHOICES
    )
