import sys
from os import environ
from pathlib import Path

from celery import Celery

environ.setdefault("DJANGO_SETTINGS_MODULE", "backend.settings.prod")

from django.conf import settings  # noqa

# Django apps live in a subfolder that needs to be added to sys.path
backend_dir = Path.cwd().joinpath("backend")
sys.path.append(str(backend_dir))

app = Celery("moana")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(bind=True)
def celery_debug_task(self):
    print("This is a debug task to verify that Celery works")
