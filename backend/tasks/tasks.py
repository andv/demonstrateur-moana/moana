import logging
from smtplib import SMTPException

from django.conf import settings
from django.core import management
from django.core.mail import send_mail
from django.utils.html import strip_tags

from sentry_sdk import capture_exception

from backend.tasks.celery import app
from backend.utils.sentry import sentry_capture

logger = logging.getLogger(__name__)


@app.task
def task_email_import_fal():
    """Import FAL files from mailbox."""
    logger.info("Starting import email task")
    management.call_command("email_import_fal")


@sentry_capture
@app.task
def task_send_alert_notification(self, html_message, team, ship, alert):
    try:
        send_mail(
            subject="Info Moana - Alerte déclenchée sur un navire",
            message=strip_tags(html_message),
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[team.notification_email],
            html_message=html_message,
            fail_silently=False,
        )
    except SMTPException as e:
        capture_exception(e)
    else:
        self.create_action_log(
            action="send alert notification",
            actor="moana-application",
            ship=ship,
            object=f"Ref {ship.ship_call_id}",
            target=team.name,
            description=alert,
        )
