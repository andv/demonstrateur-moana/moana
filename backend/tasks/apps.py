from django.apps import AppConfig


class TasksConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.tasks"

    def ready(self):
        # Make sure celery is always imported when Django starts.
        # And rename app in django admin
        from backend.tasks.celery import app as celery_app  # noqa
        from django_celery_beat.apps import BeatConfig

        BeatConfig.verbose_name = "7.1 Système - Tâches périodiques"

        __all__ = ("celery_app",)  # noqa
