from django.core.management.base import BaseCommand

from backend.logs import actions as logs_actions
from backend.logs.models import NcaPortShipData
from backend.nca_port.parsers import NcaPortParser
from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement


class Command(logs_actions.DataQualityLogLogHandler, BaseCommand):
    """
    Import NCA Port file.
    """

    parser_class = NcaPortParser
    help = "Import NCA Port files"

    def add_arguments(self, parser):
        parser.add_argument("file_path")

    def handle_ship_movements(self, nca_port_data):
        ship_data_copy = nca_port_data.copy()
        ship_call_id = ship_data_copy["ship_call_id"]
        movement_exist = NcaPortShipData.objects.filter(
            ship_call_id=ship_call_id
        ).exists()
        if movement_exist:
            return None
        ship = NcaPortShipData.objects.create(**ship_data_copy)
        ship.save()
        self.stdout.write(f"---- tracking DB: created ship movement: {ship}")

    def handle(self, *args, **options):
        file_path = options["file_path"]
        self.parser = self.parser_class(file_path)
        nca_port_data = self.parser.get_parsed_data()
        self.handle_ship_movements(nca_port_data)
        self.stdout.write(f"Successfully imported {file_path}", self.style.SUCCESS)
