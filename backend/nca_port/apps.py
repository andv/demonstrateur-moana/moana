from django.apps import AppConfig


class NcaPortConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.nca_port"
