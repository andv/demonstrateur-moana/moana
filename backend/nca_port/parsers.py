from backend.nca.parsers import NcaParser
from backend.ship_files import settings as ship_files_settings


class NcaPortParser(NcaParser):
    data = None  # NCA Port data

    def __init__(self, file_path):
        super().__init__(file_path)
        self.name_formatting_diff_list = []
        self.vessel_identification = self.body.find("VesselIdentification")
        self.ship_call_information = self.body.find("ShipCallInformation")
        self.voyage_information = self.body.find("VoyageInformation")

    def get_parsed_data(self):
        if self.file_type != ship_files_settings.NCA_PORT:
            return None
        data = {}
        data["imo_number"] = self.vessel_identification.get("IMONumber")
        data["ship_call_id"] = self.ship_call_information.get("ShipCallId")
        data["port_of_call"] = self.ship_call_information.get("PortOfCall")
        data["last_port"] = self.voyage_information.get("LastPort", "")
        data["next_port"] = self.voyage_information.get("NextPort", "")
        eta = self.format_datetime(self.voyage_information.get("ETAToPortOfCall"))
        etd = self.format_datetime(self.voyage_information.get("ETDFromPortOfCall"))
        data["eta_to_port_of_call"] = eta
        data["etd_from_port_of_call"] = etd
        data["position_in_port_of_call"] = self.voyage_information.get(
            "PositionInPortOfCall", ""
        )
        self.data = data
        return data
