from django.db import models
from django.utils.text import slugify

import magic

from backend.persons import choices as persons_choices


class FileDownloadOnShip(models.Model):
    """
    This abstract class is expected to be sub-classed on a ShipMovement-like model.
    It provides FPR/ROC helpers.
    """

    class Meta:
        abstract = True

    def get_file_type(self, file_content):
        """
        Return the file mime type, Looking at the file header.
        """
        if not file_content:
            return None
        # We cast to bytes because SQLite and Postgres store BinaryData differently:
        file_content = bytes(file_content)
        mime_type = magic.from_buffer(file_content[:2048], mime=True)
        return mime_type

    def get_file_extension(self, file_format, is_annex):
        """
        Return the file extension, based on the mime type of the file content.
        """
        file_content = self.get_file_content(file_format, is_annex)
        file_type = self.get_file_type(file_content)
        if any(ext in file_type for ext in ["text", "csv"]):
            return ".csv"
        if "zip" in file_type:
            return ".zip"
        return ""

    @property
    def file_base_name(self):
        name = f"{self.ship_name}-{self.imo_number}-{self.way}-{self.ship_call_id}"
        name = slugify(name)
        return name

    def get_file_name(self, person_list_type, file_format, is_annex):
        name = self.file_base_name
        if person_list_type:
            name += f"-{person_list_type}"
        if file_format:
            name += f"-{file_format}"
        if is_annex is True:
            name += "-annexe"
        name += self.get_file_extension(file_format, is_annex)
        return name

    def get_file_content(self, file_format, is_annex):
        file_content = None
        if is_annex is True:
            if file_format == persons_choices.PERSON_LIST_FILE_FORMAT_FPR:
                file_content = self.fpr_annex_file_content
            if file_format == persons_choices.PERSON_LIST_FILE_FORMAT_ROC:
                file_content = self.roc_annex_file_content
        else:
            if file_format == persons_choices.PERSON_LIST_FILE_FORMAT_FPR:
                file_content = self.fpr_file_content
            if file_format == persons_choices.PERSON_LIST_FILE_FORMAT_ROC:
                file_content = self.roc_file_content
        return file_content
