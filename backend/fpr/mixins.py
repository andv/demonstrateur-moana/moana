import zipfile
from io import BytesIO, StringIO

from django.conf import settings
from django.utils.text import slugify

from defusedcsv import csv

from backend.persons import choices as persons_choices
from backend.ship_files import settings as ship_files_settings


class BaseFileMixin:
    """
    Helper class to handle FPR and ROC file content.
    """

    def split_list_in_chunks(self, data_list, chunk_size):
        """
        Yield n-sized chunks from the given list.
        This is useful when we want to limit to list data to a max number
        so that the CSV files are limited to a max number of lines.
        """
        for i in range(0, len(data_list), chunk_size):
            yield data_list[i : i + chunk_size]

    @property
    def inside_zip_file_base_name(self):
        """
        Get the base file name to be used inside the zip file.
        """
        fal_data = self.parser.data
        if not fal_data:
            return ""
        ship_name = fal_data.get("ship_name", "")
        imo_number = fal_data.get("imo_number", "")
        way = fal_data.get("way", "")
        ship_call_id = fal_data.get("ship_call_id", "")
        file_type = self.parser.file_type
        if file_type == ship_files_settings.NCA_FAL5:
            person_list_type = persons_choices.PERSON_LIST_TYPE_CREW
        if file_type == ship_files_settings.NCA_FAL6:
            person_list_type = persons_choices.PERSON_LIST_TYPE_PASSENGER
        name = f"{ship_name}-{imo_number}-{way}-{ship_call_id}-{person_list_type}"
        return slugify(name)

    def get_zip_content(self, file_format, data, max_lines):
        """
        Return a zip file content. The zip contains several splitted CSV files.
        """
        data_list = list(self.split_list_in_chunks(data, max_lines))
        files = []
        for n, file_data in enumerate(data_list):
            if file_format == "fpr":
                file_content = self.get_csv_content_fpr(file_data)
            else:
                file_content = self.get_csv_content_roc(file_data)
            files.append(
                (
                    f"{self.inside_zip_file_base_name}-{file_format}-{n}.csv",
                    file_content,
                )
            )
        zip_file = BytesIO()
        with zipfile.ZipFile(
            zip_file, mode="w", compression=zipfile.ZIP_DEFLATED
        ) as zf:
            for f in files:
                zf.writestr(f[0], f[1])
        return zip_file.getvalue()


class FPRFileMixin(BaseFileMixin):
    def get_zip_content_fpr(self, data):
        """
        Return a zip file content. The zip contains several splitted CSV files.
        """
        return self.get_zip_content(
            file_format="fpr", data=data, max_lines=settings.MAX_LINES_IN_FPR_FILE
        )

    def get_file_content_fpr(self, data):
        """
        Returns the FPR file content based on the given python list data.
        The file could be either a CSV file or a zip file containing several
        CSV files.
        """

        # First, if the number of lines is less than the max,
        # we return only a single file.
        if settings.MAX_LINES_IN_FPR_FILE > len(data):
            return self.get_csv_content_fpr(data)

        # Second, if data is over the max number of lines, we split the files
        # and zip them.
        return self.get_zip_content_fpr(data)

    def get_csv_content_fpr(self, data):
        """
        Return the bytes-like file content. This is the CSV file containing
        the given data.
        """
        csv_file = StringIO(newline="")
        writer = csv.writer(csv_file)
        writer.writerows(data)
        csv_content = csv_file.getvalue()
        # The file content is expected as bytes-like object, so we convert it here:
        file_content = BytesIO()
        file_content.write(csv_content.encode("utf-8"))
        return file_content.getvalue()


class ROCFileMixin(BaseFileMixin):
    def get_zip_content_roc(self, data):
        return self.get_zip_content(
            file_format="roc", data=data, max_lines=settings.MAX_LINES_IN_ROC_FILE
        )

    def get_file_content_roc(self, data):
        """
        Returns the ROC file content based on the given python list data.
        The file could be either a CSV file or a zip file containing several
        CSV files.
        """
        # First, if the number of lines is less than the max,
        # we return only a single file.
        if settings.MAX_LINES_IN_ROC_FILE > len(data):
            return self.get_csv_content_roc(data)

        # Second, if data is over the max number of lines, we split the files
        # and zip them.
        return self.get_zip_content_roc(data)

    def get_csv_content_roc(self, data):
        """
        Return the bytes-like file content. This is the CSV file containing
        the given data.
        """
        csv_file = StringIO(newline="")
        writer = csv.writer(csv_file)
        roc_list_header = self.get_roc_list_header()
        writer.writerow(roc_list_header)
        writer.writerows(data)
        csv_content = csv_file.getvalue()
        # The file content is expected as bytes-like object, so we convert it here:
        file_content = BytesIO()
        file_content.write(csv_content.encode("utf-8"))
        return file_content.getvalue()

    def get_roc_list_header(self):
        commons_fields = [
            "Nom",
            "Prenom",
            "Date de naissance",
            "Lieu de naissance",
            "Nationalite",
            "Type de document d identite",
            "Numero de document d identite",
            "Numero de visa ou permis de residence",
        ]
        specifics_fields = []
        is_passenger = self.parser.file_type == ship_files_settings.NCA_FAL6
        if is_passenger:
            specifics_fields.append("Statut Escale")
        else:
            specifics_fields.append("Fonction")
        columns_name = commons_fields + specifics_fields
        return columns_name
