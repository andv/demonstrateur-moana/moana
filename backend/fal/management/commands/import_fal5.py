from django.conf import settings
from django.core.management.base import BaseCommand

from backend.alerts import choices as alerts_choices
from backend.alerts.mixins import AlertMixin
from backend.fal.management.commands.import_mixins import FalFileImportMixin
from backend.fal.parsers.fal5 import Fal5Parser
from backend.fpr.mixins import FPRFileMixin, ROCFileMixin
from backend.nationalities.models import Nationality
from backend.ships.models import CrewList, ShipMovement


class Command(FPRFileMixin, ROCFileMixin, FalFileImportMixin, AlertMixin, BaseCommand):
    parser_class = Fal5Parser

    def link_crew_to_ship(self, crew):
        """
        Link the given crew list entry to it's related ship movement.
        """
        related_ship = ShipMovement.objects.filter(
            ship_call_id=crew.ship_call_id, way=crew.way
        ).first()
        if related_ship:
            crew.ship_movement = related_ship
            crew.save()

    def check_nationality_alerts(self, crew):
        """
        Create an alert if one of the crew nationality hits one of the listed country in settings.
        """
        crew_nationality_list = [
            n.lower() for n in self.parser.get_crew_nationality_list()
        ]
        has_alert = any(
            nationality.lower() in crew_nationality_list
            for nationality in settings.ALERT_NATIONALITIES
        )
        if has_alert:
            self.create_alert(
                alert_type_id=alerts_choices.CREW_NATIONALITY_ALERT,
                ship_movement=crew.ship_movement,
            )

    def check_new_list_alerts(self, crew, crew_data):
        """
        Create an alert if a new list is received
        """
        if not crew_data:
            return None
        related_ship = crew.ship_movement
        if related_ship.fal5_counter >= 1:
            self.create_alert(
                alert_type_id=alerts_choices.NEW_CREW_LIST_ALERT,
                ship_movement=related_ship,
                reset_hide_for_teams=True,
            )
        related_ship.fal5_counter += 1
        related_ship.save()

    def handle_crew(self, fal_data):
        crew_list_data = self.parser.get_crew_list_data()
        nationalities_data = self.get_nationalities_data(
            self.parser.crew_nationality_counters, fal_data=fal_data
        )
        instance_data = fal_data.copy()
        # The naming on `nationalities_data` matches the counters field names,
        # so the data can be used as the instance creation/update data.
        instance_data.update(nationalities_data)
        crew_list, _ = CrewList.objects.update_or_create(
            ship_call_id=fal_data["ship_call_id"],
            way=fal_data["way"],
            defaults=instance_data,
        )
        crew_annex_list = self.parser.get_crew_annex_list()
        if crew_list_data:
            crew_list.number_of_crew_parsed = len(crew_list_data)
            crew_list.fpr_file_content = self.get_file_content_fpr(crew_list_data)
            crew_list.roc_file_content = self.get_file_content_roc(crew_list_data)
        if crew_annex_list:
            crew_list.roc_annex_file_content = self.get_file_content_roc(
                crew_annex_list
            )
            crew_list.fpr_annex_file_content = self.get_file_content_fpr(
                crew_annex_list
            )
            crew_list.save()
        self.link_crew_to_ship(crew_list)
        self.check_nationality_alerts(crew_list)
        self.check_new_list_alerts(crew_list, crew_list_data)
