from backend.logs import actions as logs_actions
from backend.logs import settings as logs_settings
from backend.nationalities.models import Nationality
from backend.ports.models import Port
from backend.ship_files import settings as ship_files_settings
from backend.ships.models import ShipMovement


class FalFileImportMixin(logs_actions.DataQualityLogLogHandler):
    """
    Helper for FAL file import.
    This mixin is meant to be used with django management `BaseCommand`.
    """

    help = "Import FAL files"
    parser_class = None

    def add_data_for_log(self, fal_data):
        ship_fal_data = self.parser.get_stopover_data()
        ship_model_data = self.get_complementary_data()
        data_to_add_in_log = ship_fal_data | ship_model_data
        fal_data.update(data_to_add_in_log)
        return fal_data

    def get_complementary_data(self):
        data = {}
        data["agent_name"] = self.ship.agent_name
        return data

    def add_log_format_name(self, differences_list=None, fal_data=None):
        """
        Creates a log entry that states the name formatting differences.
        """
        if not differences_list:
            return
        description = ""
        for difference in differences_list:
            description += f"Changes : {difference}\n"
        count_differences = len(differences_list)
        description += f"Number of names that have been altered : {count_differences}\n"
        self.create_data_quality_log(
            action=logs_settings.LOG_NAME_FORMAT_NAME,
            actor="moana-application",
            data=differences_list,
            target=count_differences,
            description=description,
            ship=fal_data,
            object=self.parser.file_type,
        )

    def add_log_name_switch(self, name_switch_list=None, fal_data=None):
        """
        Creates a log entry that states the name formatting differences.
        """
        if not name_switch_list:
            return
        description = "\n".join(name_switch_list)
        count_name_switch = len(name_switch_list)
        fal_data = self.add_data_for_log(fal_data)
        self.create_data_quality_log(
            action=logs_settings.LOG_NAME_SWITCH,
            actor="moana-application",
            data=name_switch_list,
            target=count_name_switch,
            description=description,
            ship=fal_data,
            object=self.parser.file_type,
        )

    def add_log_bad_date_of_birth(
        self,
        log_name,
        bad_date_of_birth_list=None,
        person_list_with_issues=None,
        fal_data=None,
    ):
        """
        Creates a log entry with the list of past/future date of birth detected.
        """
        if not bad_date_of_birth_list:
            return
        description = "Found bad dates: \n" + "\n".join(bad_date_of_birth_list)
        count_bad_dates = len(bad_date_of_birth_list)
        description += f"\nNumber of bad date of birth: {count_bad_dates}\n"
        self.create_data_quality_log(
            action=log_name,
            actor="moana-application",
            data=person_list_with_issues,
            target=count_bad_dates,
            description=description,
            ship=fal_data,
            object=self.parser.file_type,
        )

    def add_log_with_list_issues(
        self,
        log_name,
        count_issues=0,
        fal_data=None,
        list_issues=None,
        person_list_with_issues=None,
    ):
        """
        Creates a log entry for a list of issues like bad encoding, empty or unknown names.
        """
        if not count_issues:
            return
        description = f"""Number of issues: {count_issues}
        List of issues: {list_issues}
        """
        self.create_data_quality_log(
            action=log_name,
            actor="moana-application",
            target=count_issues,
            description=description,
            ship=fal_data,
            data=person_list_with_issues,
            object=self.parser.file_type,
        )

    def add_log_past_movement_date(
        self, log_name, past_movement_date_list=None, fal_data=None
    ):
        """
        Creates a log entry with the list of past date movement date.
        """
        if not past_movement_date_list:
            return
        string_date_list = [
            date_obj.strftime("%d/%m/%Y %H:%M:%S")
            for date_obj in past_movement_date_list
        ]
        description = "Found past dates: \n" + "\n".join(string_date_list)
        count_past_dates = len(past_movement_date_list)
        description += f"\nNumber of past movement date: {count_past_dates}\n"
        self.create_data_quality_log(
            action=log_name,
            actor="moana-application",
            data=string_date_list,
            target=count_past_dates,
            description=description,
            ship=fal_data,
            object=self.parser.file_type,
        )

    def add_log_missing_nationality(
        self, log_name, missing_nationality_list=None, fal_data=None
    ):
        """
        Creates a log entry with the list of missing nationalities detected.
        """
        if not missing_nationality_list:
            return
        description = "Missing nationalities: " + ",".join(missing_nationality_list)
        count_missing_nationalities = len(missing_nationality_list)
        description += (
            f"\nNumber of missing nationalities: {count_missing_nationalities}\n"
        )
        self.create_data_quality_log(
            action=log_name,
            actor="moana-application",
            data=missing_nationality_list,
            target=count_missing_nationalities,
            description=description,
            ship=fal_data,
            object=self.parser.file_type,
        )

    def add_log_unknown_port(self, log_name, fal_data):
        """
        Creates a log entry for any unknown ports found in the given FAL data.
        Checks the locodes of last_port, next_port and port_of_call.
        """
        port_fields = [
            "last_port",
            "next_port",
            "port_of_call",
        ]
        for field_name in port_fields:
            locode = fal_data.get(field_name)
            if not locode:
                continue
            if not Port.objects.filter(locode=locode).exists():
                description = f"Unknown port locode found: {locode}"
                self.create_data_quality_log(
                    action=log_name,
                    actor="moana-application",
                    description=description,
                    target=field_name,
                    data=locode,
                    ship=fal_data,
                    object=self.parser.file_type,
                )

    def add_arguments(self, parser):
        parser.add_argument("file_path")

    def get_or_create_ship_movement(self, fal_data):
        fal_data_without_empty = {k: v for k, v in fal_data.items() if v}
        ship, created = ShipMovement.objects.update_or_create(
            ship_call_id=fal_data["ship_call_id"],
            way=fal_data["way"],
            defaults=fal_data_without_empty,
        )
        return ship, created

    def handle_ship_movements(self, fal_data):
        ship, created = self.get_or_create_ship_movement(fal_data)
        not_fal_1 = self.parser.file_type != ship_files_settings.NCA_FAL1
        if created and not_fal_1:
            # We update the stopover data only for FAL5 and FAL6
            data = self.parser.get_stopover_data()
            ship.port_of_call = data["port_of_call"]
            ship.last_port = data["last_port"]
            ship.next_port = data["next_port"]
            ship.eta_to_port_of_call = data["eta_to_port_of_call"]
            ship.etd_from_port_of_call = data["etd_from_port_of_call"]
            ship.ata_to_port_of_call = data["ata_to_port_of_call"]
            ship.atd_from_port_of_call = data["atd_from_port_of_call"]
        ship.save()
        ship.notify_by_email()
        self.ship = ship

    def handle_crew(self, fal_data):
        pass

    def handle_passengers(self, fal_data):
        pass

    def get_nationalities_data(self, nationality_data, fal_data=None):
        """
        Return a dictionary with the nationality counters with naming
        that matches the name of the counters fields defined in Crew and
        Passengers lists models.
        """
        if not nationality_data:
            return {}
        nationalities_data = {
            "nationalities_fr": 0,
            "nationalities_eu": 0,
            "nationalities_visa": 0,
            "nationalities_visa_transit": 0,
            "nationalities_no_visa": 0,
        }
        missing_nationalities = []
        for country_code, count in nationality_data.items():
            if country_code == "FR":
                # For FR nationalities, the count is directly available
                # so we don't need to check the Nationality model.
                nationalities_data["nationalities_fr"] += count
                continue
            nationality = Nationality.objects.filter(country_code=country_code).first()
            if not nationality:
                missing_nationalities.append(country_code)
                continue
            # For non FR nationalities, we need to check the Nationality model
            # to find out the country's category.
            if nationality.is_in_eu:
                nationalities_data["nationalities_eu"] += count
            if nationality.needs_visa:
                nationalities_data["nationalities_visa"] += count
            if nationality.needs_visa_transit:
                nationalities_data["nationalities_visa_transit"] += count
            if nationality.no_needs_visa:
                nationalities_data["nationalities_no_visa"] += count
        self.add_log_missing_nationality(
            log_name=logs_settings.LOG_NAME_MISSING_NATIONALITIES,
            missing_nationality_list=missing_nationalities,
            fal_data=fal_data,
        )
        return nationalities_data

    def add_data_quality_logs_for_person(self, fal_data):
        if not self.parser.has_data_quality_log_for_person:
            return
        if self.parser.name_formatting_diff_list:
            self.add_log_format_name(
                differences_list=self.parser.name_formatting_diff_list,
                fal_data=fal_data,
            )
        if self.parser.past_date_of_birth_list:
            self.add_log_bad_date_of_birth(
                log_name=logs_settings.LOG_NAME_PAST_DATE_OF_BIRTH,
                bad_date_of_birth_list=self.parser.past_date_of_birth_list,
                person_list_with_issues=self.parser.person_list_past_dob_issues,
                fal_data=fal_data,
            )
        if self.parser.future_date_of_birth_list:
            self.add_log_bad_date_of_birth(
                log_name=logs_settings.LOG_NAME_FUTURE_DATE_OF_BIRTH,
                bad_date_of_birth_list=self.parser.future_date_of_birth_list,
                person_list_with_issues=self.parser.person_list_future_dob_issues,
                fal_data=fal_data,
            )
        if self.parser.count_name_with_bad_encoding > 0:
            self.add_log_with_list_issues(
                log_name=logs_settings.LOG_NAME_BAD_ENCODING,
                count_issues=self.parser.count_name_with_bad_encoding,
                list_issues=self.parser.name_analysing_bad_encoding_list,
                fal_data=fal_data,
            )
        if self.parser.count_empty_names > 0:
            self.add_log_with_list_issues(
                log_name=logs_settings.LOG_NAME_EMPTY_NAME,
                count_issues=self.parser.count_empty_names,
                fal_data=fal_data,
            )
        if self.parser.count_unknown_names > 0:
            self.add_log_with_list_issues(
                log_name=logs_settings.LOG_NAME_UNKNOWN_NAME,
                count_issues=self.parser.count_unknown_names,
                fal_data=fal_data,
            )
        if self.parser.name_switch_list:
            self.add_log_name_switch(
                name_switch_list=self.parser.name_switch_list,
                fal_data=fal_data,
            )
        if self.parser.count_empty_identity_document > 0:
            self.add_log_with_list_issues(
                log_name=logs_settings.LOG_NAME_EMPTY_IDENTITY_DOC,
                count_issues=self.parser.count_empty_identity_document,
                fal_data=fal_data,
            )
        if self.parser.count_name_with_only_one_letter > 0:
            self.add_log_with_list_issues(
                log_name=logs_settings.LOG_NAME_ONLY_ONE_LETTER_NAME,
                count_issues=self.parser.count_name_with_only_one_letter,
                person_list_with_issues=self.parser.person_list_only_one_letter_issues,
                fal_data=fal_data,
            )
        if self.parser.count_unwanted_numbers > 0:
            self.add_log_with_list_issues(
                log_name=logs_settings.LOG_NAME_UNWANTED_NUMBERS,
                count_issues=self.parser.count_unwanted_numbers,
                list_issues=self.parser.unwanted_numbers_list,
                person_list_with_issues=self.parser.person_list_unwanted_numbers_issues,
                fal_data=fal_data,
            )
        if self.parser.count_name_particle > 0:
            self.add_log_with_list_issues(
                log_name=logs_settings.LOG_NAME_PARTICLE_NAME,
                count_issues=self.parser.count_name_particle,
                list_issues=self.parser.name_particle_list,
                fal_data=fal_data,
            )

    def add_data_quality_logs_for_ship(self, fal_data):
        if not self.parser.has_data_quality_log_for_ship:
            return
        self.add_log_unknown_port(
            log_name=logs_settings.LOG_NAME_UNKNOWN_PORT,
            fal_data=fal_data,
        )
        if self.parser.past_movement_date_list:
            self.add_log_past_movement_date(
                log_name=logs_settings.LOG_NAME_PAST_MOVEMENT_DATE,
                past_movement_date_list=self.parser.past_movement_date_list,
                fal_data=fal_data,
            )

    def handle(self, *args, **options):
        file_path = options["file_path"]
        self.parser = self.parser_class(file_path)
        fal_data = self.parser.get_parsed_data()
        self.handle_ship_movements(fal_data)
        self.handle_crew(fal_data)
        self.handle_passengers(fal_data)
        fal_data = self.add_data_for_log(fal_data)
        self.add_data_quality_logs_for_person(fal_data)
        self.add_data_quality_logs_for_ship(fal_data)
        self.stdout.write(f"Successfully imported {file_path}", self.style.SUCCESS)
