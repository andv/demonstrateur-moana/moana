from django.conf import settings
from django.core.management.base import BaseCommand

from backend.alerts import choices as alerts_choices
from backend.alerts.mixins import AlertMixin
from backend.fal.management.commands.import_mixins import FalFileImportMixin
from backend.fal.parsers.fal6 import Fal6Parser
from backend.fpr.mixins import FPRFileMixin, ROCFileMixin
from backend.ships.models import PassengerList, ShipMovement


class Command(FPRFileMixin, ROCFileMixin, FalFileImportMixin, AlertMixin, BaseCommand):
    parser_class = Fal6Parser

    def link_passengers_to_ship(self, passengers):
        """
        Link the given passenger list entry to it's related ship movement.
        """
        related_ship = ShipMovement.objects.filter(
            ship_call_id=passengers.ship_call_id, way=passengers.way
        ).first()
        if related_ship:
            passengers.ship_movement = related_ship
            passengers.save()

    def check_nationality_alerts(self, passenger):
        """
        Create an alert if one of the passenger nationality hits one of the listed country in settings.
        """
        passenger_nationality_list = [
            n.lower() for n in self.parser.get_passenger_nationality_list()
        ]
        has_alert = any(
            nationality.lower() in passenger_nationality_list
            for nationality in settings.ALERT_NATIONALITIES
        )
        if has_alert:
            self.create_alert(
                alert_type_id=alerts_choices.PASSENGER_NATIONALITY_ALERT,
                ship_movement=passenger.ship_movement,
            )

    def check_new_list_alerts(self, passengers, passengers_data):
        """
        Create an alert if a new list is received
        """
        if not passengers_data:
            return None
        related_ship = passengers.ship_movement
        if related_ship.fal6_counter >= 1:
            self.create_alert(
                alert_type_id=alerts_choices.NEW_PASSENGER_LIST_ALERT,
                ship_movement=related_ship,
                reset_hide_for_teams=True,
            )
        related_ship.fal6_counter += 1
        related_ship.save()

    def handle_passengers(self, fal_data):
        passenger_list_data = self.parser.get_passenger_list_data()
        nationalities_data = self.get_nationalities_data(
            self.parser.passenger_nationality_counters, fal_data=fal_data
        )
        instance_data = fal_data.copy()
        # The naming on `nationalities_data` matches the counters field names,
        # so the data can be used as the instance creation/update data.
        instance_data.update(nationalities_data)
        passenger_list, _ = PassengerList.objects.update_or_create(
            ship_call_id=fal_data["ship_call_id"],
            way=fal_data["way"],
            defaults=instance_data,
        )
        passenger_annex_list = self.parser.get_passenger_annex_list()
        if passenger_list_data:
            passenger_list.number_of_passengers_parsed = len(passenger_list_data)
            passenger_list.fpr_file_content = self.get_file_content_fpr(
                passenger_list_data
            )
            passenger_list.roc_file_content = self.get_file_content_roc(
                passenger_list_data
            )
        if passenger_annex_list:
            passenger_list.roc_annex_file_content = self.get_file_content_roc(
                passenger_annex_list
            )
            passenger_list.fpr_annex_file_content = self.get_file_content_fpr(
                passenger_annex_list
            )
            passenger_list.save()
        self.link_passengers_to_ship(passenger_list)
        self.check_nationality_alerts(passenger_list)
        self.check_new_list_alerts(passenger_list, passenger_list_data)
