from django.core.management.base import BaseCommand

from backend.fal.management.commands.import_mixins import FalFileImportMixin
from backend.fal.parsers.fal1 import Fal1Parser


class Command(FalFileImportMixin, BaseCommand):
    parser_class = Fal1Parser
