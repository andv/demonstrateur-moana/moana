BAD_ENCODING_MARKERS = ["Ã", "©", "Å", "â", "Â", "¢", "?"]
UNWANTED_CHARACTERS = [";", ".", ",", "*", "N/A"]
UNKNOWN_NAMES = ["inconnu", "unknown"]
PASSENGER_EMBARK = "EMBARQUANT"
PASSENGER_DISEMBARK = "DEBARQUANT"
PASSENGER_IN_TRANSIT = "EN TRANSIT"
# When an item on this list is detected on a person name, then we will split the name in tow parts.
ALIAS_IN_PERSON_NAME_SPLITTER_LIST = [
    " DI ",
    " DIT ",
    " EP ",
    " EPS ",
    " EV ",
    " IPE ",
    " EPOUSE ",
    " ÉPOUSE ",
    " ÉP ",
    " ÉPS ",
    " NÉ ",
    " NÉE ",
    " VV ",
    " VEUVE ",
    " VEUF ",
]
