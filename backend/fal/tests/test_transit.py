import re

from django.conf import settings

import pytest

from backend.fal import settings as fal_settings
from backend.fal.tests import settings as fal_test_settings
from backend.ship_files.models import ShipFile as Fal
from backend.ships.models import PassengerList
from backend.unit_tests import utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())
row_informations = "TITIN,Milou,01/01/1960,zzukn,France,Passport,123456789,"


def test_passengers_in_transit(client):
    utils.login_saint_malo_user(client)
    fal = Fal(ship_file=fal_test_settings.FAL6_TRANSIT_ARRIVING)
    fal.save()
    passenger_list = PassengerList.objects.last()
    assert f"{row_informations},{fal_settings.PASSENGER_IN_TRANSIT}" in str(
        passenger_list.fpr_file_content
    )
    assert f"{row_informations},{fal_settings.PASSENGER_IN_TRANSIT}" in str(
        passenger_list.roc_file_content
    )


def test_for_ship_arriving_passengers_not_in_transit_are_debark(client):
    utils.login_saint_malo_user(client)
    fal = Fal(ship_file=fal_test_settings.FAL6_TRANSIT_ARRIVING)
    fal.save()
    passenger_list = PassengerList.objects.last()
    assert f"{row_informations},{fal_settings.PASSENGER_DISEMBARK}" in str(
        passenger_list.fpr_file_content
    )
    assert f"{row_informations},{fal_settings.PASSENGER_DISEMBARK}" in str(
        passenger_list.roc_file_content
    )
    assert f"{row_informations},{fal_settings.PASSENGER_EMBARK}" not in str(
        passenger_list.fpr_file_content
    )
    assert f"{row_informations},{fal_settings.PASSENGER_EMBARK}" not in str(
        passenger_list.roc_file_content
    )


def test_for_ship_departing_passengers_not_in_transit_are_embarking(client):
    utils.login_saint_malo_user(client)
    fal = Fal(ship_file=fal_test_settings.FAL6_TRANSIT_DEPARTING)
    fal.save()
    passenger_list = PassengerList.objects.last()
    assert f"{row_informations},{fal_settings.PASSENGER_EMBARK}" in str(
        passenger_list.fpr_file_content
    )
    assert f"{row_informations},{fal_settings.PASSENGER_EMBARK}" in str(
        passenger_list.roc_file_content
    )
    assert f"{row_informations},{fal_settings.PASSENGER_DISEMBARK}" not in str(
        passenger_list.fpr_file_content
    )
    assert f"{row_informations},{fal_settings.PASSENGER_DISEMBARK}" not in str(
        passenger_list.roc_file_content
    )


def test_passengers_departing_organised_by_transit(client):
    utils.login_saint_malo_user(client)
    fal = Fal(ship_file=fal_test_settings.FAL6_TRANSIT_DEPARTING)
    fal.save()
    passenger_list = PassengerList.objects.last()
    fpr_file_content = str(passenger_list.fpr_file_content)
    fpr_transit_position = re.search(
        fal_settings.PASSENGER_IN_TRANSIT, fpr_file_content
    ).start()
    fpr_embark_position = re.search(
        fal_settings.PASSENGER_EMBARK, fpr_file_content
    ).start()
    roc_file_content = str(passenger_list.roc_file_content)
    roc_transit_position = re.search(
        fal_settings.PASSENGER_IN_TRANSIT, roc_file_content
    ).start()
    roc_embark_position = re.search(
        fal_settings.PASSENGER_EMBARK, roc_file_content
    ).start()
    assert fpr_transit_position > fpr_embark_position
    assert roc_transit_position > roc_embark_position


def test_passengers_arriving_organised_by_transit(client):
    utils.login_saint_malo_user(client)
    fal = Fal(ship_file=fal_test_settings.FAL6_TRANSIT_ARRIVING)
    fal.save()
    passenger_list = PassengerList.objects.last()
    fpr_file_content = str(passenger_list.fpr_file_content)
    fpr_transit_position = re.search(
        fal_settings.PASSENGER_IN_TRANSIT, fpr_file_content
    ).start()
    fpr_debark_position = re.search(
        fal_settings.PASSENGER_DISEMBARK, fpr_file_content
    ).start()
    roc_file_content = str(passenger_list.roc_file_content)
    roc_transit_position = re.search(
        fal_settings.PASSENGER_IN_TRANSIT, roc_file_content
    ).start()
    roc_debark_position = re.search(
        fal_settings.PASSENGER_DISEMBARK, roc_file_content
    ).start()
    assert fpr_transit_position > fpr_debark_position
    assert roc_transit_position > roc_debark_position
