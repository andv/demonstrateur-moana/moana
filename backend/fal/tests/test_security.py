from django.conf import settings
from django.shortcuts import reverse

import pytest
from rest_framework import status

from backend.fal.tests.utils import count_uploaded_fal_files
from backend.ship_files.models import ShipFile as Fal
from backend.ships.models import ShipMovement
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_can_upload_fal_file(client):
    utils.login_saint_malo_user(client)
    url = reverse("upload-list")
    before = ShipMovement.objects.count()
    post_data = {
        "file": factories.ship_file.open(),
    }
    response = client.post(url, post_data, format="multipart")
    assert response.status_code == status.HTTP_201_CREATED
    after = ShipMovement.objects.count()
    assert after == before + 1


def test_cannot_upload_fal_file_if_user_not_authorized(client):
    user = utils.login_saint_malo_user(client)
    user.manual_upload_fal = False
    user.save()
    url = reverse("upload-list")
    before = ShipMovement.objects.count()
    post_data = {
        "file": factories.ship_file.open(),
    }
    response = client.post(url, post_data, format="multipart")
    assert response.status_code == status.HTTP_403_FORBIDDEN
    after = ShipMovement.objects.count()
    assert after == before


def test_cannot_upload_exe_file(client):
    utils.login_saint_malo_user(client)
    url = reverse("upload-list")
    before = ShipMovement.objects.count()
    post_data = {
        "file": factories.exe_file.open(),
    }
    response = client.post(url, post_data, format="multipart")
    assert response.status_code == status.HTTP_403_FORBIDDEN
    after = ShipMovement.objects.count()
    assert after == before


def test_cannot_upload_file_with_bad_extension(client):
    utils.login_saint_malo_user(client)
    url = reverse("upload-list")
    before = ShipMovement.objects.count()
    post_data = {
        "file": factories.fal_file_with_sh_extension.open(),
    }
    response = client.post(url, post_data, format="multipart")
    assert response.status_code == status.HTTP_403_FORBIDDEN
    after = ShipMovement.objects.count()
    assert after == before


def test_cannot_upload_invaid_fal_file(client):
    utils.login_saint_malo_user(client)
    url = reverse("upload-list")
    before = ShipMovement.objects.count()
    post_data = {
        "file": factories.xml_file_but_invalid_fal.open(),
    }
    response = client.post(url, post_data, format="multipart")
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    after = ShipMovement.objects.count()
    assert after == before


def test_fal_file_is_removed_after_upload(client):
    utils.login_saint_malo_user(client)
    url = reverse("upload-list")
    ships_before = ShipMovement.objects.count()
    fal_objects_before = Fal.objects.count()
    fal_files_before = count_uploaded_fal_files()
    post_data = {
        "file": factories.ship_file.open(),
    }
    response = client.post(url, post_data, format="multipart")
    assert response.status_code == status.HTTP_201_CREATED
    ships_after = ShipMovement.objects.count()
    fal_objects_after = Fal.objects.count()
    fal_files_after = count_uploaded_fal_files()
    assert ships_after == ships_before + 1
    assert fal_files_before == fal_files_after
    assert fal_objects_before == fal_objects_after
