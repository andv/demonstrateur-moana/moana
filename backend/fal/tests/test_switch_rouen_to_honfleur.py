from django.conf import settings

import pytest

from backend.fal.tests import settings as fal_settings
from backend.ship_files.models import ShipFile as Fal
from backend.ships.models import ShipMovement

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_rouen_port_is_switched_to_honfleur(client):
    SHIP_CALL_IN_TEST_FILE = "FRURO22428"
    fal = Fal(ship_file=fal_settings.FAL1_SWITCH_ROUEN_TO_HONFLEUR_PATH)
    fal.save()
    HONFLEUR_LOCODE = "FRHON"
    assert ShipMovement.objects.filter(
        ship_call_id=SHIP_CALL_IN_TEST_FILE, port_of_call=HONFLEUR_LOCODE
    ).exists()
