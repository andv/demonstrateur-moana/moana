from django.conf import settings

import pytest

from backend.fal.tests import settings as fal_settings
from backend.logs import settings as logs_settings
from backend.logs.models import DataQualityLog
from backend.ship_files.models import ShipFile as Fal

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_unwanted_numbers_in_fal5_creates_log():
    unwanted_numbers_logs_before = DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_UNWANTED_NUMBERS
    ).exists()
    fal5 = Fal(ship_file=fal_settings.FAL5_UNWANTED_NUMBERS_PATH)
    fal5.save()
    unwanted_numbers_logs_after = DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_UNWANTED_NUMBERS
    ).exists()
    assert not unwanted_numbers_logs_before
    assert unwanted_numbers_logs_after


def test_unwanted_numbers_in_fal6_creates_log():
    unwanted_numbers_logs_before = DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_UNWANTED_NUMBERS
    ).exists()
    fal6 = Fal(ship_file=fal_settings.FAL5_UNWANTED_NUMBERS_PATH)
    fal6.save()
    unwanted_numbers_logs_after = DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_UNWANTED_NUMBERS
    ).exists()
    assert not unwanted_numbers_logs_before
    assert unwanted_numbers_logs_after


def test_unwanted_numbers_log_have_good_count():
    fal5 = Fal(ship_file=fal_settings.FAL5_UNWANTED_NUMBERS_PATH)
    fal5.save()
    log_created_fal5 = DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_UNWANTED_NUMBERS
    )[0]
    fal6 = Fal(ship_file=fal_settings.FAL6_UNWANTED_NUMBERS_PATH)
    fal6.save()
    log_created_fal6 = DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_UNWANTED_NUMBERS
    )[1]
    assert log_created_fal5.target == "3"
    assert log_created_fal6.target == "5"
