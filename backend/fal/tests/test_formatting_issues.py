from django.conf import settings

import pytest

from backend.fal.tests import settings as fal_settings
from backend.logs import settings as logs_settings
from backend.logs.models import DataQualityLog
from backend.ship_files.models import ShipFile as Fal
from backend.unit_tests import utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


###################
# Name formatting #
###################


def test_fal6_log_entry_is_created_for_bad_name(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_FORMAT_NAME
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL6_BAD_NAME_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_FORMAT_NAME
    ).exists()


def test_fal5_log_entry_is_created_for_bad_name(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_FORMAT_NAME
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL5_BAD_NAME_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_FORMAT_NAME
    ).exists()


###################
# Date of birth   #
###################


def test_fal6_log_entry_is_created_for_past_date_of_birth(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_PAST_DATE_OF_BIRTH
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL6_PAST_DATE_OF_BIRTH_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_PAST_DATE_OF_BIRTH
    ).exists()


def test_fal5_log_entry_is_created_for_past_date_of_birth(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_PAST_DATE_OF_BIRTH
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL5_PAST_DATE_OF_BIRTH_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_PAST_DATE_OF_BIRTH
    ).exists()


def test_fal6_log_entry_is_created_for_future_date_of_birth(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_FUTURE_DATE_OF_BIRTH
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL6_FUTURE_DATE_OF_BIRTH_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_FUTURE_DATE_OF_BIRTH
    ).exists()


def test_fal5_log_entry_is_created_for_future_date_of_birth(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_FUTURE_DATE_OF_BIRTH
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL5_FUTURE_DATE_OF_BIRTH_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_FUTURE_DATE_OF_BIRTH
    ).exists()


###################
# Encoding issues #
###################


def test_fal6_log_entry_is_created_for_bad_encoding(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_BAD_ENCODING
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL6_BAD_ENCODING_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_BAD_ENCODING
    ).exists()


def test_fal5_log_entry_is_created_for_bad_encoding(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_BAD_ENCODING
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL5_BAD_ENCODING_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_BAD_ENCODING
    ).exists()


############################
# Empty and unknown issues #
############################


def test_fal6_log_entry_is_created_for_empty_name(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_EMPTY_NAME
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL6_EMPTY_NAME_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_EMPTY_NAME
    ).exists()


def test_fal5_log_entry_is_created_for_empty_name(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_EMPTY_NAME
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL5_EMPTY_NAME_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_EMPTY_NAME
    ).exists()


def test_fal6_log_entry_is_created_for_unknown_name(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_UNKNOWN_NAME
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL6_UNKNOWN_NAME_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_UNKNOWN_NAME
    ).exists()


def test_fal5_log_entry_is_created_for_unknown_name(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_UNKNOWN_NAME
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL5_UNKNOWN_NAME_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_UNKNOWN_NAME
    ).exists()


#############################
# N/A formatting            #
#############################


def test_fal6_logs_entries_are_created_for_NA_name(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_FORMAT_NAME
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL6_NA_NAME_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_FORMAT_NAME
    ).exists()


def test_fal5_logs_entries_are_created_for_NA_name(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_FORMAT_NAME
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL5_NA_NAME_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_FORMAT_NAME
    ).exists()


#############################
# Switch last name          #
#############################


def test_fal6_logs_entry_is_created_for_last_name_switch(client):
    utils.login_saint_malo_user(client)
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_SWITCH
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL6_NAME_SWITCH_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(action=logs_settings.LOG_NAME_SWITCH).exists()


def test_fal5_logs_entry_is_created_for_last_name_switch(client):
    utils.login_saint_malo_user(client)
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_SWITCH
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL5_NAME_SWITCH_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(action=logs_settings.LOG_NAME_SWITCH).exists()


def test_fal6_switch_log_is_created_for_unknown_last_name(client):
    utils.login_saint_malo_user(client)
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_SWITCH
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL6_NAME_UNKNOWN_SWITCH_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(action=logs_settings.LOG_NAME_SWITCH).exists()


def test_fal5_switch_log_is_created_for_unknown_last_name(client):
    utils.login_saint_malo_user(client)
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_SWITCH
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL5_NAME_UNKNOWN_SWITCH_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(action=logs_settings.LOG_NAME_SWITCH).exists()


#############################
# Abnormal dates            #
#############################


def test_fal1_logs_entry_is_created_when_eta_is_abnormal(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_PAST_MOVEMENT_DATE
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL1_PAST_MOVEMENT_DATE_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_PAST_MOVEMENT_DATE
    ).exists()


def test_fal6_logs_entry_is_created_when_eta_is_abnormal(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_PAST_MOVEMENT_DATE
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL6_PAST_MOVEMENT_DATE_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_PAST_MOVEMENT_DATE
    ).exists()


def test_fal5_logs_entry_is_created_when_eta_is_abnormal(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_PAST_MOVEMENT_DATE
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL5_PAST_MOVEMENT_DATE_PATH)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_PAST_MOVEMENT_DATE
    ).exists()
