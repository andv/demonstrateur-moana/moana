from os import getcwd, path

# Test data here is based on port of call Saint Malo - FRSML.
# The login user is expected to be associated with that port.
FAL1_PATH = path.join(getcwd(), "backend/fal/tests/data/NCA_FAL1.xml")
FAL5_PATH = path.join(getcwd(), "backend/fal/tests/data/NCA_FAL5.xml")
FAL6_PATH = path.join(getcwd(), "backend/fal/tests/data/NCA_FAL6.xml")
FAL5_MISSING_DATA_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-missing-data.xml"
)
FAL6_MISSING_DATA_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-missing-data.xml"
)
FAL6_NAME_ALIASES_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-name-aliases.xml"
)
FAL5_NAME_ALIASES_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-name-aliases.xml"
)
FAL6_FIRSTNAME_ALIASES_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-firstname-aliases.xml"
)
FAL5_FIRSTNAME_ALIASES_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-firstname-aliases.xml"
)
FAL6_BAD_NAME_PATH = path.join(getcwd(), "backend/fal/tests/data/NCA_FAL6-bad-name.xml")
FAL5_BAD_NAME_PATH = path.join(getcwd(), "backend/fal/tests/data/NCA_FAL5-bad-name.xml")
FAL6_PAST_DATE_OF_BIRTH_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-past-date-of-birth.xml"
)
FAL5_PAST_DATE_OF_BIRTH_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-past-date-of-birth.xml"
)
FAL6_FUTURE_DATE_OF_BIRTH_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-future-date-of-birth.xml"
)
FAL5_FUTURE_DATE_OF_BIRTH_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-future-date-of-birth.xml"
)
FAL6_BAD_ENCODING_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-bad-encoding.xml"
)
FAL5_BAD_ENCODING_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-bad-encoding.xml"
)
FAL6_EMPTY_NAME_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-empty-name.xml"
)
FAL5_EMPTY_NAME_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-empty-name.xml"
)
FAL6_UNKNOWN_NAME_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-unknown-name.xml"
)
FAL5_UNKNOWN_NAME_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-unknown-name.xml"
)
FAL6_TRANSIT_ARRIVING = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-transit-arriving.xml"
)
FAL6_TRANSIT_DEPARTING = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-transit-departing.xml"
)
FAL5_CHANGE_SENSITIVE_STOPOVER_DATA = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-change-sensitive-stopover-data.xml"
)
FAL6_CHANGE_SENSITIVE_STOPOVER_DATA = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-change-sensitive-stopover-data.xml"
)
FAL6_NA_NAME_PATH = path.join(getcwd(), "backend/fal/tests/data/NCA_FAL6-na-name.xml")
FAL5_NA_NAME_PATH = path.join(getcwd(), "backend/fal/tests/data/NCA_FAL5-na-name.xml")
FAL1_PAST_MOVEMENT_DATE_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL1-past-movement-date.xml"
)
FAL6_PAST_MOVEMENT_DATE_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-past-movement-date.xml"
)
FAL5_PAST_MOVEMENT_DATE_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-past-movement-date.xml"
)
FAL6_NAME_SWITCH_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-name-switch.xml"
)
FAL5_NAME_SWITCH_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-name-switch.xml"
)
FAL5_EMPTY_IDENTITY_DOC = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-empty-identity-doc.xml"
)
FAL6_EMPTY_IDENTITY_DOC = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-empty-identity-doc.xml"
)
FAL6_EMPTY_VISA_NUMBER = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-empty-visa-number.xml"
)
FAL6_MISSING_IDENTITY = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-missing-identity.xml"
)
FAL1_UNKNOWN_PORT = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL1-unknown-port.xml"
)
FAL5_UNKNOWN_PORT = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-unknown-port.xml"
)
FAL6_UNKNOWN_PORT = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-unknown-port.xml"
)
FAL6_NAME_UNKNOWN_SWITCH_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-unknown-name-switch.xml"
)
FAL5_NAME_UNKNOWN_SWITCH_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-unknown-name-switch.xml"
)
FAL5_FRANCE_METRO = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-france-metro.xml"
)
FAL6_FRANCE_METRO = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-france-metro.xml"
)
FAL5_LOCAL_OVERSEAS = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-local-overseas.xml"
)
FAL6_LOCAL_OVERSEAS = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-local-overseas.xml"
)
FAL1_EXTRA_SCHENGEN = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL1-extra-schengen.xml"
)
FAL1_SWITCH_ROUEN_TO_HONFLEUR_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL1-switch-rouen-to-honfleur.xml"
)
FAL5_SUSPICIOUS_CHARACTERS = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-suspicious-characters.xml"
)
FAL6_SUSPICIOUS_CHARACTERS = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-suspicious-characters.xml"
)
FAL5_ONE_LETTER_NAME_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-one-letter-name.xml"
)
FAL6_ONE_LETTER_NAME_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-one-letter-name.xml"
)
FAL5_UNWANTED_NUMBERS_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-unwanted-numbers.xml"
)
FAL6_UNWANTED_NUMBERS_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-unwanted-numbers.xml"
)
FAL5_PARTICLE_NAME_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL5-particle-name.xml"
)
FAL6_PARTICLE_NAME_PATH = path.join(
    getcwd(), "backend/fal/tests/data/NCA_FAL6-particle-name.xml"
)
