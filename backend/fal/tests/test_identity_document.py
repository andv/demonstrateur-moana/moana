from django.conf import settings

import pytest

from backend.fal.tests import settings as fal_settings
from backend.logs import settings as logs_settings
from backend.logs.models import DataQualityLog
from backend.ship_files.models import ShipFile as Fal
from backend.unit_tests import utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_log_is_creating_if_nature_of_identity_document_is_empty(client):
    utils.login_saint_malo_user(client)
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_EMPTY_IDENTITY_DOC
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL5_EMPTY_IDENTITY_DOC)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_EMPTY_IDENTITY_DOC
    ).exists()


def test_log_is_creating_if_number_of_identity_document_is_empty(client):
    utils.login_saint_malo_user(client)
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_EMPTY_IDENTITY_DOC
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL6_EMPTY_IDENTITY_DOC)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_EMPTY_IDENTITY_DOC
    ).exists()


def test_log_is_creating_if_identity_documents_are_missing(client):
    utils.login_saint_malo_user(client)
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_EMPTY_IDENTITY_DOC
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL6_MISSING_IDENTITY)
    fal.save()
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_EMPTY_IDENTITY_DOC
    ).exists()


def test_no_logs_creating_if_empty_visa_number(client):
    utils.login_saint_malo_user(client)
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_EMPTY_IDENTITY_DOC
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL6_EMPTY_VISA_NUMBER)
    fal.save()
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_EMPTY_IDENTITY_DOC
    ).exists()
