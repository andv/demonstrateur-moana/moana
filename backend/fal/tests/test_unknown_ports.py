from django.conf import settings

import pytest

from backend.fal.tests import settings as fal_settings
from backend.logs import settings as logs_settings
from backend.logs.models import DataQualityLog
from backend.ship_files.models import ShipFile as Fal

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_fal1_log_entry_is_created_for_bad_name(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_UNKNOWN_PORT
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL1_UNKNOWN_PORT)
    fal.save()
    count_logs = DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_UNKNOWN_PORT
    ).count()
    # The test file has 3 unknown ports
    assert count_logs == 3


def test_fal5_log_entry_is_created_for_bad_name(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_UNKNOWN_PORT
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL5_UNKNOWN_PORT)
    fal.save()
    count_logs = DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_UNKNOWN_PORT
    ).count()
    # The test file has 2 unknown ports
    assert count_logs == 2


def test_fal6_log_entry_is_created_for_bad_name(client):
    assert not DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_UNKNOWN_PORT
    ).exists()
    fal = Fal(ship_file=fal_settings.FAL6_UNKNOWN_PORT)
    fal.save()
    count_logs = DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_UNKNOWN_PORT
    ).count()
    # The test file has 1 unknown port
    assert count_logs == 1
