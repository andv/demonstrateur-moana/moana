from os import listdir, path

from django.conf import settings

from backend.ship_files.models import ShipFile as Fal

FAL_UPLOAD_DIR = path.join(settings.MEDIA_ROOT, Fal.ship_file.field.upload_to)


def count_uploaded_fal_files():
    count = len(
        [
            name
            for name in listdir(FAL_UPLOAD_DIR)
            if path.isfile(path.join(FAL_UPLOAD_DIR, name))
        ]
    )
    return count
