from django.conf import settings

import pytest

from backend.fal.tests import settings as fal_settings
from backend.logs import settings as logs_settings
from backend.logs.models import DataQualityLog
from backend.ship_files.models import ShipFile as Fal
from backend.ships.models import CrewList, PassengerList

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


#############
# Crew List #
#############


def create_crew_list_with_suspicious_data():
    fal = Fal(ship_file=fal_settings.FAL5_SUSPICIOUS_CHARACTERS)
    fal.save()
    crew_list = CrewList.objects.last()
    return crew_list


def test_fal5_with_suspicious_last_name_is_safe_for_list_download():
    crew_list = create_crew_list_with_suspicious_data()
    assert "'=suspicious last name" in str(crew_list.fpr_file_content)
    assert "'=suspicious last name" in str(crew_list.roc_file_content)


def test_fal5_with_suspicious_first_name_is_safe_for_list_download():
    crew_list = create_crew_list_with_suspicious_data()
    assert "'+suspicious first name" in str(crew_list.fpr_file_content)
    assert "'+suspicious first name" in str(crew_list.roc_file_content)


def test_fal5_with_suspicious_duty_is_safe_for_list_download():
    crew_list = create_crew_list_with_suspicious_data()
    assert "'@suspicious restaurant" in str(crew_list.fpr_file_content)
    assert "'@suspicious restaurant" in str(crew_list.roc_file_content)


def test_fal5_with_suspicious_place_of_birth_is_safe_for_list_download():
    crew_list = create_crew_list_with_suspicious_data()
    assert "'-suspicious city" in str(crew_list.fpr_file_content)
    assert "'-suspicious city" in str(crew_list.roc_file_content)


def test_fal5_with_suspicious_identity_document_is_safe_for_list_download():
    crew_list = create_crew_list_with_suspicious_data()
    assert "'%suspicious passport" in str(crew_list.fpr_file_content)
    assert "'%suspicious passport" in str(crew_list.roc_file_content)


def test_fal5_with_suspicious_identity_number_is_safe_for_list_download():
    crew_list = create_crew_list_with_suspicious_data()
    assert "'=suspicious identity number" in str(crew_list.fpr_file_content)
    assert "'=suspicious identity number" in str(crew_list.roc_file_content)


###################
# Passengers List #
###################


def create_passengers_list_with_suspicious_data():
    fal = Fal(ship_file=fal_settings.FAL6_SUSPICIOUS_CHARACTERS)
    fal.save()
    passengers_list = PassengerList.objects.last()
    return passengers_list


def test_fal6_with_suspicious_last_name_is_safe_for_list_download():
    passengers_list = create_passengers_list_with_suspicious_data()
    assert "'=suspicious last name" in str(passengers_list.fpr_file_content)
    assert "'=suspicious last name" in str(passengers_list.roc_file_content)


def test_fal6_with_suspicious_first_name_is_safe_for_list_download():
    passengers_list = create_passengers_list_with_suspicious_data()
    assert "'+suspicious first name" in str(passengers_list.fpr_file_content)
    assert "'+suspicious first name" in str(passengers_list.roc_file_content)


def test_fal6_with_suspicious_place_of_birth_is_safe_for_list_download():
    passengers_list = create_passengers_list_with_suspicious_data()
    assert "'@suspicious city" in str(passengers_list.fpr_file_content)
    assert "'@suspicious city" in str(passengers_list.roc_file_content)


def test_fal6_with_suspicious_identity_document_is_safe_for_list_download():
    passengers_list = create_passengers_list_with_suspicious_data()
    assert "'-suspicious passport" in str(passengers_list.fpr_file_content)
    assert "'-suspicious passport" in str(passengers_list.roc_file_content)


def test_fal6_with_suspicious_identity_number_is_safe_for_list_download():
    passengers_list = create_passengers_list_with_suspicious_data()
    assert "'%suspicious identity number" in str(passengers_list.fpr_file_content)
    assert "'%suspicious identity number" in str(passengers_list.roc_file_content)


##############
# Annex List #
##############


def test_fal6_with_suspicious_field_and_annex_is_safe_for_list_download():
    passengers_list = create_passengers_list_with_suspicious_data()
    assert "'=suspicious indentity number in annex" in str(
        passengers_list.fpr_annex_file_content
    )
    assert "'=suspicious indentity number in annex" in str(
        passengers_list.roc_annex_file_content
    )


def test_fal5_with_suspicious_field_and_annex_is_safe_for_list_download():
    crew_list = create_crew_list_with_suspicious_data()
    assert "'=suspicious indentity number in annex" in str(
        crew_list.fpr_annex_file_content
    )
    assert "'=suspicious indentity number in annex" in str(
        crew_list.roc_annex_file_content
    )
