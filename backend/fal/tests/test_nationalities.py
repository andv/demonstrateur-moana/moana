import re

from django.conf import settings

import pytest

from backend.fal.tests import settings as fal_settings
from backend.logs import settings as logs_settings
from backend.logs.models import DataQualityLog
from backend.nationalities.models import Nationality
from backend.ship_files.models import ShipFile as Fal
from backend.ships.models import CrewList, PassengerList

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def initialize_countries():
    Nationality.objects.create(
        country_name="Italy",
        country_code="IT",
        is_in_eu=True,
        needs_visa=False,
        needs_visa_transit=False,
        no_needs_visa=False,
    )
    Nationality.objects.create(
        country_name="Philippines",
        country_code="PH",
        is_in_eu=False,
        needs_visa=True,
        needs_visa_transit=False,
        no_needs_visa=False,
    )
    Nationality.objects.create(
        country_name="Yemen",
        country_code="YE",
        is_in_eu=False,
        needs_visa=True,
        needs_visa_transit=True,
        no_needs_visa=False,
    )
    Nationality.objects.create(
        country_name="Albanie",
        country_code="AL",
        is_in_eu=False,
        needs_visa=False,
        needs_visa_transit=False,
        no_needs_visa=True,
    )


######################
# Passengers         #
######################


def test_passengers_nationalities_fr_counters_are_updated():
    initialize_countries()
    fal = Fal(ship_file=fal_settings.FAL6_PATH)
    fal.save()
    count = len(re.findall('Nationality="FR"', str(fal.ship_file.read())))
    person_list = PassengerList.objects.last()
    assert person_list.nationalities_fr == count


def test_passengers_nationalities_eu_counters_are_updated():
    initialize_countries()
    fal = Fal(ship_file=fal_settings.FAL6_PATH)
    fal.save()
    count = len(re.findall('Nationality="IT"', str(fal.ship_file.read())))
    person_list = PassengerList.objects.last()
    assert person_list.nationalities_eu == count


def test_passengers_nationalities_need_visa_counters_are_updates():
    initialize_countries()
    fal = Fal(ship_file=fal_settings.FAL6_PATH)
    fal_content = str(fal.ship_file.read())
    count = len(re.findall('Nationality="PH"', fal_content))
    count += len(re.findall('Nationality="YE"', fal_content))
    fal.save()
    person_list = PassengerList.objects.last()
    assert person_list.nationalities_visa == count


def test_passengers_nationalities_need_visa_transit_counters_are_updates():
    initialize_countries()
    fal = Fal(ship_file=fal_settings.FAL6_PATH)
    fal.save()
    count = len(re.findall('Nationality="YE"', str(fal.ship_file.read())))
    person_list = PassengerList.objects.last()
    assert person_list.nationalities_visa_transit == count


def test_passengers_nationalities_need_no_visa_counters_are_updates():
    initialize_countries()
    fal = Fal(ship_file=fal_settings.FAL6_PATH)
    fal.save()
    count = len(re.findall('Nationality="AL"', str(fal.ship_file.read())))
    person_list = PassengerList.objects.last()
    assert person_list.nationalities_no_visa == count


######################
# Crew               #
######################


def test_crew_nationalities_fr_counters_are_updated():
    initialize_countries()
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    fal.save()
    count = len(re.findall('Nationality="FR"', str(fal.ship_file.read())))
    person_list = CrewList.objects.last()
    assert person_list.nationalities_fr == count


def test_crew_nationalities_eu_counters_are_updated():
    initialize_countries()
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    fal.save()
    count = len(re.findall('Nationality="IT"', str(fal.ship_file.read())))
    person_list = CrewList.objects.last()
    assert person_list.nationalities_eu == count


def test_crew_nationalities_need_visa_counters_are_updates():
    initialize_countries()
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    fal_content = str(fal.ship_file.read())
    count = len(re.findall('Nationality="PH"', fal_content))
    count += len(re.findall('Nationality="YE"', fal_content))
    fal.save()
    person_list = CrewList.objects.last()
    assert person_list.nationalities_visa == count


def test_crew_nationalities_need_visa_transit_counters_are_updates():
    initialize_countries()
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    fal.save()
    count = len(re.findall('Nationality="YE"', str(fal.ship_file.read())))
    person_list = CrewList.objects.last()
    assert person_list.nationalities_visa_transit == count


def test_crew_nationalities_need_no_visa_counters_are_updates():
    initialize_countries()
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    fal.save()
    count = len(re.findall('Nationality="AL"', str(fal.ship_file.read())))
    person_list = CrewList.objects.last()
    assert person_list.nationalities_no_visa == count


######################
# Tracking           #
######################


def test_passengers_missing_nationalities_log_entry_is_created():
    initialize_countries()
    Nationality.objects.filter(country_code="YE").delete()
    fal = Fal(ship_file=fal_settings.FAL6_PATH)
    fal.save()
    assert 'Nationality="YE"' in str(fal.ship_file.read())
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_MISSING_NATIONALITIES
    ).exists()


def test_crew_missing_nationalities_log_entry_is_created():
    initialize_countries()
    Nationality.objects.filter(country_code="YE").delete()
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    fal.save()
    assert 'Nationality="YE"' in str(fal.ship_file.read())
    assert DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_MISSING_NATIONALITIES
    ).exists()
