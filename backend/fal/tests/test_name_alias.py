from django.conf import settings

import pytest

from backend.fal.tests import settings as fal_settings
from backend.ship_files.models import ShipFile as Fal
from backend.ships.models import CrewList, PassengerList
from backend.unit_tests import utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_fal6_with_name_aliases_produce_annex_file(client):
    utils.login_saint_malo_user(client)
    before = PassengerList.objects.count()
    fal = Fal(ship_file=fal_settings.FAL6_NAME_ALIASES_PATH)
    fal.save()
    after = PassengerList.objects.count()
    assert after == before + 1
    passenger_list = PassengerList.objects.last()
    client.get(passenger_list.ship_movement.passengers_list_annex_file_roc)
    client.get(passenger_list.ship_movement.passengers_list_annex_file_fpr)
    # FPR
    assert "Coltrane DI Trane,John" in str(passenger_list.fpr_file_content)
    assert "Coltrane,John" in str(passenger_list.fpr_annex_file_content)
    assert "Trane,John" in str(passenger_list.fpr_annex_file_content)
    assert "McLeod Eps Coltrane,Alice" in str(passenger_list.fpr_file_content)
    assert "McLeod,Alice" in str(passenger_list.fpr_annex_file_content)
    assert "Coltrane,Alice" in str(passenger_list.fpr_annex_file_content)
    # ROC
    assert "Coltrane DI Trane,John" in str(passenger_list.roc_file_content)
    assert "Coltrane,John" in str(passenger_list.roc_annex_file_content)
    assert "Trane,John" in str(passenger_list.roc_annex_file_content)
    assert "McLeod Eps Coltrane,Alice" in str(passenger_list.roc_file_content)
    assert "McLeod,Alice" in str(passenger_list.roc_annex_file_content)
    assert "Coltrane,Alice" in str(passenger_list.roc_annex_file_content)


def test_fal5_with_name_aliases_produce_annex_file(client):
    utils.login_saint_malo_user(client)
    before = CrewList.objects.count()
    fal = Fal(ship_file=fal_settings.FAL5_NAME_ALIASES_PATH)
    fal.save()
    after = CrewList.objects.count()
    assert after == before + 1
    crew_list = CrewList.objects.last()
    client.get(crew_list.ship_movement.crew_list_annex_file_roc)
    client.get(crew_list.ship_movement.crew_list_annex_file_fpr)
    # FPR
    assert "Pops DI Satch,Louis" in str(crew_list.fpr_file_content)
    assert "Pops,Louis" in str(crew_list.fpr_annex_file_content)
    assert "Satch,Louis" in str(crew_list.fpr_annex_file_content)
    assert "Hardin Eps Armstrong,Lil" in str(crew_list.fpr_file_content)
    assert "Hardin,Lil" in str(crew_list.fpr_annex_file_content)
    assert "Armstrong,Lil" in str(crew_list.fpr_annex_file_content)
    # ROC
    assert "Pops DI Satch,Louis" in str(crew_list.roc_file_content)
    assert "Pops,Louis" in str(crew_list.roc_annex_file_content)
    assert "Satch,Louis" in str(crew_list.roc_annex_file_content)
    assert "Hardin Eps Armstrong,Lil" in str(crew_list.roc_file_content)
    assert "Hardin,Lil" in str(crew_list.roc_annex_file_content)
    assert "Armstrong,Lil" in str(crew_list.roc_annex_file_content)


def test_fal6_with_name_aliases_in_firstname_produce_annex_file(client):
    utils.login_saint_malo_user(client)
    before = PassengerList.objects.count()
    fal = Fal(ship_file=fal_settings.FAL6_FIRSTNAME_ALIASES_PATH)
    fal.save()
    after = PassengerList.objects.count()
    assert after == before + 1
    passenger_list = PassengerList.objects.last()
    client.get(passenger_list.ship_movement.passengers_list_annex_file_roc)
    client.get(passenger_list.ship_movement.passengers_list_annex_file_fpr)
    # FPR
    assert "McLeod,Alice Eps Coltrane" in str(passenger_list.fpr_file_content)
    assert "McLeod,Alice" in str(passenger_list.fpr_annex_file_content)
    assert "Coltrane,Alice" in str(passenger_list.fpr_annex_file_content)
    assert "Ono,Yoko Ep Lennon" in str(passenger_list.fpr_file_content)
    assert "Ono,Yoko" in str(passenger_list.fpr_annex_file_content)
    assert "Lennon,Yoko" in str(passenger_list.fpr_annex_file_content)
    # ROC
    assert "McLeod,Alice Eps Coltrane" in str(passenger_list.roc_file_content)
    assert "McLeod,Alice" in str(passenger_list.roc_annex_file_content)
    assert "Coltrane,Alice" in str(passenger_list.roc_annex_file_content)
    assert "Ono,Yoko Ep Lennon" in str(passenger_list.roc_file_content)
    assert "Ono,Yoko" in str(passenger_list.roc_annex_file_content)
    assert "Lennon,Yoko" in str(passenger_list.roc_annex_file_content)


def test_fal5_with_name_aliases_in_firstname_produce_annex_file(client):
    utils.login_saint_malo_user(client)
    before = CrewList.objects.count()
    fal = Fal(ship_file=fal_settings.FAL5_FIRSTNAME_ALIASES_PATH)
    fal.save()
    after = CrewList.objects.count()
    assert after == before + 1
    crew_list = CrewList.objects.last()
    client.get(crew_list.ship_movement.crew_list_annex_file_roc)
    client.get(crew_list.ship_movement.crew_list_annex_file_fpr)
    # FPR
    assert "McLeod,Alice Eps Coltrane" in str(crew_list.fpr_file_content)
    assert "McLeod,Alice" in str(crew_list.fpr_annex_file_content)
    assert "Coltrane,Alice" in str(crew_list.fpr_annex_file_content)
    assert "Ono,Yoko Ep Lennon" in str(crew_list.fpr_file_content)
    assert "Ono,Yoko" in str(crew_list.fpr_annex_file_content)
    assert "Lennon,Yoko" in str(crew_list.fpr_annex_file_content)
    # ROC
    assert "McLeod,Alice Eps Coltrane" in str(crew_list.roc_file_content)
    assert "McLeod,Alice" in str(crew_list.roc_annex_file_content)
    assert "Coltrane,Alice" in str(crew_list.roc_annex_file_content)
    assert "Ono,Yoko Ep Lennon" in str(crew_list.roc_file_content)
    assert "Ono,Yoko" in str(crew_list.roc_annex_file_content)
    assert "Lennon,Yoko" in str(crew_list.roc_annex_file_content)
