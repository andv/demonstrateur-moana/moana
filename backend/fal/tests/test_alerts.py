from django.conf import settings

import pytest

from backend.alerts import choices as alerts_choices
from backend.alerts.models import ShipAlert
from backend.fal.tests import settings as fal_settings
from backend.ship_files.models import ShipFile as Fal
from backend.ships.models import ShipMovement

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


####################
# Country alerts   #
####################


def has_country_alert(ship_movement):
    has_alert = ShipAlert.objects.filter(
        ship_movement=ship_movement,
        alert_type__alert_type=alerts_choices.SHIP_COUNTRY_ALERT,
    ).exists()
    return has_alert


def run_country_alert_test(ship_file, settings):
    """
    Helper method that for running country alert test for the given FAL file.
    """
    # 1. At first, we expect no alert, because the black-listed country list is empty.
    settings.ALERT_SHIP_COUNTRIES = []
    fal = Fal(ship_file=ship_file)
    fal.save()
    ship = ShipMovement.objects.get(ship_call_id="FRSML20217396")
    assert not has_country_alert(ship)
    # 2. Then, we expect an alert for FR, which is the FlagStateOfShip in this FAL file
    settings.ALERT_SHIP_COUNTRIES = ["FR"]
    fal = Fal(ship_file=ship_file)
    fal.save()
    ship = ShipMovement.objects.get(ship_call_id="FRSML20217396")
    assert has_country_alert(ship)


def test_country_alert_is_created_when_uploading_fal1_with_alert(client, settings):
    run_country_alert_test(fal_settings.FAL1_PATH, settings)


def test_country_alert_is_created_when_uploading_fal5_with_alert(client, settings):
    run_country_alert_test(fal_settings.FAL5_PATH, settings)


def test_country_alert_is_created_when_uploading_fal6_with_alert(client, settings):
    run_country_alert_test(fal_settings.FAL6_PATH, settings)


######################
# Nationality alerts #
######################


def has_crew_nationality_alert(ship_movement):
    has_alert = ShipAlert.objects.filter(
        ship_movement=ship_movement,
        alert_type__alert_type=alerts_choices.CREW_NATIONALITY_ALERT,
    ).exists()
    return has_alert


def has_passenger_nationality_alert(ship_movement):
    has_alert = ShipAlert.objects.filter(
        ship_movement=ship_movement,
        alert_type__alert_type=alerts_choices.PASSENGER_NATIONALITY_ALERT,
    ).exists()
    return has_alert


def test_nationality_alert_is_created_when_uploading_fal5_with_alert(client, settings):
    # 1. At first, we expect no alert, because the black-listed nationality list is empty.
    settings.ALERT_NATIONALITIES = []
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    fal.save()
    ship = ShipMovement.objects.get(ship_call_id="FRSML20217396")
    assert not has_crew_nationality_alert(ship)
    # 2. Then, we expect an alert for FR nationality, because a person in the FAL file has FR nationality
    settings.ALERT_NATIONALITIES = ["FR"]
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    fal.save()
    ship = ShipMovement.objects.get(ship_call_id="FRSML20217396")
    assert has_crew_nationality_alert(ship)


def test_nationality_alert_is_created_when_uploading_fal6_with_alert(client, settings):
    # 1. At first, we expect no alert, because the black-listed nationality list  is empty.
    settings.ALERT_NATIONALITIES = []
    fal = Fal(ship_file=fal_settings.FAL6_PATH)
    fal.save()
    ship = ShipMovement.objects.get(ship_call_id="FRSML20217396")
    assert not has_passenger_nationality_alert(ship)
    # 2. Then, we expect an alert for FR nationality, because a person in the FAL file has FR nationality
    settings.ALERT_NATIONALITIES = ["FR"]
    fal = Fal(ship_file=fal_settings.FAL6_PATH)
    fal.save()
    ship = ShipMovement.objects.get(ship_call_id="FRSML20217396")
    assert has_passenger_nationality_alert(ship)
