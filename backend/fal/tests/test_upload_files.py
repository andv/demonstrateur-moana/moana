import re

from django.conf import settings

import pytest

from backend.fal.tests import settings as fal_settings
from backend.geo import choices as geo_choices
from backend.ports.models import Port
from backend.ship_files import settings as ship_files_settings
from backend.ship_files.models import ShipFile as Fal
from backend.ship_types.models import ShipType, ShipTypeMapping
from backend.ships import settings as ships_settings
from backend.ships.models import CrewList, PassengerList, ShipMovement
from backend.unit_tests import utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


################
# Fal Files    #
################


def test_file_type_is_set_when_uploading_file(client):
    fal = Fal(ship_file=fal_settings.FAL1_PATH)
    fal.save()
    assert fal.file_type == ship_files_settings.NCA_FAL1


#################
# Ship Movement #
#################


def test_ship_movement_entry_is_created_when_uploading_fal1(client):
    fal = Fal(ship_file=fal_settings.FAL1_PATH)
    before = ShipMovement.objects.count()
    fal.save()
    after = ShipMovement.objects.count()
    assert after == before + 1


def test_ship_movement_is_linked_to_ports_when_uploading_fal1(client):
    Port.objects.create(locode="FRSML", name="ST Malo")
    Port.objects.create(locode="FRHON", name="Honfleur")
    Port.objects.create(locode="ZZUKN", name="Unknown")
    fal = Fal(ship_file=fal_settings.FAL1_PATH)
    fal.save()
    ship = ShipMovement.objects.first()
    assert ship.port_of_call_relation.name == "ST Malo"
    assert ship.last_port_relation.name == "Honfleur"
    assert ship.next_port_relation.name == "Unknown"


def test_ship_movement_is_associated_to_ship_type(client):
    type_name = "some-vessel-type"
    imo_used_in_fal = "9814052"
    ShipType.objects.create(name_reference=type_name)
    ShipType.objects.create(name_reference="ignore this type")
    ShipTypeMapping.objects.create(imo_number=imo_used_in_fal, ship_type_name=type_name)
    fal = Fal(ship_file=fal_settings.FAL1_PATH)
    fal.save()
    ship = ShipMovement.objects.get(imo_number=imo_used_in_fal)
    assert ship.ship_type.name_reference == type_name


def test_ship_movement_is_associated_to_geographic_area(client):
    imo_used_in_fal = "9814052"
    fal = Fal(ship_file=fal_settings.FAL1_PATH)
    fal.save()
    ship = ShipMovement.objects.get(imo_number=imo_used_in_fal)
    assert ship.geographic_area == geo_choices.AREA_FRANCE_METROPOLITAN


################
# Crew         #
################


def test_ship_movement_entry_is_created_when_uploading_fal5(client):
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    before = ShipMovement.objects.count()
    fal.save()
    after = ShipMovement.objects.count()
    assert after == before + 1


def test_ship_movement_is_linked_to_ports_when_uploading_fal5(client):
    Port.objects.create(locode="FRSML", name="ST Malo")
    Port.objects.create(locode="FRHON", name="Honfleur")
    Port.objects.create(locode="ZZUKN", name="Unknown")
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    fal.save()
    ship = ShipMovement.objects.first()
    assert ship.port_of_call_relation.name == "ST Malo"
    assert ship.last_port_relation.name == "Honfleur"


def test_crew_list_entry_is_created_when_uploading_fal5(client):
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    before = CrewList.objects.count()
    fal.save()
    after = CrewList.objects.count()
    assert after == before + 1


def test_fpr_file_content_is_created_when_uploading_fal5(client):
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    fal.save()
    crew_list = CrewList.objects.last()
    assert crew_list.fpr_file_content


def test_csv_download_is_available_when_uploading_fal5(client):
    utils.login_saint_malo_user(client)
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    fal.save()
    fal = Fal(ship_file=fal_settings.FAL1_PATH)
    fal.save()
    crew_list = CrewList.objects.last()
    assert crew_list.fpr_file_content
    client.get(crew_list.ship_movement.crew_list_file_fpr)


def test_ship_movement_entry_gets_linked_to_crew_list_entry(client):
    CrewList.objects.create(ship_call_id="ignore this")
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    fal.save()
    CrewList.objects.create(ship_call_id="ignore this as well")
    expected_linked_crew = CrewList.objects.get(
        ship_call_id="FRSML20217396", way=ships_settings.ARRIVING
    )
    fal = Fal(ship_file=fal_settings.FAL1_PATH)
    fal.save()
    ship = ShipMovement.objects.last()
    assert ship.crew_list == expected_linked_crew


def test_crew_list_entry_gets_linked_to_ship_movement_entry(client):
    ShipMovement.objects.create(ship_call_id="ignore this")
    fal = Fal(ship_file=fal_settings.FAL1_PATH)
    fal.save()
    ShipMovement.objects.create(ship_call_id="ignore this as well")
    expected_linked_ship = ShipMovement.objects.get(
        ship_call_id="FRSML20217396", way=ships_settings.ARRIVING
    )
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    fal.save()
    crew = CrewList.objects.last()
    assert crew.ship_movement == expected_linked_ship


def test_parsed_number_of_crew_members_gets_calculated(client):
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    fal.save()
    count = len(re.findall("<CrewMember ", str(fal.ship_file.read())))
    crew_list = CrewList.objects.last()
    assert crew_list.number_of_crew_parsed == count


def test_ship_movement_data_remains_if_empty_data_reuploaded_from_FAL5(client):
    # 1. First, we check that the ETA field is empty - FAL5 data
    fal = Fal(ship_file=fal_settings.FAL5_MISSING_DATA_PATH)
    fal.save()
    ship = ShipMovement.objects.last()
    assert not ship.eta_to_port_of_call
    # 2. Second, we check that the ETA field is filled - FAL1 data
    fal = Fal(ship_file=fal_settings.FAL1_PATH)
    fal.save()
    ship = ShipMovement.objects.last()
    assert ship.eta_to_port_of_call
    # 3. Last, on re-uploading again, we check that the ETA field remains
    fal = Fal(ship_file=fal_settings.FAL5_MISSING_DATA_PATH)
    fal.save()
    ship = ShipMovement.objects.last()
    assert ship.eta_to_port_of_call


def test_ship_movement_sensitive_data_remains_if_data_reuploaded_from_FAL5(client):
    fal1 = Fal(ship_file=fal_settings.FAL1_PATH)
    fal1.save()
    ship = ShipMovement.objects.last()
    ship_eta_before = ship.eta_to_port_of_call
    ship_lastport_before = ship.last_port
    fal5 = Fal(ship_file=fal_settings.FAL5_CHANGE_SENSITIVE_STOPOVER_DATA)
    fal5.save()
    ship_eta_after = ship.eta_to_port_of_call
    ship_lastport_after = ship.last_port
    assert ship_eta_before == ship_eta_after
    assert ship_lastport_before == ship_lastport_after


################
# Passengers   #
################


def test_ship_movement_entry_is_created_when_uploading_fal6(client):
    fal = Fal(ship_file=fal_settings.FAL6_PATH)
    before = ShipMovement.objects.count()
    fal.save()
    after = ShipMovement.objects.count()
    assert after == before + 1


def test_ship_movement_is_linked_to_ports_when_uploading_fal6(client):
    Port.objects.create(locode="FRSML", name="ST Malo")
    Port.objects.create(locode="FRHON", name="Honfleur")
    Port.objects.create(locode="ZZUKN", name="Unknown")
    fal = Fal(ship_file=fal_settings.FAL5_PATH)
    fal.save()
    ship = ShipMovement.objects.first()
    assert ship.port_of_call_relation.name == "ST Malo"


def test_passenger_list_entry_is_created_when_uploading_fal6(client):
    fal = Fal(ship_file=fal_settings.FAL6_PATH)
    before = PassengerList.objects.count()
    fal.save()
    after = PassengerList.objects.count()
    assert after == before + 1


def test_file_content_is_created_when_uploading_fal6(client):
    fal = Fal(ship_file=fal_settings.FAL6_PATH)
    fal.save()
    passenger_list = PassengerList.objects.last()
    assert passenger_list.fpr_file_content
    assert passenger_list.roc_file_content


def test_csv_download_is_available_when_uploading_fal6(client):
    utils.login_saint_malo_user(client)
    fal = Fal(ship_file=fal_settings.FAL6_PATH)
    fal.save()
    fal = Fal(ship_file=fal_settings.FAL1_PATH)
    fal.save()
    passenger_list = PassengerList.objects.last()
    assert passenger_list.fpr_file_content
    assert passenger_list.roc_file_content
    client.get(passenger_list.ship_movement.passengers_list_file_fpr)
    client.get(passenger_list.ship_movement.passengers_list_file_roc)


def test_ship_movement_entry_gets_linked_to_passenger_list_entry(client):
    PassengerList.objects.create(ship_call_id="ignore this")
    fal = Fal(ship_file=fal_settings.FAL6_PATH)
    fal.save()
    PassengerList.objects.create(ship_call_id="ignore this as well")
    expected_linked_passengers = PassengerList.objects.get(
        ship_call_id="FRSML20217396", way=ships_settings.ARRIVING
    )
    fal = Fal(ship_file=fal_settings.FAL1_PATH)
    fal.save()
    ship = ShipMovement.objects.last()
    assert ship.passenger_list == expected_linked_passengers


def test_passenger_list_entry_gets_linked_to_ship_movement_entry(client):
    ShipMovement.objects.create(ship_call_id="ignore this")
    fal = Fal(ship_file=fal_settings.FAL1_PATH)
    fal.save()
    ShipMovement.objects.create(ship_call_id="ignore this as well")
    expected_linked_ship = ShipMovement.objects.get(
        ship_call_id="FRSML20217396", way=ships_settings.ARRIVING
    )
    fal = Fal(ship_file=fal_settings.FAL6_PATH)
    fal.save()
    passengers = PassengerList.objects.last()
    assert passengers.ship_movement == expected_linked_ship


def test_parsed_number_of_passengers_gets_calculated(client):
    fal = Fal(ship_file=fal_settings.FAL6_PATH)
    fal.save()
    passengers = PassengerList.objects.last()
    count = len(re.findall("<Passenger ", str(fal.ship_file.read())))
    assert passengers.number_of_passengers_parsed == count


def test_ship_movement_data_remains_if_empty_data_reuploaded_from_FAL6(client):
    # 1. First, we check that the ETA field is empty - FAL6 data
    fal = Fal(ship_file=fal_settings.FAL6_MISSING_DATA_PATH)
    fal.save()
    ship = ShipMovement.objects.last()
    assert not ship.eta_to_port_of_call
    # 2. Second, we check that the ETA field is filled - FAL1 data
    fal = Fal(ship_file=fal_settings.FAL1_PATH)
    fal.save()
    ship = ShipMovement.objects.last()
    assert ship.eta_to_port_of_call
    # 3. Last, on re-uploading again, we check that the ETA field remains
    fal = Fal(ship_file=fal_settings.FAL6_MISSING_DATA_PATH)
    fal.save()
    ship = ShipMovement.objects.last()
    assert ship.eta_to_port_of_call


def test_ship_movement_sensitive_data_remains_if_data_reuploaded_from_FAL6(client):
    fal1 = Fal(ship_file=fal_settings.FAL1_PATH)
    fal1.save()
    ship = ShipMovement.objects.last()
    ship_eta_before = ship.eta_to_port_of_call
    ship_lastport_before = ship.last_port
    fal6 = Fal(ship_file=fal_settings.FAL6_CHANGE_SENSITIVE_STOPOVER_DATA)
    fal6.save()
    ship_eta_after = ship.eta_to_port_of_call
    ship_lastport_after = ship.last_port
    assert ship_eta_before == ship_eta_after
    assert ship_lastport_before == ship_lastport_after
