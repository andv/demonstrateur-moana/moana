from django.conf import settings

import pytest

from backend.fal.tests import settings as fal_settings
from backend.logs import settings as logs_settings
from backend.logs.models import DataQualityLog
from backend.ship_files.models import ShipFile as Fal

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_one_letter_name_in_fal5_creates_log():
    one_letter_name_logs_before = DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_ONLY_ONE_LETTER_NAME
    ).exists()
    fal5 = Fal(ship_file=fal_settings.FAL5_ONE_LETTER_NAME_PATH)
    fal5.save()
    one_letter_name_logs_after = DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_ONLY_ONE_LETTER_NAME
    ).exists()
    assert not one_letter_name_logs_before
    assert one_letter_name_logs_after


def test_one_letter_name_in_fal6_creates_log():
    one_letter_name_logs_before = DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_ONLY_ONE_LETTER_NAME
    ).exists()
    fal6 = Fal(ship_file=fal_settings.FAL6_ONE_LETTER_NAME_PATH)
    fal6.save()
    one_letter_name_logs_after = DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_ONLY_ONE_LETTER_NAME
    ).exists()
    assert not one_letter_name_logs_before
    assert one_letter_name_logs_after


def test_one_letter_name_log_have_good_count():
    fal5 = Fal(ship_file=fal_settings.FAL5_ONE_LETTER_NAME_PATH)
    fal5.save()
    log_created_fal5 = DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_ONLY_ONE_LETTER_NAME
    )[0]
    fal6 = Fal(ship_file=fal_settings.FAL6_ONE_LETTER_NAME_PATH)
    fal6.save()
    log_created_fal6 = DataQualityLog.objects.filter(
        action=logs_settings.LOG_NAME_ONLY_ONE_LETTER_NAME
    )[1]
    assert log_created_fal5.target == "4"
    assert log_created_fal6.target == "4"
