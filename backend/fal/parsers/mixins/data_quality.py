import re
from datetime import date

from django.conf import settings
from django.utils import timezone

from backend.fal import settings as fal_settings


class DataQualityParserMixin:

    def __init__(self):
        self.name_formatting_diff_list = []
        self.name_switch_list = []
        self.name_analysing_bad_encoding_list = []
        self.name_particle_list = []
        self.unwanted_numbers_list = []
        self.person_list_unwanted_numbers_issues = []
        self.past_date_of_birth_list = []
        self.future_date_of_birth_list = []
        self.person_list_past_dob_issues = []
        self.person_list_future_dob_issues = []
        self.count_name_with_bad_encoding = 0  # When encoding issue on first or last
        self.count_empty_names = 0  # When first or last is not set
        self.count_unknown_names = 0  # When first or last shows "inconnu"
        self.count_empty_identity_document = 0  # When number or nature is not set
        self.count_name_with_only_one_letter = 0
        self.person_list_only_one_letter_issues = []
        self.count_unwanted_numbers = 0
        self.count_name_particle = 0

    def anonymize_name(self, name_string):
        """
        Anonymize a string by replacing non-digit characters with an asterisk,
        keeping first and last characters and all digits.
        """
        if len(name_string) <= 2:
            return name_string  # No need to anonymize if the string is too short
        middle = name_string[1:-1]
        middle_anonymized = ["*" if not char.isdigit() else char for char in middle]
        middle_anonymized = "".join(middle_anonymized)
        anonymized = name_string[0] + middle_anonymized + name_string[-1]
        return anonymized

    def anonymize_number(self, number_string):
        """
        Anonymize a string by replacing non-digit characters with an asterisk,
        keeping first and last characters and all digits.
        """
        if len(number_string) <= 2:
            return number_string  # No need to anonymize if the string is too short
        middle = number_string[1:-1]
        middle_anonymized = ["*" for char in middle]
        middle_anonymized = "".join(middle_anonymized)
        anonymized = number_string[0] + middle_anonymized + number_string[-1]
        return anonymized

    def get_anonymized_person_row(
        self, first_name, last_name, number_of_identity_document
    ):
        """
        Anonymize the name and return a the result as a row with the format :
        first_name / last_name / number_of_identity_document
        """
        x_first_name = self.anonymize_name(first_name)
        x_last_name = self.anonymize_name(last_name)
        x_number_of_identity_document = self.anonymize_number(
            number_of_identity_document
        )
        name_row = [
            f"{x_first_name} / "
            + f"{x_last_name} / "
            + f"{x_number_of_identity_document}"
        ]
        return name_row

    def check_date_of_birth_issues(
        self,
        date_string,
        first_name,
        last_name,
        number_of_identity_document,
    ):
        """
        Checks and log date of birth issues.
        """
        date_formatted = self.format_date_of_birth(date_string)
        year = date.fromisoformat(date_string).year
        person_row = self.get_anonymized_person_row(
            first_name, last_name, number_of_identity_document
        )
        if year < 1900:
            if self.past_date_of_birth_list is None:
                self.past_date_of_birth_list = []
            self.past_date_of_birth_list.append(date_formatted)
            self.person_list_past_dob_issues.append(person_row)
        if year > timezone.now().year:
            if self.future_date_of_birth_list is None:
                self.future_date_of_birth_list = []
            self.future_date_of_birth_list.append(date_formatted)
            self.person_list_future_dob_issues.append(person_row)

    def format_date_of_birth(self, date_string):
        """
        Convert a date : YYYY-MM-DD into DD/MM/YYYY
        """
        d = date.fromisoformat(date_string)
        date_formatted = d.strftime("%d/%m/%Y")
        return date_formatted

    def get_diff_from_strings(self, string1, string2):
        string1_no_space = string1.replace(" ", "")
        string2_no_space = string2.replace(" ", "")
        set1 = set(string1_no_space)
        set2 = set(string2_no_space)
        difference = set1.symmetric_difference(set2)
        return " ".join(difference)

    def format_name(self, name):
        """
        Remove unwanted characters
        """
        clean_name = name
        to_be_replaced = re.compile(re.escape("ETlt;"), re.IGNORECASE)
        clean_name = to_be_replaced.sub(" ", clean_name)
        for character in fal_settings.UNWANTED_CHARACTERS:
            string_to_be_replaced = re.compile(re.escape(character), re.IGNORECASE)
            clean_name = string_to_be_replaced.sub(" ", clean_name)
        clean_name = " ".join(clean_name.split())  # Remove extra spaces
        if not clean_name:
            self.count_empty_names += 1
        difference = self.get_diff_from_strings(name, clean_name)
        if difference:
            self.name_formatting_diff_list.append(difference)
        return clean_name

    def switch_last_name(self, first_name, last_name):
        """
        Switches the first and last name if first name is present and last name is absent.
        """
        is_empty = not last_name
        is_unknown = last_name.lower() in fal_settings.UNKNOWN_NAMES
        lastname_need_switch = is_empty or is_unknown
        if first_name and lastname_need_switch:
            last_name, first_name = first_name, last_name
            self.name_switch_list.append(f"Switched last name : {last_name}")
        return first_name, last_name

    def check_unknown_values(self, first_name, last_name):
        """
        Check if the names are unknown.
        """
        if (
            first_name.lower() in fal_settings.UNKNOWN_NAMES
            or last_name.lower() in fal_settings.UNKNOWN_NAMES
        ):
            self.count_unknown_names += 1

    def check_empty_identity_document(
        self, nature_of_identity_document, number_of_identity_document
    ):
        """
        Check if the nature or the number of the identity docuement is missing
        """
        nature_cleaned = nature_of_identity_document.strip()
        number_cleaned = number_of_identity_document.strip()
        if not nature_cleaned or not number_cleaned:
            self.count_empty_identity_document += 1

    def check_bad_encoding(self, name):
        """
        Check if the given name has encoding issues. This check is based on list of characters
        that we've considered to be a good way of reveling an encoding issue. Thus, this check
        is as good as the trust we put upon that list of issue markers.
        """
        bad_char_found = []
        for bad_char in fal_settings.BAD_ENCODING_MARKERS:
            if bad_char in name:
                bad_char_found.append(bad_char)
        if len(bad_char_found) > 0:
            self.count_name_with_bad_encoding += 1
            self.name_analysing_bad_encoding_list.append(" ".join(bad_char_found))

    def check_only_one_letter_name(
        self, first_name, last_name, number_of_identity_document
    ):
        """
        Check if the first name or last name are only one letter long
        """
        has_one_letter_issue = False
        if len(first_name) == 1 or len(last_name) == 1:
            self.count_name_with_only_one_letter += 1
            has_one_letter_issue = True
        if has_one_letter_issue:
            person_row = self.get_anonymized_person_row(
                first_name, last_name, number_of_identity_document
            )
            self.person_list_only_one_letter_issues.append(person_row)
        return has_one_letter_issue

    def check_unwanted_numbers_in_fields(self, fields):
        """
        Check if specific fields contain a number. Returns True if a number is
        found in any field.
        """
        has_number = False
        fields_with_unwanted_numbers = []
        for field in fields:
            field_name = field[0]
            value = field[1]
            digit_found = [char for char in value if char.isdigit()]
            if len(digit_found) > 0:
                digit_found_stringify = " ".join(digit_found)
                fields_with_unwanted_numbers.append(
                    f"{digit_found_stringify} ({field_name})"
                )
                has_number = True
        if fields_with_unwanted_numbers:
            self.count_unwanted_numbers += 1
            self.unwanted_numbers_list.append(" ".join(fields_with_unwanted_numbers))
        return has_number

    def check_unwanted_numbers(
        self, first_name, last_name, number_of_identity_document, extra_fields
    ):
        """
        Check if unwanted numbers are found the the first name, last name
        or any given extra fields.
        This will also update the list of persons with unwanted numbers so
        that this information can be logged later on.
        """
        fields_to_check = [
            ("FirstName", first_name),
            ("LastName", last_name),
        ]
        fields_to_check.extend(extra_fields)
        has_number = self.check_unwanted_numbers_in_fields(fields=fields_to_check)
        if not has_number:
            return False
        person_row = self.get_anonymized_person_row(
            first_name, last_name, number_of_identity_document
        )
        self.person_list_unwanted_numbers_issues.append(person_row)
        return has_number

    def check_particle(self, last_name, first_name):
        """
        Check if lastname is a particle
        """
        for particle in settings.NAME_PARTICLES:
            if particle.upper() == last_name.upper():
                self.count_name_particle += 1
                self.name_particle_list.append(particle)
