class NationalityParserMixin:

    def __init__(self):
        self.crew_nationality_list = None
        self.passenger_nationality_list = None
        self.crew_nationality_counters = None
        self.passenger_nationality_counters = None

    def update_nationality_counters(self, nationality_counters, country_code):
        """
        The given nationality counters is a dictionary with nationality country code as key
        and count as value. This will updates the counter for the given nationality.
        """
        if nationality_counters is None:
            nationality_counters = {}
        if country_code in nationality_counters:
            nationality_counters[country_code] += 1
        else:
            nationality_counters[country_code] = 1
        return nationality_counters

    def get_crew_nationality_list(self):
        # If the list of nationalities is not there yet, we run the the routine that collects data
        # in order to build that list of nationalities.
        if self.crew_nationality_list is None:
            self.get_crew_list_data()
        return self.crew_nationality_list

    def get_passenger_nationality_list(self):
        # If the list of nationalities is not there yet, we run the the routine that collects data
        # in order to build that list of nationalities.
        if self.passenger_nationality_list is None:
            self.get_passenger_list_data()
        return self.passenger_nationality_list
