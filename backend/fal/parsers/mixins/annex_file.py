import re

from backend.fal import settings as fal_settings


class AnnexFileParserMixin:

    def __init__(self):
        self.passenger_annex_list = []
        self.crew_annex_list = []

    def get_crew_annex_list(self):
        return self.crew_annex_list

    def get_passenger_annex_list(self):
        return self.passenger_annex_list

    def return_aliases(self, name):
        """
        This will return a list of aliases for the given lastname.
        """
        name_alias_list = []
        for name_splitter in fal_settings.ALIAS_IN_PERSON_NAME_SPLITTER_LIST:
            if name_splitter.lower() in name.lower():
                splitted_names = re.split(name_splitter, name, flags=re.IGNORECASE)
                name_alias_list.extend(splitted_names)
        return name_alias_list

    def return_dict_aliases(self, first_name, last_name):
        """
        This will return a dict of lastname aliases when the firstname
        contains in fact married lastname.
        A dict is needed here to create two lines (one with the lastname alias
        and one with the lastname) and to add a cleaned version of the firstname
        to these entries.
        """
        firstname_alias_dict = {}
        for name_splitter in fal_settings.ALIAS_IN_PERSON_NAME_SPLITTER_LIST:
            if name_splitter.lower() in first_name.lower():
                splitted_names = re.split(
                    name_splitter, first_name, flags=re.IGNORECASE
                )
                firstname_alias_dict[splitted_names[1]] = splitted_names[0]
                firstname_alias_dict[last_name] = splitted_names[0]
        return firstname_alias_dict

    def populate_crew_annex_list(
        self,
        last_name,
        first_name,
        date_of_birth,
        place_of_birth,
        nationality,
        duty,
        nature_of_identity_document,
        number_of_identity_document,
        visa_or_resident_permit_number,
    ):
        """
        Populate the annex list containing names aliases.
        """
        if self.crew_annex_list is None:
            self.crew_annex_list = []
        last_name_aliases = self.return_aliases(last_name)
        for last_name_alias in last_name_aliases:
            self.crew_annex_list.append(
                [
                    last_name_alias,
                    first_name,
                    date_of_birth,
                    place_of_birth,
                    nationality,
                    duty,
                    nature_of_identity_document,
                    number_of_identity_document,
                    visa_or_resident_permit_number,
                ]
            )
        first_name_aliases = self.return_dict_aliases(first_name, last_name)
        for last_name_alias, first_name_alias in first_name_aliases.items():
            self.crew_annex_list.append(
                [
                    last_name_alias,
                    first_name_alias,
                    date_of_birth,
                    place_of_birth,
                    nationality,
                    duty,
                    nature_of_identity_document,
                    number_of_identity_document,
                    visa_or_resident_permit_number,
                ]
            )
        return self.crew_annex_list

    def populate_passenger_annex_list(
        self,
        last_name,
        first_name,
        date_of_birth,
        place_of_birth,
        nationality,
        transit,
        nature_of_identity_document,
        number_of_identity_document,
        visa_or_resident_permit_number,
    ):
        """
        Populate the annex list containing names aliases.
        """
        if self.passenger_annex_list is None:
            self.passenger_annex_list = []
        last_name_aliases = self.return_aliases(last_name)
        for last_name_alias in last_name_aliases:
            self.passenger_annex_list.append(
                [
                    last_name_alias,
                    first_name,
                    date_of_birth,
                    place_of_birth,
                    nationality,
                    transit,
                    nature_of_identity_document,
                    number_of_identity_document,
                    visa_or_resident_permit_number,
                ]
            )
        first_name_aliases = self.return_dict_aliases(first_name, last_name)
        for last_name_alias, first_name_alias in first_name_aliases.items():
            self.passenger_annex_list.append(
                [
                    last_name_alias,
                    first_name_alias,
                    date_of_birth,
                    place_of_birth,
                    nationality,
                    transit,
                    nature_of_identity_document,
                    number_of_identity_document,
                    visa_or_resident_permit_number,
                ]
            )
        return self.passenger_annex_list
