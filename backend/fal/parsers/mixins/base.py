from backend.nca.parsers import NcaParser
from backend.ship_files import settings as ship_files_settings
from backend.ships import settings as ships_settings
from backend.utils.countries import get_country_name_from_code


class BaseParser(NcaParser):
    data = None  # Fal data

    def __init__(self, file_path):
        super().__init__(file_path)
        self.vessel_identification = self.body.find("VesselIdentification")
        self.ship_call_information = self.body.find("ShipCallInformation")
        self.form_information = self.body.find("FormInformation")
        self.ship_information = self.form_information.find("ShipInformation")
        self.voyage_information = self.form_information.find("VoyageInformation")
        if self.file_type == ship_files_settings.NCA_FAL1:
            self.certificate_of_registry = self.form_information.find(
                "CertificateOfRegistry"
            )
            self.shipping_contact_details = self.certificate_of_registry.find(
                "ShippingContactDetails"
            )
            self.other_information = self.form_information.find("OtherInformations")
        if self.file_type == ship_files_settings.NCA_FAL5:
            self.crew_list = self.form_information.find("CrewList")
        if self.file_type == ship_files_settings.NCA_FAL6:
            self.passenger_list = self.form_information.find("PassengerList")

    def format_country(self, country_code):
        return get_country_name_from_code(country_code)

    def get_way(self, entry_or_exit):
        """
        The field "entry_or_exit" is defained like this : 0 = Entry = Arriving / 1 = Exit = Departing
        """
        ARRIVING = 0
        DEPARTING = 1
        if entry_or_exit == "" or entry_or_exit is None:
            return ""
        way = ""
        if int(entry_or_exit) == DEPARTING:
            way = ships_settings.DEPARTING
        if int(entry_or_exit) == ARRIVING:
            way = ships_settings.ARRIVING
        return way

    def get_common_data(self):
        """
        No matter what FAL we are importing, all of those field can be imported
        """
        data = {}
        way = self.get_way(entry_or_exit=self.form_information.get("EntryOrExit"))
        data["imo_number"] = self.vessel_identification.get("IMONumber")
        data["ship_call_id"] = self.ship_call_information.get("ShipCallId")
        data["way"] = way
        data["ship_name"] = self.ship_information.get("ShipName", "")
        data["call_sign"] = self.ship_information.get("CallSign", "")
        data["flag_state_of_ship"] = self.ship_information.get("FlagStateOfShip", "")
        self.data = data
        return data

    def get_stopover_data(self):
        """
        Specifics field that need to be imported following those rules :
        - FAL 1 : always
        - FAL 5 ou 6 : only if create a ship movement
        """
        data = {}
        eta = self.format_datetime(self.voyage_information.get("ETAToPortOfCall"))
        etd = self.format_datetime(self.voyage_information.get("ETDFromPortOfCall"))
        ata = self.format_datetime(self.voyage_information.get("ATAToPortOfCall"))
        atd = self.format_datetime(self.voyage_information.get("ATDFromPortOfCall"))
        data["last_port"] = self.voyage_information.get("LastPort", "")
        data["next_port"] = self.voyage_information.get("NextPort", "")
        data["eta_to_port_of_call"] = eta
        data["etd_from_port_of_call"] = etd
        data["ata_to_port_of_call"] = ata
        data["atd_from_port_of_call"] = atd
        data["port_of_call"] = self.ship_call_information.get("PortOfCall")
        return data
