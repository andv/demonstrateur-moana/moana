from backend.fal.parsers.mixins.base import BaseParser
from backend.ship_files import settings as ship_files_settings


class Fal1Parser(BaseParser):
    has_data_quality_log_for_person = False
    has_data_quality_log_for_ship = True

    def get_parsed_data(self):
        if self.file_type != ship_files_settings.NCA_FAL1:
            return None
        data = {}
        data.update(self.get_common_data())
        data.update(self.get_stopover_data())

        data["name_of_master"] = self.ship_information.get("NameOfMaster", "")
        data["registry_location_code"] = self.certificate_of_registry.get("Locode")
        data["registry_location_name"] = self.certificate_of_registry.get(
            "LocationName", ""
        )
        data["agent_name"] = self.shipping_contact_details.get("NameOfAgent", "")
        data["agent_phone"] = self.shipping_contact_details.get("Phone", "")
        data["agent_fax"] = self.shipping_contact_details.get("Fax", "")
        data["agent_email"] = self.shipping_contact_details.get("Email", "")
        data["gross_tonnage"] = self.other_information.get("GrossTonnage")
        data["net_tonnage"] = self.other_information.get("NetTonnage")
        data["position_in_port_of_call"] = self.other_information.get(
            "PositionInPortOfCall", ""
        )
        data["brief_particulars_of_voyage"] = self.other_information.get(
            "BriefParticularsOfVoyage", ""
        )
        data["number_of_crew"] = self.other_information.get("NumberOfCrew")
        data["number_of_passengers"] = self.other_information.get("NumberOfPassengers")
        return data
