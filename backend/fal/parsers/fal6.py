from backend.fal import settings as fal_settings
from backend.fal.parsers.mixins.annex_file import AnnexFileParserMixin
from backend.fal.parsers.mixins.base import BaseParser
from backend.fal.parsers.mixins.data_quality import DataQualityParserMixin
from backend.fal.parsers.mixins.nationalty import NationalityParserMixin
from backend.ship_files import settings as ship_files_settings
from backend.ships import settings as ship_settings


class Fal6Parser(
    BaseParser, DataQualityParserMixin, AnnexFileParserMixin, NationalityParserMixin
):
    has_data_quality_log_for_person = True
    has_data_quality_log_for_ship = True

    def __init__(self, fal_path):
        NationalityParserMixin.__init__(self)
        DataQualityParserMixin.__init__(self)
        AnnexFileParserMixin.__init__(self)
        super().__init__(fal_path)

    def get_parsed_data(self):
        if self.file_type != ship_files_settings.NCA_FAL6:
            return None
        data = {}
        data.update(self.get_common_data())
        return data

    def get_passenger_list_data(self):
        self.passenger_nationality_list = []
        if not self.passenger_list:
            return []
        data = []
        # Since this routine has access to each person's nationality,
        # it's a good place to store that information in a list.
        for passenger in self.passenger_list.findall("Passenger"):
            nationality = passenger.get("Nationality", "")
            self.passenger_nationality_list.append(nationality)
            self.passenger_nationality_counters = self.update_nationality_counters(
                self.passenger_nationality_counters, country_code=nationality
            )
            last_name = passenger.get("LastName", "")
            first_name = passenger.get("FirstName", "")
            place_of_birth = passenger.get("PlaceOfBirth", "")
            nature_of_identity_document = passenger.get("NatureOfIdentityDocument", "")
            number_of_identity_document = passenger.get("NumberOfIdentityDocument", "")
            visa_or_resident_permit_number = passenger.get(
                "VisaOrResidencePermitNumber", ""
            )
            transit = passenger.get("Transit", "")
            last_name = self.format_name(last_name)
            first_name, last_name = self.switch_last_name(
                first_name=first_name, last_name=last_name
            )
            first_name = self.format_name(first_name)
            self.check_unknown_values(first_name=first_name, last_name=last_name)
            self.check_bad_encoding(name=f"{last_name} {first_name}")
            self.check_only_one_letter_name(
                first_name, last_name, number_of_identity_document
            )
            fields_to_check_for_numbers = [
                ("NatureOfIdentityDocument", nature_of_identity_document),
                ("Nationality", nationality),
                ("Transit", transit),
            ]
            self.check_unwanted_numbers(
                first_name,
                last_name,
                number_of_identity_document,
                extra_fields=fields_to_check_for_numbers,
            )
            self.check_particle(last_name=last_name, first_name=first_name)
            self.check_empty_identity_document(
                nature_of_identity_document=nature_of_identity_document,
                number_of_identity_document=number_of_identity_document,
            )
            date_of_birth = passenger.get("DateOfBirth", "")
            self.check_date_of_birth_issues(
                date_of_birth,
                first_name,
                last_name,
                number_of_identity_document,
            )
            row = [
                last_name,
                first_name,
                self.format_date_of_birth(date_of_birth),
                place_of_birth,
                self.format_country(nationality),
                nature_of_identity_document,
                number_of_identity_document,
                visa_or_resident_permit_number,
                self.format_transit(transit),  # Needs to be last
            ]
            self.populate_passenger_annex_list(*row)
            data.append(row)
        ordered_passenger_data = self.order_passenger_data(data)
        return ordered_passenger_data

    def order_passenger_data(self, data):
        data.sort(key=self.get_status_value)
        return data

    def get_status_value(self, elem):
        last_index = len(elem) - 1
        return elem[last_index]

    def format_transit(self, transit):
        if transit == "Y":
            status = fal_settings.PASSENGER_IN_TRANSIT
        else:
            if self.data["way"] == ship_settings.ARRIVING:
                status = fal_settings.PASSENGER_DISEMBARK
            else:
                status = fal_settings.PASSENGER_EMBARK
        return status
