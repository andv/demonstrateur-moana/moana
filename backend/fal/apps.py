from django.apps import AppConfig


class FalConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.fal"
