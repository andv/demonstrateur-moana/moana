from django.apps import AppConfig


class TravelDurationConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.travel_duration"
    verbose_name = "Durée de traversée"
