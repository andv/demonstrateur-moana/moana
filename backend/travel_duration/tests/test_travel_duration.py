from django.conf import settings
from django.utils import timezone

import pytest

from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.travel_duration import choices as travel_duration_choices

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_metropolitan_movements_with_relevant_port_have_travel_duration():
    tomorrow = timezone.now() + timezone.timedelta(days=1)
    intra_schengen = ShipMovement.objects.create(
        port_of_call="FRSML",
        ship_call_id="intra_schengen_ship_123",
        way=ships_settings.ARRIVING,
        last_port="ESXXX",
        eta_to_port_of_call=tomorrow,
    )
    extra_schengen = ShipMovement.objects.create(
        port_of_call="FRSML",
        ship_call_id="extra_schengen_ship_123",
        way=ships_settings.ARRIVING,
        last_port="AZXXX",
        eta_to_port_of_call=tomorrow,
    )
    assert (
        intra_schengen.travel_duration == travel_duration_choices.TRAVEL_DURATION_SHORT
    )
    assert (
        extra_schengen.travel_duration == travel_duration_choices.TRAVEL_DURATION_LONG
    )


def test_metropolitan_movements_without_relevant_port_have_undetermined_travel_duration():
    ship = ShipMovement.objects.create(
        port_of_call="FRSML",
        ship_call_id="no_relevant_port_ship_123",
        way=ships_settings.ARRIVING,
    )
    assert ship.travel_duration == travel_duration_choices.TRAVEL_DURATION_UNDETERMINED


def test_metropolitan_movement_with_unknown_relevant_port_have_long_travel_duration():
    tomorrow = timezone.now() + timezone.timedelta(days=1)
    unknown = ShipMovement.objects.create(
        port_of_call="FRSML",
        ship_call_id="unknown_ship_123",
        way=ships_settings.ARRIVING,
        last_port="ZZUKN",
        eta_to_port_of_call=tomorrow,
    )
    assert (
        unknown.travel_duration == travel_duration_choices.TRAVEL_DURATION_UNDETERMINED
    )


def test_departing_movement_does_not_have_travel_duration():
    departing = ShipMovement.objects.create(
        port_of_call="FRSML",
        ship_call_id="departing_ship_123",
        way=ships_settings.DEPARTING,
    )
    assert departing.travel_duration == ""
    assert departing.h24_before_arrival == None


def test_drom_movement_does_not_have_travel_duration():
    tomorrow = timezone.now() + timezone.timedelta(days=1)
    drom = ShipMovement.objects.create(
        port_of_call="MQFDF",
        ship_call_id="drom_ship_123",
        way=ships_settings.ARRIVING,
        last_port="FRSML",
        eta_to_port_of_call=tomorrow,
    )
    assert drom.travel_duration == ""
    assert drom.h24_before_arrival == None


def test_movement_with_short_travel_does_not_have_h24_before_arrival_field():
    tomorrow = timezone.now() + timezone.timedelta(days=1)
    short_travel = ShipMovement.objects.create(
        port_of_call="FRSML",
        ship_call_id="short_travel_ship_123",
        way=ships_settings.ARRIVING,
        last_port="ESXXX",
        eta_to_port_of_call=tomorrow,
    )
    assert short_travel.h24_before_arrival == None


def test_movement_with_long_travel_has_h24_before_arrival_field_equals_to_24_hours_before_relevant_time():
    today = timezone.now()
    tomorrow = today + timezone.timedelta(days=1)
    long_travel = ShipMovement.objects.create(
        port_of_call="FRSML",
        ship_call_id="long_travel_ship_123",
        way=ships_settings.ARRIVING,
        last_port="AZXXX",
        eta_to_port_of_call=tomorrow,
    )
    assert long_travel.h24_before_arrival == today
