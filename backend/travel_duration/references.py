# List of countries area code with a travel duraion estimated to be less than 24 hours
METROPOLITAN_SHORT_TRAVEL_DURATION = [
    "DE",  # All schengen countries excepted Island
    "AT",
    "BE",
    "CH",
    "IS",
    "DK",
    "ES",
    "EE",
    "FI",
    "FR",
    "GR",
    "HU",
    "LI",
    "IT",
    "LV",
    "LT",
    "LU",
    "MT",
    "NL",
    "PL",
    "PT",
    "CZ",
    "NO",
    "SK",
    "SI",
    "SE",
    "RO",
    "BG",
    "FR",
    "GB",  # Great-Britain
    "IE",  # Ireland
    "DZ",  # Algeria
    "TN",  # Tunisia
    "GG",  # Guernsey
    "JE",  # Jersey
    "MC",  # Monaco
]
