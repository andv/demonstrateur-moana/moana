import datetime

from django.db import models

from dateutil.relativedelta import relativedelta

from backend.geo.ports import PortGeography
from backend.ships import settings as ships_settings
from backend.travel_duration import choices as travel_duration_choices
from backend.travel_duration import references as travel_duration_references


class WithTravelDuration(models.Model):
    travel_duration = models.CharField(
        verbose_name="Durée de la traversée",
        blank=True,
        max_length=250,
        choices=travel_duration_choices.TRAVEL_DURATION_CHOICES,
    )
    h24_before_arrival = models.DateTimeField(
        verbose_name="24 heures avant l'arrivée dans le port", blank=True, null=True
    )

    class Meta:
        abstract = True

    def is_long_travel(self):
        return self.travel_duration == travel_duration_choices.TRAVEL_DURATION_LONG


class TravelDurationModelHelper:
    """
    This abstract class is expected to be sub-classed on a ShipMovement-like model.
    It allow us to calculate TravelDurationField once related field are saved in ShipMovement :
    - travel_duration needs relevant_port
    - h24_before_arrival needs relevant_time
    """

    class Meta:
        abstract = True

    def get_area_code(self):
        return self.relevant_port[:2]

    def get_travel_duration(self):
        if not self.relevant_port or PortGeography(self.relevant_port).is_unknown():
            return travel_duration_choices.TRAVEL_DURATION_UNDETERMINED
        area_code = self.get_area_code()
        if area_code in travel_duration_references.METROPOLITAN_SHORT_TRAVEL_DURATION:
            return travel_duration_choices.TRAVEL_DURATION_SHORT
        else:
            return travel_duration_choices.TRAVEL_DURATION_LONG

    def set_h24_before_arrival(self):
        if not self.relevant_time:
            return
        if self.is_long_travel():
            self.h24_before_arrival = self.relevant_time + relativedelta(days=-1)

    def save(self, *args, **kwargs):
        if (
            self.port_of_call_is_in_france_metropolitan()
            and self.way == ships_settings.ARRIVING
        ):
            self.travel_duration = self.get_travel_duration()
            self.set_h24_before_arrival()
        return super().save(*args, **kwargs)
