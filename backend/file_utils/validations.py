import pathlib

from django.conf import settings

import magic


class FileValidationMixin:
    """
    General purpose file validation.
    """

    file_mime_type_whitelist = None
    file_extension_whitelist = None
    file_max_size = None

    def is_mime_type_valid(self, mime_type):
        if mime_type.lower() not in self.file_mime_type_whitelist:
            return False
        return True

    def file_mime_type_is_valid(self, csv_file):
        mime_type = magic.from_buffer(csv_file.read(2048), mime=True)
        return self.is_mime_type_valid(mime_type)

    def file_extension_is_valid(self, file_obj):
        file_extension = pathlib.PurePath(file_obj.name).suffix
        file_extension = file_extension.lower()
        extension_whitelist = [ext.lower() for ext in self.file_extension_whitelist]
        if file_extension not in extension_whitelist:
            return False
        return True

    def file_size_is_valid(self, file):
        if file.size > self.file_max_size:
            return False
        return True


class CSVFileValidationMixin(FileValidationMixin):
    """
    CSV file validation.
    """

    file_mime_type_whitelist = settings.CSV_FILE_MIME_TYPE_WHITELIST
    file_extension_whitelist = settings.CSV_FILE_EXTENSION_WHITELIST
    file_max_size = settings.CSV_FILE_MAX_SIZE
