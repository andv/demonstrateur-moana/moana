from email.mime import base

from django.conf import settings
from django.contrib import admin
from django.urls import include, path, re_path
from django.views.generic.base import RedirectView

from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from magicauth.urls import urlpatterns as magicauth_urls
from rest_framework import permissions, routers

from backend.accounts import views as accounts_views
from backend.alerts import views as alerts_views
from backend.alerts_config import views as alerts_config_views
from backend.backoffice import views as backoffice_views
from backend.current_user import views as current_user_views
from backend.faq import views as faq_views
from backend.home.views import index_view
from backend.issues import views as issues_views
from backend.logs import views as logs_views
from backend.persons import choices as persons_choices
from backend.persons import views as persons_views
from backend.school_trips import views as school_trips_views
from backend.school_trips_auth import views as school_trips_auth_views
from backend.school_trips_upload import views as school_trips_upload_views
from backend.settings.base import SCHOOL_TRIPS_UPLOAD_FORM_URL
from backend.ship_files import views as ship_files_views
from backend.ships import views as ships_views
from backend.website import views as website_views

router = routers.DefaultRouter()
router.register(r"ships", ships_views.ShipMovementViewSet, basename="ships")
router.register(r"ships", ships_views.ShipMovementActionsViewSet, basename="ships")
router.register(r"upload", ship_files_views.ShipFileUploadViewSet, basename="upload")
router.register(r"pages", website_views.WebsiteViewSet, basename="pages")
router.register(r"users", current_user_views.CurrentUserViewSet, basename="users")
router.register(r"accounts", accounts_views.AccountsViewSet, basename="accounts")
router.register(r"team", accounts_views.TeamViewSet, basename="team")
router.register(r"alerts", alerts_views.AlertsViewSet, basename="alerts")
router.register(r"tracking", logs_views.ActionTrackingViewSet, basename="tracking")
router.register(r"issues", issues_views.IssuesViewSet, basename="issues")
router.register(
    r"school-trips", school_trips_views.SchoolTripViewSet, basename="school-trips"
)
router.register(r"faq", faq_views.FaqQuestionAnswerViewSet, basename="faq")
router.register(
    r"alerts-config",
    alerts_config_views.NotificationEmailViewSet,
    basename="alerts-config",
)
router.register(
    r"alerts-config", alerts_config_views.ShipWatchlistViewSet, basename="alerts-config"
)
router.register(
    r"alerts-config", alerts_config_views.AddIMOListViewSet, basename="alerts-config"
)


admin.site.enable_nav_sidebar = False
admin.site.site_header = "Admin Moana - ANDV Maritime"


#############
# Base urls #
#############

urlpatterns = [
    re_path(r"^$", index_view, name="index"),
    path("pages/<slug>", index_view, name="pages"),
    re_path("suivi-des-bus-scolaires", index_view, name="school-trips-list"),
    re_path("statistiques", index_view, name="stats"),
    re_path("gestion-des-alertes", index_view, name="manage-alerts"),
    re_path("centre-aide", index_view, name="faq"),
    re_path("mon-equipe", index_view, name="team"),
    re_path("mon-equipe/ajouter-des-utilisateurs", index_view, name="team-add-user"),
    re_path(
        "mon-equipe/supprimer-des-utilisateurs", index_view, name="team-delete-users"
    ),
]


##############
# Auth urls  #
##############

urlpatterns.extend(magicauth_urls)
urlpatterns.extend(
    [
        path("sso/", include("djangosaml2.urls")),
        path(settings.LOGOUT_URL, accounts_views.logout_view, name="logout"),
    ]
)

if settings.ALLOW_USER_PASSWORD_AUTH:
    urlpatterns.extend(
        [
            path(
                settings.USER_LOGIN_WITH_PASSWORD_URL,
                accounts_views.login_view,
                name="login",
            )
        ]
    )

if settings.ALLOW_ADMIN_PASSWORD_AUTH:
    urlpatterns.extend(
        [
            path(
                settings.ADMIN_URL + "login/",
                backoffice_views.AdminLoginView.as_view(),
                name="admin-login",
            )
        ]
    )


#################
# School Trips  #
#################

if settings.SCHOOL_TRIPS_ENABLED:
    urlpatterns.extend(
        [
            # user can access school-trips-upload page with both SCHOOL_TRIPS_HOME_URL
            # and SCHOOL_TRIPS_UPLOAD_FORM_URL urls
            path(
                settings.SCHOOL_TRIPS_HOME_URL,
                RedirectView.as_view(url="/" + SCHOOL_TRIPS_UPLOAD_FORM_URL),
                name="school-trips-home",
            ),
            path(
                settings.SCHOOL_TRIPS_LOGIN_URL,
                school_trips_auth_views.login_view,
                name="school-trips-login",
            ),
            path(
                settings.SCHOOL_TRIPS_LOGOUT_URL,
                school_trips_auth_views.logout_view,
                name="school-trips-logout",
            ),
            path(
                settings.SCHOOL_TRIPS_EMAIL_SENT_URL,
                school_trips_auth_views.email_sent_view,
                name="school-trips-email-sent",
            ),
            path(
                settings.SCHOOL_TRIPS_UPLOAD_FORM_URL,
                school_trips_upload_views.upload_view,
                name="school-trips-upload",
            ),
            path(
                settings.SCHOOL_TRIPS_UPLOAD_SUCCESS_URL,
                school_trips_upload_views.success_view,
                name="school-trips-success",
            ),
        ]
    )

###############
# Admin urls  #
###############

urlpatterns.extend([path(settings.ADMIN_URL, admin.site.urls)])


#############
# API urls  #
#############

urlpatterns.extend([path("api/", include(router.urls))])


###########
# Swagger #
###########

if settings.ALLOW_API_DOC:
    schema_view = get_schema_view(
        openapi.Info(
            title="Moana - ANDV",
            default_version="v1",
            description="Moana API",
        ),
        public=True,
        permission_classes=(permissions.AllowAny,),
    )
    urlpatterns.extend(
        [
            path(
                "api/swagger/",
                schema_view.with_ui("swagger", cache_timeout=0),
                name="schema-swagger-ui",
            ),
        ]
    )

#################
# Feature urls  #
#################

urlpatterns.extend(
    [
        path(
            "csv/passagers/fpr/<int:pk>/",
            persons_views.DownloadPersonListView.as_view(),
            {
                "person_list_type": persons_choices.PERSON_LIST_TYPE_PASSENGER,
                "file_format": persons_choices.PERSON_LIST_FILE_FORMAT_FPR,
            },
            name="passengers_fpr_file",
        ),
        path(
            "csv/crew/fpr/<int:pk>/",
            persons_views.DownloadPersonListView.as_view(),
            {
                "person_list_type": persons_choices.PERSON_LIST_TYPE_CREW,
                "file_format": persons_choices.PERSON_LIST_FILE_FORMAT_FPR,
            },
            name="crew_fpr_file",
        ),
        path(
            "csv/passagers/fpr/annexe/<int:pk>/",
            persons_views.DownloadPersonListAnnexView.as_view(),
            {
                "person_list_type": persons_choices.PERSON_LIST_TYPE_PASSENGER,
                "file_format": persons_choices.PERSON_LIST_FILE_FORMAT_FPR,
            },
            name="passengers_fpr_annex_file",
        ),
        path(
            "csv/crew/fpr/annexe/<int:pk>/",
            persons_views.DownloadPersonListAnnexView.as_view(),
            {
                "person_list_type": persons_choices.PERSON_LIST_TYPE_CREW,
                "file_format": persons_choices.PERSON_LIST_FILE_FORMAT_FPR,
            },
            name="crew_fpr_annex_file",
        ),
        path(
            "csv/passagers/roc/<int:pk>/",
            persons_views.DownloadPersonListView.as_view(),
            {
                "person_list_type": persons_choices.PERSON_LIST_TYPE_PASSENGER,
                "file_format": persons_choices.PERSON_LIST_FILE_FORMAT_ROC,
            },
            name="passengers_roc_file",
        ),
        path(
            "csv/crew/roc/<int:pk>/",
            persons_views.DownloadPersonListView.as_view(),
            {
                "person_list_type": persons_choices.PERSON_LIST_TYPE_CREW,
                "file_format": persons_choices.PERSON_LIST_FILE_FORMAT_ROC,
            },
            name="crew_roc_file",
        ),
        path(
            "csv/passagers/roc/annexe/<int:pk>/",
            persons_views.DownloadPersonListAnnexView.as_view(),
            {
                "person_list_type": persons_choices.PERSON_LIST_TYPE_PASSENGER,
                "file_format": persons_choices.PERSON_LIST_FILE_FORMAT_ROC,
            },
            name="passengers_roc_annex_file",
        ),
        path(
            "csv/crew/roc/annexe/<int:pk>/",
            persons_views.DownloadPersonListAnnexView.as_view(),
            {
                "person_list_type": persons_choices.PERSON_LIST_TYPE_CREW,
                "file_format": persons_choices.PERSON_LIST_FILE_FORMAT_ROC,
            },
            name="crew_roc_annex_file",
        ),
        path(
            "csv/school-trips/<int:pk>/",
            school_trips_views.SchoolTripFileView.as_view(),
            name="school_trip_file",
        ),
    ]
)


##############
# Debug urls #
##############

if settings.DEBUG and "debug_toolbar" in settings.INSTALLED_APPS:
    import debug_toolbar

    urlpatterns.extend([path(r"__debug__/", include(debug_toolbar.urls))])


def trigger_error(request):
    _division_by_zero = 1 / 0  # noqa


if settings.SENTRY_DEBUG:
    urlpatterns.extend([path("__sentry-debug__/", trigger_error)])

##############
# Monitoring #
##############

if settings.ENABLE_MONITORING:
    urlpatterns.extend([path(settings.MONITORING_URL, include("watchman.urls"))])

##########
# Editor #
##########

urlpatterns.extend([path("tinymce/", include("tinymce.urls"))])
