from django.db import models

##################
# Models         #
##################


class WithNationalityCounters(models.Model):
    """
    This abstract class is expected to be sub-classed on models that need
    fields related to a person nationalities. This class can be used used in models
    defined in the `persons` app or in tracking logs.
    """

    nationalities_fr = models.PositiveIntegerField(
        "# nationalité FR", blank=True, null=True
    )
    nationalities_eu = models.PositiveIntegerField(
        "# nationalité EU",
        blank=True,
        null=True,
        help_text="Pays Européen hors France",
    )
    nationalities_visa = models.PositiveIntegerField(
        "# nationalité avec visa", blank=True, null=True
    )
    nationalities_visa_transit = models.PositiveIntegerField(
        "# nationalité avec visa transit", blank=True, null=True
    )
    nationalities_no_visa = models.PositiveIntegerField(
        "# nationalité nous soumis visa", blank=True, null=True
    )

    class Meta:
        abstract = True


class NationalityCounterHelper:

    def get_nationality_counters_from_ship_movement(self, ship):
        """
        From the given ship movement instance, get the nationality counters,
        which is the sum of counter from both lists, passengers and crew.
        The resulting dictionary can be used to update the counters fields on models
        that inherits from `WithNationalityCounters` class.
        """
        passengers_list = getattr(ship, "passenger_list", None)
        crew_list = getattr(ship, "crew_list", None)
        total_nationalities_fr = 0
        total_nationalities_eu = 0
        total_nationalities_visa = 0
        total_nationalities_visa_transit = 0
        total_nationalities_no_visa = 0
        if crew_list:
            total_nationalities_fr += ship.crew_list.nationalities_fr or 0
            total_nationalities_eu += ship.crew_list.nationalities_eu or 0
            total_nationalities_visa += ship.crew_list.nationalities_visa or 0
            total_nationalities_visa_transit += (
                ship.crew_list.nationalities_visa_transit or 0
            )
            total_nationalities_no_visa += ship.crew_list.nationalities_no_visa or 0
        if passengers_list:
            total_nationalities_fr += ship.passenger_list.nationalities_fr or 0
            total_nationalities_eu += ship.passenger_list.nationalities_eu or 0
            total_nationalities_visa += ship.passenger_list.nationalities_visa or 0
            total_nationalities_visa_transit += (
                ship.passenger_list.nationalities_visa_transit or 0
            )
            total_nationalities_no_visa += (
                ship.passenger_list.nationalities_no_visa or 0
            )

        counter_data = {
            "nationalities_fr": total_nationalities_fr,
            "nationalities_eu": total_nationalities_eu,
            "nationalities_visa": total_nationalities_visa,
            "nationalities_visa_transit": total_nationalities_visa_transit,
            "nationalities_no_visa": total_nationalities_no_visa,
        }
        return counter_data
