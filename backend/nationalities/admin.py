from django.contrib import admin

from backend.nationalities.models import Nationality

NATIONALITY_FIELDSET = [
    (
        "Nationalités",
        {
            "fields": [
                "nationalities_fr",
                "nationalities_eu",
                "nationalities_visa",
                "nationalities_visa_transit",
                "nationalities_no_visa",
            ],
        },
    ),
]


@admin.register(Nationality)
class NationalityAdmin(admin.ModelAdmin):
    list_display = [
        "country_name",
        "country_code",
        "is_in_eu",
        "needs_visa",
        "needs_visa_transit",
        "no_needs_visa",
        "created",
        "modified",
    ]
    list_filter = ["is_in_eu", "needs_visa", "needs_visa_transit", "no_needs_visa"]
    search_fields = ("country_name", "country_code")
    date_hierarchy = "created"
