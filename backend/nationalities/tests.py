from django.conf import settings

import pytest

from backend.nationalities.models import Nationality

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_get_country_code_from_name_before_saving_nationality():

    french_nationality = Nationality.objects.create(
        country_name="France",
        is_in_eu=True,
        needs_visa=False,
        needs_visa_transit=False,
        no_needs_visa=True,
    )

    japanese_nationality = Nationality.objects.create(
        country_name="Japan",
        is_in_eu=False,
        needs_visa=True,
        needs_visa_transit=True,
        no_needs_visa=False,
    )

    assert french_nationality.country_code == "FR"
    assert japanese_nationality.country_code == "JP"
