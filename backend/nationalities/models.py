from django.db import models

from model_utils.models import TimeStampedModel

from backend.utils.countries import get_country_code_from_name


class Nationality(TimeStampedModel):
    country_name = models.CharField("pays", max_length=256, blank=True)
    country_code = models.CharField("code pays", max_length=2, blank=True)
    is_in_eu = models.BooleanField(default=False)
    needs_visa = models.BooleanField(default=False)
    needs_visa_transit = models.BooleanField(default=False)
    no_needs_visa = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Nationalité"
        verbose_name_plural = "Nationalités"

    def __str__(self):
        return f"{self.country_name} ({self.country_code})"

    def save(self, *args, **kwargs):
        if not self.country_code:
            self.country_code = get_country_code_from_name(self.country_name)
        super().save(*args, **kwargs)
