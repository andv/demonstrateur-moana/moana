from django.core.management.base import BaseCommand

from defusedcsv import csv

from backend.nationalities.models import Nationality


class Command(BaseCommand):
    help = "Load a nationalities csv file into the database"

    def add_arguments(self, parser):
        parser.add_argument("csv_file", type=str)

    def handle(self, *args, **kwargs):
        csv_file_name = kwargs["csv_file"]
        with open(csv_file_name, "r") as csv_file:
            reader = csv.reader(csv_file)
            for i, row in enumerate(reader):
                if i == 0:  # Skip the header row
                    continue
                name = row[0]
                if not name:
                    continue
                is_in_eu = bool(row[2])
                needs_visa = bool(row[3])
                needs_visa_transit = bool(row[4])
                no_needs_visa = bool(row[5])
                self.stdout.write(
                    f"Processing: {name}: In EU: {is_in_eu} / Visa: {needs_visa} / Visa Transit {needs_visa_transit} / No Visa {no_needs_visa}"
                )

                _, created = Nationality.objects.get_or_create(
                    country_name=name,
                    defaults={
                        "is_in_eu": is_in_eu,
                        "needs_visa": needs_visa,
                        "needs_visa_transit": needs_visa_transit,
                        "no_needs_visa": no_needs_visa,
                    },
                )
