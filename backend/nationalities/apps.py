from django.apps import AppConfig


class NationalitiesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.nationalities"
    verbose_name = "4.3 Référentiels - Nationalités"
