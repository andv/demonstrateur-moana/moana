from django.apps import AppConfig


class SipConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.sip"
    verbose_name = "5.1 SIP"
