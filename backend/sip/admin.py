from django.contrib import admin

from backend.neptune.admin import NEPTUNE_API_ADMIN_FIELDSETS
from backend.sip.models import SipHaropaport, SipNeptune, SipSwing, SipVigiesip


VIGIE_SIP_API_ADMIN_FIELDSETS = (
    (
        "Timezone",
        {
            "fields": [
                "timezone",
            ]
        },
    ),
)


@admin.register(SipSwing)
@admin.register(SipHaropaport)
class SipAdmin(admin.ModelAdmin):
    list_display = [
        "port",
        "url",
        "url_dashboard",
        "import_enabled",
        "display_after_import",
        "update_existing",
        "execution_group",
        "last_import_start",
        "last_import_end",
    ]
    readonly_fields = ["last_import_start", "last_import_end"]
    autocomplete_fields = ["port"]
    fieldsets = (
        ("Configuration SIP", {"fields": ["port", "url", "url_dashboard"]}),
        (
            "Configuration de l'import",
            {
                "fields": [
                    "import_enabled",
                    "display_after_import",
                    "update_existing",
                    "execution_group",
                ]
            },
        ),
        (
            "Import dates",
            {"fields": ["last_import_start", "last_import_end"]},
        ),
    )


@admin.register(SipVigiesip)
class SipVigiesipAdmin(SipAdmin):
    fieldsets = SipAdmin.fieldsets + VIGIE_SIP_API_ADMIN_FIELDSETS


@admin.register(SipNeptune)
class SipNeptuneAdmin(SipAdmin):
    fieldsets = SipAdmin.fieldsets + NEPTUNE_API_ADMIN_FIELDSETS
