from datetime import timedelta

from django.core.management.base import BaseCommand
from django.utils import timezone

from backend.neptune.api import NeptuneAPIClient
from backend.neptune.models import NeptuneCertificate, NeptunePrivateKey
from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.sip.management.commands.sip_import import SipImportCommand
from backend.sip.models import SipNeptune


class Command(SipImportCommand, BaseCommand):
    sip_class = SipNeptune
    log_action_user = "sip neptune"
    neptune_client = None

    def initialize_neptune_client(self, sip):
        """
        Initialize the Neptune client with authentication certificate and URL
        taken from SIP configuration.
        """
        try:
            private_key = NeptunePrivateKey.objects.get(sip_neptune=sip)
            certificate = NeptuneCertificate.objects.get(sip_neptune=sip)
        except (NeptunePrivateKey.DoesNotExist, NeptuneCertificate.DoesNotExist):
            self.stderr.error("Missing key or certificate for this SIP configuration.")
            return
        cert_path = certificate.file_path
        key_path = private_key.file_path
        base_url = sip.url
        self.neptune_client = NeptuneAPIClient(cert_path, key_path, base_url)
        days_before = sip.days_before
        days_ahead = sip.days_ahead
        now = timezone.now()
        self.end_date = (now + timedelta(days=days_ahead)).strftime("%Y-%m-%d")
        self.start_date = (now - timedelta(days=days_before)).strftime("%Y-%m-%d")

    def list_escales_to_ship_call_data(self, list_escales):
        annonce = list_escales["annonce"]
        imo = list_escales["porteur"]["imo_number"]
        ship_call_id = self.get_ship_call_id(imo, annonce)
        ship_call_data = {
            "ship_call_id": ship_call_id,
        }
        return ship_call_data

    def get_ship_call_data_from_sip(self, sip):
        """
        Get the raw ship call data from Neptune's API.
        """
        if not self.neptune_client:
            self.initialize_neptune_client(sip)
        if not self.neptune_client:
            self.stdout.error("Could not initialize Neptune client.")
            return []
        ship_calls = self.neptune_client.get_list_escales(
            search_type="ETA", start_date=self.start_date, end_date=self.end_date
        )
        return ship_calls

    def get_ship_data(self, sip, ship_call_data, way):
        """
        Prepare the "ship data" dict from the raw ship call data.
        This cleaned data that will be saved as a ShipMovement instance.
        """
        status = ship_call_data["statutEscale"]
        self.stdout.write(f"-- Statut escale: {status}")
        annonce = ship_call_data["numeroAnnonce"]
        if not annonce:
            self.stdout.write("---- No annonce number in Neptune response.")
            return None
        imo = ship_call_data["codeOMI"]
        if not imo:
            self.stdout.write("---- No IMO number in Neptune response.")
            return None
        ship_call_id = self.get_ship_call_id(
            sip=sip,
            imo=imo,
            annonce=annonce,
        )
        self.stdout.write(f"-- Got ship call id: {ship_call_id}")
        position_in_port = self.get_position_in_port(way, ship_call_data)
        ship_data = {
            "ship_call_id": ship_call_id,
            "way": way,
            "imo_number": imo,
            "ship_name": ship_call_data.get("nom", ""),
            "last_port": ship_call_data["locodeProvenance"],
            "next_port": ship_call_data["locodeDestination"],
            "port_of_call": self.get_port_of_call(ship_call_data),
            "agent_name": self.get_agent_name(ship_call_data),
            "position_in_port_of_call": position_in_port,
        }
        times_data = self.get_times_data(ship_call_data, status)
        ship_data.update(times_data)
        return ship_data

    def get_times_data(self, ship_call_data, status):
        """
        Prepare the estimated and actual datetime fields.
        """
        eta = ship_call_data["dateETA"]
        etd = ship_call_data["dateETD"]
        has_eta_or_etd = True
        if not eta:
            self.stdout.write("---- No ETA in Neptune response.")
        if not etd:
            self.stdout.write("---- No ETD in Neptune response.")
        if not eta and not etd:
            has_eta_or_etd = False
            self.stdout.write(f"---- ETA/ETD missing for status {status}.")
        ata = ship_call_data["dateArrivee"]
        atd = ship_call_data["dateDepart"]
        has_ata_or_atd = True
        if not ata:
            self.stdout.write("---- No ATA in Neptune response.")
        if not atd:
            self.stdout.write("---- No ATD in Neptune response.")
        if not ata and not atd:
            has_ata_or_atd = False
            self.stdout.write(f"---- ATA/ATD missing for status {status}.")
        if not has_eta_or_etd and not has_ata_or_atd:
            self.stdout.write(
                f"---- Both estimated and actual missing for status {status}."
            )
        data = {
            "eta_to_port_of_call": eta,
            "etd_from_port_of_call": etd,
            "ata_to_port_of_call": ata,
            "atd_from_port_of_call": atd,
        }
        return data

    def get_port_of_call(self, ship_call_data):
        """
        Neptune's API provides data for both Marseille and Fos-sur-Mer.
        The port of call is indicated in the `locodeArrivee` element.
        Notice how we don't use `sip.port.locode` here.
        """
        return ship_call_data["locodeArrivee"]

    def get_agent_name(self, ship_call_data):
        return ship_call_data.get("libelleTiersAgent", "")

    def should_be_deleted(self, status):
        return status == "INVALIDEE"

    def should_be_skipped(self, status):
        return status == "PREVUE"

    def get_ship_call_id(self, sip, imo, annonce):
        """
        Get the ship call id from the given imo and annonce number.
        The ship call id is build it with like this:
        <sip.port.locode>_<imo_number>_<annonce_number>.
        Particularly, for Marseille : FRMRS_<imo_number>_<annonce_number>.
        Existing movements can have a ship call id with suffix like "_1" or "_2", etc.
        We have to look at existing ship movements and find the one with the highest suffix.
        """
        self.stdout.write(f"-- IMO: {imo}, Annonce: {annonce}")
        ship_call_id = f"{sip.port.locode}_{imo}_{annonce}"
        # Notice how the ship call ID is build with the SIP port that's configured
        # in database. It's not using the port_of_call coming from `locodeArrivee`
        # from the Neptune API response.
        possible_ship_movements = ShipMovement.objects.filter(
            ship_call_id__startswith=ship_call_id
        ).order_by("ship_call_id")
        if not possible_ship_movements:
            return ship_call_id
        ship = possible_ship_movements.first()
        return ship.ship_call_id

    def is_earliest(self, date_to_check, previous_earliest):
        """
        Return True if date_to_check is before previous_earliest.
        """
        if not date_to_check:  # No date to check, so it's not the earliest.
            return False
        if not previous_earliest:  # No previous earliest, so it's the earliest.
            return True
        return date_to_check < previous_earliest

    def is_latest(self, date_to_check, previous_latest):
        """
        Return True if date_to_check is after previous_latest.
        """
        if not date_to_check:  # No date to check, so it's not the latest.
            return False
        if not previous_latest:  # No previous latest, so it's the latest.
            return True
        return date_to_check > previous_latest

    def format_position_for_sejour(self, sejour):
        """
        The position in port is formed by the concatenation of PQA and the terminal fields.
        """
        if not sejour:
            return ""
        pqa = sejour.get("paqAttribue", "")
        terminal = sejour.get("nomTerminal", "")
        position = f"{pqa} {terminal}"
        return position

    def get_arrival_position(self, sejour_list):
        """
        Get the position in port for an arrival movement.
        The arrival position is taken from the sejour with the earliest date of "accostage".
        """
        earliest_date = None
        position = ""
        for sejour in sejour_list:
            date_accostage = sejour["dateAccostage"]
            self.stdout.write(f"---- Date accostage: {date_accostage}")
            possible_position = self.format_position_for_sejour(sejour)
            if self.is_earliest(
                date_to_check=date_accostage, previous_earliest=earliest_date
            ):
                self.stdout.write(f"---- Earliest accostage: {date_accostage}")
                earliest_date = date_accostage
                position = possible_position
        self.stdout.write(f"---- Arrival position: {position}")
        return position

    def get_departure_position(self, sejour_list):
        """
        Get the position in port for a departure movement.
        The departure position is taken from the sejour with the latest date of "appareillage".
        """
        latest_date = None
        position = ""
        for sejour in sejour_list:
            date_appareillage = sejour["dateAppareillage"]
            self.stdout.write(f"---- Date appareillage: {date_appareillage}")
            possible_position = possible_position = self.format_position_for_sejour(
                sejour
            )
            if self.is_latest(
                date_to_check=date_appareillage, previous_latest=latest_date
            ):
                self.stdout.write(f"---- Latest appareillage: {date_appareillage}")
                latest_date = date_appareillage
                position = possible_position
        self.stdout.write(f"---- Departure position: {position}")
        return position

    def get_position_in_port(self, way, ship_call_data):
        """ """
        sejours = ship_call_data["sejours"]
        position = ""
        if way == ships_settings.ARRIVING:
            position = self.get_arrival_position(sejours)
        if way == ships_settings.DEPARTING:
            position = self.get_departure_position(sejours)
        return position

    def handle_ship_call(self, sip, ship_call_data):
        self.stdout.write(
            f"Processing ship call for dates: {self.start_date} - {self.end_date}"
        )
        status = ship_call_data["statutEscale"]
        should_be_skipped = self.should_be_skipped(status)
        if should_be_skipped:
            self.stdout.write(f"---- Skipping ship call for status {status}.")
            return
        should_be_deleted = self.should_be_deleted(status)
        for way in [ships_settings.ARRIVING, ships_settings.DEPARTING]:
            ship_data = self.get_ship_data(sip, ship_call_data, way)
            if not ship_data:
                self.stdout.write("---- No ship data.")
                continue
            if should_be_deleted:
                self.delete_ship_movement(ship_data, sip)
                continue
            if sip.import_enabled:
                self.handle_ship_movement(ship_data, sip)
