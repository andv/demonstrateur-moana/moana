import json
from urllib.request import Request, urlopen
from zoneinfo import ZoneInfo

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone
from django.utils.timezone import datetime

from backend.ships import settings as ships_settings
from backend.sip.management.commands.sip_import import SipImportCommand
from backend.sip.models import SipSwing


class Command(SipImportCommand, BaseCommand):
    sip_class = SipSwing
    log_action_user = "sip s-wing"

    def format_datetime(self, timestamp):
        """
        Convert the incoming timestamp into python datetime object
        """
        if not timestamp:
            return None
        dt_obj = datetime.fromtimestamp(int(timestamp))
        dt_paris = timezone.make_aware(dt_obj, timezone=ZoneInfo("Europe/Paris"))
        return dt_paris

    def get_ship_call_data_from_sip(self, sip):
        api_request = Request(sip.url)
        if sip.port.locode == "FRLEH":
            api_username = settings.SIP_SWING_API_USERNAME
            api_password = settings.SIP_SWING_API_PASSWORD
        elif sip.port.locode == "RELPT":
            api_username = settings.SIP_SWING_LAREUNION_API_USERNAME
            api_password = settings.SIP_SWING_LAREUNION_API_PASSWORD
        if not api_username or not api_password:
            raise ValueError("Les identifiants de l'API SIP n'ont pas été trouvés.")
        credentials = f"{api_username}:{api_password}"
        api_request.add_header("X-Authorization", credentials)
        response = urlopen(api_request)
        data = json.loads(response.read())
        return data["escales"]

    def get_position_in_port(self, ship_call_data, way):
        """
        Gets the position in port of call.
        """
        position = ""
        if way == ships_settings.ARRIVING:
            position = ship_call_data.get("firstDock", "")
        if way == ships_settings.DEPARTING:
            position = ship_call_data.get("lastDock", "")
        return position

    def get_ship_data(self, sip, ship_call_data, way):
        """
        Prepare the ship data from the ship call data.
        This is the saved in ShipMovement instance.
        """
        last_port = ship_call_data.get("lastPortt", "")  # Typo on Portt is on the API.
        next_port = ship_call_data.get("nextPort", "")
        arrival_state = ship_call_data.get("arrivalState")
        departure_state = ship_call_data.get("departureState")
        arrival_time = self.format_datetime(ship_call_data.get("arrival"))
        departure_time = self.format_datetime(ship_call_data.get("departure"))
        fal5 = "fal5in" if way == ships_settings.ARRIVING else "fal5out"
        fal6 = "fal6in" if way == ships_settings.ARRIVING else "fal6out"
        position = self.get_position_in_port(ship_call_data, way)
        ship_data = {
            "port_of_call": sip.port.locode,
            "last_port": last_port,
            "next_port": next_port,
            "way": way,
            "imo_number": ship_call_data.get("shipIMO", ""),
            "ship_name": ship_call_data.get("shipName"),
            "agent_name": ship_call_data.get("agent", ""),
            "position_in_port_of_call": position,
        }
        if arrival_state == "real":
            ship_data["ata_to_port_of_call"] = arrival_time
        else:
            ship_data["eta_to_port_of_call"] = arrival_time
        if departure_state == "real":
            ship_data["atd_from_port_of_call"] = departure_time
        else:
            ship_data["etd_from_port_of_call"] = departure_time
        if ship_call_data.get(fal5) == "1":
            ship_data["swing_crew_list_id"] = ship_call_data.get(fal5 + "_id")
            ship_data["sip_fal5_available"] = True
        if ship_call_data.get(fal6) == "1":
            ship_data["sip_fal6_available"] = True
            ship_data["swing_passengers_list_id"] = ship_call_data.get(fal6 + "_id")
        return ship_data

    def handle_ship_call(self, sip, ship_call_data):
        ship_call_id = ship_call_data.get("shipCallId")
        self.stdout.write(f"Processing ship call id : {ship_call_id}")
        is_cancelled = ship_call_data.get("isCancel", "")
        for way in [ships_settings.ARRIVING, ships_settings.DEPARTING]:
            ship_data = self.get_ship_data(sip, ship_call_data, way)
            ship_data["ship_call_id"] = ship_call_id
            if is_cancelled == "1":
                self.delete_ship_movement(ship_data, sip)
                continue
            if sip.import_enabled:
                self.handle_ship_movement(ship_data, sip)
