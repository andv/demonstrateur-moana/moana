import json

from django.conf import settings
from django.utils import timezone

from deepdiff import DeepDiff

from backend.logs.actions import ActionLogHandler
from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.utils.sentry import sentry_capture


class SipImportCommand(ActionLogHandler):
    """
    Base command for importing SIP data.
    This command processes SIP entries. It prioritizes entries based on their last
    import time, the oldest is processed first.
    If execution groups are specified in command parameters, it will import the entries
    for the given groups. Execution groups are not mandatory and if it not provided,
    all entries are imported.
    Usage examples:
    - With execution groups: python manage.py sip_import_vigiesip --execution-group 1 2 3
      This will process SIP entries in groups 1, 2, and 3 sequentially.
    - Without parameters: python manage.py sip_import_vigiesip
      This will process all SIP entries, regardless of their execution group.
    """

    sip_class = None
    log_action_user = None

    def add_arguments(self, parser):
        parser.add_argument(
            "--execution-group",
            nargs="*",
            type=int,
            help="List of execution group numbers",
        )

    def is_not_empty(self, value):
        empty_criterias = ["", None]
        return value not in empty_criterias

    def log_action(self, action_type, description="", ship=None, data=None):
        """
        Log actions in database for tracking and statistics.
        """
        action = f"{self.log_action_user} {action_type}"
        ref = ""
        if ship:
            ref = f"Ref {ship.ship_call_id}"
        self.create_action_log(
            action=action,
            actor=self.log_action_user,
            object=ref,
            target=ship,
            description=description,
            ship=ship,
            data=data,
        )

    def is_old_movement(self, ship_data):
        days_old = settings.DATA_MAX_DAYS_OLD
        eta = ship_data.get("eta_to_port_of_call")
        etd = ship_data.get("etd_from_port_of_call")
        if ship_data.get("ata_to_port_of_call"):
            ata = ship_data.get("ata_to_port_of_call")
        if ship_data.get("atd_to_port_of_call"):
            atd = ship_data.get("atd_to_port_of_call")
        way = ship_data["way"]
        cutoff_date = timezone.now() - timezone.timedelta(days=days_old)
        cutoff_date = cutoff_date.replace(hour=0, minute=0, second=0, microsecond=0)
        if way == ships_settings.ARRIVING:
            if ship_data.get("ata_to_port_of_call"):
                date_to_check = ata
            else:
                date_to_check = eta
        if way == ships_settings.DEPARTING:
            if ship_data.get("atd_to_port_of_call"):
                date_to_check = atd
            else:
                date_to_check = eta

            date_to_check = etd
        is_old = False
        if date_to_check and date_to_check < cutoff_date:
            is_old = True
        return is_old

    def compare_dict(self, second_dict, first_dict):
        fields_to_ignore = ["created", "modified"]
        for field in fields_to_ignore:
            second_dict.pop(field)
            first_dict.pop(field)
        all_diff = DeepDiff(second_dict, first_dict)
        return all_diff

    def compare_and_log_diff(self, ship, data_before, data_after):
        all_diff = self.compare_dict(data_before, data_after)
        all_diff_serialized = json.loads(all_diff.to_json())
        changed_diff = all_diff_serialized.get("values_changed")
        if changed_diff:
            count_changes = len(changed_diff)
            description = f"Number of fields changed: {count_changes}"
            self.log_action(
                action_type="changed movement",
                description=description,
                ship=ship,
                data=changed_diff,
            )

    def handle_ship_movement(self, ship_data, sip):
        ship_call_id = ship_data["ship_call_id"]
        way = ship_data["way"]
        existing_ships_qs = ShipMovement.objects.filter(
            ship_call_id=ship_call_id, way=way
        )
        movement_exist = existing_ships_qs.exists()
        if not sip.update_existing:
            if movement_exist:
                self.stdout.write(
                    f"---- no action taken on existing movement: {ship_call_id}"
                )
                return None
        ship_data_without_empty = {
            k: v for k, v in ship_data.items() if self.is_not_empty(v)
        }
        if self.is_old_movement(ship_data_without_empty):
            self.stdout.write(f"---- no action taken on old movement: {ship_call_id}")
            return None
        if movement_exist:  # Let's capture the data before any alteration:
            data_before = existing_ships_qs.first().__dict__.copy()
        ship, created = ShipMovement.objects.update_or_create(
            ship_call_id=ship_call_id,
            way=way,
            defaults=ship_data_without_empty,
        )
        # We always override the display flag to match the SIP config
        ship.display = sip.display_after_import
        ship.save()
        ship.notify_by_email()
        if created:
            self.log_action(action_type="added movement", ship=ship)
        else:
            data_after = ship.__dict__.copy()  # That's the updated ship movement
            self.compare_and_log_diff(ship, data_before, data_after)
        action = "created" if created else "updated"
        self.stdout.write(f"---- {action} movement: {ship}")
        return ship

    def delete_ship_movement(self, ship_data, sip):
        ship_call_id = ship_data["ship_call_id"]
        way = ship_data["way"]
        ship = ShipMovement.objects.filter(
            ship_call_id=ship_call_id,
            way=way,
        ).first()
        if not ship:
            return None
        if not sip.update_existing:
            self.stdout.write(f"---- not deleting existing movement: {ship}")
            return None
        ship.delete()
        self.log_action(action_type="deleted movement", ship=ship)
        self.stdout.write(f"---- deleted ship movement: {ship}")

    def get_ship_call_data_from_sip(self, sip):
        raise NotImplementedError

    def handle_ship_call(self, sip, ship_call_data):
        raise NotImplementedError

    @sentry_capture
    def handle_sip(self, sip):
        sip.last_import_start = timezone.now()
        sip.save()
        self.stdout.write(f"Start import for {sip.port}")
        self.stdout.write(f"Importing from : {sip.url}")
        data = self.get_ship_call_data_from_sip(sip)
        for ship_call_data in data:
            self.handle_ship_call(sip, ship_call_data)
        # Before updating the sip object we call the last version of this object
        # to make sure we have the last version of each fields
        sip.refresh_from_db()
        sip.last_import_end = timezone.now()
        sip.save()

    @sentry_capture
    def handle(self, *args, **options):
        execution_groups = options.get("execution_group")
        if execution_groups:
            for group in execution_groups:
                self.stdout.write(f"Processing execution group: {group}")
                sip_entries = self.sip_class.objects.filter(
                    execution_group=group
                ).order_by("last_import_end")
                for sip in sip_entries:
                    self.handle_sip(sip)
        else:
            self.stdout.write(
                "No execution group specified: Processing all SIP entries."
            )
            sip_entries = self.sip_class.objects.all().order_by("last_import_end")
            for sip in sip_entries:
                self.handle_sip(sip)
