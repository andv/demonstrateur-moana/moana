import json
from urllib.request import Request, urlopen

from django.conf import settings

from backend.ships import settings as ships_settings
from backend.sip.management.commands.sip_import_swing import Command as SipSwingCommand
from backend.sip.models import SipHaropaport


class Command(SipSwingCommand):
    """
    The Haropaport SIP is using almost the same API as Swing SIP.
    """

    sip_class = SipHaropaport
    log_action_user = "sip haropaport"
    api_username = settings.SIP_HAROPAPORT_API_USERNAME
    api_password = settings.SIP_HAROPAPORT_API_PASSWORD

    def get_ship_call_data_from_sip(self, sip):
        api_request = Request(sip.url)
        if not self.api_username or not self.api_password:
            raise ValueError("Les identifiants de l'API SIP n'ont pas été trouvés.")
        credentials = f"{self.api_username}:{self.api_password}"
        api_request.add_header("X-Authorization", credentials)
        response = urlopen(api_request)
        data = json.loads(response.read())
        return data["escales"]
