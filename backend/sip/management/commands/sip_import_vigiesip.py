import json
import re
from urllib.request import urlopen
from zoneinfo import ZoneInfo

from django.core.management.base import BaseCommand
from django.utils import timezone
from django.utils.timezone import datetime, timedelta
from django.conf import settings

from backend.ships import settings as ships_settings
from backend.sip.management.commands.sip_import import SipImportCommand
from backend.sip.models import SipVigiesip


def is_not_empty(value):
    empty_criterias = ["", None]
    return value not in empty_criterias


class Command(SipImportCommand, BaseCommand):
    sip_class = SipVigiesip
    log_action_user = "sip vigiesip"

    def get_locode_from_string(self, port_string):
        """
        The raw port string looks like this : "SAINT HELIER (JERSEY) (JESTH)"
        We only need the locode wich is the content of the last parentheses.
        """
        all_parentheses = re.findall(r"\(.*?\)", port_string)
        if not all_parentheses:
            return ""
        last_parenthenses = all_parentheses[-1]
        locode = last_parenthenses.strip("(").strip(")")
        return locode

    def format_datetime(self, datetime_string, sip_timezone):
        """
        Convert an iso datetime string to a timezone aware datetime.
        The timezone used in Vigiesip depends on Sip port location.
        """
        if not datetime_string:
            return None
        dt_obj = datetime.strptime(datetime_string, "%d/%m/%Y %H:%M:%S")
        dt_aware = timezone.make_aware(dt_obj, timezone=ZoneInfo(sip_timezone))
        return dt_aware

    def get_ship_call_data_from_sip(self, sip):
        response = urlopen(sip.url)
        data = json.loads(response.read())
        return data

    def handle_ship_call(self, sip, ship_call_data):
        """
        The "Annonce" is the equivalent of the ship call.
        That's a ship stopover with possibly multiple departing/arriving movments.
        """
        ship_call_id = ship_call_data.get("ANNONCE")
        ship_call_id = f"{sip.port.locode}{ship_call_id}"
        self.stdout.write(f"Processing ship call id : {ship_call_id}")
        cutoff_date_for_import = timezone.now() - timezone.timedelta(
            days=settings.MAX_DAYS_IN_PAST_FOR_IMPORT
        )
        for movement_data in ship_call_data.get("MOUVEMENTS"):
            # if the movement_update_date datetime object is
            # three month in the past we continue the forloop without saving ship_data
            # to prevent import script to stop due to a too long execution process.
            movement_update_date = self.format_datetime(
                movement_data.get("DATEMAJ"), sip.timezone
            )
            if movement_update_date < cutoff_date_for_import:
                continue
            movement_type = movement_data.get("TYPE")
            way = None
            if movement_type == "DPQ":
                way = ships_settings.ARRIVING
            if movement_type == "DS":
                way = ships_settings.DEPARTING
            if way is None:
                continue
            ship_info_data = ship_call_data.get("INFOS_NAVIRE")
            last_port = movement_data.get("PROVENANCE", "")
            last_port = self.get_locode_from_string(last_port)
            next_port = movement_data.get("DESTINATION_NAVIRE", "")
            next_port = self.get_locode_from_string(next_port)
            eta = self.format_datetime(movement_data.get("ETA"), sip.timezone)
            etd = self.format_datetime(movement_data.get("ETD"), sip.timezone)

            ship_data = {
                "port_of_call": sip.port.locode,
                "last_port": last_port,
                "next_port": next_port,
                "ship_call_id": ship_call_id,
                "way": way,
                "imo_number": ship_call_data.get("IMO", ""),
                "ship_name": ship_info_data.get("NAVIRE"),
                "position_in_port_of_call": movement_data.get("DESTINATION", ""),
                "eta_to_port_of_call": eta,
                "etd_from_port_of_call": etd,
                "agent_name": movement_data.get("AGENT", ""),
                "sip_fal5_available": movement_data.get("PRESENCE_LISTE_EQUIPAGE"),
                "sip_fal6_available": movement_data.get("PRESENCE_LISTE_PASSAGER"),
            }
            movement_state = movement_data.get("ETAT")
            if movement_state in ("S", "s"):
                self.delete_ship_movement(ship_data, sip)
                continue
            if sip.import_enabled:
                self.handle_ship_movement(ship_data, sip)
