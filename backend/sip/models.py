from django.core.exceptions import ValidationError
from django.db import models

from backend.neptune.models import NeptuneBaseModel
from backend.accounts.choices import TIMEZONES_CHOICES


class SipBaseModel(models.Model):
    url = models.CharField("url", max_length=256)
    url_dashboard = models.CharField("url dashboard", max_length=256, blank=True)
    import_enabled = models.BooleanField(
        "import activé",
        default=True,
        help_text="Déclenche l'import de nouveaux mouvements de navires",
    )
    display_after_import = models.BooleanField(
        "afficher les mouvements importés",
        default=False,
        help_text="Après l'import SIP, activer l'affichage des mouvements de navires importés",
    )
    update_existing = models.BooleanField(
        "mettre à jour les mouvements existants",
        default=False,
        help_text="Mettre à jour les mouvements existants, en utilisant les données du SIP",
    )
    execution_group = models.PositiveIntegerField(
        "groupe d'exécution",
        blank=True,
        null=True,
        help_text="Numéro qui sert à grouper les entrées SIP lors de l'exécution des imports",
    )
    last_import_start = models.DateTimeField(
        "début du dernier import",
        null=True,
        blank=True,
        help_text="Date et heure du début du dernier import",
    )
    last_import_end = models.DateTimeField(
        "fin du dernier import",
        null=True,
        blank=True,
        help_text="Date et heure de fin du dernier import",
    )

    class Meta:
        abstract = True

    def __str__(self):
        return f"SIP for {self.port}"

    def clean(self):
        if self.display_after_import and not self.import_enabled:
            raise ValidationError(
                {
                    "display_after_import": "Pour activer l'affichage, il faut aussi activer l'import"
                }
            )
        if self.update_existing and not self.import_enabled:
            raise ValidationError(
                {
                    "update_existing": "Pour activer les mise à jour, il faut aussi activer l'import"
                }
            )


class SipVigiesip(SipBaseModel):
    port = models.OneToOneField(
        to="ports.Port",
        verbose_name="port",
        related_name="sip_vigiesip",
        on_delete=models.CASCADE,
    )
    timezone = models.CharField(
        verbose_name="Fuseau horaire",
        max_length=255,
        choices=TIMEZONES_CHOICES,
        default="Europe/Paris",
        help_text="Sélectionner le fuseau horaire du sip",
    )

    class Meta:
        verbose_name = "SIP Vigiesip"
        verbose_name_plural = "SIP Vigiesip"


class SipSwing(SipBaseModel):
    port = models.OneToOneField(
        to="ports.Port",
        verbose_name="port",
        related_name="sip_swing",
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = "SIP S-Wing"
        verbose_name_plural = "SIP S-Wing"


class SipHaropaport(SipBaseModel):
    port = models.OneToOneField(
        to="ports.Port",
        verbose_name="port",
        related_name="sip_haropaport",
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = "SIP Haropa Port"
        verbose_name_plural = "SIP Haropa Port"


class SipNeptune(SipBaseModel, NeptuneBaseModel):
    port = models.OneToOneField(
        to="ports.Port",
        verbose_name="port",
        related_name="sip_neptune",
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = "SIP Neptune"
        verbose_name_plural = "SIP Neptune"
