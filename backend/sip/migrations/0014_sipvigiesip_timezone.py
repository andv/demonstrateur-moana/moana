# Generated by Django 4.2.14 on 2024-07-31 12:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("sip", "0013_alter_sipharopaport_import_enabled_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="sipvigiesip",
            name="timezone",
            field=models.CharField(
                choices=[
                    ("Europe/London", "Europe/Londres"),
                    ("Europe/Paris", "Europe/Paris"),
                    ("America/Guadeloupe", "Guadeloupe/Basse Terre"),
                    ("America/Guyana", "Guyane/Cayenne"),
                    ("Pacific/Gambier", "Îles Gambier/Polynésie française"),
                    ("Pacific/Marquesas", "Îles Marquises/Polynésie française"),
                    ("America/Martinique", "Martinique/Fort-de-France"),
                    ("America/Marigot", "Saint-Martin/Marigot"),
                    ("Indian/Mayotte", "Mayotte/Mamoudzou"),
                    ("Pacific/Noumea", "Nouvelle Calédonie/Nouméa"),
                    ("Pacific/Tahiti", "Polynésie Française/Papeete"),
                    ("Indian/Reunion", "Réunion/Saint-Denis"),
                    ("America/St_Barthelemy", "Saint-Barthelemy/Gustavia"),
                    ("America/Miquelon", "Saint-Pierre-et-Miquelon/Saint-Pierre"),
                    ("UTC", "UTC"),
                    ("Pacific/Wallis", "Wallis-et-Futuna/Mata-Utu"),
                ],
                default="Europe/Paris",
                help_text="Sélectionner le fuseau horaire du sip",
                max_length=255,
                verbose_name="Fuseau horaire",
            ),
        ),
    ]
