from django.apps import AppConfig


class SchoolTripsAuthConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.school_trips_auth"
