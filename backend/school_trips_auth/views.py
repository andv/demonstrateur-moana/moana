from django.contrib.auth import views as auth_views
from django.urls import reverse_lazy

from magicauth import views as magicauth_views


class LoginView(magicauth_views.LoginView):
    """
    Login for School Trips users.
    """

    template_name = "school_trips_auth/login.html"

    def get_next_url(self, request):
        """
        Force the user to go to the upload page after login.
        """
        return reverse_lazy("school-trips-upload")

    def send_token(self, user_email, extra_context):
        """
        Change email subject and content
        """
        extra_context.update({"is_school_trips": True})
        self.email_subject = "Login link to access Moana"
        return super().send_token(user_email, extra_context)

    def get_success_url(self, **kwargs):
        return reverse_lazy("school-trips-email-sent")


class EmailSentView(magicauth_views.EmailSentView):
    """
    Display the email sent message for School Trips users.
    """

    template_name = "school_trips_auth/email_sent.html"


class LogoutView(auth_views.LogoutView):
    """
    User Logout for School Trips users.
    """

    next_page = reverse_lazy("school-trips-login")


login_view = LoginView.as_view()
logout_view = LogoutView.as_view()
email_sent_view = EmailSentView.as_view()
