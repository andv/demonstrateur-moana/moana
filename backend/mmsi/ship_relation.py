from django.db import models

from backend import mmsi
from backend.mmsi.models import Mmsi


class MmsiOnShip(models.Model):
    """
    This abstract class is expected to be sub-classed on a ShipMovement-like model.
    It provides mmsi field.
    """

    mmsi_number = models.CharField(
        "numéro MMSI",
        max_length=256,
        blank=True,
    )

    class Meta:
        abstract = True

    def get_mmsi_number(self):
        mmsi = Mmsi.objects.filter(imo_number=self.imo_number).first()
        return mmsi.mmsi_number if mmsi else ""

    def save(self, *args, **kwargs):
        self.mmsi_number = self.get_mmsi_number()
        return super().save(*args, **kwargs)
