from django.conf import settings

import pytest

from backend.mmsi.models import Mmsi
from backend.ships.models import ShipMovement

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_ship_has_corresponding_mmsi_number_from_imo(client):
    mmsi_number = "123"
    imo_number = "456"
    Mmsi.objects.create(imo_number=imo_number, mmsi_number=mmsi_number)
    ship_with_right_imo = ShipMovement.objects.create(imo_number=imo_number)
    ship_with_wrong_imo = ShipMovement.objects.create(imo_number="789")
    assert ship_with_right_imo.mmsi_number == mmsi_number
    assert ship_with_wrong_imo.mmsi_number != mmsi_number


def test_ship_without_imo_doesnt_have_mmsi_neither(client):
    ship = ShipMovement.objects.create(imo_number="")
    assert ship.mmsi_number == ""
