from os import getcwd, path

from django.conf import settings
from django.core import management

import pytest

from backend.mmsi.models import Mmsi

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


MMSI_FILE_PATH = path.join(getcwd(), "backend/mmsi/tests/data/mmsi.csv")


def test_import_command_creates_mmsi(client):
    before = Mmsi.objects.count()
    management.call_command("import_mmsi", MMSI_FILE_PATH)
    after = Mmsi.objects.count()
    assert after > before
