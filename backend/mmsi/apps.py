from django.apps import AppConfig


class MmsiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.mmsi"
    verbose_name = "4.4 Référentiels - Numéros MMSI"
