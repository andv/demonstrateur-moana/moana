from import_export import fields as import_export_fields
from import_export import resources

from backend.mmsi.models import Mmsi


class MmsiResource(resources.ModelResource):
    mmsi = import_export_fields.Field(attribute="mmsi_number", column_name="mmsi")
    imo = import_export_fields.Field(attribute="imo_number", column_name="imonumber")

    class Meta:
        model = Mmsi
        import_id_fields = ("mmsi",)
        fields = ("mmsi", "imo")
        skip_unchanged = True
        use_transactions = False
