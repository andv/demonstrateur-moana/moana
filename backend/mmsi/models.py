from django.db import models

from model_utils.models import TimeStampedModel


class Mmsi(TimeStampedModel):
    mmsi_number = models.CharField("MMSI number", max_length=256)
    imo_number = models.CharField("IMO number", max_length=256)

    def __str__(self):
        return self.mmsi_number
