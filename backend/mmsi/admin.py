from django.contrib import admin

from import_export.admin import ImportMixin

from backend.mmsi.models import Mmsi
from backend.mmsi.resources import MmsiResource


@admin.register(Mmsi)
class MmsiAdmin(ImportMixin, admin.ModelAdmin):
    list_display = ["mmsi_number", "imo_number"]
    search_fields = ("mmsi_number", "imo_number")
    date_hierarchy = "created"
    resource_class = MmsiResource
