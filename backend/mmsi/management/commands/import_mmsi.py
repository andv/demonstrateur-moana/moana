from django.core.management.base import BaseCommand

from backend.initialization.data_import import CSVImportMixin
from backend.mmsi.resources import MmsiResource


class Command(CSVImportMixin, BaseCommand):
    help = "Import mmsi from CSV file"
    resource_class = MmsiResource
