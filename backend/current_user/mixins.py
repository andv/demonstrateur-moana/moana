##################
# Serializers    #
##################


class CurrentUserSerializerMixin:
    """
    This mixin class is expected to be sub-classed on a serializer.
    """

    @property
    def current_user(self):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
        return user


##################
# Views          #
##################


class CurrentUserViewMixin:
    """
    This mixin class is expected to be used on an API view. It provides helpers for
    handling the current user that's found in the request context.
    """

    @property
    def current_user(self):
        return self.request.user

    @property
    def current_team(self):
        return self.current_user.team
