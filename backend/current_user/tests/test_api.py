from django.conf import settings
from django.shortcuts import reverse

import pytest

from backend.unit_tests import utils
from backend.unit_tests.factories import UserFactory

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_current_user_api_loads(client):
    utils.login_saint_malo_user(client)
    url = reverse("users-current-user")
    response = client.get(url)
    assert "FRSML" in response.content.decode()


def test_anonymous_user_does_not_raise_error(client):
    client.logout()
    url = reverse("users-current-user")
    response = client.get(url)
    assert "{}" == response.content.decode()


def test_team_manager_api_loads(client):
    user = utils.login_saint_malo_user(client)
    team = user.team
    UserFactory(
        team=team,
        is_team_manager=True,
        first_name="McCoy",
        last_name="Tyner",
        email="mccoy@tyner.com",
    )
    url = reverse("users-team-manager")
    response = client.get(url)
    assert response.status_code == 200
    assert response.json() == {"name": "Tyner McCoy"}


def test_team_manager_api_no_manager(client):
    utils.login_saint_malo_user(client)
    url = reverse("users-team-manager")
    response = client.get(url)
    assert response.status_code == 200
    assert response.json() == {"name": ""}
