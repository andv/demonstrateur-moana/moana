import time

from django.conf import settings

import jwt
from rest_framework import decorators, viewsets
from rest_framework.response import Response

from backend.accounts.models import User
from backend.ports.serializers import PortSerializer
from backend.ports.user_relation import UserPortsViewMixin


class CurrentUserViewSet(UserPortsViewMixin, viewsets.ViewSet):
    """ """

    @decorators.action(
        detail=False, methods=["GET"], url_path="current-user", url_name="current-user"
    )
    def get_user(self, request):
        """
        List basics informations of current user authenticated
        """
        if not request.user.is_authenticated:
            return Response({})
        ports_data = {}
        ports_serializer = PortSerializer(self.assigned_ports, many=True)
        if ports_serializer.data:
            ports_data = ports_serializer.data
        return Response(
            {
                "team": request.user.team.name,
                "manual_upload_fal": request.user.manual_upload_fal,
                "ports": ports_data,
                "timezone": request.user.team.timezone,
                "download_fpr_lists": request.user.team.download_fpr_lists,
                "download_roc_lists": request.user.team.download_roc_lists,
                "show_fpr_checkboxes": request.user.team.show_fpr_checkboxes,
                "show_roc_checkboxes": request.user.team.show_roc_checkboxes,
                "id": request.user.id,
                "email": request.user.email,
                "show_school_trips_list_page": request.user.team.show_school_trips_list_page,
                "historic_duration": request.user.team.historic_duration,
                "is_team_manager": request.user.is_team_manager,
                "team_notification_email": request.user.team.notification_email,
                "metabase_usage_url": self.get_metabase_usage_url(request),
                "metabase_nationalities_url": self.get_metabase_nationalities_url(
                    request
                ),
            }
        )

    def get_metabase_usage_url(self, request):
        payload = {
            "resource": {"dashboard": settings.METABASE_DASHBOARD_USAGE_ID},
            "params": {"%C3%A9quipe": [self.request.user.team.name]},
            "exp": round(time.time()) + (60 * 10),  # 10 minute expiration
        }
        token = jwt.encode(payload, settings.METABASE_EMBEDED_KEY, algorithm="HS256")

        iframe_url = (
            settings.METABASE_SITE_URL
            + "/embed/dashboard/"
            + token
            + "#bordered=true&titled=true"
        )

        return iframe_url

    def get_metabase_nationalities_url(self, request):
        ports_list = list(self.request.user.team.ports.values_list("name", flat=True))

        payload = {
            "resource": {"dashboard": settings.METABASE_DASHBOARD_NATIONALITIES_ID},
            "params": {"port": ports_list},
            "exp": round(time.time()) + (60 * 10),  # 10 minute expiration
        }
        token = jwt.encode(payload, settings.METABASE_EMBEDED_KEY, algorithm="HS256")

        iframe_url = (
            settings.METABASE_SITE_URL
            + "/embed/dashboard/"
            + token
            + "#bordered=true&titled=true"
        )

        return iframe_url

    @decorators.action(
        detail=False, methods=["GET"], url_path="team-manager", url_name="team-manager"
    )
    def get_team_manager(self, request):
        """
        Retrieve first user team manager complete name
        """
        if not request.user.is_authenticated:
            return Response({})
        manager = User.objects.filter(
            team=request.user.team, is_team_manager=True
        ).first()
        name = ""
        if manager:
            name = f"{manager.last_name} {manager.first_name}"
        return Response({"name": name})
