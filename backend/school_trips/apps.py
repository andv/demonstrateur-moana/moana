from django.apps import AppConfig


class SchoolTripsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.school_trips"
    verbose_name = "3.4 Mouvements - Voyages scolaires"
