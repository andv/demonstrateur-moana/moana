from http import HTTPStatus
from tabnanny import check

from django.conf import settings
from django.urls import reverse

import pytest

from backend.school_trips import choices as school_trips_choices
from backend.unit_tests import utils
from backend.unit_tests.factories import SchoolTripFactory

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_can_set_status_to_not_processed(client):
    utils.login_user_with_access_to_school_trips_list_page(client)
    school_trip = SchoolTripFactory(check_status=school_trips_choices.TRIP_PROCESSED)
    assert school_trip.check_status == school_trips_choices.TRIP_PROCESSED
    url = reverse("school-trips-update-status", kwargs={"pk": school_trip.id})
    data = {"check_status": school_trips_choices.TRIP_NOT_PROCESSED}
    response = client.post(url, data)
    assert response.status_code == 200
    school_trip.refresh_from_db()
    assert school_trip.check_status == school_trips_choices.TRIP_NOT_PROCESSED


def test_can_set_status_to_processed(client):
    utils.login_user_with_access_to_school_trips_list_page(client)
    school_trip = SchoolTripFactory(
        check_status=school_trips_choices.TRIP_NOT_PROCESSED
    )
    assert school_trip.check_status == school_trips_choices.TRIP_NOT_PROCESSED
    url = reverse("school-trips-update-status", kwargs={"pk": school_trip.id})
    data = {"check_status": school_trips_choices.TRIP_PROCESSED}
    response = client.post(url, data)
    assert response.status_code == 200
    school_trip.refresh_from_db()
    assert school_trip.check_status == school_trips_choices.TRIP_PROCESSED


def test_can_download_school_trip_csv_file(client):
    user = utils.login_user_with_access_to_school_trips_list_page(client)
    user.access_person_list = True
    user.save()
    school_trip = SchoolTripFactory()
    url = reverse("school_trip_file", args=[school_trip.pk])
    response = client.get(url)
    assert response.status_code == 200


def test_cannot_download_school_trip_csv_file_if_download_file_not_allowed(client):
    user = utils.login_user_with_access_to_school_trips_list_page(client)
    user.access_person_list = False
    user.save()
    school_trip = SchoolTripFactory()
    url = reverse("school_trip_file", args=[school_trip.pk])
    response = client.get(url)
    assert response.status_code == HTTPStatus.FORBIDDEN


def test_cannot_download_school_trip_csv_file_if_download_file_not_allowed(client):
    user = utils.login_user_without_access_to_school_trips_list_page(client)
    user.access_person_list = True
    user.save()
    school_trip = SchoolTripFactory()
    url = reverse("school_trip_file", args=[school_trip.pk])
    response = client.get(url)
    assert response.status_code == HTTPStatus.FORBIDDEN


def test_cannot_access_shool_trips_detail_api_endpoint_if_team_is_not_allowed(client):
    user = utils.login_user_without_access_to_school_trips_list_page(client)
    user.team.access_school_trips_upload_page = False
    user.save()
    school_trip = SchoolTripFactory()
    url = reverse("school-trips-detail", args=[school_trip.pk])
    response = client.get(url)
    assert response.status_code == HTTPStatus.FORBIDDEN


def test_cannot_access_shool_trips_list_api_endpoint_if_team_is_not_allowed(client):
    user = utils.login_user_without_access_to_school_trips_list_page(client)
    user.save()
    school_trip = SchoolTripFactory()
    url = reverse("school-trips-list")
    response = client.get(url)
    assert response.status_code == HTTPStatus.FORBIDDEN
