from django.db import models
from django.urls import reverse

from model_utils.models import TimeStampedModel

from backend.school_trips import choices as school_trips_choices


class SchoolTrip(TimeStampedModel):
    """
    A school trip is a associated to a vehicle - the person list file is
    the list of people on traveling on that vehicle.
    """

    registration_plate = models.CharField(
        "immatriculation véhicule", max_length=256, blank=True
    )
    trip_date = models.DateTimeField(
        "date du voyage",
        blank=False,
        null=False,
    )
    file_content = models.BinaryField("fichier csv", blank=True, null=True)
    bus_operator = models.CharField("compagnie de bus", max_length=256, blank=True)
    contact_info = models.CharField("info contact", max_length=256, blank=True)
    check_status = models.CharField(
        "status de la vérification",
        max_length=256,
        choices=school_trips_choices.CHECK_STATUS_CHOICES,
        default=school_trips_choices.TRIP_NOT_PROCESSED,
    )

    class Meta:
        verbose_name = "voyage scolaire"
        verbose_name_plural = "voyages scolaires"

    def __str__(self):
        return f"Voyage {self.trip_date} - {self.registration_plate}"

    @property
    def file_url(self):
        return reverse("school_trip_file", args=[self.pk])
