from django.db import models

TRIP_NOT_PROCESSED = "not-processed"
TRIP_PROCESSED = "processed"

CHECK_STATUS_CHOICES = (
    (TRIP_NOT_PROCESSED, "Non traitée"),
    (TRIP_PROCESSED, "Traitée"),
)
