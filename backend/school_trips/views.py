from django.core.exceptions import PermissionDenied
from django.core.files.base import ContentFile
from django.http import Http404
from django.shortcuts import get_object_or_404

from django_downloadview import VirtualDownloadView
from rest_framework import decorators, status, viewsets
from rest_framework.permissions import BasePermission
from rest_framework.response import Response

from backend.logs.actions import ActionLogHandler
from backend.school_trips import choices as school_trips_choices
from backend.school_trips import settings as school_trips_settings
from backend.school_trips.models import SchoolTrip
from backend.school_trips.serializers import (
    SchoolTripSerializer,
    SchoolTripStatusSerializer,
)


class SchoolTripBase(ActionLogHandler):
    def log_action(self, action, user, object, data=""):
        self.create_user_action_log(action=action, user=user, object=object, data=data)


class HasSchoolTripsListPageAccess(BasePermission):
    """
    Check if the user's team has access to the school trips list page.
    """

    def has_permission(self, request, view):
        return (
            request.user.is_authenticated
            and request.user.team.show_school_trips_list_page
        )


class SchoolTripViewSet(viewsets.ReadOnlyModelViewSet, SchoolTripBase):
    queryset = SchoolTrip.objects.all()
    serializer_class = SchoolTripSerializer
    permission_classes = [HasSchoolTripsListPageAccess]

    @decorators.action(detail=True, methods=["POST"], url_path="update-status")
    def update_status(self, request, pk=None):
        trip = self.get_object()
        serializer = SchoolTripStatusSerializer(data=request.data)
        if serializer.is_valid():
            trip.check_status = serializer.validated_data["check_status"]
            trip.save()
            response_data = SchoolTripSerializer(trip).data
            if trip.check_status == school_trips_choices.TRIP_PROCESSED:
                action = school_trips_settings.SCHOOL_TRIPS_PROCESSED_LOG_ACTION
            if trip.check_status == school_trips_choices.TRIP_NOT_PROCESSED:
                action = school_trips_settings.SCHOOL_TRIPS_NOT_PROCESSED_LOG_ACTION
            self.log_action(
                action=action,
                user=request.user,
                object=trip,
                data=serializer.validated_data,
            )
            return Response(response_data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SchoolTripFileView(VirtualDownloadView, SchoolTripBase):
    """
    Download a school trip file from the database file content field.
    """

    def get_object(self):
        """
        Returns an instance SchoolTrip.
        """
        pk = self.kwargs.get("pk")
        qs = SchoolTrip.objects.all()
        trip = get_object_or_404(qs, pk=pk)
        if not trip.file_content:
            raise Http404("No file content for this trip")
        return trip

    def get_file(self):
        has_access = (
            self.request.user.access_person_list
            and self.request.user.team.show_school_trips_list_page
        )
        if not has_access:
            raise PermissionDenied()
        trip = self.get_object()
        file_name = f"voyage-scolaire-{trip.registration_plate}.csv"
        file_content = trip.file_content
        self.log_action(
            action=school_trips_settings.SCHOOL_TRIPS_FILE_DOWNLOAD_LOG_ACTION,
            user=self.request.user,
            object=trip,
        )
        return ContentFile(file_content, name=file_name)
