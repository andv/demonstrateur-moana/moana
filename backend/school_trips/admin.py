from django.contrib import admin

from backend.school_trips.models import SchoolTrip


@admin.register(SchoolTrip)
class SchoolTripAdmin(admin.ModelAdmin):
    list_display = [
        "registration_plate",
        "trip_date",
        "bus_operator",
        "contact_info",
        "check_status",
        "created",
        "modified",
    ]
    search_fields = ["registration_plate"]
    list_filter = ["check_status"]
    date_hierarchy = "created"
