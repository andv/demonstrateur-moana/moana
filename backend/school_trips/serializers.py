from rest_framework import serializers

from backend.school_trips.models import SchoolTrip


class SchoolTripSerializer(serializers.ModelSerializer):
    class Meta:
        model = SchoolTrip
        fields = [
            "id",
            "registration_plate",
            "trip_date",
            "bus_operator",
            "check_status",
            "file_url",
            "created",
            "modified",
        ]


class SchoolTripStatusSerializer(serializers.Serializer):
    check_status = serializers.CharField()
