import string
from os import path
from zoneinfo import ZoneInfo

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils.timezone import datetime, timedelta

import factory
from faker import Factory as FakerFactory
from pytest_factoryboy import register

from backend.ships import settings as ships_settings

faker = FakerFactory.create("fr_FR")
lazyfy = factory.LazyFunction

today = datetime.now(ZoneInfo("Europe/Paris"))
yesterday = today - timedelta(days=1)


##############
# Users      #
##############


@register
class UserFactory(factory.django.DjangoModelFactory):
    first_name = lazyfy(faker.first_name)
    last_name = lazyfy(faker.last_name)
    email = factory.LazyAttribute(
        lambda params: (
            faker.email() if params.domain == "" else faker.email(domain=params.domain)
        )
    )
    is_active = True
    is_staff = True
    access_person_list = True
    manual_upload_fal = True

    @factory.post_generation
    def password(self, create, extracted, **kwargs):
        password = extracted or "12345"
        self.set_password(password)

    class Meta:
        model = get_user_model()

    class Params:
        domain = ""  # 'gouv.fr or 'andv.app'


@register
class TeamFactory(factory.django.DjangoModelFactory):
    name = lazyfy(faker.name)

    class Meta:
        model = "accounts.Team"

    @factory.post_generation
    def ports(self, create, extracted, **kwargs):
        port_list = extracted
        if not port_list:
            port_list = [PortFactory(locode="FRSML")]
        for port in port_list:
            self.ports.add(port)


##############
# Ports      #
##############


@register
class PortFactory(factory.django.DjangoModelFactory):
    locode = "FRSML"

    class Meta:
        model = "ports.Port"
        django_get_or_create = ["locode"]


##############
# Files      #
##############

TEST_DATA_DIR = path.join(settings.BASE_DIR, "backend/unit_tests/data/")

ship_file = SimpleUploadedFile(
    name="fal.xml",
    content=open(path.join(TEST_DATA_DIR, "fal.xml"), "rb").read(),
    content_type="text/xml",
)

exe_file = SimpleUploadedFile(
    name="test.exe",
    content=open(path.join(TEST_DATA_DIR, "test.exe"), "rb").read(),
    content_type="application/x-dosexec",
)

fal_file_with_sh_extension = SimpleUploadedFile(
    name="fal.sh",
    content=open(path.join(TEST_DATA_DIR, "fal.sh"), "rb").read(),
    content_type="text/xml",
)

xml_file_but_invalid_fal = SimpleUploadedFile(
    name="invalid_fal.xml",
    content=open(path.join(TEST_DATA_DIR, "invalid_fal.xml"), "rb").read(),
    content_type="text/xml",
)


#####################
# ShipMovement      #
#####################


@register
class ShipMovementFactory(factory.django.DjangoModelFactory):
    ship_name = lazyfy(faker.name)
    ship_call_id = lazyfy(
        lambda: faker.bothify(text="FR???######", letters=string.ascii_uppercase)
    )
    imo_number = lazyfy(lambda: faker.bothify(text="#######"))
    way = ships_settings.ARRIVING
    port_of_call_relation = factory.SubFactory(PortFactory)

    class Meta:
        model = "ships.ShipMovement"


#####################
# School Trips      #
#####################


@register
class SchoolTripFactory(factory.django.DjangoModelFactory):
    registration_plate = lazyfy(faker.name)
    trip_date = today
    file_content = b"test file content"

    class Meta:
        model = "school_trips.SchoolTrip"
