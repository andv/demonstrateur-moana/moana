<?xml version="1.0" encoding="ISO-8859-1"?>
<NCA_FAL1>
    <Header Version="5.2"
           LARefId="1943034"
           SentAt="2021-09-20T18:36:31"
           To="FRSMLPAF"
           From="FRSMLPOR"/>
    <Body>
        <VesselIdentification IMONumber="9814052"/>
        <ShipCallInformation ShipCallId="FRSML20217396" PortOfCall="FRSML"/>
        <FormInformation EntryOrExit="0">
            <ShipInformation ShipName="LE DUMONT D'URVILLE"
                          CallSign="FLDS"
                          FlagStateOfShip="FR"
                          NameOfMaster="provost"/>
            <VoyageInformation LastPort="FRHON"
                            NextPort="ZZUKN"
                            ETAToPortOfCall="2021-09-21T05:00:00"/>
            <CertificateOfRegistry Locode="WFMAU" LocationName="MATA'UTU (WFMAU)">
                <ShippingContactDetails NameOfAgent="AGENCE MARITIME MALOUINES"
                                    Phone="+33000000000"
                                    Email="test@test.com"/>
            </CertificateOfRegistry>
            <OtherInformations GrossTonnage="9988.000"
                            NetTonnage="2996.000"
                            PositionInPortOfCall="V1"
                            BriefParticularsOfVoyage="HONFLEUR (FRHON) / SAINT-MALO"
                            NumberOfCrew="113"
                            NumberOfPassengers="45"/>
        </FormInformation>
    </Body>
</NCA_FAL1>
