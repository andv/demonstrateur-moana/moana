from zoneinfo import ZoneInfo

from django.contrib.auth.models import Permission
from django.utils.timezone import datetime

from backend.fal.tests import settings as fal_settings
from backend.ship_files.models import ShipFile as Fal
from backend.ships.models import CrewList, PassengerList, ShipMovement
from backend.unit_tests import factories


def login(client, login_user=None, password="12345"):
    """
    Log a user in for the given test session.
    """
    if not login_user:
        login_user = factories.UserFactory()
        login_user.set_password(password)
        login_user.save()
    login_success = client.login(username=login_user.email, password=password)
    assert login_success
    return login_user


def login_saint_malo_user(client):
    login_user = login(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    team_saint_malo = factories.TeamFactory()
    team_saint_malo.ports.add(saint_malo)
    login_user.team = team_saint_malo
    login_user.save()
    return login_user


def login_martinique_user(client):
    login_user = login(client)
    martinique = factories.PortFactory(locode="MQFDF")  # Fort De France
    team_martinique = factories.TeamFactory()
    team_martinique.ports.add(martinique)
    team_martinique.timezone = "America/Martinique"
    team_martinique.save()
    login_user.team = team_martinique
    login_user.save()
    return login_user


def login_multiports_user(client):
    login_user = login(client)
    saint_malo = factories.PortFactory(locode="FRSML")
    le_havre = factories.PortFactory(locode="FRLEH")
    bordeaux = factories.PortFactory(locode="FRBOD")
    team_multiports = factories.TeamFactory()
    team_multiports.ports.add(saint_malo)
    team_multiports.ports.add(le_havre)
    team_multiports.ports.add(bordeaux)
    login_user.team = team_multiports
    login_user.save()
    return login_user


def login_school_trips_upload_user(client):
    login_user = login(client)
    team = factories.TeamFactory(access_school_trips_upload_page=True)
    login_user.team = team
    login_user.save()
    return login_user


def login_user_without_access_to_school_trips_list_page(client):
    login_user = login(client)
    team = factories.TeamFactory(show_school_trips_list_page=False)
    login_user.team = team
    login_user.save()
    return login_user


def login_user_with_access_to_school_trips_list_page(client):
    login_user = login(client)
    team = factories.TeamFactory(show_school_trips_list_page=True)
    login_user.team = team
    login_user.save()
    return login_user


def initialize_ship_movement_with_list(ship_movement_name):
    """Defining a ship_movement with lists"""

    today = datetime.now(ZoneInfo("Europe/Paris"))
    fal6 = Fal(ship_file=fal_settings.FAL6_PATH)
    fal6.save()
    fal5 = Fal(ship_file=fal_settings.FAL5_PATH)
    fal5.save()
    passenger_list = PassengerList.objects.all()[0]
    crew_list = CrewList.objects.all()[0]
    ship_movement = ShipMovement.objects.all()[0]
    ship_movement.ship_name = ship_movement_name
    ship_movement.eta_to_port_of_call = today
    ship_movement.save()

    return ship_movement
