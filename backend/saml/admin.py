from django.contrib import admin

from filesify.admin import FilesifyAdmin
from solo.admin import SingletonModelAdmin

from backend.saml.models import (
    CheopsMetadata,
    DgddiMetadata,
    ProxymaMetadata,
    SamlCertificate,
)


@admin.register(SamlCertificate)
class SamlCertificateAdmin(FilesifyAdmin):
    pass


@admin.register(CheopsMetadata)
class CheopsMetadataAdmin(SingletonModelAdmin, FilesifyAdmin):
    pass


@admin.register(ProxymaMetadata)
class ProxymaMetadataAdmin(SingletonModelAdmin, FilesifyAdmin):
    pass


@admin.register(DgddiMetadata)
class DgddiMetadataAdmin(SingletonModelAdmin, FilesifyAdmin):
    pass
