from urllib.parse import urljoin

from django.conf import settings

import saml2


class SAMLServiceProviderMiddleware:
    """
    Middleware to set the entity ID and the assertion endpoints for the SAML
    service provider. These settings are constructed dynamically looking at
    the request host.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        host = request.get_host()
        base_url = settings.SAML_SP_BASE_URL
        base_url_alt = settings.SAML_SP_BASE_URL_ALT
        selected_base_url = base_url
        if host in base_url_alt:
            selected_base_url = base_url_alt
        # Construct the entity ID using the selected base URL
        entity_id = urljoin(selected_base_url, settings.SAML_SP_METADATA_URL_PATH)
        settings.SAML_CONFIG["entityid"] = entity_id
        acs_url = urljoin(base_url, settings.SAML_ACS_URL_PATH)
        acs_url_alt = urljoin(base_url_alt, settings.SAML_ACS_URL_PATH)
        acs_endpoints = [(acs_url, saml2.BINDING_HTTP_POST)]
        if host in base_url_alt:
            # Use alternative ACS endpoint if the host is the alternative base URL
            acs_endpoints = [(acs_url_alt, saml2.BINDING_HTTP_POST)]
        settings.SAML_CONFIG["service"]["sp"]["endpoints"][
            "assertion_consumer_service"
        ] = acs_endpoints
        response = self.get_response(request)
        return response
