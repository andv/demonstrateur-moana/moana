# Generated by Django 4.2.16 on 2024-10-16 09:09

from django.db import migrations, models
import mirage.fields


class Migration(migrations.Migration):

    dependencies = [
        ("saml", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="DgddiMetadata",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("file_path", models.CharField(max_length=255)),
                ("comment", models.TextField(blank=True, null=True)),
                (
                    "file_content",
                    mirage.fields.EncryptedTextField(
                        help_text="This field will be encrypted on save and decrypted on display."
                    ),
                ),
            ],
            options={
                "verbose_name": "Metadonnées IDP DGDDI",
                "verbose_name_plural": "Metadonnées IDP DGDDI",
            },
        ),
    ]
