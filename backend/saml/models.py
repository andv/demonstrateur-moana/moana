from django.conf import settings

from filesify.models import CryptoFilesify
from solo.models import SingletonModel


class SamlCertificate(CryptoFilesify):
    class Meta:
        verbose_name = "Certificat SAML"
        verbose_name_plural = "Certificats SAML"


class CheopsMetadata(SingletonModel, CryptoFilesify):
    class Meta:
        verbose_name = "Metadonnées IDP Cheops"
        verbose_name_plural = "Metadonnées IDP Cheops"


class ProxymaMetadata(SingletonModel, CryptoFilesify):
    class Meta:
        verbose_name = "Metadonnées IDP Proxyma"
        verbose_name_plural = "Metadonnées IDP Proxyma"


class DgddiMetadata(SingletonModel, CryptoFilesify):
    class Meta:
        verbose_name = "Metadonnées IDP DGDDI"
        verbose_name_plural = "Metadonnées IDP DGDDI"
