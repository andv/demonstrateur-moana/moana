from django.apps import AppConfig

from filesify.mixins import FilesifyPostMigrateMixin


class SamlConfig(FilesifyPostMigrateMixin, AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.saml"
    filesify_limit_to_models = (
        "saml.CheopsMetadata",
        "saml.ProxymaMetadata",
        "saml.SamlCertificate",
        "saml.DgddiMetadata",
    )
    verbose_name = "7.2 Système - SAML"
