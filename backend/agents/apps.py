from django.apps import AppConfig


class ShipAgentConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.agents"
    verbose_name = "Agents maritimes"
