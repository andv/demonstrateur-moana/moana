from rest_framework import decorators, status, viewsets
from rest_framework.exceptions import PermissionDenied, ValidationError
from rest_framework.response import Response

from backend.accounts.models import Team
from backend.alerts.models import ShipWatchlist
from backend.alerts_config import settings as alerts_config_settings
from backend.alerts_config.serializers import (
    AddIMOListSerializer,
    TeamNotificationEmailSerializer,
)
from backend.logs.actions import ActionLogHandler


class AlertsConfigMixin(ActionLogHandler):
    def get_team_from_request(self, request):
        if not request.user.is_team_manager:
            raise PermissionDenied("Only team manager can perform this action.")
        team = request.user.team
        if not team:
            raise ValidationError("User is not associated with a team.")
        return team

    def log_action(self, action, user, object="", target="", description="", data=""):
        self.create_user_action_log(
            action=action,
            user=user,
            object=object,
            target=target,
            description=description,
            data=data,
        )


class NotificationEmailViewSet(AlertsConfigMixin, viewsets.GenericViewSet):
    serializer_class = TeamNotificationEmailSerializer

    @decorators.action(detail=False, methods=["PATCH"], url_path="notification-email")
    def notification_email(self, request):
        """
        Modify user's team email adress used for ship alert notification
        """
        team = self.get_team_from_request(request)
        serializer = self.serializer_class(team, data=request.data, partial=True)
        request.data["notification_email"] = request.data["notification_email"].strip()
        request.data["notification_email"] = request.data["notification_email"].lower()

        if serializer.is_valid():
            old_email = team.notification_email
            serializer.save()
            self.log_action(
                action=alerts_config_settings.LOG_ACTION_UPDATE_NOTIFICATION_EMAIL,
                user=request.user,
                target=team.name,
                description=f"Updated notification email for team. Old email : {old_email}",
                data=serializer.data,
            )
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ShipWatchlistViewSet(AlertsConfigMixin, viewsets.GenericViewSet):

    @decorators.action(detail=True, methods=["DELETE"], url_path="delete-imo")
    def delete_imo(self, request, pk=None):
        """
        Delete IMO from user's team watchlist
        """
        team = self.get_team_from_request(request)
        imo_to_delete = pk
        if not imo_to_delete:
            return Response(
                {"error": "IMO number is required."},
                status=status.HTTP_400_BAD_REQUEST,
            )
        try:
            watchlist = ShipWatchlist.objects.get(team=team)
        except ShipWatchlist.DoesNotExist:
            return Response(
                {"error": "Watchlist not found for the team."},
                status=status.HTTP_404_NOT_FOUND,
            )
        imo_list = watchlist.get_imo_list()
        if imo_to_delete not in imo_list:
            return Response(
                {"error": "IMO number not found in the watchlist."},
                status=status.HTTP_404_NOT_FOUND,
            )
        imo_list.remove(imo_to_delete)
        watchlist.imo_list = " ".join(imo_list)
        watchlist.save()
        self.log_action(
            action=alerts_config_settings.LOG_ACTION_DELETE_IMO,
            user=request.user,
            target=team.name,
            description=f"Deleted IMO number {imo_to_delete} from watchlist",
            data={"imo_number": imo_to_delete},
        )
        return Response({"status": "IMO number deleted successfully."})


class AddIMOListViewSet(AlertsConfigMixin, viewsets.GenericViewSet):
    serializer_class = AddIMOListSerializer

    @decorators.action(detail=False, methods=["POST"], url_path="add-imo-list")
    def add_imo_list(self, request):
        """
        Add IMO in user's team watchlist
        """
        team = self.get_team_from_request(request)
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        imo_list_to_add = serializer.validated_data["imo_list"]
        try:
            watchlist = ShipWatchlist.objects.get(team=team)
        except ShipWatchlist.DoesNotExist:
            watchlist = ShipWatchlist.objects.create(team=team, imo_list="")
        current_imo_list = watchlist.get_imo_list()
        updated_imo_list = list(set(current_imo_list + imo_list_to_add))
        watchlist.imo_list = " ".join(updated_imo_list)
        watchlist.save()
        self.log_action(
            action=alerts_config_settings.LOG_ACTION_ADD_IMO_LIST,
            user=request.user,
            target=team.name,
            description="Added IMO numbers to watchlist",
            data={"imo_list": imo_list_to_add},
        )
        return Response({"status": "IMO numbers added successfully."})
