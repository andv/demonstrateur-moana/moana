from django.apps import AppConfig


class AlertsConfigConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.alerts_config"
    verbose_name = "Config des alertes"
