from django.conf import settings
from django.shortcuts import reverse

import pytest
from rest_framework import status

from backend.alerts.models import ShipAlert, ShipWatchlist
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


IMO_NUMBER = "1234567"
IMO_LIST = "2345678 " + IMO_NUMBER
UNKNOWN_IMO = "9999999"
XSS_IMO = "<s><s>"  # Not really an xss as the / is not allowed in this context


def initialize_test(client):
    user = utils.login_saint_malo_user(client)
    user.is_team_manager = True
    team = factories.TeamFactory(name="Test Team", notification_email="some@email.com")
    user.team = team
    user.save()
    ShipWatchlist.objects.create(team=team, imo_list=IMO_LIST)
    return team, user


def send_api_request(client, imo):
    url = reverse("alerts-config-delete-imo", kwargs={"pk": imo})
    return client.delete(url)


def test_can_delete_imo_from_watchlist(client):
    team, _ = initialize_test(client)
    response = send_api_request(client, IMO_NUMBER)
    assert response.status_code == status.HTTP_200_OK
    watchlist = ShipWatchlist.objects.get(team=team)
    assert IMO_NUMBER not in watchlist.get_imo_list()


def test_cannot_delete_imo_if_not_in_watchlist(client):
    team, _ = initialize_test(client)
    response = send_api_request(client, UNKNOWN_IMO)
    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_cannot_delete_imo_if_watchlist_not_found(client):
    team, _ = initialize_test(client)
    ShipWatchlist.objects.filter(team=team).delete()
    response = send_api_request(client, IMO_NUMBER)
    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_cannot_delete_imo_if_not_team_manager(client):
    team, user = initialize_test(client)
    user.is_team_manager = False
    user.save()
    response = send_api_request(client, IMO_NUMBER)
    assert response.status_code == status.HTTP_403_FORBIDDEN
    watchlist = ShipWatchlist.objects.get(team=team)
    assert IMO_NUMBER in watchlist.get_imo_list()


def test_cannot_delete_imo_with_xss(client):
    team, _ = initialize_test(client)
    response = send_api_request(client, XSS_IMO)
    assert response.status_code == status.HTTP_404_NOT_FOUND
    watchlist = ShipWatchlist.objects.get(team=team)
    assert XSS_IMO not in watchlist.get_imo_list()
