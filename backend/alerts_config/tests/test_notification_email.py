from django.conf import settings
from django.shortcuts import reverse

import pytest
from rest_framework import status

from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())

OLD_EMAIL = "old@courriel.com"
NEW_EMAIL = "new@email.com"
XSS_EMAIL = "<script>alert('XSS')</script>@example.com"
GOOD_EMAIL = "good@email.com"
GOOD_SUBDOMAIN_EMAIL = "good@subdomain.email.com"
BAD_EMAIL = "really@bad@email.com"
BAD_SUBDOMAIN_EMAIL = "bad@subdomainemail.com"
VERY_BAD_SUBDOMAIN_EMAIL = "very@bad@subdomain.email.com"
FAKE_SUBDOMAIN_EMAIL = "fake@subdomain-email.com"


def initialize_test(client):
    user = utils.login_saint_malo_user(client)
    user.is_team_manager = True
    team = factories.TeamFactory(name="Test Team", notification_email=OLD_EMAIL)
    user.team = team
    user.save()
    return team, user


def send_api_request(client, data):
    url = reverse("alerts-config-notification-email")
    return client.patch(url, data=data, content_type="application/json")


def test_can_update_notification_email(client):
    team, _ = initialize_test(client)
    response = send_api_request(client, {"notification_email": NEW_EMAIL})
    assert response.status_code == status.HTTP_200_OK
    team.refresh_from_db()
    assert team.notification_email == NEW_EMAIL


def test_can_clear_notification_email(client):
    team, _ = initialize_test(client)
    response = send_api_request(client, {"notification_email": ""})
    assert response.status_code == status.HTTP_200_OK
    team.refresh_from_db()
    assert team.notification_email == ""


def test_cannot_update_notification_email_if_team_not_found(client):
    team, user = initialize_test(client)
    user.team = None
    user.save()
    response = send_api_request(client, {"notification_email": NEW_EMAIL})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert team.notification_email != NEW_EMAIL


def test_cannot_update_notification_email_if_not_team_manager(client):
    team, user = initialize_test(client)
    user.is_team_manager = False
    user.save()
    response = send_api_request(client, {"notification_email": NEW_EMAIL})
    assert response.status_code == status.HTTP_403_FORBIDDEN
    team.refresh_from_db()
    assert team.notification_email != NEW_EMAIL


def test_cannot_update_notification_email_with_xss(client):
    team, _ = initialize_test(client)
    response = send_api_request(client, {"notification_email": XSS_EMAIL})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    team.refresh_from_db()
    assert team.notification_email == OLD_EMAIL


def test_check_correct_emails_addresses(client):
    team, _ = initialize_test(client)
    response = send_api_request(client, {"notification_email": GOOD_EMAIL})
    assert response.status_code == status.HTTP_200_OK
    response = send_api_request(client, {"notification_email": GOOD_SUBDOMAIN_EMAIL})
    assert response.status_code == status.HTTP_200_OK


def test_check_incorrect_emails_addresses(client):
    team, _ = initialize_test(client)
    response = send_api_request(client, {"notification_email": BAD_EMAIL})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    response = send_api_request(client, {"notification_email": BAD_SUBDOMAIN_EMAIL})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    response = send_api_request(
        client, {"notification_email": VERY_BAD_SUBDOMAIN_EMAIL}
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    response = send_api_request(client, {"notification_email": FAKE_SUBDOMAIN_EMAIL})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
