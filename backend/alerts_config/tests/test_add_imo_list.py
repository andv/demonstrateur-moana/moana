from django.conf import settings
from django.shortcuts import reverse

import pytest
from rest_framework import status

from backend.alerts.models import ShipWatchlist
from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())

IMO_LIST = ["1111111", "2222222"]
NEW_IMO_LIST = ["8888888", "9999999"]
INVALID_IMO_LIST = ["1234567", "invalid"]
XSS_IMO_LIST = ["<s></s>", "9999999"]
SHORT_IMO_LIST = ["123456", "7654321"]
LONG_IMO_LIST = ["12345678", "87654321"]


def initialize_test(client):
    user = utils.login_saint_malo_user(client)
    user.is_team_manager = True
    team = factories.TeamFactory(name="Test Team")
    user.team = team
    user.save()
    ShipWatchlist.objects.create(team=team, imo_list=" ".join(IMO_LIST))
    return team, user


def send_api_request(client, data):
    url = reverse("alerts-config-add-imo-list")
    return client.post(url, data=data, content_type="application/json")


def test_can_add_imo_list(client):
    team, _ = initialize_test(client)
    response = send_api_request(client, {"imo_list": NEW_IMO_LIST})
    assert response.status_code == status.HTTP_200_OK
    watchlist = ShipWatchlist.objects.get(team=team)
    expected_imo_list = set(IMO_LIST + NEW_IMO_LIST)
    assert set(watchlist.get_imo_list()) == expected_imo_list


def test_cannot_add_imo_list_if_not_team_manager(client):
    team, user = initialize_test(client)
    user.is_team_manager = False
    user.save()
    response = send_api_request(client, {"imo_list": NEW_IMO_LIST})
    assert response.status_code == status.HTTP_403_FORBIDDEN
    watchlist = ShipWatchlist.objects.get(team=team)
    assert set(watchlist.get_imo_list()) == set(IMO_LIST)


def test_cannot_add_imo_list_if_team_not_found(client):
    team, user = initialize_test(client)
    user.team = None
    user.save()
    response = send_api_request(client, {"imo_list": NEW_IMO_LIST})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    watchlist = ShipWatchlist.objects.get(team=team)
    assert set(watchlist.get_imo_list()) == set(IMO_LIST)


def test_cannot_add_imo_list_with_invalid_imo(client):
    team, _ = initialize_test(client)
    response = send_api_request(client, {"imo_list": INVALID_IMO_LIST})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    watchlist = ShipWatchlist.objects.get(team=team)
    assert set(watchlist.get_imo_list()) == set(IMO_LIST)


def test_cannot_add_imo_list_with_xss(client):
    team, _ = initialize_test(client)
    response = send_api_request(client, {"imo_list": XSS_IMO_LIST})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    watchlist = ShipWatchlist.objects.get(team=team)
    assert set(watchlist.get_imo_list()) == set(IMO_LIST)


def test_cannot_add_imo_list_with_short_imo(client):
    team, _ = initialize_test(client)
    response = send_api_request(client, {"imo_list": SHORT_IMO_LIST})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    watchlist = ShipWatchlist.objects.get(team=team)
    assert set(watchlist.get_imo_list()) == set(IMO_LIST)


def test_cannot_add_imo_list_with_long_imo(client):
    team, _ = initialize_test(client)
    response = send_api_request(client, {"imo_list": LONG_IMO_LIST})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    watchlist = ShipWatchlist.objects.get(team=team)
    assert set(watchlist.get_imo_list()) == set(IMO_LIST)
