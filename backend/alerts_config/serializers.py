import re
from rest_framework import serializers
from backend.accounts.models import Team
from django.conf import settings


class TeamNotificationEmailSerializer(serializers.ModelSerializer):
    notification_email = serializers.EmailField(required=True, allow_blank=True)

    class Meta:
        model = Team
        fields = ["notification_email"]

    def validate_notification_email(self, email_address: str):
        """Check email format"""
        for domain_name in settings.ALLOWED_DOMAIN_NAME_ARRAY:
            if email_address == "" or re.match(
                rf"^[^@\s]+@[^@\s]+\.{domain_name}$|^[^@\s]+(@{domain_name})$",
                email_address,
            ):
                return email_address
        raise serializers.ValidationError("Saisissez une adresse e-mail valide.")


class AddIMOListSerializer(serializers.Serializer):
    imo_list = serializers.ListField(child=serializers.CharField(max_length=7))

    def validate_imo_list(self, value):
        for imo in value:
            if not imo.isdigit():
                raise serializers.ValidationError("IMO must be valid digits.")
            if len(imo) != 7:
                raise serializers.ValidationError("IMO must be 7 characters long.")
        return value
