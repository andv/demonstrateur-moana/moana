from django.apps import AppConfig


class FaqConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.faq"
    verbose_name = "6.2 Site internet - FAQ"
