from rest_framework import serializers

from backend.faq.models import FaqQuestionAnswer


class FaqQuestionAnswerSerializer(serializers.ModelSerializer):
    faq_category_name = serializers.ReadOnlyField(source="faq_category.name")

    class Meta:
        model = FaqQuestionAnswer
        read_only_fields = ("faq_category_name",)
        fields = [
            "id",
            "question",
            "answer",
            "faq_category_name",
        ]
