from django.db import models
from django.utils import timezone

from tinymce.models import HTMLField


class FaqCategory(models.Model):
    name = models.TextField(
        "Nom",
        blank=False,
    )

    class Meta:
        verbose_name = "Rubrique de la FAQ"
        verbose_name_plural = "Rubriques de la FAQ"
        ordering = ["pk"]

    def __str__(self):
        return self.name


class FaqQuestionAnswer(models.Model):
    question = models.TextField("Question", blank=False)

    answer = HTMLField("Réponses", blank=False)

    faq_category = models.ForeignKey(
        "faq.FaqCategory",
        verbose_name="Rubrique",
        related_name="faqquestionanswer",
        help_text="Rubrique liée à cette Question-Réponse",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )

    order = models.PositiveIntegerField("Rang", blank=False, default=0, db_index=True)

    class Meta:
        verbose_name = "Question-Réponse de la FAQ"
        verbose_name_plural = "Question-Réponse de la FAQ"
        ordering = ["order"]

    def __str__(self):
        return self.question
