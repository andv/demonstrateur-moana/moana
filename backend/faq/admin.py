from django.contrib import admin
from adminsortable2.admin import SortableAdminMixin

from backend.faq.models import FaqCategory, FaqQuestionAnswer


@admin.register(FaqCategory)
class FaqCategoryAdmin(admin.ModelAdmin):
    list_display = [
        "name",
    ]
    fieldsets = (
        (
            "Configuration",
            {
                "fields": [
                    "name",
                ]
            },
        ),
    )


@admin.register(FaqQuestionAnswer)
class FaqQuestionAnswerAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = [
        "question",
        "answer",
        "faq_category",
        "order",
    ]
    fieldsets = (("Configuration", {"fields": ["question", "answer", "faq_category"]}),)
