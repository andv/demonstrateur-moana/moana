from rest_framework import decorators, viewsets

from backend.faq.models import FaqQuestionAnswer
from backend.faq.serializers import (
    FaqQuestionAnswerSerializer,
)


class FaqQuestionAnswerViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = FaqQuestionAnswer.objects.all()
    serializer_class = FaqQuestionAnswerSerializer
