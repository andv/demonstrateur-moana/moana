from backend.geo import choices as geo_choices
from backend.geo import references as geo_references


class PortGeography:
    def __init__(self, locode):
        self.locode = locode

    def in_france(self):
        if not self.locode:
            return
        if self.locode[:2].upper() == "FR":
            return True
        if self.in_france_overseas():
            return True
        return False

    def in_france_overseas(self):
        if not self.locode:
            return
        if self.locode[:2].upper() in geo_references.FRANCE_OVERSEAS_AREA_CODES:
            return True
        return False

    def in_schengen(self):
        if not self.locode:
            return
        if self.locode[:2].upper() in geo_references.SCHENGEN_AREA_CODES:
            return True
        return False

    def is_unknown(self):
        if not self.locode:
            return
        return self.locode.upper() == "ZZUKN"

    def get_geographic_area(self):
        if not self.locode:
            return geo_choices.AREA_UNKNOWN
        if self.is_unknown():
            return geo_choices.AREA_UNKNOWN
        if self.in_france_overseas():
            return geo_choices.AREA_FRANCE_OVERSEAS
        if self.in_france():
            return geo_choices.AREA_FRANCE_METROPOLITAN
        if self.in_schengen():
            return geo_choices.AREA_INTRA_SCHENGEN
        else:
            return geo_choices.AREA_EXTRA_SCHENGEN
