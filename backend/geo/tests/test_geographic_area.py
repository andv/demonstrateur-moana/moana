from django.conf import settings

import pytest

from backend.geo import choices as geo_choices
from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())

in_schengen = "ESSPC"  # Spain
in_france_1 = "FRSML"  # Saint Malo
in_france_2 = "FRROS"  # Roscoff
in_france_overseas_1 = "MQFDF"  # Fort De France, Martinique
in_france_overseas_1bis = "MQSPI"  # St Pierre, Martinique
in_france_overseas_2 = "GPPTP"  # Pointe a Pitre, Guadeloupe
in_extra_schengen = "CLCHB"  # Chacabuco, Chili
port_unknown = "ZZUKN"


@pytest.fixture(params=[in_france_1, in_france_overseas_1])
def ship(request, client):
    """
    Create a ship with different port of call geographic areas.
    """
    return ShipMovement.objects.create(
        port_of_call=request.param,
        way=ships_settings.ARRIVING,
    )


def test_arriving_ship_is_schengen_if_last_port_is_in_schengen(ship):
    ship.way = ships_settings.ARRIVING
    ship.last_port = in_schengen
    ship.save()
    if ship.port_of_call_is_in_france_metropolitan():
        assert ship.geographic_area == geo_choices.AREA_INTRA_SCHENGEN
    if ship.port_of_call_is_in_france_overseas():
        assert ship.geographic_area == geo_choices.AREA_INTRA_SCHENGEN


def test_departing_ship_is_schengen_if_next_port_is_in_schengen(ship):
    ship.way = ships_settings.DEPARTING
    ship.next_port = in_schengen
    ship.save()
    if ship.port_of_call_is_in_france_metropolitan():
        assert ship.geographic_area == geo_choices.AREA_INTRA_SCHENGEN
    if ship.port_of_call_is_in_france_overseas():
        assert ship.geographic_area == geo_choices.AREA_INTRA_SCHENGEN


def test_arriving_ship_is_metropolitan_or_x_overseas_if_last_port_is_in_france(
    ship,
):
    ship.way = ships_settings.ARRIVING
    ship.last_port = in_france_2
    ship.save()
    if ship.port_of_call_is_in_france_metropolitan():
        assert ship.geographic_area == geo_choices.AREA_FRANCE_METROPOLITAN
    if ship.port_of_call_is_in_france_overseas():
        assert ship.geographic_area == geo_choices.AREA_METROPOLITAN_X_OVERSEAS


def test_departing_ship_is_metropolitan_or_x_overseas_if_next_port_is_in_france(
    ship,
):
    ship.way = ships_settings.DEPARTING
    ship.next_port = in_france_2
    ship.save()
    if ship.port_of_call_is_in_france_metropolitan():
        assert ship.geographic_area == geo_choices.AREA_FRANCE_METROPOLITAN
    if ship.port_of_call_is_in_france_overseas():
        assert ship.geographic_area == geo_choices.AREA_METROPOLITAN_X_OVERSEAS


def test_arriving_ship_is_x_overseas_or_inter_overseas_if_last_port_is_overseas(
    ship,
):
    ship.way = ships_settings.ARRIVING
    ship.last_port = in_france_overseas_2
    ship.save()
    if ship.port_of_call_is_in_france_metropolitan():
        assert ship.geographic_area == geo_choices.AREA_METROPOLITAN_X_OVERSEAS
    if ship.port_of_call_is_in_france_overseas():
        assert ship.geographic_area == geo_choices.AREA_INTER_OVERSEAS


def test_departing_ship_is_x_overseas_or_inter_overseas_if_next_port_is_overseas(
    ship,
):
    ship.way = ships_settings.DEPARTING
    ship.next_port = in_france_overseas_2
    ship.save()
    if ship.port_of_call_is_in_france_metropolitan():
        assert ship.geographic_area == geo_choices.AREA_METROPOLITAN_X_OVERSEAS
    if ship.port_of_call_is_in_france_overseas():
        assert ship.geographic_area == geo_choices.AREA_INTER_OVERSEAS


def test_arriving_ship_is_x_overseas_or_local_overseas_if_last_port_is_same_overseas_department(
    ship,
):
    ship.way = ships_settings.ARRIVING
    ship.last_port = in_france_overseas_1bis
    ship.save()
    if ship.port_of_call_is_in_france_metropolitan():
        assert ship.geographic_area == geo_choices.AREA_METROPOLITAN_X_OVERSEAS
    if ship.port_of_call_is_in_france_overseas():
        assert ship.geographic_area == geo_choices.AREA_LOCAL_OVERSEAS


def test_departing_ship_is_x_overseas_or_inter_overseas_if_next_port_is_is_same_overseas_department(
    ship,
):
    ship.way = ships_settings.DEPARTING
    ship.next_port = in_france_overseas_1bis
    ship.save()
    if ship.port_of_call_is_in_france_metropolitan():
        assert ship.geographic_area == geo_choices.AREA_METROPOLITAN_X_OVERSEAS
    if ship.port_of_call_is_in_france_overseas():
        assert ship.geographic_area == geo_choices.AREA_LOCAL_OVERSEAS


def test_arriving_ship_is_extra_schengen_if_last_port_is_in_extra_schengen(
    ship,
):
    ship.way = ships_settings.ARRIVING
    ship.last_port = in_extra_schengen
    ship.save()
    if ship.port_of_call_is_in_france_metropolitan():
        assert ship.geographic_area == geo_choices.AREA_EXTRA_SCHENGEN
    if ship.port_of_call_is_in_france_overseas():
        assert ship.geographic_area == geo_choices.AREA_EXTRA_SCHENGEN


def test_departing_ship_is_extra_schengen_if_next_port_is_in_extra_schengen(
    ship,
):
    ship.way = ships_settings.DEPARTING
    ship.next_port = in_extra_schengen
    ship.save()
    if ship.port_of_call_is_in_france_metropolitan():
        assert ship.geographic_area == geo_choices.AREA_EXTRA_SCHENGEN
    if ship.port_of_call_is_in_france_overseas():
        assert ship.geographic_area == geo_choices.AREA_EXTRA_SCHENGEN


def test_arriving_ship_is_unknown_if_last_port_is_unknown(ship):
    ship.way = ships_settings.ARRIVING
    ship.last_port = port_unknown
    ship.save()
    if ship.port_of_call_is_in_france_metropolitan():
        assert ship.geographic_area == geo_choices.AREA_UNKNOWN
    if ship.port_of_call_is_in_france_overseas():
        assert ship.geographic_area == geo_choices.AREA_UNKNOWN


def test_departing_ship_is_unknown_if_next_port_is_unknown(ship):
    ship.way = ships_settings.DEPARTING
    ship.next_port = port_unknown
    ship.save()
    if ship.port_of_call_is_in_france_metropolitan():
        assert ship.geographic_area == geo_choices.AREA_UNKNOWN
    if ship.port_of_call_is_in_france_overseas():
        assert ship.geographic_area == geo_choices.AREA_UNKNOWN


def test_arriving_ship_is_unknown_if_last_port_is_empty(ship):
    ship.way = ships_settings.ARRIVING
    ship.last_port = ""
    ship.save()
    ship = ShipMovement.objects.create(
        way=ships_settings.ARRIVING, last_port="", port_of_call=in_france_1
    )
    assert ship.geographic_area == geo_choices.AREA_UNKNOWN


def test_departing_ship_is_unknown_if_next_port_is_empty(ship):
    ship.way = ships_settings.DEPARTING
    ship.next_port = ""
    ship.save()
    ship = ShipMovement.objects.create(
        way=ships_settings.DEPARTING, next_port="", port_of_call=in_france_1
    )
    assert ship.geographic_area == geo_choices.AREA_UNKNOWN
