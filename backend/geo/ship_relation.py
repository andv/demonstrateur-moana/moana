from django.db import models

from backend.geo import choices as geo_choices
from backend.geo.ports import PortGeography


class GeoOnShip(models.Model):
    """
    This abstract class is expected to be sub-classed on a ShipMovement-like model.
    It provides port geographic area fields and helpers.
    """

    geographic_area = models.CharField(
        "zone géographique",
        max_length=255,
        choices=geo_choices.AREA_CHOICES,
        blank=True,
    )

    class Meta:
        abstract = True

    def get_geographic_area(self):
        if self.port_of_call_is_in_france_metropolitan():
            if self.to_or_from_area_is_in_france_metropolitan():
                return geo_choices.AREA_FRANCE_METROPOLITAN
            if self.to_or_from_area_is_in_france_overseas():
                return geo_choices.AREA_METROPOLITAN_X_OVERSEAS
        if self.port_of_call_is_in_france_overseas:
            if self.to_or_from_area_is_in_france_metropolitan():
                return geo_choices.AREA_METROPOLITAN_X_OVERSEAS
            if self.has_same_alpha2_code():
                return geo_choices.AREA_LOCAL_OVERSEAS
            if self.to_or_from_area_is_in_france_overseas():
                return geo_choices.AREA_INTER_OVERSEAS
        return self.to_or_from_area

    def port_of_call_is_in_france_metropolitan(self):
        return self.port_of_call_geographic_area == geo_choices.AREA_FRANCE_METROPOLITAN

    def port_of_call_is_in_france_overseas(self):
        return self.port_of_call_geographic_area == geo_choices.AREA_FRANCE_OVERSEAS

    def has_same_alpha2_code(self):
        """
        Compare the alpha2 codes of the port of call with the
        port of arrival or destination.
        Alpha2 codes are the 2 first digits of the port locode.

        """
        if not self.port_of_call or not self.to_or_from_port:
            return False
        return self.port_of_call[:2] == self.to_or_from_port[:2]

    @property
    def to_or_from_port(self):
        if self.is_arriving:
            return self.last_port
        if self.is_departing:
            return self.next_port

    @property
    def to_or_from_area(self):
        if self.is_arriving:
            return self.last_port_geographic_area
        if self.is_departing:
            return self.next_port_geographic_area

    def to_or_from_area_is_in_france_metropolitan(self):
        return self.to_or_from_area == geo_choices.AREA_FRANCE_METROPOLITAN

    def to_or_from_area_is_in_france_overseas(self):
        return self.to_or_from_area == geo_choices.AREA_FRANCE_OVERSEAS

    def get_last_port_geographic_area_display(self):
        area_name = self.last_port_geographic_area
        display_name = self.get_geographic_display(area_name)
        return display_name

    def get_next_port_geographic_area_display(self):
        area_name = self.next_port_geographic_area
        display_name = self.get_geographic_display(area_name)
        return display_name

    def get_geographic_display(self, area_name):
        all_areas = dict(geo_choices.AREA_CHOICES)
        display_value = all_areas[area_name]
        return display_value

    @property
    def last_port_geographic_area(self):
        area = PortGeography(self.last_port).get_geographic_area()
        return area

    @property
    def next_port_geographic_area(self):
        area = PortGeography(self.next_port).get_geographic_area()
        return area

    @property
    def port_of_call_geographic_area(self):
        area = PortGeography(self.port_of_call).get_geographic_area()
        return area

    def save(self, *args, **kwargs):
        self.geographic_area = self.get_geographic_area() or ""
        return super().save(*args, **kwargs)
