AREA_FRANCE_METROPOLITAN = "france-metropolitan"
AREA_FRANCE_OVERSEAS = "france-overseas"
AREA_INTRA_SCHENGEN = "intra-schengen"
AREA_EXTRA_SCHENGEN = "extra-schengen"
AREA_METROPOLITAN_X_OVERSEAS = "metropolitan-x-overseas"
AREA_LOCAL_OVERSEAS = "local-overseas"
AREA_INTER_OVERSEAS = "inter-overseas"
AREA_UNKNOWN = "unknown"

AREA_ALLOWED_TO_DOWNLOAD_LISTS = (
    (AREA_FRANCE_OVERSEAS, "FR Outre-mer"),
    (AREA_INTRA_SCHENGEN, "Intra Schengen"),
    (AREA_EXTRA_SCHENGEN, "Extra Schengen"),
    (AREA_INTER_OVERSEAS, "Inter Outre-mer"),
    (AREA_METROPOLITAN_X_OVERSEAS, "Métropole x Outre-mer"),
    (AREA_UNKNOWN, "Inconnu"),
)

AREA_NOT_ALLOWED_TO_DOWNLOAD_LISTS = (
    (AREA_FRANCE_METROPOLITAN, "FR Métropole"),
    (AREA_LOCAL_OVERSEAS, "Local Outre-mer"),
)

AREA_CHOICES = AREA_NOT_ALLOWED_TO_DOWNLOAD_LISTS + AREA_ALLOWED_TO_DOWNLOAD_LISTS
