from django.apps import AppConfig


class GeographicAreasConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.geo"
