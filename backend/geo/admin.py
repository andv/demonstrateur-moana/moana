class GeoModelAdmin:
    def get_fields(self, request, obj=None, **kwargs):
        fields = super().get_fields(request, obj, **kwargs)
        # Push `geographic_area` field last
        fields.remove("geographic_area")
        fields.append("geographic_area")
        return fields
