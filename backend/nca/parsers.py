from django.utils import timezone
from django.utils.dateparse import parse_datetime

from defusedxml import ElementTree as ET

from backend.ship_files import settings as ship_files_settings


class NcaParser:
    past_movement_date_list = None  # When the movement date is too old

    def get_xml_root(self, file_path):
        # We use `fromstring()` instead of `parse()` in order to read this file in UTF-8.
        # Using `parse()` introduce encoding issues because the XML encoding declaration
        # is Latin-1 where we expect UTF-8.
        with open(self.file_path, encoding="utf-8") as file_obj:
            xml_root = ET.fromstring(file_obj.read())
        return xml_root

    def __init__(self, file_path):
        self.file_path = file_path
        self.root = self.get_xml_root(self.file_path)
        self.file_type = ship_files_settings.SHIP_FILE_TYPES[self.root.tag]
        self.header = self.root.find("Header")
        self.body = self.root.find("Body")

    def get_parsed_data(self):
        """
        Depending on the file type, this method returns the parsed data.
        This method should be defined when subclassing the base class.
        """
        raise NotImplementedError

    def clean_past_date(self, date_obj):
        """
        If the given date is too old, we consider that's abnormal,
        we should be keep it.
        """
        today = timezone.now()
        ten_year_ago = today - timezone.timedelta(days=365 * 10)
        if date_obj < ten_year_ago:
            if self.past_movement_date_list is None:
                self.past_movement_date_list = []
            self.past_movement_date_list.append(date_obj)
            return None
        return date_obj

    def format_datetime(self, datetime_string):
        """
        Convert an iso datetime string to a timezone aware datetime.
        The timezone used in FAL files is UTC+0.
        This is also where the abnormal dates are cleaned-up.
        """
        if not datetime_string:
            return None
        dt_obj = parse_datetime(datetime_string)
        dt_utc = timezone.make_aware(dt_obj, timezone=timezone.utc)
        dt_utc = self.clean_past_date(dt_utc)
        return dt_utc
