from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        if settings.TRACKMAN_ENABLED:
            self.stdout.write("Start migrate for stats database.")
            for app_label in settings.STATS_RELATED_APPS:
                call_command("migrate", app_label=app_label, database=settings.TRACKMAN_DATABASE_ALIAS)
        else:
            self.stdout.write("Tracking on stats database is not enabled.")
