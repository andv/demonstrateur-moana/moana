import sys

from django.apps import AppConfig


class AccountsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.accounts"
    verbose_name = "1.1 Utilisateurs - Gestion utilisateurs"

    def ready(self):
        if "migrate" not in sys.argv:
            import backend.accounts.signals  # noqa
