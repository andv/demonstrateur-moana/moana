# Generated by Django 3.2.16 on 2023-03-21 13:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20221220_1007'),
    ]

    operations = [
        migrations.AddField(
            model_name='team',
            name='show_roc_checkboxes',
            field=models.BooleanField(default=False, help_text="Cocher pour afficher le suivi du traitement des listes ROC aux membres de l'équipe", verbose_name='Traitement ROC'),
        ),
    ]
