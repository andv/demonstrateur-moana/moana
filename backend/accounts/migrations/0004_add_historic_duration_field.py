# Generated by Django 3.2.20 on 2023-07-17 13:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_add_roc_tracking_option'),
    ]

    operations = [
        migrations.AddField(
            model_name='team',
            name='historic_duration',
            field=models.IntegerField(default=7, help_text="La durée maximale est de 60 jours, cela vient de la configuration de purge de l'application.", verbose_name='Nombre de jours'),
        ),
    ]
