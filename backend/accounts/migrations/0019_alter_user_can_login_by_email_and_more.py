# Generated by Django 4.2.16 on 2024-11-07 14:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0018_user_is_team_manager"),
    ]

    operations = [
        migrations.AlterField(
            model_name="user",
            name="can_login_by_email",
            field=models.BooleanField(
                default=False,
                help_text="Cocher pour autoriser la connexion par un lien envoyé par email",
                verbose_name="Peut se connecter par e-mail",
            ),
        ),
        migrations.AlterField(
            model_name="user",
            name="is_team_manager",
            field=models.BooleanField(
                default=False,
                help_text="Pour les utilisateurs référents qui peuvent modifier les configurations de leur équipe, par exemple les alertes.",
                verbose_name="Est gestionnaire d'équipe",
            ),
        ),
    ]
