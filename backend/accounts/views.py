from django.contrib.auth import views as auth_views
from django.urls import reverse_lazy

from rest_framework import decorators, viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from backend.accounts.models import User
from backend.accounts.serializers import TeamSerializer, UserSerializer


class LoginView(auth_views.LoginView):
    """
    User Login
    """

    template_name = "accounts/login.html"


class LogoutView(auth_views.LogoutView):
    """
    User Logout
    """

    next_page = reverse_lazy("magicauth-login")


login_view = LoginView.as_view()
logout_view = LogoutView.as_view()


class AccountsViewSet(viewsets.ViewSet):

    @decorators.action(
        detail=False,
        methods=["POST"],
        url_path="auth-email-allowed",
        url_name="auth-email-allowed",
        permission_classes=[AllowAny],
    )
    def can_login_by_email(self, request):
        """
        Verify if user is allowed to authenticate himself with his email
        """

        email = request.data.get("email")
        user = User.objects.filter(email=email).first()
        if not user:
            return Response({}, status=403)
        return Response(user.can_login_by_email, status=200)


class TeamViewSet(viewsets.ViewSet):

    @decorators.action(
        detail=False,
        methods=["GET"],
        url_path="users",
        url_name="users",
        permission_classes=[AllowAny],
    )
    def get_active_users(self, request):
        """
        List all users from user's team
        """
        team = request.user.team
        if not team:
            return Response({}, status=403)
        users = team.users.exclude(is_active=False).order_by("email")
        serialized_users = UserSerializer(users, many=True)
        response_data = {"count": len(users), "users": serialized_users.data.copy()}
        return Response(response_data, status=200)

    @decorators.action(
        detail=False,
        methods=["GET"],
        url_path="settings",
        url_name="settings",
        permission_classes=[AllowAny],
    )
    def get_settings(self, request):
        """
        List all team's settings
        """
        team = request.user.team
        if not team:
            return Response({}, status=403)
        serialized_team = TeamSerializer(team)
        response_data = serialized_team.data.copy()
        return Response(response_data, status=200)
