from django.core.management.base import BaseCommand
from django.db.models import Q

from backend.accounts.models import User


class Command(BaseCommand):
    def handle(self, *args, **options):
        users = User.objects.exclude(Q(team__name="MOANA") | Q(is_superuser=True))
        number_users = users.count()
        users.delete()
        self.stdout.write(self.style.SUCCESS(f"{number_users} user(s) deleted"))
