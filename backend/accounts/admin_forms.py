from django import forms
from django.contrib.auth.forms import UserCreationForm


class CleanEmailMixin:
    def clean_email(self):
        data = self.cleaned_data["email"]
        return data.lower()


class UserAdminForm(CleanEmailMixin, forms.ModelForm):
    pass


class UserAdminCreationForm(CleanEmailMixin, UserCreationForm):
    pass
