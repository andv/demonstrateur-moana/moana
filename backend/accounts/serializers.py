from rest_framework import serializers

from backend.accounts.models import Team, User


class UserSerializer(serializers.ModelSerializer):
    """
    This is a serializer class for the User model.
    """

    class Meta:
        model = User
        fields = (
            "first_name",
            "last_name",
            "email",
        )


class TeamSerializer(serializers.ModelSerializer):
    """
    This is a serializer class for the Team model.
    """

    ports = serializers.SerializerMethodField("get_ports")
    manager = serializers.CharField(source="get_team_manager")

    def get_ports(self, team):
        ports_name = team.ports.order_by("name").values_list("name", flat=True)
        return ports_name

    class Meta:
        model = Team
        fields = ("name", "ports", "manager", "timezone", "historic_duration")
