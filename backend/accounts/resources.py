from import_export import fields as import_export_fields
from import_export import resources

from backend.accounts.models import User


class UserResource(resources.ModelResource):
    team_name = import_export_fields.Field(attribute="team", column_name="team_name")

    def before_save_instance(self, instance, using_transactions, dry_run):
        """
        Set unusable password for imported users.
        """
        instance.email = instance.email.strip()
        instance.set_unusable_password()
        instance.save()
        return super().before_save_instance(instance, using_transactions, dry_run)

    class Meta:
        model = User
        import_id_fields = ("team", "email")
        fields = (
            "email",
            "team",
            "team_name",
            "last_name",
            "first_name",
            "is_staff",
            "is_superuser",
            "last_login",
            "date_joined",
        )
