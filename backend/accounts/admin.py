from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model
from django.contrib.auth import models as auth_models
from django.contrib.auth.admin import GroupAdmin

from import_export.admin import ExportActionModelAdmin, ImportMixin

from backend.accounts.admin_forms import UserAdminCreationForm, UserAdminForm
from backend.accounts.models import AccessGroup, Team, UserForUnprivilegedAdmin
from backend.accounts.resources import UserResource

admin.site.unregister(auth_models.Group)

User = get_user_model()


class UserSetInLine(admin.TabularInline):
    model = AccessGroup.user_set.through
    raw_id_fields = ("user",)
    extra = 0


@admin.register(AccessGroup)
class AccessGroupAdmin(GroupAdmin):
    fieldsets = ((None, {"fields": ("name", "permissions")}),)
    inlines = [UserSetInLine]


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):
    BASE_ACCESS_FIELDS = [
        "is_active",
        "is_staff",
        "is_superuser",
    ]
    EXTRA_ACCESS_FIELDS = [
        "can_login_by_email",
        "access_person_list",
        "manual_upload_fal",
        "is_team_manager",
    ]
    form = UserAdminForm
    add_form = UserAdminCreationForm
    list_display = (
        ["email", "first_name", "last_name", "team"]
        + BASE_ACCESS_FIELDS
        + EXTRA_ACCESS_FIELDS
    )
    list_editable = EXTRA_ACCESS_FIELDS
    list_filter = ["team", "groups", "is_staff", "is_superuser"] + EXTRA_ACCESS_FIELDS
    fieldsets = (
        ("Utilisateur", {"fields": ("email", "first_name", "last_name")}),
        ("Équipe métier", {"fields": ("team",)}),
        (
            "Groupes d'accès",
            {"fields": ("groups",)},
        ),
        (
            "Accès de bases",
            {"fields": BASE_ACCESS_FIELDS},
        ),
        (
            "Autres permissions",
            {"fields": EXTRA_ACCESS_FIELDS},
        ),
        ("Dates", {"fields": ("last_login", "date_joined")}),
    )
    readonly_fields = ["last_login", "date_joined"]
    search_fields = ["first_name", "last_name", "email"]
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("email", "password1", "password2"),
            },
        ),
    )
    ordering = ["email"]
    resource_class = UserResource


@admin.register(UserForUnprivilegedAdmin)
class UserForUnprivilegedAdminAdmin(UserAdmin, ImportMixin, ExportActionModelAdmin):
    readonly_fields = (
        [
            "last_login",
            "date_joined",
            "groups",
        ]
        + UserAdmin.BASE_ACCESS_FIELDS
        + UserAdmin.EXTRA_ACCESS_FIELDS
    )
    list_editable = []


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    BASE_CONFIG_FIELDS = [
        "historic_duration",
        "download_fpr_lists",
        "download_roc_lists",
        "show_fpr_checkboxes",
        "show_roc_checkboxes",
        "show_school_trips_list_page",
        "access_school_trips_upload_page",
    ]
    list_display = ["id", "name", "notification_email"] + BASE_CONFIG_FIELDS
    list_display_links = ["id", "name"]
    list_editable = BASE_CONFIG_FIELDS
    list_filter = BASE_CONFIG_FIELDS
    fieldsets = (
        ("Équipe", {"fields": ("name",)}),
        ("Ports", {"fields": ("ports",)}),
        (
            "Gestion des listes",
            {
                "fields": (
                    "download_fpr_lists",
                    "show_fpr_checkboxes",
                    "download_roc_lists",
                    "show_roc_checkboxes",
                )
            },
        ),
        ("Fuseau horaire", {"fields": ("timezone",)}),
        (
            "Historique",
            {"fields": ("historic_duration",)},
        ),
        (
            "Notifications",
            {"fields": ("notification_email",)},
        ),
        (
            "Voyages scolaires pour le contrôle des listes",
            {"fields": ("show_school_trips_list_page",)},
        ),
        (
            "Voyages scolaires pour les compagnies maritimes",
            {"fields": ("access_school_trips_upload_page",)},
        ),
    )
    search_fields = ["name"]
    autocomplete_fields = ["ports"]
