from django.conf import settings
from django.shortcuts import reverse

import pytest

from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_it_only_returns_users_from_same_team(client):
    user = utils.login_saint_malo_user(client)
    other_team = factories.TeamFactory(name="other team")
    factories.UserFactory(email="user@in.team", team=user.team)
    factories.UserFactory(email="user@not-in.team", team=other_team)
    url = reverse("team-users")
    response = client.get(url)
    assert "user@in.team" in str(response.content)
    assert "user@not-in.team" not in str(response.content)


def test_it_does_not_return_inactive_users(client):
    user = utils.login_saint_malo_user(client)
    factories.UserFactory(email="active@user.fr", is_active=True, team=user.team)
    factories.UserFactory(email="inactive@user.fr", is_active=False, team=user.team)
    url = reverse("team-users")
    response = client.get(url)
    assert "active@user.fr" in str(response.content)
    assert "inactive@user.fr" not in str(response.content)


def test_api_returns_forbidden_if_no_team(client):
    user = factories.UserFactory()
    utils.login(client, login_user=user)
    url = reverse("team-users")
    response = client.get(url)
    assert "403" in str(response.status_code)
    url = reverse("team-settings")
    response = client.get(url)
    assert "403" in str(response.status_code)


def test_it_returns_all_settings(client):
    user = utils.login_saint_malo_user(client)
    team = user.team
    user_manager = factories.UserFactory(team=team, is_team_manager=True)
    user_manager.last_name = "Doe"
    user_manager.first_name = "Jane"
    user_manager.save()
    port_a = factories.PortFactory(locode="AAAAA", name="Port A")
    port_b = factories.PortFactory(locode="BBBBB", name="Port B")
    team.ports.set([port_a, port_b])
    team.name = "Equipe"
    team.timezone = "Europe/London"
    team.historic_duration = 31
    team.save()
    url = reverse("team-settings")
    response = client.get(url)
    assert "31" in str(response.content)
    assert "Europe/London" in str(response.content)
    assert "Doe Jane" in str(response.content)
    assert "Port A" in str(response.content)
    assert "Port B" in str(response.content)
    assert "Equipe" in str(response.content)
