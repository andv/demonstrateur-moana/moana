from django.conf import settings
from django.shortcuts import reverse

import pytest

from backend.unit_tests import factories, utils

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_api_returns_true_if_user_can_login_by_email(client):
    user = factories.UserFactory()
    user.can_login_by_email = True
    user.save()
    utils.login(client, login_user=user)
    url = reverse("accounts-auth-email-allowed")
    response = client.post(
        url,
        data={
            "email": user.email,
        },
    )
    assert "200" in str(response.status_code)
    assert "true" in str(response.content)


def test_api_returns_false_if_user_cant_login_by_email(client):
    user = factories.UserFactory()
    user.can_login_by_email = False
    user.save()
    utils.login(client, login_user=user)
    url = reverse("accounts-auth-email-allowed")
    response = client.post(
        url,
        data={
            "email": user.email,
        },
    )
    assert "200" in str(response.status_code)
    assert "false" in str(response.content)


def test_api_returns_forbidden_if_anonymous_user(client):
    url = reverse("accounts-auth-email-allowed")
    response = client.post(
        url,
        data={
            "email": "anonymous@user.fr",
        },
    )
    assert "403" in str(response.status_code)
