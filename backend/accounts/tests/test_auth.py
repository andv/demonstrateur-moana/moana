from django.conf import settings
from django.shortcuts import reverse

import pytest

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_login_page_loads(client, settings):
    settings.ALLOW_USER_PASSWORD_AUTH = True
    url = reverse("login")
    response = client.get(url)
    assert response.status_code == 200
    assert "connexion" in str(response.content).casefold()
