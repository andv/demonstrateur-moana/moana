from django.conf import settings

import pytest

from backend.unit_tests import factories

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_user_email_is_case_insensitive(client, settings):
    factories.UserFactory(email="test@test.com")
    with pytest.raises(Exception) as e:
        factories.UserFactory(email="TEST@test.com")


def test_team_is_related_to_paris_timezone_by_default():
    user = factories.UserFactory()
    team = factories.TeamFactory()
    user.team = team
    user.save()
    assert user.team.timezone == "Europe/Paris"
