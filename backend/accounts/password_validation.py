from django.core.exceptions import ValidationError


class NoReusePasswordValidator:

    def validate(self, password, user=None):
        if not user:
            return None
        if user.check_password(password):
            raise ValidationError(
                "Ce mot de passe a déjà été utilisé.",
                code="reused_password",
            )

    def get_help_text(self):
        return "Votre mot de passe ne peut avoir été déjà utilisé."
