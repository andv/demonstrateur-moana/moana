from django.contrib.auth import get_user_model
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.dispatch import receiver

from backend.logs.actions import ActionLogHandler

User = get_user_model()


log_handler = ActionLogHandler()


@receiver(user_logged_in, sender=User)
def add_action_log_for_login(sender, user, request, **kwargs):
    backend = request.session.get("_auth_user_backend")
    description = ""
    if backend == "django.contrib.auth.backends.ModelBackend":
        login_method = "email"
    elif backend == "djangosaml2.backends.Saml2Backend":
        login_method = "sso"
    else:
        login_method = "unrecognized method"
        description = backend
    log_handler.create_user_action_log(
        action=f"logged in {login_method}", description=description, user=user
    )


@receiver(user_logged_out, sender=User)
def add_action_log_for_logout(sender, user, request, **kwargs):
    log_handler.create_user_action_log(action="logged out", user=user)
