from django.contrib.auth.models import AbstractUser, Group
from django.db import models

from backend.accounts import settings as accounts_settings
from backend.accounts.choices import TIMEZONES_CHOICES
from backend.accounts.managers import UserManager


class Team(models.Model):
    name = models.CharField("nom", max_length=255, unique=True)
    ports = models.ManyToManyField(
        "ports.Port",
        verbose_name="ports",
        related_name="user_groups",
        blank=True,
    )
    timezone = models.CharField(
        verbose_name="Fuseau horaire",
        max_length=255,
        choices=TIMEZONES_CHOICES,
        default="Europe/Paris",
        help_text="Sélectionner un fuseau horaire spécifique à appliquer sur l'interface front.",
    )
    download_fpr_lists = models.BooleanField(
        "Télécharger les listes FPR",
        default=True,
        help_text="Cocher pour afficher le téléchargement des listes au format FPR aux membres de l'équipe",
    )
    download_roc_lists = models.BooleanField(
        "Télécharger les listes ROC",
        default=False,
        help_text="Cocher pour afficher le téléchargement des listes au format ROC aux membres de l'équipe",
    )
    show_fpr_checkboxes = models.BooleanField(
        "Traitement FPR",
        default=True,
        help_text="Cocher pour afficher le suivi du traitement des listes FPR aux membres de l'équipe",
    )
    show_roc_checkboxes = models.BooleanField(
        "Traitement ROC",
        default=False,
        help_text="Cocher pour afficher le suivi du traitement des listes ROC aux membres de l'équipe",
    )
    historic_duration = models.IntegerField(
        "Nombre de jours",
        default=accounts_settings.HISTORIC_DURATION_DEFAULT_DAYS,
        help_text="La durée maximale est de 60 jours, cela vient de la configuration de purge de l'application.",
    )
    show_school_trips_list_page = models.BooleanField(
        "Afficher la page suivi des bus scolaires.",
        default=False,
        help_text="Cocher pour afficher le suivi des bus scolaires aux membres de l'équipe",
    )
    access_school_trips_upload_page = models.BooleanField(
        "Peut accéder à la page de téléversement pour les voyages scolaires",
        default=False,
        help_text="Cocher pour autoriser l'accès à la page de téléversement pour les voyages scolaires.",
    )
    notification_email = models.EmailField(
        "email",
        blank=True,
        help_text="Adresse mail de l'équipe - laisser vide pour désactiver les notifications",
    )

    class Meta:
        verbose_name = "équipe métier"
        verbose_name_plural = "équipes métiers"

    def __str__(self):
        return self.name

    def get_team_manager(self):
        team_manager = self.users.filter(is_team_manager=True).first()
        if not team_manager:
            return ""
        first_name = team_manager.first_name
        last_name = team_manager.last_name
        return f"{last_name} {first_name}"


class AccessGroup(Group):
    class Meta:
        verbose_name = "groupe d'accès"
        verbose_name_plural = "groupes d'accès"


class User(AbstractUser):
    username = None
    email = models.EmailField("email", unique=True)
    team = models.ForeignKey(
        "accounts.Team",
        verbose_name="team",
        blank=True,
        null=True,
        help_text=("L'équipe à laquelle cette personne est rattachée."),
        related_name="users",
        on_delete=models.SET_NULL,
    )
    can_login_by_email = models.BooleanField(
        "Peut se connecter par e-mail",
        default=False,
        help_text="Cocher pour autoriser la connexion par un lien envoyé par email",
    )
    access_person_list = models.BooleanField(
        "Peut accéder aux fichiers des listes de personnes",
        default=True,
        help_text="Précise si l'utilisateur peut accéder aux fichiers des listes de personnes - FAL ou voyages scolaires.",
    )
    manual_upload_fal = models.BooleanField(
        "Peut téléverser des fichiers FAL",
        default=False,
        help_text="Précise si l'utilisateur a accès aux téléversements de fichiers FAL.",
    )
    is_team_manager = models.BooleanField(
        "Est gestionnaire d'équipe",
        default=False,
        help_text=(
            "Pour les utilisateurs référents qui peuvent modifier les configurations de leur équipe, par exemple les alertes."
        ),
    )

    objects = UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = "utilisateur - gestion des accès"
        verbose_name_plural = "utilisateurs - gestion des accès"

    def __str__(self):
        return f"{self.email}"

    def save(self, *args, **kwargs):
        self.email = self.email.lower()
        return super().save(*args, **kwargs)


class UserForUnprivilegedAdmin(User):
    """
    A user model that can be managed by unprivileged admin.
    """

    class Meta:
        proxy = True
        verbose_name = "utilisateur"
        verbose_name_plural = "utilisateurs"
