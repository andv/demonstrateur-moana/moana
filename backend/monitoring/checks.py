from django.conf import settings
from django.db import connections
from django.utils import timezone

from watchman import checks as watchman_checks
from watchman.decorators import check

from backend.logs.models import UserActionLog
from backend.monitoring import settings as monitoring_settings

#################
# FAL Import    #
#################


def fal_import():
    return {"fal_import": check_fal_import()}


@check
def check_fal_import():
    now = timezone.now()
    business_hours_start = now.replace(hour=7, minute=0, second=0)
    business_hours_end = now.replace(hour=20, minute=0, second=0)
    if business_hours_start <= now < business_hours_end:
        time_threshold = timezone.timedelta(minutes=15)
    else:
        time_threshold = timezone.timedelta(hours=1)
    recent_fal_imports = UserActionLog.objects.filter(
        action__in=monitoring_settings.ACTIONS_FAL_IMPORT,
        created__gte=now - time_threshold,
    )
    if not recent_fal_imports.exists():
        message = "No FAL import logs found in the last 15 minutes - or 1 hour if outside business hours."
        response = {"ok": False, "extra_info": message}
        raise Exception(message)
    else:
        response = {"ok": True}
    return response


#################
# Databases     #
#################


def app_database():
    result = watchman_checks._check_database("default")
    result["usage"] = "app database"
    return result


def stats_database():
    result = watchman_checks._check_database("stats")
    result["usage"] = "stats database"
    return result
