from django.core.management.base import BaseCommand

from backend.initialization.data_import import CSVImportMixin
from backend.ports.resources import PortResource


class Command(CSVImportMixin, BaseCommand):
    help = "Import ports from CSV file"
    resource_class = PortResource
