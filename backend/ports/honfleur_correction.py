from django.db import models

from backend.logs import actions as logs_actions
from backend.logs import settings as logs_settings

ROUEN_LOCODE = "FRURO"
HONFLEUR_LOCODE = "FRHON"
HONFLEUR_POSITIONS = ("QSH1", "QSH2", "QSH3")


class HonfleurPortCorrectionMixin(logs_actions.DataQualityLogLogHandler, models.Model):
    """
    Honfleur have a specific treatment, because in data will not have
    the proper port of call. The port of call comes as Rouen where is
    should be Honfleur. We can tell that it's supposed to be
    Honfleur's port by looking at the position on port.
    """

    class Meta:
        abstract = True

    def get_existing_port_of_call(self):
        """
        Retrieve the port of call from the existing instance.
        """
        ship = self.__class__.objects.filter(pk=self.pk).first()
        port = ship.port_of_call if ship else ""
        return port.upper()

    def switch_rouen_to_honfleur_port(self):
        new_port = self.port_of_call
        position = self.position_in_port_of_call.upper()
        port_should_be_honfleur = (
            new_port == ROUEN_LOCODE and position in HONFLEUR_POSITIONS
        )
        if not port_should_be_honfleur:
            return  # There is nothing to change.
        self.port_of_call = HONFLEUR_LOCODE
        existing_port = self.get_existing_port_of_call()
        existing_port_is_already_honfleur = existing_port == HONFLEUR_LOCODE
        if existing_port_is_already_honfleur:
            return
        # We only log the switch when the port in db has changed.
        ship_data = self.__dict__.copy()
        self.create_data_quality_log(
            action=logs_settings.LOG_NAME_SWITCH_ROUEN_TO_HONFLEUR,
            actor="moana-application",
            description=f"Port of call switched to {HONFLEUR_LOCODE} with position {position}",
            target=self.port_of_call,
            data=position,
            ship=ship_data,
            object=f"{self.ship_call_id}",
        )

    def save(self, *args, **kwargs):
        self.switch_rouen_to_honfleur_port()
        super().save(*args, **kwargs)
