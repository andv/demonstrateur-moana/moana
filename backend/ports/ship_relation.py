from django.db import models

from backend.ports.models import Port
from backend.utils.countries import get_country_name_from_code


class PortOnShip(models.Model):
    """
    This abstract class is expected to be sub-classed on a ShipMovement-like model.
    It provides port fields and helpers.
    """

    port_of_call = models.CharField("port of call", max_length=256, blank=True)
    last_port = models.CharField("last port", max_length=256, blank=True)
    next_port = models.CharField("next port", max_length=256, blank=True)
    relevant_port = models.CharField(
        "relevant port",
        max_length=256,
        blank=True,
        help_text="Correspond au dernier port pour un mouvement en arrivée, ou au prochain port pour un mouvement au départ",
    )

    class Meta:
        abstract = True

    @property
    def country_name(self):
        return get_country_name_from_code(self.flag_state_of_ship)

    def get_port_display_name(self, locode, port):
        """
        Return a display name with the country.
        """
        if not locode:
            return ""
        port_name = locode
        if port:
            port_name = port.name
        country = get_country_name_from_code(locode[:2])
        display_name = f"{port_name} ({country})"
        return display_name

    @property
    def next_port_name(self):
        return self.get_port_display_name(self.next_port, self.next_port_relation)

    @property
    def port_of_call_name(self):
        return self.get_port_display_name(self.port_of_call, self.port_of_call_relation)

    @property
    def last_port_name(self):
        return self.get_port_display_name(self.last_port, self.last_port_relation)

    @property
    def relevant_port_name(self):
        return self.get_port_display_name(
            self.relevant_port, self.relevant_port_relation
        )

    def set_relevant_port(self):
        if self.is_arriving:
            self.relevant_port = self.last_port
        elif self.is_departing:
            self.relevant_port = self.next_port

    def save(self, *args, **kwargs):
        self.set_relevant_port()
        return super().save(*args, **kwargs)


class PortRelationOnShip(models.Model):
    """
    This abstract class is expected to be sub-classed on a ShipMovement-like model.
    It provides port relation fields and helpers.
    """

    port_of_call_relation = models.ForeignKey(
        to="ports.Port",
        verbose_name="port of call relation",
        related_name="ship_movements",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    last_port_relation = models.ForeignKey(
        to="ports.Port",
        verbose_name="last port relation",
        related_name="last_port_ignore_+",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    next_port_relation = models.ForeignKey(
        to="ports.Port",
        verbose_name="next port relation",
        related_name="next_port_ignore_+",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    relevant_port_relation = models.ForeignKey(
        to="ports.Port",
        verbose_name="relevant port relation",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )

    class Meta:
        abstract = True

    def link_ship_to_ports(self):
        port_of_call = Port.objects.filter(locode=self.port_of_call).first()
        if port_of_call:
            self.port_of_call_relation = port_of_call
        last_port = Port.objects.filter(locode=self.last_port).first()
        if last_port:
            self.last_port_relation = last_port
        next_port = Port.objects.filter(locode=self.next_port).first()
        if next_port:
            self.next_port_relation = next_port
        relevant_port = Port.objects.filter(locode=self.relevant_port).first()
        if relevant_port:
            self.relevant_port_relation = relevant_port
        return self

    def save(self, *args, **kwargs):
        self.link_ship_to_ports()
        return super().save(*args, **kwargs)
