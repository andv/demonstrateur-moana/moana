from django.contrib import admin

from import_export.admin import ImportMixin

from backend.ports.models import Port
from backend.ports.resources import PortResource


@admin.register(Port)
class PortAdmin(ImportMixin, admin.ModelAdmin):
    list_display = [
        "locode",
        "name",
        "created",
        "modified",
    ]
    search_fields = ("locode", "name")
    date_hierarchy = "created"
    resource_class = PortResource
