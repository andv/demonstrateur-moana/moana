from django.conf import settings

import pytest

from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import factories

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_the_relevant_port_for_arriving_ship_is_next_port(client):
    saint_malo = factories.PortFactory(locode="FRSML")
    arriving = ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_call_id="12345",
        way=ships_settings.ARRIVING,
        last_port="LAST",
        next_port="NEXT",
    )
    arriving = ShipMovement.objects.get(
        ship_call_id="12345", way=ships_settings.ARRIVING
    )
    assert arriving.relevant_port == "LAST"


def test_the_relevant_port_for_departing_ship_is_next_port(client):
    saint_malo = factories.PortFactory(locode="FRSML")
    departing = ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_call_id="12345",
        way=ships_settings.DEPARTING,
        last_port="LAST",
        next_port="NEXT",
    )
    departing = ShipMovement.objects.get(
        ship_call_id="12345", way=ships_settings.DEPARTING
    )
    assert departing.relevant_port == "NEXT"


def test_the_relevant_port_relation_for_arriving_ship_is_last_port_relation(client):
    saint_malo = factories.PortFactory(locode="FRSML")
    last_port = factories.PortFactory(locode="LAST")
    next_port = factories.PortFactory(locode="NEXT")
    arriving = ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_call_id="12345",
        way=ships_settings.ARRIVING,
        last_port="LAST",
        next_port="NEXT",
    )
    arriving = ShipMovement.objects.get(
        ship_call_id="12345", way=ships_settings.ARRIVING
    )
    assert arriving.relevant_port_relation == last_port


def test_the_relevant_port_relation_for_departing_ship_is_next_port_relation(client):
    saint_malo = factories.PortFactory(locode="FRSML")
    last_port = factories.PortFactory(locode="LAST")
    next_port = factories.PortFactory(locode="NEXT")
    departing = ShipMovement.objects.create(
        port_of_call_relation=saint_malo,
        ship_call_id="12345",
        way=ships_settings.DEPARTING,
        last_port="LAST",
        next_port="NEXT",
    )
    departing = ShipMovement.objects.get(
        ship_call_id="12345", way=ships_settings.DEPARTING
    )
    assert departing.relevant_port_relation == next_port
