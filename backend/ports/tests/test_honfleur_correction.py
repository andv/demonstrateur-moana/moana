from django.conf import settings

import pytest

from backend import logs
from backend.logs import settings as logs_settings
from backend.logs.models.data_quality import DataQualityLog
from backend.ports.honfleur_correction import (
    HONFLEUR_LOCODE,
    HONFLEUR_POSITIONS,
    ROUEN_LOCODE,
)
from backend.ships import settings as ships_settings
from backend.ships.models import ShipMovement
from backend.unit_tests import factories

pytestmark = pytest.mark.django_db(databases=settings.DATABASES.keys())


def test_rouen_port_is_switched_to_honfleur_for_arriving_ship(client):
    honfleur_position = "QSH1"
    assert "QSH1" in HONFLEUR_POSITIONS
    ship = ShipMovement.objects.create(
        port_of_call=ROUEN_LOCODE,
        ship_call_id="12345",
        way=ships_settings.ARRIVING,
        position_in_port_of_call=honfleur_position,
    )
    ship.refresh_from_db()
    assert ship.port_of_call == HONFLEUR_LOCODE


def test_rouen_port_is_switched_to_honfleur_for_departing_ship(client):
    honfleur_position = "QSH2"
    assert honfleur_position in HONFLEUR_POSITIONS
    ship = ShipMovement.objects.create(
        port_of_call=ROUEN_LOCODE,
        ship_call_id="12345",
        way=ships_settings.DEPARTING,
        position_in_port_of_call=honfleur_position,
    )
    ship.refresh_from_db()
    assert ship.port_of_call == HONFLEUR_LOCODE


def test_rouen_port_is_not_switched_for_other_positions(client):
    ship = ShipMovement.objects.create(
        port_of_call=ROUEN_LOCODE,
        ship_call_id="12345",
        way=ships_settings.ARRIVING,
        position_in_port_of_call="OTHER",
    )
    ship.refresh_from_db()
    assert ship.port_of_call == ROUEN_LOCODE


def test_log_entry_created_when_port_switched_to_honfleur(client):
    honfleur_position = "QSH1"
    assert honfleur_position in HONFLEUR_POSITIONS
    log_name = logs_settings.LOG_NAME_SWITCH_ROUEN_TO_HONFLEUR
    count_before = DataQualityLog.objects.filter(action=log_name).count()
    ship = ShipMovement.objects.create(
        port_of_call=ROUEN_LOCODE,
        ship_call_id="12345",
        way=ships_settings.ARRIVING,
        position_in_port_of_call=honfleur_position,
    )
    ship.refresh_from_db()
    count_after = DataQualityLog.objects.filter(action=log_name).count()
    assert count_after == count_before + 1


def test_log_entry_not_created_when_port_is_already_honfleur(client):
    # 1. To start, the port is Honfleur
    ship = ShipMovement.objects.create(
        port_of_call=HONFLEUR_LOCODE,
        ship_call_id="12345",
        way=ships_settings.ARRIVING,
        position_in_port_of_call="Ignore",
    )
    honfleur_position = "QSH1"
    assert honfleur_position in HONFLEUR_POSITIONS
    log_name = logs_settings.LOG_NAME_SWITCH_ROUEN_TO_HONFLEUR
    count_before = DataQualityLog.objects.filter(action=log_name).count()
    # 2. Then comes a movement for Rouen with Honfleur's position.
    ship.refresh_from_db()
    ship.port_of_call = ROUEN_LOCODE
    ship.position_in_port_of_call = honfleur_position
    ship.save()
    # 3. Since the existing movement is already set to Honfleur,
    # we should not switch it again and the there should not be
    # a log entry.But, the movement should remain set to Honfleur.
    ship.refresh_from_db()
    count_after = DataQualityLog.objects.filter(action=log_name).count()
    assert count_after == count_before
    count_after = DataQualityLog.objects.filter(action=log_name).count()
    assert ship.port_of_call == HONFLEUR_LOCODE
