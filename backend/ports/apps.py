from django.apps import AppConfig


class PortsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.ports"
    verbose_name = "4.2 Référentiels - Ports"
