class UserPortsViewMixin:
    """
    Helper class for handling user's associated ports.
    This is meant to be used in a Django view.
    """

    @property
    def assigned_ports(self):
        """
        Get the ports assigned to the user.
        """
        user = self.request.user
        if not hasattr(user, "team") or not user.team:
            return []
        assigned_ports = user.team.ports.all()
        return assigned_ports
