from rest_framework import serializers

from backend.ports.models import Port


class PortSerializer(serializers.ModelSerializer):

    swing_url = serializers.CharField(source="sip_swing.url_dashboard")
    vigiesip_url = serializers.CharField(source="sip_vigiesip.url_dashboard")

    class Meta:
        model = Port
        fields = ("locode", "name", "swing_url", "vigiesip_url")
