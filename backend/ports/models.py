from django.db import models

from model_utils.models import TimeStampedModel


class Port(TimeStampedModel):
    locode = models.CharField("Locode", max_length=256, primary_key=True)
    name = models.CharField("nom", max_length=256, blank=True)

    class Meta:
        verbose_name = "port"
        verbose_name_plural = "ports"

    def __str__(self):
        return f"{self.name} ({self.locode})"
