from import_export import fields as import_export_fields
from import_export import resources

from backend.ports.models import Port


class PortResource(resources.ModelResource):
    locode = import_export_fields.Field(attribute="locode", column_name="Locode")
    name = import_export_fields.Field(attribute="name", column_name="Port")

    class Meta:
        model = Port
        import_id_fields = ("locode",)
        fields = ("locode", "name")
