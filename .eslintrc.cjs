module.exports =  {
  root: true,
  env: {
    node: true,
    es2022: true,
  },
  extends: [
    'plugin:vue/essential',
    'plugin:vue/vue3-essential',
    'eslint:recommended',
    'plugin:cypress/recommended',
    'prettier', // needs to be last
  ],
  rules: {},
}
