describe('Footer menu', () => {
  beforeEach(() => {
    cy.login('user-team-manager.json')
  })
  it('can go to "Accessibilité" page', () => {
    cy.getAccessibilityPage()
    cy.visit('/pages/accessibilite')
    cy.get('[data-cy="accessibility-link"]').as('accessibilityLink')
    cy.get('@accessibilityLink').scrollIntoView()
    cy.get('@accessibilityLink').should('exist')
    cy.get('@accessibilityLink').should('be.visible')
    cy.get('@accessibilityLink').should(
      'contain',
      'Accessibilité : non conforme',
    )
    cy.get('@accessibilityLink').should(
      'have.attr',
      'href',
      '/pages/accessibilite',
    )
    cy.get('@accessibilityLink').click()
    cy.location('pathname').should('have.string', '/pages/accessibilite')
    cy.contains('Veuillez-nous excuser pour la gêne occasionnée.').should(
      'not.exist',
    )
    cy.contains("La page d'accessibilite").should('exist')
  })

  it('can go to "Mentions légales" page', () => {
    cy.getLegalMentionsPage()
    cy.visit('/pages/mentions-legales')
    cy.get('[data-cy="legal-mentions-link"]').as('legalMentionsLink')
    cy.get('@legalMentionsLink').scrollIntoView()
    cy.get('@legalMentionsLink').should('exist')
    cy.get('@legalMentionsLink').should('be.visible')
    cy.get('@legalMentionsLink').should('contain', 'Mentions légales')
    cy.get('@legalMentionsLink').should(
      'have.attr',
      'href',
      '/pages/mentions-legales',
    )
    cy.get('@legalMentionsLink').click()
    cy.location('pathname').should('have.string', '/pages/mentions-legales')
    cy.contains('Veuillez-nous excuser pour la gêne occasionnée.').should(
      'not.exist',
    )
    cy.contains('La page des mentions légales').should('exist')
  })

  it('can go to "Conditions générales d\'utilisation" page', () => {
    cy.getUsageConditionsPage()
    cy.visit('pages/conditions-generales-utilisation')
    cy.get('[data-cy="general-conditions-link"]').as('generalConditionsLink')
    cy.get('@generalConditionsLink').scrollIntoView()
    cy.get('@generalConditionsLink').should('exist')
    cy.get('@generalConditionsLink').should('be.visible')
    cy.get('@generalConditionsLink').should(
      'contain',
      "Conditions Générales d'Utilisation",
    )
    cy.get('@generalConditionsLink').should(
      'have.attr',
      'href',
      '/pages/conditions-generales-utilisation',
    )
    cy.get('@generalConditionsLink').click()
    cy.location('pathname').should(
      'have.string',
      '/pages/conditions-generales-utilisation',
    )
    cy.contains('Veuillez-nous excuser pour la gêne occasionnée.').should(
      'not.exist',
    )
    cy.contains("La page des conditions d'utilisation").should('exist')
  })

  it('can go to "App versions" page', () => {
    cy.getVersionPage()
    cy.visit('/pages/version')
    cy.get('[data-cy="versions-link"]').as('appVersionsLink')
    cy.get('@appVersionsLink').scrollIntoView()
    cy.get('@appVersionsLink').should('exist')
    cy.get('@appVersionsLink').should('be.visible')
    cy.get('@appVersionsLink').should('contain', 'Version')
    cy.get('@appVersionsLink').should('have.attr', 'href', '/pages/version')
    cy.get('@appVersionsLink').click()
    cy.location('pathname').should('have.string', '/pages/version')
    cy.contains('Veuillez-nous excuser pour la gêne occasionnée.').should(
      'not.exist',
    )
    cy.contains("La page de l'historique des versions").should('exist')
  })
})
