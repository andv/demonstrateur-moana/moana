describe('Header top menu', () => {
  beforeEach(() => {
    cy.login()
    cy.getShips()
    cy.visit('/')
  })

  it('can go to "Mon équipe"', function () {
    cy.get('[data-cy="navigation-team"]').as('myTeamLink')
    cy.get('@myTeamLink')
      .should('exist')
      .should('be.visible')
      .should('contain', 'Mon équipe')
      .should('have.attr', 'href', '/mon-equipe')
    cy.getTeamSettings()
    cy.getTeamUsers()
    cy.get('@myTeamLink').click()
    cy.location('pathname').should('have.string', '/mon-equipe')
  })

  it('can go to "Centre d\'aide"', function () {
    cy.get('[data-cy="navigation-help-center"]').as('helpCenterLink')
    cy.get('@helpCenterLink')
      .should('exist')
      .should('be.visible')
      .should('contain', "Centre d'aide")
      .should('have.attr', 'href', '/centre-aide')
    cy.getFAQ()
    cy.catchTrackingException()
    cy.get('@helpCenterLink').click()
    cy.location('pathname').should('have.string', '/centre-aide')
  })

  it('can go to "Gestion des alertes"', function () {
    cy.getTeamManagerName()
    cy.getWatchlist()
    cy.get('[data-cy="navigation-settings-alerts"]').as('settingsAlertsLink')
    cy.get('@settingsAlertsLink')
      .should('exist')
      .should('be.visible')
      .should('contain', 'Gestion des alertes')
      .should('have.attr', 'href', '/gestion-des-alertes')
    cy.get('@settingsAlertsLink').click()
    cy.location('pathname').should('have.string', '/gestion-des-alertes')
  })
})

describe('Header tabs menu', () => {
  beforeEach(() => {
    cy.login()
    cy.getShips()
    cy.visit('/')
  })

  it('displays default links', function () {
    cy.get('[data-cy="navigation-ships"]').as('shipsLink')
    cy.get('[data-cy="navigation-stats"]').as('statsLink')

    cy.get('@shipsLink')
      .should('exist')
      .should('be.visible')
      .should('contain', 'Suivi des navires')
      .should('have.attr', 'href', '/')

    cy.get('@statsLink')
      .should('exist')
      .should('be.visible')
      .should('contain', 'Statistiques')
      .should('have.attr', 'href', '/statistiques')
  })

  it('does not display school trip links if user is not allowed', function () {
    cy.get('[data-cy="navigation-school-trips"]').should('not.exist')
  })
})

describe('Header tabs menu for school trips users', () => {
  beforeEach(() => {
    cy.login('user-school-trips.json')
    cy.getShips()
    cy.visit('/')
  })

  it('displays school trip links', function () {
    cy.get('[data-cy="navigation-school-trips"]').as('schoolTripsLink')
    cy.get('@schoolTripsLink')
      .should('exist')
      .should('be.visible')
      .should('contain', 'Suivi des bus scolaires')
      .should('have.attr', 'href', '/suivi-des-bus-scolaires')
  })
})
