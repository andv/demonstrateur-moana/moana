describe('Email notification on for team manager user', () => {
  beforeEach(() => {
    cy.login('user-team-manager.json')
    cy.getTeamManagerName()
    cy.getWatchlist()
    cy.visit('/gestion-des-alertes')
    cy.get('[data-cy="alerts-email-toggle"]').as('emailToggle')
  })

  it('displays email and checked toggle', function () {
    cy.get('@emailToggle').should('exist').should('be.visible')
    cy.get('@emailToggle').find('input').should('be.checked')
    cy.get('@emailToggle').should('contain', 'team-with-notification@moana.fr')
  })

  it('can desactivate email notification', function () {
    cy.changeNotificationEmail('')
    cy.get('@emailToggle').click('topRight')
    cy.get('@emailToggle').should('exist').should('be.visible')
    cy.get('@emailToggle').find('input').should('not.be.checked')
    cy.get('@emailToggle').should(
      'not.contain',
      'team-with-notification@moana.fr',
    )
    cy.get('@emailToggle').should('contain', 'Aucune adresse mail enregistrée')
  })
})

describe('Email notification off for team manager user', () => {
  beforeEach(() => {
    cy.login('user-team-manager-no-email-notification.json')
    cy.getTeamManagerName()
    cy.getWatchlist()
    cy.visit('/gestion-des-alertes')
    cy.get('[data-cy="alerts-email-toggle"]').as('emailToggle')
  })

  it('displays toggle uncheck and no email', function () {
    cy.get('@emailToggle').should('exist').should('be.visible')
    cy.get('@emailToggle').find('input').should('not.be.checked')
    cy.get('@emailToggle').should(
      'not.contain',
      'team-with-notification@moana.fr',
    )
  })

  it('can activate email notification', function () {
    const newEmail = 'new@email.fr'
    cy.changeNotificationEmail(newEmail)
    cy.get('@emailToggle').click('topRight')
    cy.get('[data-cy="alerts-modal-notifications-on"]').as('addEmailModal')
    cy.get('@addEmailModal').should('exist').should('be.visible')
    cy.get('@addEmailModal').find('input').type(newEmail)
    cy.get('@addEmailModal')
      .find('[data-cy="alerts-modal-notifications-submit"]')
      .should('contain', 'Valider l’activation')
      .click()
    cy.get('@addEmailModal').should('not.exist')
    cy.get('@emailToggle').find('input').should('be.checked')
    cy.get('@emailToggle').should('contain', newEmail)
  })

  it('can cancel email notification activate modal', function () {
    cy.get('@emailToggle').click('topRight')
    cy.get('[data-cy="alerts-modal-notifications-on"]').as('addEmailModal')
    cy.get('@addEmailModal')
      .find('[data-cy="alerts-modal-notifications-cancel"]')
      .should('contain', 'Annuler')
      .click()
    cy.get('@addEmailModal').should('not.exist')
    cy.get('@emailToggle').find('input').should('not.be.checked')
    cy.get('@emailToggle').click('topRight')
    cy.get('@addEmailModal')
      .find('button[title="Fermer la fenêtre modale"]')
      .click()
    cy.get('@addEmailModal').should('not.exist')
    cy.get('@emailToggle').find('input').should('not.be.checked')
  })

  it('displays error message if email is incorrect', function () {
    const errorMessage = 'Saisissez une adresse e-mail valide.'
    cy.changeNotificationEmail([errorMessage], 400)
    cy.get('@emailToggle').click('topRight')
    cy.get('[data-cy="alerts-modal-notifications-on"]').as('addEmailModal')
    cy.get('@addEmailModal').find('.fr-input-group').as('inputGroup')
    cy.get('@inputGroup').find('input').type('bad@email')
    cy.get('@addEmailModal').find('button:submit').click()
    cy.get('@inputGroup').should('be.visible')
    cy.get('@inputGroup').should('have.class', 'fr-input-group--error')
    cy.get('@inputGroup')
      .find('.fr-error-text')
      .should('exist')
      .should('be.visible')
      .should('contain', errorMessage)
  })
})

describe('Email notification for non-team manager user', () => {
  beforeEach(() => {
    cy.getTeamManagerName()
    cy.getWatchlist()
  })

  it('displays banner with team manager name', function () {
    cy.login()
    cy.visit('/gestion-des-alertes')
    cy.get('[data-cy="message-team-manager-only"]', { timeout: 5000 })
      .should('exist')
      .should('be.visible')
      .should('contain', 'Jane Doe')
  })

  it('does not display toggle', function () {
    cy.login('user-email-notification-off.json')
    cy.visit('/gestion-des-alertes')
    cy.get('[data-cy="alerts-email-toggle"]').should('not.exist')
    cy.get('[data-cy="alerts-email-read-only"]', { timeout: 5000 })
      .should('exist')
      .should('be.visible')
  })

  it('displays email if notifications are on', function () {
    cy.login('user-email-notification-on.json')
    cy.visit('/gestion-des-alertes')
    cy.get('[data-cy="alerts-email-read-only"]', { timeout: 5000 }).should(
      'contain',
      'email@notification.on',
    )
  })

  it('displays sentence if notifications are off', function () {
    cy.login('user-email-notification-off.json')
    cy.visit('/gestion-des-alertes')
    cy.get('[data-cy="alerts-email-read-only"]', { timeout: 5000 }).should(
      'contain',
      'Notifications désactivées',
    )
  })
})

describe('IMO list for all users', () => {
  beforeEach(() => {
    cy.login()
    cy.getTeamManagerName()
    cy.getWatchlist('watchlist.json')
    cy.visit('/gestion-des-alertes')
    cy.get('[data-cy="alert-watchlist"]').as('alertWatchlist')
    cy.get('[data-cy="alert-watchlist-table"]').as('watchlistTable')
    cy.get('[data-cy="alert-watchlist-pagination"]').as('watchlistPagination')
    cy.get('[data-cy="alert-watchlist-search-bar"]').as('searchBar')
  })

  it('displays total of imo watched', function () {
    cy.get('@alertWatchlist').should('exist').should('be.visible')
    cy.get('@alertWatchlist')
      .find('[data-cy="alert-watchlist-title"]')
      .should('contain', '4 alertes')
  })

  it('displays list with pagination', function () {
    cy.get('@watchlistTable').should('exist').should('be.visible')
    cy.get('@watchlistPagination').should('exist').should('be.visible')
    cy.get('@watchlistTable').find('tbody tr').should('have.length', 3) // 3 = VITE_IMO_WACHLIST_PAGE_SIZE
    cy.get('@watchlistTable').should('contain', '1234567')
    cy.get('@watchlistTable').should('contain', '2234567')
    cy.get('@watchlistTable').should('contain', '3234567')
    cy.get('@watchlistTable').should('not.contain', '4234567')
    cy.get('@watchlistPagination').find('a').last().click()
    cy.get('@watchlistTable').find('tbody tr').should('have.length', 1) // 1 = Total IMO - VITE_IMO_WACHLIST_PAGE_SIZE
    cy.get('@watchlistTable').should('contain', '4234567')
    cy.get('@watchlistTable').should('not.contain', '1234567')
    cy.get('@watchlistTable').should('not.contain', '2234567')
    cy.get('@watchlistTable').should('not.contain', '3234567')
  })

  it('can search imo by number', function () {
    cy.get('@searchBar').should('exist').should('be.visible')
    cy.get('@watchlistTable').should('contain', '1234567')
    cy.get('@watchlistTable').should('contain', '2234567')
    cy.get('@watchlistTable').should('contain', '3234567')
    cy.get('@searchBar').find('input').type('1234567')
    cy.get('@watchlistTable').should('contain', '1234567')
    cy.get('@watchlistTable').should('not.contain', '2234567')
    cy.get('@watchlistTable').should('not.contain', '3234567')
    cy.get('@watchlistTable').should('not.contain', '4234567')
    cy.get('@searchBar').find('input').clear()
    cy.get('@watchlistTable').should('contain', '1234567')
    cy.get('@watchlistTable').should('contain', '2234567')
    cy.get('@watchlistTable').should('contain', '3234567')
    cy.get('@searchBar').find('input').type('4234567')
    cy.get('@watchlistTable').should('contain', '4234567')
    cy.get('@watchlistTable').should('not.contain', '1234567')
    cy.get('@watchlistTable').should('not.contain', '2234567')
    cy.get('@watchlistTable').should('not.contain', '3234567')
  })

  it('displays message if search is empty', function () {
    const imoUnknown = '0000000'
    cy.get('[data-cy="alert-watchlist-empty"]').should('not.exist')
    cy.get('@searchBar').find('input').type(imoUnknown)
    cy.get('@watchlistTable').should('not.exist')
    cy.get('[data-cy="alert-watchlist-empty"]')
      .should('exist')
      .should('be.visible')
      .should(
        'contain',
        `Aucun IMO trouvé pour votre recherche : ${imoUnknown}`,
      )
    cy.get('@searchBar').find('input').clear()
    cy.get('@watchlistTable').should('exist').should('be.visible')
    cy.get('[data-cy="alert-watchlist-empty"]').should('not.exist')
  })
})

describe('No IMO watched', () => {
  beforeEach(() => {
    cy.login()
    cy.getTeamManagerName()
    cy.getWatchlist('watchlist-empty.json')
    cy.visit('/gestion-des-alertes')
  })

  it('displays total sentence in singular', function () {
    cy.get('[data-cy="alert-watchlist-title"]')
      .should('not.contain', '0 alerte')
      .should('contain', 'IMO')
  })

  it('displays message if team has no imo watched', function () {
    cy.get('[data-cy="alert-watchlist-table"]').should('not.exist')
    cy.get('[data-cy="alert-watchlist-empty"]')
      .should('exist')
      .should('be.visible')
      .should('contain', 'Aucun IMO mis en alerte')
  })
})

describe('IMO manager buttons for non-referent user', () => {
  beforeEach(() => {
    cy.login()
    cy.getTeamManagerName()
    cy.getWatchlist('watchlist.json')
    cy.visit('/gestion-des-alertes')
  })

  it('does not display buttons', function () {
    cy.get('[data-cy="alert-watchlist-add-imo-button"]').should('not.exist')
    cy.get('[data-cy="alert-watchlist-delete-imo-button"]').should('not.exist')
  })
})

describe('Team manager add IMO', () => {
  beforeEach(() => {
    cy.login('user-team-manager.json')
    cy.getWatchlist('watchlist-one-imo.json')
    cy.getTeamManagerName()
    cy.postAddImo()
    cy.visit('/gestion-des-alertes')
    cy.get('[data-cy="alert-watchlist-add-imo-button"]').as('addImoButton')
    cy.get('[data-cy="alert-watchlist-table"]').as('watchlistTable')
    cy.get('[data-cy="alert-watchlist-success"]').should('not.exist')
    cy.get('@addImoButton').click()
    cy.get('[data-cy="modal-watchlist-add"]')
      .should('exist')
      .should('be.visible')
      .as('modalAdd')
    cy.get('[data-cy="modal-watchlist-add-submit"]').as('submitModal')
    cy.get('@submitModal').should('have.attr', 'disabled')
    cy.get('[data-cy="modal-watchlist-add-input"]').as('inputModal')
  })

  it('can add one imo to the list', function () {
    const imoToAdd = '1231237'
    cy.get('@watchlistTable').should('not.contain', imoToAdd)
    cy.get('@inputModal').type(imoToAdd, { force: true })
    cy.get('@submitModal').should('not.have.attr', 'disabled')
    cy.get('@submitModal').click()
    cy.get('@modalAdd').should('not.exist')
    cy.get('[data-cy="alert-watchlist-success"]')
      .should('exist')
      .should('be.visible')
      .should('contain', "L'IMO a été ajouté avec succès")
  })

  it('can add multiple imo to the list', function () {
    const firstImo = 1231237
    const secondImo = 7654321
    const thirdImo = 9875425
    const imosToAdd = `${firstImo} ${secondImo} ${thirdImo}`
    cy.get('@watchlistTable').should('not.contain', firstImo)
    cy.get('@watchlistTable').should('not.contain', secondImo)
    cy.get('@watchlistTable').should('not.contain', thirdImo)
    cy.get('@inputModal').type(imosToAdd, { force: true })
    cy.get('@submitModal').should('not.have.attr', 'disabled')
    cy.get('@submitModal').click()
    cy.get('@modalAdd').should('not.exist')
    cy.get('[data-cy="alert-watchlist-success"]')
      .should('exist')
      .should('be.visible')
      .should('contain', 'Les IMO ont été ajoutés avec succès')
  })

  it('can cancel imo adding modal', function () {
    cy.get('[data-cy="modal-watchlist-add-cancel"]').click()
    cy.get('@modalAdd').should('not.exist')
    cy.get('@addImoButton').click()
    cy.get('[data-cy="modal-watchlist-add"]').as('modalAdd')
    cy.get('@modalAdd').find('button[title="Fermer la fenêtre modale"]').click()
    cy.get('@modalAdd').should('not.exist')
  })
})

describe('Team manager delete IMO', () => {
  beforeEach(() => {
    cy.login('user-team-manager.json')
    cy.getWatchlist('watchlist-one-imo.json')
    cy.getTeamManagerName()
    cy.deleteImo()
    cy.visit('/gestion-des-alertes')
    cy.get('[data-cy="alert-watchlist-success"]').should('not.exist')
    cy.get('[data-cy="alert-watchlist-delete-imo-button"]')
      .first()
      .as('deleteImoButton')
    cy.get('@deleteImoButton').click()
    cy.get('[data-cy="modal-watchlist-delete"]')
      .should('exist')
      .should('be.visible')
      .as('modalDelete')
    cy.get('[data-cy="alert-watchlist-table"] tbody td').as('imoRow')
  })

  it('can delete imo', function () {
    const imoNumberToDelete = '1234567'
    cy.get('@imoRow')
      .should('exist')
      .should('be.visible')
      .should('contain', imoNumberToDelete)
    cy.get('[data-cy="modal-watchlist-delete-submit"]').as('submitModal')
    cy.get('@submitModal').should('contain', 'Valider la suppression')
    cy.get('@modalDelete')
      .find('[data-cy="modal-watchlist-delete-sentence"]')
      .should('contain', imoNumberToDelete)
    cy.get('@submitModal').click()
    cy.get('[data-cy="alert-watchlist-success"]')
      .should('exist')
      .should('be.visible')
      .should(
        'contain',
        `L'IMO n°${imoNumberToDelete} a été supprimé avec succès`,
      )
    cy.get('@imoRow').should('not.exist')
  })

  it('can cancel imo deleting modal', function () {
    cy.get('[data-cy="modal-watchlist-delete-cancel"]').click()
    cy.get('@modalDelete').should('not.exist')
    cy.get('@deleteImoButton').click()
    cy.get('[data-cy="modal-watchlist-delete"]')
      .find('button[title="Fermer la fenêtre modale"]')
      .click()
    cy.get('@modalDelete').should('not.exist')
  })
})

describe('Countries list', () => {
  beforeEach(() => {
    cy.login()
    cy.getWatchlist()
    cy.getTeamManagerName()
    cy.visit('/gestion-des-alertes')
    cy.get('[data-cy="alert-countries-list-table"]').as('countriesTable')
  })

  it('displays country alpha code and french name', function () {
    cy.get('@countriesTable').should('exist').should('be.visible')
    cy.get('@countriesTable').find('tbody tr').should('have.length', 3)
    cy.get('@countriesTable').should('contain', 'France')
    cy.get('@countriesTable').should('contain', 'FR')
    cy.get('@countriesTable').should('contain', 'Espagne')
    cy.get('@countriesTable').should('contain', 'ES')
    cy.get('@countriesTable').should('contain', 'ZZ')
    cy.get('@countriesTable').should('contain', 'Pays inconnu')
  })

  it('displays total of countries watched', function () {
    cy.get('[data-cy="alert-countries-title"]').contains('Pavillon')
    cy.get('[data-cy="alert-countries-title"]').contains('3 alertes')
  })
})

describe('Countries read-only banner', () => {
  it('displays banner if user is team manager', function () {
    cy.login('user-team-manager.json')
    cy.getWatchlist()
    cy.getTeamManagerName()
    cy.visit('/gestion-des-alertes')
    cy.get('[data-cy="alert-countries-banner"]')
      .should('exist')
      .should('be.visible')
  })

  it('does not display banner if user is not a team manager', function () {
    cy.login()
    cy.getWatchlist()
    cy.getTeamManagerName()
    cy.visit('/gestion-des-alertes')
    cy.get('[data-cy="alert-countries-banner"]').should('not.exist')
  })
})
