describe('Ships with sensitive alerts banners', () => {
  beforeEach(() => {
    cy.login()
    cy.getShips('ships-with-alerts.json')
    cy.visit('/')
    cy.get('[data-cy="box-ship"]').as('ships')
  })

  it('displays watchlist alert banner', () => {
    cy.get('@ships').first().as('shipWithWatchlistAlert')
    cy.get('@shipWithWatchlistAlert').scrollIntoView()
    cy.get('@shipWithWatchlistAlert').contains('Ship with alert IMO')
    cy.get('@shipWithWatchlistAlert')
      .find('[data-cy="alert-sensitive-banner"]')
      .should('exist')
      .should('be.visible')
      .should('contain', 'IMO sensible')
      .should('contain', 'IMO 1234567')
      .should('have.length', 1)
      .find('svg')
      .should('exist')
  })

  it('displays ship country alert banner', () => {
    cy.get('@ships').eq(1).as('shipWitCountryAlert')
    cy.get('@shipWitCountryAlert').scrollIntoView()
    cy.get('@shipWitCountryAlert').contains('Ship with alert country')
    cy.get('@shipWitCountryAlert').scrollIntoView()
    cy.get('@shipWitCountryAlert')
      .find('[data-cy="alert-sensitive-banner"]')
      .should('exist')
      .should('be.visible')
      .should('contain', 'Pavillon sensible')
      .should('contain', 'France')
      .should('have.length', 1)
      .find('svg')
      .should('exist')
  })
})

describe('Ships with new list alert banner', () => {
  beforeEach(() => {
    cy.login()
    cy.getShips('ships-with-alerts.json')
    cy.visit('/')
    cy.get('[data-cy="box-ship"]').as('ships')
    cy.get('@ships').eq(2).as('shipWithNewPassengersList')
    cy.get('@ships').eq(3).as('shipWithNewCrewList')
    cy.get('@ships').eq(5).as('shipListsNotDownloadable')
  })

  it('displays new passengers list banner', function () {
    cy.get('@shipWithNewPassengersList').scrollIntoView()
    cy.get('@shipWithNewPassengersList').contains(
      'Ship with new passengers list',
    )
    cy.get('@shipWithNewPassengersList')
      .find('[data-cy="alert-new-list-banner"]')
      .should('exist')
      .should('be.visible')
      .should('contain', 'Liste passagers')
      .should('have.length', 1)
      .should('have.class', 'fr-notice--info')
  })

  it('displays new crew list banner', function () {
    cy.get('@shipWithNewCrewList').scrollIntoView()
    cy.get('@shipWithNewCrewList').contains('Ship with new crew list')
    cy.get('@shipWithNewCrewList')
      .find('[data-cy="alert-new-list-banner"]')
      .should('exist')
      .should('be.visible')
      .should('contain', 'Liste équipage')
      .should('have.length', 1)
      .should('have.class', 'fr-notice--info')
  })

  it('hides new passenger list banner when user click on "stop showing" button', function () {
    cy.postAlertsStopShowing()
    cy.get('@shipWithNewPassengersList').scrollIntoView()
    cy.get('@shipWithNewPassengersList').find('.fr-btn--close').click()
    cy.get('@shipWithNewPassengersList')
      .find('[data-cy="alert-new-list-banner"]')
      .should('not.exist')
  })

  it('hides new crew list banner if user click on "stop showing" button', function () {
    cy.postAlertsStopShowing()
    cy.get('@shipWithNewCrewList').scrollIntoView()
    cy.get('@shipWithNewCrewList').find('.fr-btn--close').click()
    cy.get('@shipWithNewCrewList')
      .find('[data-cy="alert-new-list-banner"]')
      .should('not.exist')
  })

  it('does not display new list banners if lists are not downloadable', function () {
    cy.get('@shipListsNotDownloadable').scrollIntoView()
    cy.get('@shipListsNotDownloadable').contains(
      'Ship with new list alerts but not downloadabled list',
    )
    cy.get('@shipListsNotDownloadable')
      .find('[data-cy="alert-new-list-banner"]')
      .should('not.exist')
  })
})

describe('Ships with multiple or without alerts', () => {
  beforeEach(() => {
    cy.login()
    cy.getShips('ships-with-alerts.json')
    cy.visit('/')
    cy.get('[data-cy="box-ship"]').as('ships')
  })

  it('displays all alerts banners', function () {
    cy.get('@ships').eq(4).as('shipMultipleAlerts')
    cy.get('@shipMultipleAlerts').scrollIntoView()
    cy.get('@shipMultipleAlerts').contains('Ship with all alerts')
    cy.get('@shipMultipleAlerts')
      .find('[data-cy="alert-sensitive-banner"]')
      .should('have.length', 2)
    cy.get('@shipMultipleAlerts')
      .find('[data-cy="alert-new-list-banner"]')
      .should('have.length', 2)
  })

  it('does not display banner if ship has none', function () {
    cy.get('@ships').last().as('shipWithoutAlert')
    cy.get('@shipWithoutAlert').scrollIntoView()
    cy.get('@shipWithoutAlert').contains('Ship without alert')
    cy.get('@shipWithoutAlert')
      .find('[data-cy="alert-sensitive-banner"]')
      .should('have.length', 0)
    cy.get('@shipWithoutAlert')
      .find('[data-cy="alert-new-list-banner"]')
      .should('have.length', 0)
  })
})
