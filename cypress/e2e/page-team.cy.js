describe('Team informations', () => {
  beforeEach(() => {
    cy.login()
    cy.getTeamSettings()
    cy.getTeamUsers()
    cy.visit('/mon-equipe')
  })

  it('displays settings', function () {
    cy.get('[data-cy="team-name"]').contains('MOANA')
    cy.get('[data-cy="team-ports"]').contains('Ajaccio')
    cy.get('[data-cy="team-ports"]').contains('St Malo')
    cy.get('[data-cy="team-timezone"]').contains('Europe/Paris')
    cy.get('[data-cy="team-manager"]').contains('Mon manager')
    cy.get('[data-cy="team-historic"]').contains('7')
  })

  it('displays users', function () {
    cy.get('[data-cy="team-users"]').contains('doe.joe@moana.fr')
    cy.get('[data-cy="team-users"]').contains('doe.jess@moana.fr')
    cy.get('[data-cy="team-users"]').contains('doe.jane@moana.fr')
    cy.get('[data-cy="team-users"] tbody tr').should('have.length', 3)
  })

  it('displays total of users', function () {
    cy.get('[data-cy="team-users-count"]').contains(3)
  })

  it('redirects to logout page', function () {
    cy.get('[data-cy="team-logout"]').as('teamLogout')
    cy.get('@teamLogout').scrollIntoView()
    cy.get('@teamLogout').click()
    cy.location('pathname').should('eq', '/logout')
  })
})

describe('Team manager actions', () => {
  beforeEach(() => {
    cy.login('user-team-manager.json')
    cy.getTeamSettings()
    cy.getTeamUsers()
    cy.catchTrackingException()
    cy.visit('/mon-equipe')
  })

  it('displays manage buttons if user is team manager', function () {
    cy.get('[data-cy="team-users-manage"]').should('be.visible')
  })

  it('goes to add user page and see iframe', function () {
    cy.get('[data-cy="team-add-user-link"]').click()
    cy.location('pathname').should('have.string', 'ajouter-des-utilisateurs')
    cy.get('[data-cy="team-iframe"]').should('exist')
  })

  it('goes to add delete page and see iframe', function () {
    cy.get('[data-cy="team-delete-user-link"]').click()
    cy.location('pathname').should('have.string', 'supprimer-des-utilisateurs')
    cy.get('[data-cy="team-iframe"]').should('exist')
  })
})

describe('Team restriction user that is not a manager', () => {
  beforeEach(() => {
    cy.login()
    cy.getTeamSettings()
    cy.getTeamUsers()
  })

  it('does not display manage buttons if user is not team manager', function () {
    cy.visit('/mon-equipe')
    cy.get('[data-cy="team-users-manage"]').should('not.exist')
  })

  it('does not display iframe on add user page if user is not a team manager', function () {
    cy.getTeamManagerName()
    cy.visit('/mon-equipe/ajouter-des-utilisateurs')
    cy.get('[data-cy="team-not-manager"]').should('exist')
    cy.get('[data-cy="team-iframe"]').should('not.exist')
  })

  it('cannot access delete users page', function () {
    cy.getTeamManagerName()
    cy.visit('/mon-equipe/supprimer-des-utilisateurs')
    cy.get('[data-cy="team-not-manager"]').should('exist')
    cy.get('[data-cy="team-iframe"]').should('not.exist')
  })
})
