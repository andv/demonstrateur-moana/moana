describe('Modal theme', () => {
  beforeEach(() => {
    cy.get('[data-cy="display-settings"]').as('displaySettingsLink')
    it('can go to "Paramètres d\'affichage" page', () => {
      cy.get('@displaySettingsLink').scrollIntoView()
      cy.get('@displaySettingsLink')
      cy.get('@displaySettingsLink').should('exist')
      cy.get('@displaySettingsLink').should('be.visible')
      cy.get('@displaySettingsLink').should(
        'contain',
        "Paramètres d'affichage ",
      )
      cy.get('@displaySettingsLink').click()
      cy.get('[data-cy="light-theme-display"]')
        .should('exist')
        .should('be.visible')
        .should('contain', 'Thème clair')

      cy.get('[data-cy="dark-theme-display"]')
        .should('exist')
        .should('be.visible')
        .should('contain', 'Thème sombre')

      cy.get('[data-cy="system-theme-display"]')
        .should('exist')
        .should('be.visible')
        .should('contain', 'Système')
        .first()
        .within(() => {
          cy.get('span').should(
            'contain',
            "S'adapte automatiquement aux paramètres de votre ordinateur",
          )
        })
      cy.get('button[title="Fermer la fenêtre modale"]').as('closeModaleButton')
      cy.get('@closeModaleButton').click()
      cy.contains('Thème clair').should('not.exist')
      cy.contains('Moana').should('be.visible')
      cy.contains('Aide au contrôle maritime').should('be.visible')
      // check modale choice
      cy.get('@displaySettingsLink').click()
      cy.get('[data-cy="dark-theme-display"]').find('label').click()
      cy.get('[data-cy="dark-theme-display"]')
        .find('input')
        .should('be.checked')
      cy.get('[data-cy="light-theme-display"]')
        .find('input')
        .should('not.be.checked')
      cy.get('[data-cy="system-theme-display"]')
        .find('input')
        .should('not.be.checked')
    })
  })
})
