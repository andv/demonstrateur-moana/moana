describe('Page ships', () => {
  beforeEach(() => {
    cy.login()
    cy.getShips()
    cy.visit('/')
  })

  it('displays all ships from api response', function () {
    cy.get('[data-cy="home-ships-list"]').should('be.visible')
    cy.get('[data-cy="box-ship"]').should('have.length', 2)
  })

  it('removes loader once ships are loaded', function () {
    cy.get('[data-cy="home-ships-loader"]').should('not.exist')
  })

  it('displays total number of ships', function () {
    cy.get('[data-cy="home-ships-total"]').contains('2 mouvements')
  })
})
