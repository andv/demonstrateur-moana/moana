describe('Accessibility and markup', () => {
  it('tests page "Suivi des navires"', () => {
    cy.login()
    cy.getShips()
    cy.visit('/')
    cy.htmlvalidate()
  })

  it('tests page "Centre d\'aide"', () => {
    cy.login()
    cy.getFAQ()
    cy.visit('/centre-aide')
    cy.htmlvalidate()
  })

  it('tests page "Mon équipe"', () => {
    cy.login()
    cy.getTeamManagerName()
    cy.getTeamSettings()
    cy.getTeamUsers()
    cy.visit('/mon-equipe')
    cy.htmlvalidate()
  })

  it('tests page "Mon équipe" for team manager user', () => {
    cy.login('user-team-manager.json')
    cy.getTeamManagerName()
    cy.getTeamSettings()
    cy.getTeamUsers()
    cy.visit('/mon-equipe')
    cy.htmlvalidate()
  })

  it('tests page "Gestion des alertes"', () => {
    cy.login()
    cy.getWatchlist()
    cy.getTeamManagerName()
    cy.visit('/gestion-des-alertes')
    cy.htmlvalidate()
  })

  it('tests page "Gestion des alertes" for team manager user', () => {
    cy.login('user-team-manager.json')
    cy.getWatchlist()
    cy.getTeamManagerName()
    cy.visit('/gestion-des-alertes')
    cy.htmlvalidate()
  })
})
