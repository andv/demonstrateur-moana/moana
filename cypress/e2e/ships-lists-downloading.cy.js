describe('Download FPR lists', () => {
  beforeEach(() => {
    cy.login('user-fpr.json')
    cy.getShips('ships-with-lists.json')
    cy.goToHome()
    cy.preventOpenWindow()
    cy.get('[data-cy="box-ship"]').first().as('shipWithoutAnnexes')
    cy.get('@shipWithoutAnnexes').scrollIntoView()
    cy.get('@shipWithoutAnnexes')
      .find('[data-cy="box-dropdown-list-direct-link"]')
      .as('directListLinks')
  })

  it('shows crew list link', function () {
    cy.get('@directListLinks').last().as('crewListDirectLink')
    cy.get('@crewListDirectLink').contains('Équipage (1)')
    cy.get('@crewListDirectLink').click()
    cy.get('@open').should(
      'have.been.calledOnceWithExactly',
      '/demo-crew-list-file-fpr.csv',
    )
  })

  it('shows passengers list link', function () {
    cy.get('@directListLinks').first().as('passengersListDirectLink')
    cy.get('@passengersListDirectLink').contains('Passagers (2)')
    cy.get('@passengersListDirectLink').click()
    cy.get('@open').should(
      'have.been.calledOnceWithExactly',
      '/demo-passengers-list-file-fpr.csv',
    )
  })
})

describe('Download ROC lists', () => {
  beforeEach(() => {
    cy.login('user-roc.json')
    cy.getShips('ships-with-lists.json')
    cy.goToHome()
    cy.preventOpenWindow()
    cy.get('[data-cy="box-ship"]').first().as('shipWithoutAnnexes')
    cy.get('@shipWithoutAnnexes').scrollIntoView()
    cy.get('@shipWithoutAnnexes')
      .find('[data-cy="box-dropdown-list-direct-link"]')
      .as('directListLinks')
  })

  it('shows crew list link', function () {
    cy.get('@directListLinks').last().as('crewListDirectLink')
    cy.get('@crewListDirectLink').contains('Équipage (1)')
    cy.get('@crewListDirectLink').click()
    cy.get('@open').should(
      'have.been.calledOnceWithExactly',
      '/demo-crew-list-file-roc.csv',
    )
  })

  it('shows passengers list link', function () {
    cy.get('@directListLinks').first().as('passengersListDirectLink')
    cy.get('@passengersListDirectLink').contains('Passagers (2)')
    cy.get('@passengersListDirectLink').click()
    cy.get('@open').should(
      'have.been.calledOnceWithExactly',
      '/demo-passengers-list-file-roc.csv',
    )
  })
})

describe('Dropdown for ROC and FPR lists', () => {
  beforeEach(() => {
    cy.login()
    cy.getShips('ships-with-lists.json')
    cy.goToHome()
    cy.preventOpenWindow()
    cy.get('[data-cy="box-ship"]').first().as('shipWithoutAnnexes')
    cy.get('@shipWithoutAnnexes').scrollIntoView()
    cy.get('@shipWithoutAnnexes')
      .find('[data-cy="box-dropdown-list-button"]')
      .as('dropdownListButton')
    cy.get('@dropdownListButton').first().as('passengersDropdown')
    cy.get('@dropdownListButton').last().as('crewDropdown')
  })

  it('shows dropdown', function () {
    cy.get('@crewDropdown').contains('Équipage (1)')
    cy.get('@crewDropdown').get('svg').should('be.visible')
    cy.get('@passengersDropdown').contains('Passagers (2)')
    cy.get('@passengersDropdown').get('svg').should('be.visible')
  })

  it('shows lists when open dropdown', function () {
    cy.get('@crewDropdown').click()
    cy.get('@crewDropdown')
      .get('[data-cy="box-dropdown-fpr-link"]')
      .should('be.visible')
    cy.get('@crewDropdown')
      .get('[data-cy="box-dropdown-roc-link"]')
      .should('be.visible')

    cy.get('@passengersDropdown').click()
    cy.get('@passengersDropdown')
      .get('[data-cy="box-dropdown-fpr-link"]')
      .should('be.visible')
    cy.get('@passengersDropdown')
      .get('[data-cy="box-dropdown-roc-link"]')
      .should('be.visible')
  })

  it('downloads crew lists when click on fpr link', function () {
    cy.get('@crewDropdown').click()
    cy.get('@crewDropdown').get('[data-cy="box-dropdown-fpr-link"]').click()
    cy.get('@open').should(
      'have.been.calledOnceWithExactly',
      '/demo-crew-list-file-fpr.csv',
    )
  })

  it('downloads crew lists when click on roc link', function () {
    cy.get('@crewDropdown').click()
    cy.get('@crewDropdown').get('[data-cy="box-dropdown-roc-link"]').click()
    cy.get('@open').should(
      'have.been.calledOnceWithExactly',
      '/demo-crew-list-file-roc.csv',
    )
  })

  it('downloads passengers lists when click on fpr link', function () {
    cy.get('@passengersDropdown').click()
    cy.get('@passengersDropdown')
      .get('[data-cy="box-dropdown-fpr-link"]')
      .click()
    cy.get('@open').should(
      'have.been.calledOnceWithExactly',
      '/demo-passengers-list-file-fpr.csv',
    )
  })

  it('downloads passengers lists when click on roc link', function () {
    cy.get('@passengersDropdown').click()
    cy.get('@passengersDropdown')
      .get('[data-cy="box-dropdown-roc-link"]')
      .click()
    cy.get('@open').should(
      'have.been.calledOnceWithExactly',
      '/demo-passengers-list-file-roc.csv',
    )
  })

  it('closes crew dropdown', function () {
    cy.get('@crewDropdown').click()
    cy.get('@crewDropdown')
      .get('[data-cy="box-dropdown-fpr-link"]')
      .should('exist')
    cy.get('@crewDropdown')
      .get('[data-cy="box-dropdown-roc-link"]')
      .should('exist')
    cy.get('@crewDropdown').get('[data-cy="box-dropdown-list-close"]').click()
    cy.get('@crewDropdown')
      .get('[data-cy="box-dropdown-fpr-link"]')
      .should('not.exist')
    cy.get('@crewDropdown')
      .get('[data-cy="box-dropdown-roc-link"]')
      .should('not.exist')
  })

  it('closes passengers dropdown', function () {
    cy.get('@passengersDropdown').click()
    cy.get('@passengersDropdown')
      .get('[data-cy="box-dropdown-fpr-link"]')
      .should('exist')
    cy.get('@passengersDropdown')
      .get('[data-cy="box-dropdown-roc-link"]')
      .should('exist')
    cy.get('@passengersDropdown')
      .get('[data-cy="box-dropdown-list-close"]')
      .click()
    cy.get('@passengersDropdown')
      .get('[data-cy="box-dropdown-fpr-link"]')
      .should('not.exist')
    cy.get('@passengersDropdown')
      .get('[data-cy="box-dropdown-roc-link"]')
      .should('not.exist')
  })
})

describe('Download annexes lists', () => {
  beforeEach(() => {
    cy.login()
    cy.getShips('ships-with-lists.json')
    cy.goToHome()
    cy.preventOpenWindow()
    cy.get('[data-cy="box-ship"]').last().as('shipWithAnnexes')
    cy.get('@shipWithAnnexes').scrollIntoView()
    cy.get('@shipWithAnnexes')
      .find('[data-cy="box-dropdown-list-button"]')
      .as('dropdownListButton')
    cy.get('@dropdownListButton').first().as('passengersDropdown')
    cy.get('@dropdownListButton').last().as('crewDropdown')
  })

  it('can download fpr crew annex', function () {
    cy.get('@crewDropdown').click()
    cy.get('@crewDropdown')
      .get('[data-cy="box-dropdown-fpr-annex-link"]')
      .as('crewAnnexFpr')
    cy.get('@crewDropdown')
      .get('[data-cy="box-dropdown-annex-mention"]')
      .should('exist')
    cy.get('@crewAnnexFpr').should('exist')
    cy.get('@crewAnnexFpr').contains('Annexe FPR')
    cy.get('@crewAnnexFpr').click()
    cy.get('@open').should(
      'have.been.calledOnceWithExactly',
      '/demo-crew-list-annex-file-fpr.csv',
    )
  })

  it('can download roc crew annex', function () {
    cy.get('@crewDropdown').click()
    cy.get('@crewDropdown')
      .get('[data-cy="box-dropdown-roc-annex-link"]')
      .as('crewAnnexRoc')
    cy.get('@crewDropdown')
      .get('[data-cy="box-dropdown-annex-mention"]')
      .should('exist')
    cy.get('@crewAnnexRoc').should('exist')
    cy.get('@crewAnnexRoc').contains('Annexe ROC')
    cy.get('@crewAnnexRoc').click()
    cy.get('@open').should(
      'have.been.calledOnceWithExactly',
      '/demo-crew-list-annex-file-roc.csv',
    )
  })

  it('can download fpr passengers annex', function () {
    cy.get('@passengersDropdown').click()
    cy.get('@passengersDropdown')
      .get('[data-cy="box-dropdown-fpr-annex-link"]')
      .as('passengersAnnexFpr')
    cy.get('@passengersDropdown')
      .get('[data-cy="box-dropdown-annex-mention"]')
      .should('exist')
    cy.get('@passengersAnnexFpr').should('exist')
    cy.get('@passengersAnnexFpr').contains('Annexe FPR')
    cy.get('@passengersAnnexFpr').click()
    cy.get('@open').should(
      'have.been.calledOnceWithExactly',
      '/demo-passengers-list-annex-file-fpr.csv',
    )
  })

  it('can download roc passengers annex', function () {
    cy.get('@passengersDropdown').click()
    cy.get('@passengersDropdown')
      .get('[data-cy="box-dropdown-roc-annex-link"]')
      .as('passengersAnnexRoc')
    cy.get('@passengersDropdown')
      .get('[data-cy="box-dropdown-annex-mention"]')
      .should('exist')
    cy.get('@passengersAnnexRoc').should('exist')
    cy.get('@passengersAnnexRoc').contains('Annexe ROC')
    cy.get('@passengersAnnexRoc').click()
    cy.get('@open').should(
      'have.been.calledOnceWithExactly',
      '/demo-passengers-list-annex-file-roc.csv',
    )
  })
})
