/*
LOGIN :
We prevent test from crashing by intercepting default api calls, and logging our user.
*/
Cypress.Commands.add('login', (userFile) => {
  cy.intercept('GET', '/api/users/current-user', {
    status: 201,
    fixture: userFile || 'user.json',
  }).as('getCurrentUser')

  cy.intercept('GET', '/api/issues/*', {
    statusCode: 201,
    body: [],
  }).as('getIssues')
})

/*
GET SHIPS :
Load specific ships file for the test
*/
Cypress.Commands.add('getShips', (shipsFile) => {
  cy.intercept('GET', '/api/ships/*', {
    statusCode: 201,
    fixture: shipsFile || 'ships.json',
  }).as('getShips')
})

/*
PREVENT OPEN WINDOW :
New window are not handle properly by cypress
*/
Cypress.Commands.add('preventOpenWindow', () => {
  cy.window().then((win) => {
    cy.stub(win, 'open').as('open')
  })
})

/*
GO TO HOME :
*/
Cypress.Commands.add('goToHome', () => {
  cy.visit('/')
  cy.wait(['@getShips', '@getCurrentUser'])
})

/*
GET TEAM SETTINGS :
Load team setting
*/
Cypress.Commands.add('getTeamSettings', () => {
  cy.intercept('GET', '/api/team/settings', {
    statusCode: 201,
    fixture: 'team-settings.json',
  })
})

/*
GET TEAM USERS :
Load team users
*/
Cypress.Commands.add('getTeamUsers', () => {
  cy.intercept('GET', '/api/team/users', {
    statusCode: 201,
    fixture: 'team-users.json',
  })
})

/*
GET TEAM MANAGER :
Manager name for user's team
*/
Cypress.Commands.add('getTeamManagerName', () => {
  cy.intercept('GET', '/api/users/team-manager', {
    statusCode: 201,
    body: {
      name: 'Jane Doe',
    },
  })
})

/*
GET FAQ :
Load questions and answers
*/
Cypress.Commands.add('getFAQ', () => {
  cy.intercept('GET', '/api/faq', {
    statusCode: 201,
    fixture: 'questions-and-answers.json',
  })
})

/*
GET WATCHLIST :
List of IMO watched by the team
*/
Cypress.Commands.add('getWatchlist', (watchlist) => {
  cy.intercept('GET', '/api/alerts/watchlist', {
    statusCode: 201,
    fixture: watchlist || 'watchlist-empty.json',
  })
})

/*
UNCAUGHT EXCEPTION :
- Sometimes front-end tracking does not have to be included in test and it causes test to fail.
*/
Cypress.Commands.add('catchTrackingException', () => {
  cy.on('uncaught:exception', (err) => {
    if (err.config.url.includes('tracking/')) {
      return false
    }
  })
})

/* POST STOP SHOWING
Send alert id
*/
Cypress.Commands.add('postAlertsStopShowing', () => {
  cy.intercept('post', '/api/alerts/*/stop-showing', {
    status: 201,
    body: {},
  })
})

/* PATCH EMAIL NOTIFICATION
Set new mail address for the user team
*/
Cypress.Commands.add('changeNotificationEmail', (newEmail, status) => {
  cy.intercept('patch', '/api/alerts-config/notification-email', {
    statusCode: status || 201,
    body: {
      notification_email: newEmail,
    },
  })
})

/* POST ADD IMO
Add imo to the team watchlist
*/
Cypress.Commands.add('postAddImo', () => {
  cy.intercept('post', '/api/alerts-config/add-imo-list', (req) => {
    let imosAreInvalid = false
    const imos = req.body
    if (typeof imos === 'string') imosAreInvalid = true
    for (let i = 0; i < imos.length; i++) {
      if (imos[i].contains(' ')) imosAreInvalid = true
    }
    req.reply({
      statusCode: imosAreInvalid ? 400 : 201,
      body: {},
    })
  })
})

/* POST DELETE IMO
Delete imo from the team watchlist
*/
Cypress.Commands.add('deleteImo', () => {
  cy.intercept('delete', '/api/alerts-config/*/delete-imo', {
    statusCode: 201,
    body: {},
  })
})

/* GET ACCESSIBILITY PAGE :
Display accessibility page
*/
Cypress.Commands.add('getAccessibilityPage', () => {
  cy.intercept('GET', '/api/pages/accessibilite', {
    statusCode: 201,
    fixture: 'footer-accessibility-details.json',
  })
})

/* GET LEGAL MENTIONS PAGE :
Display legal mentions page
*/
Cypress.Commands.add('getLegalMentionsPage', () => {
  cy.intercept('GET', '/api/pages/mentions-legales', {
    statusCode: 201,
    fixture: 'footer-legal-mentions.json',
  })
})

/* GET USE CONDITIONS PAGE :
Display condtions use page
*/

Cypress.Commands.add('getUsageConditionsPage', () => {
  cy.intercept('GET', '/api/pages/conditions-generales-utilisation', {
    statusCode: 201,
    fixture: 'footer-usage-conditions.json',
  })
})

/* GET VERSIONS PAGE :
Display versions page
*/
Cypress.Commands.add('getVersionPage', () => {
  cy.intercept('GET', '/api/pages/version', {
    statusCode: 201,
    fixture: 'version.json',
  })
})
