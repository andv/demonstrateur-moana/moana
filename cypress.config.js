import { defineConfig } from 'cypress'
import cypressFailFast from 'cypress-fail-fast/plugin.js'
import htmlvalidate from 'cypress-html-validate/plugin'

export default defineConfig({
  e2e: {
    baseUrl: 'http://localhost:8081',
    defaultCommandTimeout: 10000,
    screenshotOnRunFailure: true,
    screenshotFolder: 'cypress/screenshots',
    setupNodeEvents(on, config) {
      cypressFailFast(on, config)
      htmlvalidate.install(
        on,
        { rules: { 'require-sri': 'off' } },
        { exclude: ['[data-cy-exclude-dsfr]'] },
      )
    },
    viewportHeight: 900,
    viewportWidth: 1440,
  },
})
