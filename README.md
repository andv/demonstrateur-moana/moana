# Moana ANDV Maritime

> ⬇️ Sommaire :

> [Versions](#versions)

> [Organisation générale](#organisation-générale)

> [Installation sans Docker](#installation-sans-docker)

> [Installation avec Docker](#installation-avec-docker)

> [Import des référentiels de données](#import-des-référentiels-de-données)

> [Base de données](#base-de-données)

> [Gestion des configurations](#gestion-des-configurations)

> [À propos des types de navires](#à-propos-des-types-de-navires)

> [Filtrage des mouvements de navires](#filtrage-des-mouvements-de-navires)

> [À propos des tâches périodiques](#à-propos-des-tâches-périodiques)

> [Organisation du code](#organisation-du-code)

> [Multiples bases de données](#multiples-bases-de-données)

> [À propos du "Data Processing" - Traitement des Données](#à-propos-du-data-processing---traitement-des-données)

> [Développement et harmonisation du code](#développement-et-harmonisation-du-code)

> [Configuration de l'application](#configuration-de-lapplication)

> [Modèles de données](#modèles-de-données)

> [Cypress](#cypress)



## Versions

L'historique des versions de cette application est disponible
dans le dossier [releases](https://gitlab.mim-libre.fr/andv/demonstrateur-moana/moana/-/tree/main/docs/releases)

## Organisation générale

- Le front-end est géré par Vue, Yarn et Vite.
- Le backend et l'API sont gérés par Django et Django REST framework.

#### Les dossiers du code

| Location             | Content                                                                                       |
| -------------------- | --------------------------------------------------------------------------------------------- |
| `/backend`           | Django Project & Backend Config                                                               |
| `/backend/api`       | Django App (`/api`)                                                                           |
| `/frontend`          | Vue App                                                                                      |
| `/frontend/main.js`  | JS Application Entry Point                                                                    |
| `/index.html`        | Html Application Entry Point                                                                  |
| `/static`            | Static Assets                                                                                 |
| `/dist`              | Bundled Assets Output (generated at `yarn build`)                                             |
| `/data_processing`   | Data processing                                                                               |

- Le `entry point` est servi par Django sur l'URL `/` : `index.html` + bundled assets.
- L'API est servie sur `/api/`. Une documentation swagger peut être mise à disposition sur "/api/swagger/".
- Les fichiers statics sont sur l'URL `/static/`.
- L'admin Django est par défaut sur `/admin/` et cette URL est configurable.

## Installation sans Docker

### Prérequis

- [x] Yarn 3.7.0 - [instructions](https://yarnpkg.com/getting-started/install)
- [x] Python 3 - [instructions](https://wiki.python.org/moin/BeginnersGuide)
- [x] Pipenv - [instructions](https://pipenv.pypa.io/en/latest/installation.html)
- [x] Recommended Extensions VSCode - Taper dans la barre de recherche des extensions de VS Code : `@recommended`

### Code :

```
git clone git@gitlab.mim-libre.fr:andv/demonstrateur-moana/moana.git
cd moana
```

### Setup :

```
yarn install
yarn build
pipenv install --dev && pipenv shell
```

A noter qu'avec PowerShell sous Windows, il faut décomposer la dernière ligne en deux commandes `pipenv install --dev`, puis `pipenv shell`
### Obtenir les bases de données tests

Pour avoir un jeu de données, on peut utiliser la base de données de test.

Il faut copier les fichiers `.sqlite` suivants :

```
cp db.sqlite3.example db.sqlite3
cp db_stats.sqlite3.example db_stats.sqlite3
```

### Fichiers .env

Des exemples de fichier `.env` et `.env.development` sont disponibles.

```
cp .env.example .env
cp .env.development.example .env.development
```

### Migration et fichiers static

```
python manage.py migrate
python manage.py migrate_stats_database
python manage.py collectstatic --noinput
```

⚠️ Pour **Windows**, il peut y avoir une erreur du type `ModuleNotFoundError: No module named 'pkg_resources'`, qui sera corrigée en installant `setuptools` :
```
pipenv install setuptools
```
Si le problème persiste, suivez les conseils au chapitre ["Serveur de développement django"](#serveur-de-développement-django-) sur l'installation de `python-magic`.

### Tests unitaires :

```
python manage.py collectstatic --noinput
pytest
```

Il est possible d'analyser la couverture de tests unitaires en utilisant Coverage :

```
pipenv run coverage run -m pytest && pipenv run coverage html
```


### Serveur de développement django :

```
python manage.py runserver
```

⚠️ Pour **mac OS** il peut y avoir un conflit avec le plugin python-magic.
Pour cela il faut désinstaller le plugin, puis le ré-installer via l'url du repo :

```
pipenv uninstall python-magic
pipenv install git+https://github.com/ahupp/python-magic
```

⚠️ Pour **Windows**, la solution passera également par la désinstallation de python-magic et l'installation d'un autre plugin :
```
pipenv uninstall python-magic
pipenv install python-magic-bin
```
### Serveur front - à lancer depuis un autre terminal :

```
yarn dev
```

### À la fin, on devrait avoir :

- Frontend Vuejs : [`localhost:8080`](http://localhost:8080/)
- Backend Django :[`localhost:8000/api/`](http://localhost:8000/api/).

Pour servir l'appli Vuejs via le backend et répliquer ce qui se passe en prod :

```
yarn build
python manage.py runserver
```

Dans ce cas-là, l'application front devrait être disponible ici :

- [`localhost:8000`](http://localhost:8000/)

### Mise à jour des dépendances

Pour mettre à jour une dépendance django, lancer la commande :

```
pipenv shell
pipenv install {nomDuPaquet}={numeroDeVersion} [--dev]
```

Pour mettre à jour une dépendance yarn, lancer la commande :

```
yarn add {nomDuPaquet}[numeroDeVersion] [--dev]
```

## Installation avec Docker

### Prérequis :
- [x] [Docker](https://docs.docker.com/get-docker/)
- [x] [Docker-compose](https://docs.docker.com/compose/install/)

Il est important d'avoir au préalable :

- [x] cloner le repo :
```
git clone git@gitlab.mim-libre.fr:andv/demonstrateur-moana/moana.git
```

- [x] créer les fichiers :
  - `.env` (voir [section "Fichiers .env"](#fichiers-env))
  - `db.sqlite3` et `db_stats.sqlite3` (voir [section "Obtenir les bases de données tests"](#obtenir-les-bases-de-données-tests))

- [x] décommenter la variable `VITE_DOCKER_PROXY_URL` dans le fichier .env créé

### installation

Dans un terminal se placer à la racine du projet:
```cd moana```

Lancer la commande :
```docker compose up```

Cette commande peut être relativement longue en fonction de votre machine.

⚠️ Sous **windows**, il peut y avoir une erreur du type `Unknown command: 'migrate\r'. Did you mean migrate?`'.
Cette erreur provient du formatage des fichiers `scripts`, les retours chariots et sauts de lignes 'CRLF' (Windows) devront être remplacés par des sauts de lignes 'LF' (Unix).

Le backend est disponible ici :

- [`localhost:8000`](http://localhost:8000/api)

et permet de se connecter depuis l'url:

- [`localhost:8000/admin`](http://localhost:8000/admin) en utilisant les comptes renseignés dans la partie [ ### Comptes utilisateurs](#comptes-utilisateurs).

Le frontend est disponible ici :

- [`localhost:8080`](http://localhost:8080)


### Lancer des commandes

Dans un nouvel onglet du terminal on peut lancer la commande :

```docker compose exec {NomDuService} bash```

Cette commande est valable pour le service `backend` comme pour le service `frontend` en fonction du besoin.

Après cette commande on peut passer des commandes telles que ```python manage.py shell_plus``` par exemple.

### Servir l'application Vuejs depuis le backend

Pour servir l'appli Vuejs via le backend et répliquer ce qui se passe en prod :

```
docker compose up
```

Dans un autre onglet du terminal :

```
docker compose exec frontend bash
yarn build
exit
```

Dans ce cas-là, l'application front devrait être disponible ici :

- [`localhost:8000`](http://localhost:8000/)


### Import des référentiels de données sur Docker

La base de données SQLite doit être enrichie avec des référentiels de données.
Pour se faire il convient de lancer des imports (voir [section ### Option 2 : Importer les données](#importer-les-donnees)).

Avec Docker ces commandes doivent être passées depuis le service backend :

`docker compose exec backend bash`

### Stopper Docker

Pour stopper le container Docker on peut passer la commande :

`docker compose down`

### Mise à jour des dépendances depuis Docker

Pour mettre à jour une dépendance django, lancer la commande :

```
docker compose exec backend bash
pipenv shell
pipenv install {nomDuPaquet}={numeroDeVersion} [--dev]
```

Pour mettre à jour une dépendance yarn, lancer la commande :

```
docker compose exec frontend bash
yarn add {nomDuPaquet}[numeroDeVersion] [--dev]
```

Il faut ensuite refaire un build des containers:

```
docker compose down
docker compose up --build
```

## Import des référentiels de données

Deux options sont possibles : soit importer les données, soit utiliser les bases
de données tests.

### Option 1 : Utiliser les bases de données tests

Quand on utilise les bases de données de tests, les référentiels sont déjà disponibles.

Voir les instructions plus haut.


### Option 2 : Importer les données

```
python manage.py import_ports data/ports.csv
python manage.py import_ship_types data/ship-types.csv
python manage.py import_ship_types_mapping data/ship-types-mapping.csv
python manage.py import_mmsi data/mmsi.csv
```

### Pour lancer ces imports sur Scalingo :

```
scalingo --app <my-app> run --detached "python manage.py import_ports data/ports.csv"
scalingo --app <my-app> run --detached "python manage.py import_ship_types data/ship-types.csv"
scalingo --app <my-app> run --detached "python manage.py import_ship_types_mapping data/ship-types-mapping.csv"
scalingo --app <my-app> run --detached "python manage.py import_mmsi data/mmsi.csv"

```

### Convertir les fichiers parquet

Pour les référentiels de type navires et mmsi les fichiers références peuvent nous être transmis au format .parquet.
Ce format n'est pas utilisable dans nos process il faut le convertir en csv avec la commande :
```
python manage.py convert_parquet_to_csv --from [input fichier parquet] --to [output fichier csv]
```

## Base de données

En mode development, on peut utiliser les bases de données SQLite.

💡 Pour visualiser les bases de données SQLite dans VS Code il faut télécharger
l'extension [SQLite](https://marketplace.visualstudio.com/items?itemName=alexcvzz.vscode-sqlite)

### Logs

Les logs d'actions sont gérés dans une base de données séparée de l'application principale.
Pour déclencher ces logs, il faut :

Activer la variable d'environnement `TRACKMAN_ENABLED`.

Puis faire tourner les migrations liées aux logs d'action :

```
python manage.py migrate_stats_database
```

### Comptes utilisateurs

En mode development il est possible de se connecter avec un email - mot de passe.

Des utilisateurs tests sont déjà créés dans les données de tests :


| mail           | mot de passe | rôle           |
| -------------- | ------------ | -------------- |
| admin@demo.com | moana12345   | administrateur |
| jane@demo.com  | moana12345   | membre équipe  |


### Urls de connexion

- http://localhost:8000/utilisateurs/login/ : pour accéder à l'application
- http://localhost:8000/admin : pour accéder au back-office

Sur Scalingo, en production, la variable d'environnement `VITE_API_ROOT` est définie avec une url absolue ce qui permet d'accèder aux pages créées depuis l'admin django et de les recharger.
Pour reproduire ce comportement sur le staging ou les app reviews il convient de modifier cette variable dans ces app depuis l'interface admin de Scalingo.

## Gestion des configurations

### Variables d'environnement

Nous utilisons les variables d'environnements pour les configurations secrètes ou pour
les configurations qui diffèrent entre les différents environnements, prod, staging.

Pour plus de détails, voir le module `settings.prod`.


### Les fichiers Django settings

Il y a 3 fichiers "django settings" :

- `settings.prod` pour les environnements de prod et staging
- `settings.local` pour le développement local
- `settings.test` pour les tests unitaires

Ces fichiers se basent sur le fichier `settings.base` qui contient les configurations
communes.

Le fichier `settings.test` hérite de la configuration locale.

## À propos des types de navires

Si vous cherchez à importer les données des types de navires, se référer au chapitre
concernant l'import des référentiels de données

### Type de navires

Sur Moana, nous maintenons un référentiel des types de navires et cette base de données
contient :

- Un type de référence - provenant de Lloyd's Register.
- Une traduction en français.
- Un type "simplifié" qui permet de regrouper plusieurs type de navires.

Voir le modèle de données `ship_types.models.ShipType` pour plus de détails.

### Correspondance avec les navires

Il existe par ailleurs une correspondance entre les navires et le type associé.
On utilise les champs suivant pour établir cette correspondance :

- Numéro IMO du navire.
- Le nom de référence du référentiel type de navire.

Voir le modèle de données `ship_types.models.ShipTypeMapping` pour plus de détails.

### Format des fichiers

Pour connaître le format CSV pour ces imports, voir les fichiers dans : `moana/data`

### Remarques

1. Il faut d'abord importer la liste des types de navires, avant d'importer les
   correspondances entre le navire et son type.

2. Au moment de l'upload des fichiers FAL, l'application va interroger le référentiel
   "type-navire" pour faire le lien entre les mouvements de navire et le type de navire
   qu'il faudra associer, puis afficher sur la page.

### Doublons dans le référentiel de correspondance type navire

Le fichier CSV de référence qui contient les associations entre l'IMO et le type navire est `data/ship-types-mapping.csv`.

Pour s'assurer que ce fichier ne contient pas de doublons, on peut utiliser la commande :

```
./manage.py clean_ship_types_mapping_file data/ship-types-mapping-original.csv
```

Parfois la base de données des correspondances ship-type/imo contient aussi des doublons. Pour les supprimer, on utilise ceci :

```
./manage.py clean_ship_types_mapping_db
```

## Filtrage des mouvements de navires

Les utilisateurs peuvent appliquer des filtres via l'interface, par exemple pour ne montrer que certains types de navires,
ou bien pour distinguer les mouvements récents des historiques de mouvements.

Ces filtres explicites viennent s'ajouter à d'autres filtres qui sont appliqués automatiquement.
Ce filtrage initial s'applique globalement, avant même que l'utilisateur ne reçoive la liste des mouvements de navire.
Pour voir le détail de ces filtres initiaux, il faut regarder la construction de la `queryset` dans `ships.views`.

Voici quelques règles appliquées par les filtres initiaux :

- On ne retient que les mouvements dont le port correspond aux ports associés à l'utilisateur.
- On exclut les mouvements France Métroplolitain.
- On exclut les mouvements dont les horaires sont trop lointains dans le futur - 2 semaines.

## À propos des tâches périodiques

### Scalingo Scheduler

Scalingo fourni un service pour lancer des tâches périodiques.

> Plus de détails : https://doc.scalingo.com/platform/app/task-scheduling/scalingo-scheduler

Nous utilisons la solution Scalingo par exemple pour supprimer les données de
manière régulière ou bien pour déclencher les imports de données depuis les API
des systèmes portuaires SIP.

La liste des tâches est définie dans le fichier `cron.json`.

### Celery Beat

Nous avons aussi recours à notre propre infrastructure de gestion des tâches périodiques
basée sur Celery Beat. Pour l'import des emails contenant les fichiers FAL, nous avons
besoin de tâches qui s'exécutent fréquemment - plus fréquemment que ce qui est possible
avec Scalingo qui limite à une exécution toutes les 10 min.

Pour configurer les taches Celery Beat, il faut se rendre dans l'admin Django, dans
la section "tâches périodiques."

## Organisation du code

La documentation détaillée des applications est maintenue directement dans le code.
Décrivons ici une vue d'ensemble de l'organisation du code.

### Backend

Les applications Django sont dans le dossier `backend`.
L'approche général est de travailler avec de petites application qui ont une fonction limité.

Regroupons ces applications pour en donner une vue d'ensemble.

#### Fonctionnalité autour des mouvements de navires
- `ships` : L'application centrale autour des mouvements de navires.
- `persons` : La gestion des listes de personnes.
- `alerts` : Gestion des alertes sur les mouvements de navires et les listes.
- `person_checks` : Les fonctionnalités autour des cases à cocher traitement FPR/ROC.

#### Référentiels
- `ports` : La base de données des ports.
- `geo` : Les zones géographiques.
- `ship_types` : Les types de navires.
- `sip` : Les systèmes d'information portuaires.
- `nationalities` : Gestion des pays et des nationalités.

#### Gestion des fichiers NCA, FAL, FPR
- `ship_files` : Les fichiers FAL ou NCA_PORT sont des "ship files". Ils contiennent des informations d'escale ou des listes de personnes.
- `nca` : Ce qui est commun à la gestion des fichiers NCA qui peuvent par exemple être "NCA FAL" et "NCA PORT".
- `fal` : La gestion des fichiers FAL qui sont un type particulier de fichier NCA.
- `nca_port` : Gestion des fichiers NCA PORT qui sont un type particulier de fichiers NCA.
- `fpr` : Les fichiers téléchargeables au format FPR.

#### Backoffice et configuration
- `settings` : Les configurations Django de l'application.
- `backoffice` : En rapport avec l'admin Django.
- `accounts` : Gestion des utilisateurs et l'authentification à l'application.
- `cheops` : Gestion du SSO Cheops.


#### Taches et schedulings
- `tasks` : Les tâches asynchrones.
- `email_import` : La gestion des imports emails.
- `marathon` : Un utilitaire pour gérer les tâches qui fonctionne en mode "single runner".

### Traçabilité
- `logs` : Les logs d'actions, traçabilité, anomalies de données.
- `issues` : Les signalements de problèmes de données que nous rapportent les utilisateurs.

#### Utilitaires
- `initialization` : Les données de départ, par exemple pour les référentiels "type navires".
- `cleanup` : Les suppressions de données.
- `db` : Utilitaires de gestion des bases de données. Gestion du "multi-database".
- `utils` : Les utilitaires généraux.
- `current_user` : À propos de l'utilisateur actuel de la session.
- `unit_tests` : Les utilitaires et les "factories" utilisés pour les tests unitaires.

#### Site et templates
- `home` : La page de démarrage.
- `website` : Les pages "statiques" de type mentions légales.


### Data processing

Le dossier `data_processing` contient les outils de traitement des données. Ces traitements
peuvent être appliqués en amont d'une analyse de données qui permettra de visualiser
les données, par exemple sur un outil comme Metabase.

- `fal_coverage` : Concerne la réception et la couverture des fichiers FAL.
- `person_list` : Concerne l'utilisation qui est faite des listes de personnes disponibles au téléchargement.


### Frontend

Le fichier `index.html` est le point d'entrée ou l'application VueJS principale s'insère.

Le code Vuejs de l'application principal est maintenu dans le dossier `frontend/`.

Certaines pages de l'application fonctionnent en dehors de l'application VueJS principale et sont
servies directement par Django.
Le dossier `templates/` contient les templates de ces pages-là. Il s'agit notamment de la
page d'authentification.

### Les scripts

Le dossier `scripts` contient les utilitaires, par exemple :

- Pour l'hébergement pour démarrer les services.
- Un utilitaire pour copier des données.


## Multiples bases de données

L'application utilise deux bases de données :

1. La *base de données application* pour gérer les fonctionnalités de l'application, les référentiels,
   certaines configurations, etc. Les applications du dossier `backend` utilisent cette base de données -
   à l'exception de l'application `logs`.

2. La *base de données stats* pour les logs d'action, la traçabilité, les anomalies de données, les traitements
   de données. Cette base contient les tables de l'application `logs` ainsi que les tables générées par les
   traitements faits depuis le dossier `data_processing`.

```
# En développement, nous utilisons une base de données SQLite pour chaque base de données.

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
         # ...
    },
    'stats': {
        'ENGINE': 'django.db.backends.sqlite3',
        # ...
    }
}
```

## À propos du "Data Processing" - Traitement des Données

**Voir le dossier `data_processing` et l'application `backend.logs` pour les détails.**

Le traitement de données est en fait un pré-traitement : Généralement, il s'agit de tâches qui tournent de manières
périodiques, par exemple tous les jours, afin de préparer les données qui seront ensuite analysées et visualisées
dans un outil comme Metabase.

Les données sujettes à ce traitement peuvent inclure :

- Les activités/actions effectuées par les utilisateurs. Par exemple, le téléchargement de fichiers, la sélection
  de cases à cocher. La source de données provient généralement de l'application `logs`.
- Les anomalies de données que nous détectons lorsque nous recevons des fichiers ou récupérons des données via des
  API. Par exemple, des dates incohérentes, des champs manquants, des problèmes d'encodage.

Quelques remarques :
- Distinguons la notion de *traitement de données* de celle *d'analyse des données* - Data processing / Data Analysis.
  Le résultat du traitement est stocké dans des tables dans la base de données. Ces tables sont ensuite accessibles dans
  l'outil d'analyse de donnée, Metabase.
- Pour des raisons de simplicité, nous avons choisi de stocker les tables de traitement `data_processing` avec les tables
  de l'application `logs` dans la même base de données `stats`.


## Développement et harmonisation du code

Pour maintenir la cohérence et améliorer la lisibilité du code, nous utilisons des linters.

Si vous travaillez avec Visual Studio Code, vous pouvez accéder aux configurations par défaut dans le dossier `.vscode`.

La configuration des linters est maintenue dans le dépôt GIT et s'applique automatiquement pour uniformiser le style de codage.
Ceci a pour but de minimiser les débats et la documentation autour des bonnes pratiques d'harmonisation du code.

### Python

Pour le code python, nous utilisons [Black](https://black.readthedocs.io/en/stable/).

```
pip install black
On peut activer Black dans VSCode en installant l'extension Black (ms-python.python).
```

### Front-end

Pour le code front-end nous utilisons [Prettier](https://prettier.io/).

```
npm install --save-dev --save-exact prettier
Et installer l'extension VSCode pour Prettier (esbenp.prettier-vscode).
```

### Configuration

Les linters sont intègrés à VSCode avec des extensions qui sont suggérées par le fichier `.vscode/extensions.json`.

La configuration des linters se trouve dans le fichier `.vscode/settings.json`.

## Configuration de l'application

Une partie de la configuration de l'application se fait via des variables d'environnement.
Pour voir la liste complète des configurations, se référer aux fichiers settings dans le dossier
`backend/settings/`.

#### `MAX_LINES_IN_FPR_FILE`

Cette configuration permet de limiter le nombre de lignes dans les fichiers FPR.
Les fichiers qui dépassent cette limite sont découpés puis rassemblés dans un fichier ZIP.

#### `EMAIL_IMPORT_SENDERS_WHITELIST`

Contient une liste d'adresses emails : il s'agit des expéditeurs - champ `from` du message.
Cette "whitelist" est utilisée par l'outil qui importe les fichiers reçus par email.
Seules les adresses indiquées dans cette liste sont autorisées à importer des données.

#### `PARENT_POSTGRESQL_URL`.

Cette variable ne concerne que l'environnement Staging sur Scalingo.

Quand on crée une "app review" sur Scalingo, il nous faut peupler la base de données Postgres
attachée à cette app. Pour cela, on utilise la base de données de Staging, puisque c'est
l'application "parente". L'app review en création est informée de l'URL de la base de
staging grâce à la variable `PARENT_POSTGRESQL_URL`. La copie de donnée initiale se fait dans
le `first_deploy.sh` qui est exécuté par Scalingo lors de la toute première création de l'app
review.

On remarque que cette variable a pour valeur une copie de la variable de staging appelée
`SCALINGO_POSTGRESQL_URL` - cette variable-là est créée par Scalingo au moment où on ajoute
un addon Postgres. Il est tentant de définir la variable `PARENT_POSTGRESQL_URL` en
utilisant le système d'alias de Scalingo avec un `$SCALINGO_POSTGRESQL_URL`, mais c'est une
fausse bonne idée, car dans l'app review, la valeur de `PARENT_POSTGRESQL_URL` serait alors
l'URL de la base de l'app review et non l'URL de la base parente.

#### `DATA_MAX_DAYS_OLD`

Cette variable gère la durée entre chaque effacement des listes passagers et équipages de l'application.
Par défaut sa valeur est à 60j.

Les équipes utilisatrices ayant un niveau d'historique de liste différents, une configuration est disponible dans le modèle des équipes (Team).
Cela permet de personnaliser leur durée d'historique, et donc leur accès ou non aux listes avant suppression définitive de ces dernières.

Cette variable est aussi utilisée lorsqu'on importe les données depuis les SIP, pour éviter
d'importer des données trop anciennes.


## Modèles de données

Les modèles de données sont documentés directement dans le code, dans les fichiers `models.py`.
Une version graphique des modèles de données est disponible dans le dossier
[docs/db-models/](https://gitlab.mim-libre.fr/andv/demonstrateur-moana/moana/-/tree/main/docs/db-models/).


Pour mettre à jours ces fichier de visualisation :

```
./manage.py graph_models -a -g -o docs/db-models/db-models-all.png
./manage.py graph_models  ships persons alerts person_checks ship_files nca fal nca_port fpr -g -o docs/db-models/db-models-ships-functionalities.png
./manage.py graph_models ports geo ship_types sip nationalities -g -o docs/db-models/db-models-references.png
./manage.py graph_models settings backoffice accounts cheops tasks email_import marathon issues website -g -o docs/db-models/db-models-backoffice-and-config.png
./manage.py graph_models logs fal_coverage person_list -g -o docs/db-models/db-models-stats.png
```


## Cypress

Nous utilisons Cypress pour les tests end-to-end.

### Commandes
Il est possible de lancer les tests de 2 façons :
- dans l'application desktop avec la commande `yarn run cypress:app`
- dans le terminal avec la commande `yarn run cypress:e2e` avant cette commande il faut lancer le dev de l'app en local `cypress:dev`

### Écriture
Afin de maintenir une bonne lisibilité, et une séparation entre la sémantique, la stylisation et les tests nous pointons au maximum les éléments HTML avec l'attribut "data-cy"

Le backend n'est pas testé avec Cypress, nous créons des fichiers de données (fixtures) en `.json` pour remplacer les réponses api.

Pour les performances de l'application nous avons un lazy-load en place pour l'affichage des mouvements, dans les tests il faut déclencher un scroll jusqu'à l'élément testé pour qu'il s'affiche, pour cela il faut utiliser la fonction [`.scrollIntoView()`](https://docs.cypress.io/api/commands/scrollIntoView)

Pour que les variables d'environnement `VITE_` nécessaires à notre front-end ne soient pas en conflit, ou dépendantes, de celles utilisées en local, un fichier d'environnement dédié aux tests est utilisé : `.env.cypress`.

### Tester l'accessibilité et la syntaxe HTML

Pour maintenir la qualité de code et le respect des standards en accessibilité et en syntaxe html, les pages d'application front-end sont testées avec : html-validate. Au développement d'une nouvelle page il suffit de l'ajouter dans le fichier `app-accessibility.cy.js` et de la tester dans ses différents états s'il y en a.

> 💡 Parfois certains tests ne sont pas corrigeables car l'erreur provient des librairies DSFR. Dans ce cas l'attribut `data-cy-exclude-dsfr` peut être ajouté au composant pour l'exclure des tests.
