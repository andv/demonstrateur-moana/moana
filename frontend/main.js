/* VUE 3 */
import { createApp } from 'vue'
import App from './App.vue'

/* Sentry */
import * as Sentry from '@sentry/vue'

/* DSFR */
import VueDsfr from '@gouvminint/vue-dsfr'
import '@gouvfr/dsfr/dist/dsfr.min.css' // Import des styles du DSFR
import '@gouvminint/vue-dsfr/styles' // Import des styles globaux propre à VueDSFR
import '@gouvfr/dsfr/dist/utility/utility.main.min.css' // Classes utilitaires: utilisée par exemple pour les fonds de couleurs

/* Icônes */
import 'remixicon/fonts/remixicon.css'

/* Project config */
import store from '@/store'
import router from '@/router'
import BaseDisplayRawHtml from '@/components/BaseDisplayRawHtml.vue'
import BaseLinkDownload from '@/components/BaseLinkDownload.vue'
import { OnClickOutside } from '@vueuse/components'
import '@/css/global.css'

const app = createApp(App)

if (import.meta.env.VITE_SENTRY_URL) {
  Sentry.init({
    app,
    dsn: import.meta.env.VITE_SENTRY_URL,
    tracesSampleRate: import.meta.env.VITE_SENTRY_TRACES_SAMPLE_RATE,
    integrations: [new Sentry.browserTracingIntegration()],
  })
}

app.config.globalProperties.$filters = {
  default(value, default_value) {
    return value ? value : default_value
  },
}
app.config.globalProperties.moanaContactEmail =
  import.meta.env.VITE_TEAM_EMAIL_CONTACT

app.use(store)
app.use(router)
app.use(VueDsfr)

app.component('BaseDisplayRawHtml', BaseDisplayRawHtml)
app.component('BaseLinkDownload', BaseLinkDownload)
app.component('OnClickOutside', OnClickOutside)
app.mount('#app')
