const state = {
  scheme: 'light',
  theme: 'light',
}

const getters = {
  mode: (state) => {
    return state.mode
  },
  theme: (state) => {
    return state.theme
  },
}

const mutations = {
  changeMode(state, mode) {
    state.mode = mode
  },
  changeTheme(state, theme) {
    state.theme = theme
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
}
