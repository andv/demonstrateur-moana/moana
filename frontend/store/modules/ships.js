import shipService from '@/services/shipService'

const state = {
  list: [],
  countPages: 0,
  currentPage: 0,
  filterParams: {
    way: '',
    search: '',
    shipTypes: [],
    portsOfCall: [],
    alerts: [],
    lastPort: [],
    localMovements: false,
    dateAfter: '',
    dateBefore: '',
    dateSuggestion: '',
    listToCheckType: [],
  },
  counterShips: {
    total: '',
    alerts: '',
  },
  loading: false,
}

const getters = {
  filterParams: (state) => {
    return state.filterParams
  },
}

const mutations = {
  setShips(state, ships) {
    state.list = ships
  },
  setCountPages(state, countPages) {
    state.countPages = countPages
  },
  setCurrentPage(state, currentPage) {
    state.currentPage = currentPage
  },
  updateShip(state, ship) {
    state.list = state.list.map((element) => {
      if (element.id === ship.id) element = ship
      return element
    })
  },
  setFilters(state, filterParams) {
    state.filterParams = filterParams
  },
  setFilter(state, filter) {
    const { name, value } = filter
    state.filterParams[name] = value
  },
  setCounterShips(state, counts) {
    state.counterShips = counts
  },
  setLoading(state, loading) {
    state.loading = loading
  },
  setPortsOfCall(state, portsOfCall) {
    state.filterParams.portsOfCall = portsOfCall
  },
  resetFilter(state, name) {
    const filtersTypeArray = [
      'shipTypes',
      'alerts',
      'portsOfCall',
      'lastPort',
      'listToCheckType',
    ]
    const filtersTypeBoolean = ['localMovements']
    if (filtersTypeArray.includes(name)) {
      state.filterParams[name] = []
    } else if (filtersTypeBoolean.includes(name)) {
      state.filterParams[name] = false
    } else {
      state.filterParams[name] = ''
    }
  },
  resetAllFilters(state) {
    state.filterParams = {
      way: '',
      search: '',
      shipTypes: [],
      portsOfCall: [],
      alerts: [],
      lastPort: [],
      localMovements: false,
      dateAfter: '',
      dateBefore: '',
      dateSuggestion: '',
      listToCheckType: [],
    }
  },
  resetShips(state) {
    state.list = []
  },
  resetCounterShips(state) {
    state.counterShips = {
      total: '',
      alerts: '',
    }
  },
}

const actions = {
  resetBeforeFetch({ commit }) {
    commit('setCurrentPage', 0)
    commit('resetShips')
    commit('setLoading', true)
    shipService.emptyCache()
    commit('resetCounterShips')
  },
  async fetchShips({ commit, state }) {
    const urlParams = new URLSearchParams()
    urlParams.append('page', state.currentPage + 1)
    urlParams.append('search', state.filterParams.search)
    urlParams.append('way', state.filterParams.way)
    for (let i = 0; i < state.filterParams.shipTypes.length; i++) {
      const value = state.filterParams.shipTypes[i]
      urlParams.append('ship_types', value)
    }
    for (let i = 0; i < state.filterParams.portsOfCall.length; i++) {
      const value = state.filterParams.portsOfCall[i]
      urlParams.append('ports_of_call', value)
    }
    for (let i = 0; i < state.filterParams.lastPort.length; i++) {
      const value = state.filterParams.lastPort[i]
      urlParams.append('last_port', value)
    }
    if (state.filterParams.alerts.length > 0) {
      state.filterParams.alerts.forEach((value) => {
        urlParams.append('alerts', value)
      })
    }
    urlParams.append('local_movements', state.filterParams.localMovements)
    urlParams.append('date_before', state.filterParams.dateBefore)
    urlParams.append('date_after', state.filterParams.dateAfter)
    for (let i = 0; i < state.filterParams.listToCheckType.length; i++) {
      const value = state.filterParams.listToCheckType[i]
      urlParams.append('list_to_check_type', value)
    }

    commit('setLoading', true)
    await shipService.fetchShips(urlParams).then((shipsData) => {
      commit('setShips', shipsData.results)
      commit('setCountPages', shipsData.count_pages)
      commit('setCounterShips', {
        total: shipsData.count,
        alerts: shipsData.count_ships_alerts,
      })
      commit('setLoading', false)
    })
  },
  async fetchShip({ commit }, id) {
    await shipService.fetchShip(id).then((ship) => {
      commit('updateShip', ship)
    })
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
