import schoolTripsService from '@/services/schoolTripsService'

const state = {
  list: [],
  show: false,
}

const mutations = {
  setList(state, list) {
    state.list = list
  },
  setShow(state, show) {
    state.show = show
  },
  updateSchoolTrip(state, schoolTrip) {
    state.list = state.list.map((element) => {
      if (element.id === schoolTrip.id) element = schoolTrip
      return element
    })
  },
}

const actions = {
  async fetchList({ commit }) {
    await schoolTripsService.getSchoolTrips().then((list) => {
      commit('setList', list)
    })
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
}
