const state = {
  sipDashboard: null,
  manualUploadFal: null,
  fprDownload: null,
  rocDownload: null,
  fprTracking: null,
  rocTracking: null,
  hasLoaded: false,
  ports: null,
  id: null,
  team: null,
  timezone: null,
  email: null,
  historicDuration: null,
  metabaseUsageUrl: null,
  metabaseNationalitiesUrl: null,
  isTeamManager: false,
  teamNotificationEmail: null,
  imoWatchlist: [],
}

const getters = {
  portsName: (state) => {
    return state.portsName
  },
  manualUploadFal: (state) => {
    return state.manualUploadFal
  },
  getImoWatchlist: (state) => {
    return state.imoWatchlist
  },
}

const mutations = {
  changeAllInformations(state, user) {
    const {
      sipDashboard,
      manualUploadFal,
      fprDownload,
      rocDownload,
      fprTracking,
      rocTracking,
      ports,
      id,
      team,
      timezone,
      email,
      historicDuration,
      metabaseUsageUrl,
      metabaseNationalitiesUrl,
      isTeamManager,
      teamNotificationEmail,
    } = user
    state.sipDashboard = sipDashboard
    state.manualUploadFal = manualUploadFal
    state.fprDownload = fprDownload
    state.fprTracking = fprTracking
    state.rocTracking = rocTracking
    state.rocDownload = rocDownload
    state.ports = ports.sort((a, b) => a.name > b.name)
    state.hasLoaded = true
    state.id = id
    state.team = team
    state.timezone = timezone
    state.email = email
    state.historicDuration = historicDuration
    state.metabaseUsageUrl = metabaseUsageUrl
    state.metabaseNationalitiesUrl = metabaseNationalitiesUrl
    state.isTeamManager = isTeamManager
    state.teamNotificationEmail = teamNotificationEmail
  },
  changeTeamNotificationEmail(state, email) {
    state.teamNotificationEmail = email
  },
  changeImoWatchlist(state, imoWatchlist) {
    state.imoWatchlist = imoWatchlist
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
}
