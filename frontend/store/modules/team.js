import teamService from '@/services/teamService'

const state = {
  name: '',
  ports: [],
  manager: '',
  timezone: '',
  historicDuration: '',
  users: [],
  usersCount: '',
}

const mutations = {
  setSettings(state, settings) {
    const { name, ports, manager, timezone, historic_duration } = settings
    state.name = name
    state.ports = ports
    state.manager = manager
    state.timezone = timezone
    state.historicDuration = historic_duration
  },
  setUsers(state, data) {
    const { users, count } = data
    state.users = users
    state.usersCount = count
  },
}

const actions = {
  async fetchSettings({ commit }) {
    await teamService.getSettings().then((settings) => {
      commit('setSettings', settings)
    })
  },
  async fetchUsers({ commit }) {
    await teamService.getUsers().then((data) => {
      commit('setUsers', data)
    })
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
}
