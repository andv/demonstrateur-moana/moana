import faqService from '@/services/faqService'

const state = {
  list: [],
}

const mutations = {
  setList(state, list) {
    state.list = list
  },
}

const actions = {
  async fetchList({ commit }) {
    await faqService.getFaq().then((list) => {
      commit('setList', list)
    })
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
}
