import { createStore } from 'vuex'
import ships from './modules/ships'
import scheme from './modules/scheme'
import user from './modules/user'
import schoolTrips from './modules/schoolTrips'
import faq from './modules/faq'
import team from './modules/team'

export default createStore({
  modules: {
    ships,
    scheme,
    user,
    schoolTrips,
    faq,
    team,
  },
})
