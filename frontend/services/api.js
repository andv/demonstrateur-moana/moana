import axios from 'axios'
import Cookies from 'js-cookie'

export default axios.create({
  baseURL: import.meta.env.VITE_API_ROOT || '/api',
  timeout: 5000,
  headers: {
    'Content-Type': 'application/json',
    'X-CSRFToken': Cookies.get('csrftoken'),
  },
})
