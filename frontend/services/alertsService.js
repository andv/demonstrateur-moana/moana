import api from '@/services/api'

export default {
  getWatchlist() {
    return api
      .get('alerts/watchlist/', { cache: false })
      .then((response) => response.data)
  },
  updateEmailNotification(email) {
    return api
      .patch('alerts-config/notification-email/', { notification_email: email })
      .then((response) => response.data)
  },
  deleteImo(imoNumber) {
    return api
      .delete(`/alerts-config/${imoNumber}/delete-imo/`)
      .then((response) => response.data)
  },
  addImo(list) {
    return api
      .post('/alerts-config/add-imo-list/', { imo_list: list })
      .then((response) => response.data)
  },
}
