import api from '@/services/api'

export default {
  add(payload) {
    const data = {
      action: payload.action || '',
      actor: '',
      team: '',
      object: payload.object || '',
      target: payload.target || '',
      description: payload.description || '',
      data: payload.data || '',
    }
    return api.post('tracking/', data).then((response) => response.data)
  },
}
