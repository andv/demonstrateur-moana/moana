import api from '@/services/api'

export default {
  getSchoolTrips() {
    return api.get('school-trips/').then((response) => response.data)
  },
  checkList(listId, payload) {
    return api
      .post(`school-trips/${listId}/update-status/`, payload)
      .then((response) => response.data)
  },
}
