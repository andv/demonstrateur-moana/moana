import api from '@/services/api'
import { setupCache } from 'axios-cache-interceptor'

const apiCached = setupCache(api)
let requestsId = []

export default {
  saveRequestId(id) {
    const isAlreadyCached = requestsId.indexOf(id) >= 0
    if (!isAlreadyCached) requestsId.push(id)
  },
  fetchShips(params = {}) {
    const id = params.toString()
    this.saveRequestId(id)
    return apiCached
      .get('ships/', {
        params,
        cache: true,
        id,
      })
      .then((response) => {
        return response.data
      })
  },
  fetchShip(shipMovementId) {
    return apiCached
      .get(`ships/${shipMovementId}`, { cache: false })
      .then((response) => response.data)
  },
  markAsChecked(shipMovementId, payload) {
    return api
      .post(`ships/${shipMovementId}/mark-as-checked/`, payload)
      .then((response) => response.data)
  },
  deleteAlert(alertId) {
    return api
      .post(`alerts/${alertId}/stop-showing/`)
      .then((response) => response.data)
  },
  emptyCache() {
    for (let i = 0; i < requestsId.length; i++) {
      apiCached.storage.remove(requestsId[i])
    }
    requestsId = []
  },
}
