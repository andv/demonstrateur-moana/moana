import api from '@/services/api'

export default {
  reportShip(shipMovementId, payload) {
    return api
      .post(`ships/${shipMovementId}/report-issue/`, payload)
      .then((response) => response.data)
  },
  reportMissingShip(payload) {
    return api
      .post('issues/report-missing-ship/', payload)
      .then((response) => response.data)
  },
  getMissingShipIssues() {
    return api
      .get('issues/?issue_type=issue-missing-ship')
      .then((response) => response.data)
  },
}
