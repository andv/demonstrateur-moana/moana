import api from '@/services/api'

export default {
  getSettings() {
    return api.get('team/settings').then((response) => response.data)
  },
  getUsers() {
    return api.get('team/users').then((response) => response.data)
  },
}
