import * as Sentry from '@sentry/vue'

export default {
  sentryError(props) {
    const { component, message, user } = props
    Sentry.setContext('App info', {
      component: component,
      userId: user,
    })
    Sentry.captureException(new Error(message))
  },
}
