import api from '@/services/api'

export default {
  getCurrentUser() {
    return api.get('users/current-user/').then((response) => response.data)
  },
  getTeamManager() {
    return api.get('users/team-manager/').then((response) => response.data)
  },
}
