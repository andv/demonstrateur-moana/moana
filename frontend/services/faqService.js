import api from '@/services/api'

export default {
  getFaq() {
    return api.get('faq/').then((response) => response.data)
  },
}
