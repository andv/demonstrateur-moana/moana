import { DateTime } from 'luxon'

const options = {
  full: {
    weekday: 'long',
    year: 'numeric',
    month: 'short',
    day: 'numeric',
    minute: 'numeric',
    hour: 'numeric',
  },
  dateSlash: {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
  },
  date: {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  },
  shortMonth: {
    day: 'numeric',
    month: 'short',
  },
  time: {
    minute: 'numeric',
    hour: 'numeric',
  },
  noYear: {
    month: 'short',
    day: 'numeric',
  },
  dayAndMonth: {
    month: 'numeric',
    day: 'numeric',
  },
  dayMonthTime: {
    month: 'numeric',
    day: 'numeric',
    minute: 'numeric',
    hour: 'numeric',
  },
}

export default {
  format(value, format, timezone) {
    if (format === 'api') {
      const day = value.day
      const month = value.month
      const year = value.year
      return `${year}-${month}-${day}`
    } else if (format === 'pretty') {
      return value.setLocale('fr').toLocaleString(options['noYear'])
    } else {
      const date = DateTime.fromMillis(Date.parse(value))
      return date
        .setZone(timezone)
        .setLocale('fr')
        .toLocaleString(options[format])
    }
  },
  generate(timezone) {
    const today = DateTime.now()
      .setZone(timezone)
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
    const tomorrow = today.plus({ days: 1 })
    const yesterday = today.minus({ days: 1 })
    const sevenDaysAfter = today.plus({ days: 6 })
    const fourteenDaysAfter = today.plus({ days: 13 })
    const todayDate = today.day
    const nowDateTime = DateTime.now().setZone(timezone)

    return {
      today,
      tomorrow,
      yesterday,
      sevenDaysAfter,
      fourteenDaysAfter,
      todayDate,
      nowDateTime,
    }
  },
  areSame(dateA, dateB, timezone) {
    dateA = this.parseToDatetime(dateA, timezone)
    dateB = this.parseToDatetime(dateB, timezone)
    const isSameDay = dateA.day === dateB.day
    const isSameMonth = dateA.month === dateB.month
    const isSameYear = dateA.year === dateB.year
    return isSameDay && isSameMonth && isSameYear
  },
  generateHistoricMaxDay(historicDuration, timezone) {
    const historicMaxDay = DateTime.now()
      .setZone(timezone)
      .minus({ days: historicDuration })
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
    return historicMaxDay
  },
  parseToDatetime(value, timezone) {
    const isAlreadyDateTime = value instanceof DateTime
    if (isAlreadyDateTime) return value.setZone(timezone)
    else return DateTime.fromMillis(Date.parse(value)).setZone(timezone)
  },
}
