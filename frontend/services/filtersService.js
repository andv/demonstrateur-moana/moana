import countriesLocode from '@/plugins/countries-locode'

/*
For options object structure, if it's :
- multi select choice use {label, name}
- radio choice use {label, value}
*/

const wayOptions = [
  {
    label: 'Arrivées',
    value: 'arriving',
  },
  {
    label: 'Départs',
    value: 'departing',
  },
]

const shipTypesOptions = [
  {
    label: 'Croisière',
    name: 'cruise',
  },
  {
    label: 'Cargo',
    name: 'cargo',
  },
  {
    label: 'Ferry',
    name: 'ferry',
  },
  {
    label: 'Autres',
    name: 'other',
  },
]

const listCountries = import.meta.env.VITE_ALERT_SHIP_COUNTRIES
  ? import.meta.env.VITE_ALERT_SHIP_COUNTRIES.replace(',', ' ou ')
  : 'Aucun'

const alertsOptions = [
  {
    label: `Pavillons sensibles (${listCountries})`,
    name: 'ship-country',
  },
  {
    label: 'IMO sensibles',
    name: 'ship-watchlist',
  },
]

const localMovementsOptions = {
  hint: 'Ces mouvements n’étant pas soumis aux obligations déclaratives les listes ne sont pas téléchargeables.',
  label: 'Afficher les mouvements locaux',
}

const getPersonChecksTypeOptions = (fprTracking, rocTracking) => {
  const listToCheckTypeOptions = []
  if (fprTracking) {
    listToCheckTypeOptions.push({
      label: 'Afficher les listes FPR à traiter',
      name: 'fpr',
    })
  }
  if (rocTracking) {
    listToCheckTypeOptions.push({
      label: 'Afficher les listes ROC à traiter',
      name: 'roc',
    })
  }
  return listToCheckTypeOptions
}

const getTeamPortsOfCall = (ports) => {
  const portsOfCall = []
  ports.forEach((port) => {
    portsOfCall.push({
      label: port.name,
      name: port.locode,
    })
  })
  return portsOfCall
}

const getCountriesLocode = () => {
  const countries = []
  countriesLocode.forEach((country) => {
    countries.push({
      label: `${country.french} (${country.locode})`,
      name: country.locode,
    })
  })
  return countries
}

const getOptions = (props) => {
  const { ports, name, fprTracking, rocTracking } = props
  switch (true) {
    case name === 'way':
      return wayOptions
    case name === 'shipTypes':
      return shipTypesOptions
    case name === 'portsOfCall':
      return getTeamPortsOfCall(ports)
    case name === 'lastPort':
      return getCountriesLocode(ports)
    case name === 'alerts':
      return alertsOptions
    case name === 'localMovements':
      return localMovementsOptions
    case name === 'listToCheckType':
      return getPersonChecksTypeOptions(fprTracking, rocTracking)
  }
}

export default { getOptions }
