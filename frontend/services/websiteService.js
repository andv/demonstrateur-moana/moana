import api from '@/services/api'

export default {
  getPages() {
    return api.get('pages/').then((response) => response.data)
  },
  getPage(page) {
    return api.get(`pages/${page}`).then((response) => response.data)
  },
}
