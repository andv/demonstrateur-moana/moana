const countriesLocode = [
  {
    locode: 'AF',
    english: 'Afghanistan',
    french: 'Afghanistan',
  },
  {
    locode: 'ZA',
    english: 'South Africa',
    french: 'Afrique du Sud',
  },
  {
    locode: 'AL',
    english: 'Albania',
    french: 'Albanie',
  },
  {
    locode: 'DZ',
    english: 'Algeria',
    french: 'Algérie',
  },
  {
    locode: 'DE',
    english: 'Germany',
    french: 'Allemagne',
  },
  {
    locode: 'AD',
    english: 'Andorra',
    french: 'Andorre',
  },
  {
    locode: 'AO',
    english: 'Angola',
    french: 'Angola',
  },
  {
    locode: 'AI',
    english: 'Anguilla',
    french: 'Anguilla',
  },
  {
    locode: 'AQ',
    english: 'Antarctica',
    french: 'Antarctique',
  },
  {
    locode: 'AG',
    english: 'Antigua and Barbuda',
    french: 'Antigua-et-Barbuda',
  },
  {
    locode: 'SA',
    english: 'Saudi Arabia',
    french: 'Arabie Saoudite',
  },
  {
    locode: 'AR',
    english: 'Argentina',
    french: 'Argentine',
  },
  {
    locode: 'AM',
    english: 'Armenia',
    french: 'Arménie',
  },
  {
    locode: 'AW',
    english: 'Aruba',
    french: 'Aruba',
  },
  {
    locode: 'AU',
    english: 'Australia',
    french: 'Australie',
  },
  {
    locode: 'AT',
    english: 'Austria',
    french: 'Autriche',
  },
  {
    locode: 'AZ',
    english: 'Azerbaijan',
    french: 'Azerbaïdjan',
  },
  {
    locode: 'BS',
    english: 'Bahamas',
    french: 'Bahamas',
  },
  {
    locode: 'BH',
    english: 'Bahrain',
    french: 'Bahreïn',
  },
  {
    locode: 'BD',
    english: 'Bangladesh',
    french: 'Bangladesh',
  },
  {
    locode: 'BB',
    english: 'Barbados',
    french: 'Barbade',
  },
  {
    locode: 'BE',
    english: 'Belgium',
    french: 'Belgique',
  },
  {
    locode: 'BZ',
    english: 'Belize',
    french: 'Belize',
  },
  {
    locode: 'BJ',
    english: 'Benin',
    french: 'Bénin',
  },
  {
    locode: 'BM',
    english: 'Bermuda',
    french: 'Bermudes',
  },
  {
    locode: 'BT',
    english: 'Bhutan',
    french: 'Bhoutan',
  },
  {
    locode: 'BY',
    english: 'Belarus',
    french: 'Biélorussie',
  },
  {
    locode: 'BO',
    english: 'Bolivia',
    french: 'Bolivie',
  },
  {
    locode: 'BQ',
    english: 'Bonaire',
    french: 'Bonaire',
  },
  {
    locode: 'BA',
    english: 'Bosnia and Herzegovina',
    french: 'Bosnie-Herzégovine',
  },
  {
    locode: 'BW',
    english: 'Botswana',
    french: 'Botswana',
  },
  {
    locode: 'BR',
    english: 'Brazil',
    french: 'Brésil',
  },
  {
    locode: 'BN',
    english: 'Brunei Darussalam',
    french: 'Brunéi Darussalam',
  },
  {
    locode: 'BG',
    english: 'Bulgaria',
    french: 'Bulgarie',
  },
  {
    locode: 'BF',
    english: 'Burkina Faso',
    french: 'Burkina Faso',
  },
  {
    locode: 'BI',
    english: 'Burundi',
    french: 'Burundi',
  },
  {
    locode: 'KH',
    english: 'Cambodia',
    french: 'Cambodge',
  },
  {
    locode: 'CM',
    english: 'Cameroon',
    french: 'Cameroun',
  },
  {
    locode: 'CA',
    english: 'Canada',
    french: 'Canada',
  },
  {
    locode: 'CV',
    english: 'Cape Verde',
    french: 'Cap-Vert',
  },
  {
    locode: 'CL',
    english: 'Chile',
    french: 'Chili',
  },
  {
    locode: 'CN',
    english: 'China',
    french: 'Chine',
  },
  {
    locode: 'CY',
    english: 'Cyprus',
    french: 'Chypre',
  },
  {
    locode: 'CO',
    english: 'Colombia',
    french: 'Colombie',
  },
  {
    locode: 'KM',
    english: 'Comoros',
    french: 'Comores',
  },
  {
    locode: 'CG',
    english: 'Congo',
    french: 'Congo',
  },
  {
    locode: 'KP',
    english: 'Korea',
    french: 'Corée',
  },
  {
    locode: 'KR',
    english: 'Korea',
    french: 'Corée',
  },
  {
    locode: 'CR',
    english: 'Costa Rica',
    french: 'Costa Rica',
  },
  {
    locode: 'CI',
    english: "Côte d'Ivoire",
    french: "Côte d'Ivoire",
  },
  {
    locode: 'HR',
    english: 'Croatia',
    french: 'Croatie',
  },
  {
    locode: 'CU',
    english: 'Cuba',
    french: 'Cuba',
  },
  {
    locode: 'CW',
    english: 'Curaçao',
    french: 'Curaçao',
  },
  {
    locode: 'DK',
    english: 'Denmark',
    french: 'Danemark',
  },
  {
    locode: 'DJ',
    english: 'Djibouti',
    french: 'Djibouti',
  },
  {
    locode: 'DM',
    english: 'Dominica',
    french: 'Dominique',
  },
  {
    locode: 'EG',
    english: 'Egypt',
    french: 'Égypte',
  },
  {
    locode: 'AE',
    english: 'United Arab Emirates',
    french: 'Émirats arabes unis',
  },
  {
    locode: 'EC',
    english: 'Ecuador',
    french: 'Équateur',
  },
  {
    locode: 'ER',
    english: 'Eritrea',
    french: 'Érythrée',
  },
  {
    locode: 'ES',
    english: 'Spain',
    french: 'Espagne',
  },
  {
    locode: 'EE',
    english: 'Estonia',
    french: 'Estonie',
  },
  {
    locode: 'SZ',
    english: 'Eswatini',
    french: 'Eswatini',
  },
  {
    locode: 'ET',
    english: 'Ethiopia',
    french: 'Éthiopie',
  },
  {
    locode: 'RU',
    english: 'Russian Federation',
    french: 'Fédération de Russie',
  },
  {
    locode: 'FJ',
    english: 'Fiji',
    french: 'Fidji',
  },
  {
    locode: 'FI',
    english: 'Finland',
    french: 'Finlande',
  },
  {
    locode: 'FR',
    english: 'France',
    french: 'France',
  },
  {
    locode: 'GA',
    english: 'Gabon',
    french: 'Gabon',
  },
  {
    locode: 'GM',
    english: 'Gambia',
    french: 'Gambie',
  },
  {
    locode: 'GE',
    english: 'Georgia',
    french: 'Géorgie',
  },
  {
    locode: 'GS',
    english: 'South Georgia and the South Sandwich Islands',
    french: 'Géorgie du Sud et les îles Sandwich du Sud',
  },
  {
    locode: 'GH',
    english: 'Ghana',
    french: 'Ghana',
  },
  {
    locode: 'GI',
    english: 'Gibraltar',
    french: 'Gibraltar',
  },
  {
    locode: 'GR',
    english: 'Greece',
    french: 'Grèce',
  },
  {
    locode: 'GD',
    english: 'Grenada',
    french: 'Grenade',
  },
  {
    locode: 'GL',
    english: 'Greenland',
    french: 'Groenland',
  },
  {
    locode: 'GP',
    english: 'Guadeloupe',
    french: 'Guadeloupe',
  },
  {
    locode: 'GU',
    english: 'Guam',
    french: 'Guam',
  },
  {
    locode: 'GT',
    english: 'Guatemala',
    french: 'Guatemala',
  },
  {
    locode: 'GG',
    english: 'Guernsey',
    french: 'Guernesey',
  },
  {
    locode: 'GN',
    english: 'Guinea',
    french: 'Guinée',
  },
  {
    locode: 'GQ',
    english: 'Equatorial Guinea',
    french: 'Guinée équatoriale',
  },
  {
    locode: 'GW',
    english: 'Guinea-Bissau',
    french: 'Guinée-Bissau',
  },
  {
    locode: 'GY',
    english: 'Guyana',
    french: 'Guyana',
  },
  {
    locode: 'GF',
    english: 'French Guiana',
    french: 'Guyane française',
  },
  {
    locode: 'HT',
    english: 'Haiti',
    french: 'Haïti',
  },
  {
    locode: 'HN',
    english: 'Honduras',
    french: 'Honduras',
  },
  {
    locode: 'HK',
    english: 'Hong Kong',
    french: 'Hong Kong',
  },
  {
    locode: 'HU',
    english: 'Hungary',
    french: 'Hongrie',
  },
  {
    locode: 'CX',
    english: 'Christmas Island',
    french: 'Île Christmas',
  },
  {
    locode: 'IM',
    english: 'Isle of Man',
    french: 'Île de Man',
  },
  {
    locode: 'HM',
    english: 'Heard Island and McDonald Islands',
    french: 'Île Heard et îles McDonald',
  },
  {
    locode: 'NF',
    english: 'Norfolk Island',
    french: 'Île Norfolk',
  },
  {
    locode: 'AX',
    english: 'Åland Islands',
    french: 'Îles Åland',
  },
  {
    locode: 'KY',
    english: 'Cayman Islands',
    french: 'Îles Caïmans',
  },
  {
    locode: 'CC',
    english: 'Cocos Islands',
    french: 'Îles Cocos',
  },
  {
    locode: 'CK',
    english: 'Cook Islands',
    french: 'Îles Cook',
  },
  {
    locode: 'FO',
    english: 'Faroe Islands',
    french: 'Îles Féroé',
  },
  {
    locode: 'FK',
    english: 'Falkland Islands',
    french: 'Îles Malouines',
  },
  {
    locode: 'MP',
    english: 'Northern Mariana Islands',
    french: 'Îles Mariannes du Nord',
  },
  {
    locode: 'MH',
    english: 'Marshall Islands',
    french: 'Îles Marshall',
  },
  {
    locode: 'UM',
    english: 'United States Minor Outlying Islands',
    french: 'Îles mineures éloignées des États-Unis',
  },
  {
    locode: 'SB',
    english: 'Solomon Islands',
    french: 'Îles Salomon',
  },
  {
    locode: 'TC',
    english: 'Turks and Caicos Islands',
    french: 'Îles Turques-et-Caïques',
  },
  {
    locode: 'VG',
    english: 'Virgin Islands',
    french: 'Îles Vierges',
  },
  {
    locode: 'VI',
    english: 'Virgin Islands',
    french: 'Îles Vierges',
  },
  {
    locode: 'IN',
    english: 'India',
    french: 'Inde',
  },
  {
    locode: 'ID',
    english: 'Indonesia',
    french: 'Indonésie',
  },
  {
    locode: 'XZ',
    english: 'Installations in International Waters',
    french: 'Installations en eaux internationales',
  },
  {
    locode: 'IQ',
    english: 'Iraq',
    french: 'Irak',
  },
  {
    locode: 'IR',
    english: 'Iran',
    french: 'Iran',
  },
  {
    locode: 'IE',
    english: 'Ireland',
    french: 'Irlande',
  },
  {
    locode: 'IS',
    english: 'Iceland',
    french: 'Islande',
  },
  {
    locode: 'IL',
    english: 'Israel',
    french: 'Israël',
  },
  {
    locode: 'IT',
    english: 'Italy',
    french: 'Italie',
  },
  {
    locode: 'JM',
    english: 'Jamaica',
    french: 'Jamaïque',
  },
  {
    locode: 'JP',
    english: 'Japan',
    french: 'Japon',
  },
  {
    locode: 'JE',
    english: 'Jersey',
    french: 'Jersey',
  },
  {
    locode: 'JO',
    english: 'Jordan',
    french: 'Jordanie',
  },
  {
    locode: 'KZ',
    english: 'Kazakhstan',
    french: 'Kazakhstan',
  },
  {
    locode: 'KE',
    english: 'Kenya',
    french: 'Kenya',
  },
  {
    locode: 'KG',
    english: 'Kyrgyzstan',
    french: 'Kirghizistan',
  },
  {
    locode: 'KI',
    english: 'Kiribati',
    french: 'Kiribati',
  },
  {
    locode: 'KW',
    english: 'Kuwait',
    french: 'Koweït',
  },
  {
    locode: 'LS',
    english: 'Lesotho',
    french: 'Lesotho',
  },
  {
    locode: 'LV',
    english: 'Latvia',
    french: 'Lettonie',
  },
  {
    locode: 'LB',
    english: 'Lebanon',
    french: 'Liban',
  },
  {
    locode: 'LR',
    english: 'Liberia',
    french: 'Libéria',
  },
  {
    locode: 'LY',
    english: 'Libya',
    french: 'Libye',
  },
  {
    locode: 'LI',
    english: 'Liechtenstein',
    french: 'Liechtenstein',
  },
  {
    locode: 'LT',
    english: 'Lithuania',
    french: 'Lituanie',
  },
  {
    locode: 'LU',
    english: 'Luxembourg',
    french: 'Luxembourg',
  },
  {
    locode: 'MO',
    english: 'Macao',
    french: 'Macao',
  },
  {
    locode: 'MK',
    english: 'North Macedonia',
    french: 'Macédoine du Nord',
  },
  {
    locode: 'MG',
    english: 'Madagascar',
    french: 'Madagascar',
  },
  {
    locode: 'MY',
    english: 'Malaysia',
    french: 'Malaisie',
  },
  {
    locode: 'MW',
    english: 'Malawi',
    french: 'Malawi',
  },
  {
    locode: 'MV',
    english: 'Maldives',
    french: 'Maldives',
  },
  {
    locode: 'ML',
    english: 'Mali',
    french: 'Mali',
  },
  {
    locode: 'MT',
    english: 'Malta',
    french: 'Malte',
  },
  {
    locode: 'MA',
    english: 'Morocco',
    french: 'Maroc',
  },
  {
    locode: 'MQ',
    english: 'Martinique',
    french: 'Martinique',
  },
  {
    locode: 'MU',
    english: 'Mauritius',
    french: 'Maurice',
  },
  {
    locode: 'MR',
    english: 'Mauritania',
    french: 'Mauritanie',
  },
  {
    locode: 'YT',
    english: 'Mayotte',
    french: 'Mayotte',
  },
  {
    locode: 'MX',
    english: 'Mexico',
    french: 'Mexique',
  },
  {
    locode: 'FM',
    english: 'Micronesia',
    french: 'Micronésie',
  },
  {
    locode: 'MD',
    english: 'Moldova',
    french: 'Moldavie',
  },
  {
    locode: 'MC',
    english: 'Monaco',
    french: 'Monaco',
  },
  {
    locode: 'MN',
    english: 'Mongolia',
    french: 'Mongolie',
  },
  {
    locode: 'ME',
    english: 'Montenegro',
    french: 'Monténégro',
  },
  {
    locode: 'MS',
    english: 'Montserrat',
    french: 'Montserrat',
  },
  {
    locode: 'MZ',
    english: 'Mozambique',
    french: 'Mozambique',
  },
  {
    locode: 'MM',
    english: 'Myanmar',
    french: 'Myanmar',
  },
  {
    locode: 'NA',
    english: 'Namibia',
    french: 'Namibie',
  },
  {
    locode: 'NR',
    english: 'Nauru',
    french: 'Nauru',
  },
  {
    locode: 'NP',
    english: 'Nepal',
    french: 'Népal',
  },
  {
    locode: 'NI',
    english: 'Nicaragua',
    french: 'Nicaragua',
  },
  {
    locode: 'NE',
    english: 'Niger',
    french: 'Niger',
  },
  {
    locode: 'NG',
    english: 'Nigeria',
    french: 'Nigéria',
  },
  {
    locode: 'NU',
    english: 'Niue',
    french: 'Niue',
  },
  {
    locode: 'NO',
    english: 'Norway',
    french: 'Norvège',
  },
  {
    locode: 'NC',
    english: 'New Caledonia',
    french: 'Nouvelle-Calédonie',
  },
  {
    locode: 'NZ',
    english: 'New Zealand',
    french: 'Nouvelle-Zélande',
  },
  {
    locode: 'OM',
    english: 'Oman',
    french: 'Oman',
  },
  {
    locode: 'UG',
    english: 'Uganda',
    french: 'Ouganda',
  },
  {
    locode: 'UZ',
    english: 'Uzbekistan',
    french: 'Ouzbékistan',
  },
  {
    locode: 'PK',
    english: 'Pakistan',
    french: 'Pakistan',
  },
  {
    locode: 'PW',
    english: 'Palau',
    french: 'Palaos',
  },
  {
    locode: 'PS',
    english: 'Palestine',
    french: 'Palestine',
  },
  {
    locode: 'PA',
    english: 'Panama',
    french: 'Panama',
  },
  {
    locode: 'PG',
    english: 'Papua New Guinea',
    french: 'Papouasie-Nouvelle-Guinée',
  },
  {
    locode: 'PY',
    english: 'Paraguay',
    french: 'Paraguay',
  },
  {
    locode: 'NL',
    english: 'Netherlands',
    french: 'Pays-Bas',
  },
  {
    locode: 'PE',
    english: 'Peru',
    french: 'Pérou',
  },
  {
    locode: 'PH',
    english: 'Philippines',
    french: 'Philippines',
  },
  {
    locode: 'PN',
    english: 'Pitcairn',
    french: 'Pitcairn',
  },
  {
    locode: 'PL',
    english: 'Poland',
    french: 'Pologne',
  },
  {
    locode: 'PF',
    english: 'French Polynesia',
    french: 'Polynésie française',
  },
  {
    locode: 'PR',
    english: 'Puerto Rico',
    french: 'Porto Rico',
  },
  {
    locode: 'PT',
    english: 'Portugal',
    french: 'Portugal',
  },
  {
    locode: 'QA',
    english: 'Qatar',
    french: 'Qatar',
  },
  {
    locode: 'SY',
    english: 'Syrian Arab Republic',
    french: 'République arabe syrienne',
  },
  {
    locode: 'CF',
    english: 'Central African Republic',
    french: 'République centrafricaine',
  },
  {
    locode: 'LA',
    english: "Lao People's Democratic Republic",
    french: 'République démocratique populaire lao',
  },
  {
    locode: 'DO',
    english: 'Dominican Republic',
    french: 'République dominicaine',
  },
  {
    locode: 'CZ',
    english: 'Czech Republic',
    french: 'République tchèque',
  },
  {
    locode: 'RE',
    english: 'Reunion',
    french: 'Réunion',
  },
  {
    locode: 'RO',
    english: 'Romania',
    french: 'Roumanie',
  },
  {
    locode: 'GB',
    english: 'United Kingdom',
    french: 'Royaume-Uni',
  },
  {
    locode: 'RW',
    english: 'Rwanda',
    french: 'Rwanda',
  },
  {
    locode: 'EH',
    english: 'Western Sahara',
    french: 'Sahara occidental',
  },
  {
    locode: 'BL',
    english: 'Saint Barthélemy',
    french: 'Saint-Barthélemy',
  },
  {
    locode: 'KN',
    english: 'Saint Kitts and Nevis',
    french: 'Saint-Christophe-et-Niévès',
  },
  {
    locode: 'SM',
    english: 'San Marino',
    french: 'Saint-Marin',
  },
  {
    locode: 'MF',
    english: 'Saint Martin',
    french: 'Saint-Martin',
  },
  {
    locode: 'SX',
    english: 'Sint Maarten',
    french: 'Saint-Martin',
  },
  {
    locode: 'PM',
    english: 'Saint Pierre and Miquelon',
    french: 'Saint-Pierre-et-Miquelon',
  },
  {
    locode: 'VA',
    english: 'Holy See',
    french: 'Saint-Siège (État de la Cité du Vatican)',
  },
  {
    locode: 'VC',
    english: 'Saint Vincent and the Grenadines',
    french: 'Saint-Vincent-et-les-Grenadines',
  },
  {
    locode: 'SH',
    english: 'Saint Helena',
    french: 'Sainte-Hélène',
  },
  {
    locode: 'LC',
    english: 'Saint Lucia',
    french: 'Sainte-Lucie',
  },
  {
    locode: 'SV',
    english: 'El Salvador',
    french: 'Salvador',
  },
  {
    locode: 'WS',
    english: 'Samoa',
    french: 'Samoa',
  },
  {
    locode: 'AS',
    english: 'American Samoa',
    french: 'Samoa américaines',
  },
  {
    locode: 'ST',
    english: 'Sao Tome and Principe',
    french: 'Sao Tomé-et-Principe',
  },
  {
    locode: 'SN',
    english: 'Senegal',
    french: 'Sénégal',
  },
  {
    locode: 'RS',
    english: 'Serbia',
    french: 'Serbie',
  },
  {
    locode: 'SC',
    english: 'Seychelles',
    french: 'Seychelles',
  },
  {
    locode: 'SL',
    english: 'Sierra Leone',
    french: 'Sierra Leone',
  },
  {
    locode: 'SG',
    english: 'Singapore',
    french: 'Singapour',
  },
  {
    locode: 'SK',
    english: 'Slovakia',
    french: 'Slovaquie',
  },
  {
    locode: 'SI',
    english: 'Slovenia',
    french: 'Slovénie',
  },
  {
    locode: 'SO',
    english: 'Somalia',
    french: 'Somalie',
  },
  {
    locode: 'SD',
    english: 'Sudan',
    french: 'Soudan',
  },
  {
    locode: 'SS',
    english: 'South Sudan',
    french: 'Soudan du Sud',
  },
  {
    locode: 'LK',
    english: 'Sri Lanka',
    french: 'Sri Lanka',
  },
  {
    locode: 'SE',
    english: 'Sweden',
    french: 'Suède',
  },
  {
    locode: 'CH',
    english: 'Switzerland',
    french: 'Suisse',
  },
  {
    locode: 'SR',
    english: 'Suriname',
    french: 'Surinam',
  },
  {
    locode: 'SJ',
    english: 'Svalbard and Jan Mayen',
    french: 'Svalbard et Jan Mayen',
  },
  {
    locode: 'TJ',
    english: 'Tajikistan',
    french: 'Tadjikistan',
  },
  {
    locode: 'TW',
    english: 'Taiwan',
    french: 'Taïwan',
  },
  {
    locode: 'TZ',
    english: 'Tanzania',
    french: 'Tanzanie',
  },
  {
    locode: 'TD',
    english: 'Chad',
    french: 'Tchad',
  },
  {
    locode: 'TF',
    english: 'French Southern Territories',
    french: 'Terres australes françaises',
  },
  {
    locode: 'IO',
    english: 'British Indian Ocean Territory',
    french: "Territoire britannique de l'océan Indien",
  },
  {
    locode: 'TH',
    english: 'Thailand',
    french: 'Thaïlande',
  },
  {
    locode: 'TL',
    english: 'Timor-Leste',
    french: 'Timor oriental',
  },
  {
    locode: 'TG',
    english: 'Togo',
    french: 'Togo',
  },
  {
    locode: 'TK',
    english: 'Tokelau',
    french: 'Tokelau',
  },
  {
    locode: 'TO',
    english: 'Tonga',
    french: 'Tonga',
  },
  {
    locode: 'TT',
    english: 'Trinidad and Tobago',
    french: 'Trinité-et-Tobago',
  },
  {
    locode: 'TN',
    english: 'Tunisia',
    french: 'Tunisie',
  },
  {
    locode: 'TM',
    english: 'Turkmenistan',
    french: 'Turkménistan',
  },
  {
    locode: 'TR',
    english: 'Türkiye',
    french: 'Turquie',
  },
  {
    locode: 'TV',
    english: 'Tuvalu',
    french: 'Tuvalu',
  },
  {
    locode: 'UA',
    english: 'Ukraine',
    french: 'Ukraine',
  },
  {
    locode: 'UY',
    english: 'Uruguay',
    french: 'Uruguay',
  },
  {
    locode: 'VU',
    english: 'Vanuatu',
    french: 'Vanuatu',
  },
  {
    locode: 'VE',
    english: 'Venezuela',
    french: 'Venezuela',
  },
  {
    locode: 'VN',
    english: 'Viet Nam',
    french: 'Vietnam',
  },
  {
    locode: 'WF',
    english: 'Wallis and Futuna',
    french: 'Wallis-et-Futuna',
  },
  {
    locode: 'YE',
    english: 'Yemen',
    french: 'Yémen',
  },
  {
    locode: 'ZM',
    english: 'Zambia',
    french: 'Zambie',
  },
  {
    locode: 'ZW',
    english: 'Zimbabwe',
    french: 'Zimbabwe',
  },
]

export default countriesLocode
