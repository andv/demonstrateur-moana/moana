const listIssuesType = [
  {
    name: 'Mouvements',
    list: [
      {
        value: 'issue-movement-bad-times',
        name: 'Problème horaires',
      },
      {
        value: 'issue-movement-bad-ship-type',
        name: 'Problème type de navire',
      },
      {
        value: 'issue-movement-bad-last-port',
        name: 'Problème port de provenance',
      },
      {
        value: 'issue-movement-bad-next-port',
        name: 'Problème port de destination',
      },
      {
        value: 'issue-movement-bad-anais-link',
        name: 'Lien ANAIS incorrect ou manquant',
      },
    ],
  },
  {
    name: 'Équipage',
    list: [
      {
        value: 'issue-crew-char-encoding',
        name: 'Problème encodage caractères',
      },
      {
        value: 'issue-crew-bad-date-of-birth',
        name: 'Problème date de naissance',
      },
      {
        value: 'issue-crew-name-switched',
        name: 'Problème nom/prénom',
      },
      {
        value: 'issue-crew-empty-fields',
        name: 'Champs vides',
      },
      {
        value: 'issue-crew-not-present',
        name: 'Liste non présente',
      },
      {
        value: 'issue-crew-received-late',
        name: 'Liste reçue en retard',
      },
      {
        value: 'issue-crew-identity-document',
        name: "Problème sur les documents d'identité",
      },
    ],
  },
  {
    name: 'Passagers',
    list: [
      {
        value: 'issue-passengers-char-encoding',
        name: 'Problème encodage caractères',
      },
      {
        value: 'issue-passengers-bad-date-of-birth',
        name: 'Problème date de naissance',
      },
      {
        value: 'issue-passengers-name-switched',
        name: 'Problème nom/prénom',
      },
      {
        value: 'issue-passengers-empty-fields',
        name: 'Champs vides',
      },
      {
        value: 'issue-passengers-not-present',
        name: 'Liste non présente',
      },
      {
        value: 'issue-passengers-received-late',
        name: 'Liste reçue en retard',
      },
      {
        value: 'issue-passengers-identity-document',
        name: "Problème sur les documents d'identité",
      },
    ],
  },
  {
    name: 'Autres',
    list: [{ value: 'issue-other', name: 'Autre problème' }],
  },
]

export { listIssuesType }
