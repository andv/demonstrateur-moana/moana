import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '@/views/HomePage.vue'
import LegalsPage from '@/views/LegalsPage.vue'
import SchoolTripsPage from '@/views/SchoolTripsPage.vue'
import StatisticsPage from '@/views/StatisticsPage.vue'
import AlertsPages from '@/views/AlertsPage.vue'
import FaqPage from '@/views/FaqPage.vue'
import TeamPage from '@/views/TeamPage.vue'
import TeamEmbedPage from '@/views/TeamEmbedPage.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Home',
      component: HomePage,
    },
    {
      path: '/pages/:name',
      name: 'Legals',
      component: LegalsPage,
    },
    {
      path: '/suivi-des-bus-scolaires',
      name: 'SchoolTrips',
      component: SchoolTripsPage,
    },
    {
      path: '/statistiques',
      name: 'StatisticsPage',
      component: StatisticsPage,
    },
    {
      path: '/gestion-des-alertes',
      name: 'Alerts',
      component: AlertsPages,
    },
    {
      path: '/centre-aide',
      name: 'FaqPage',
      component: FaqPage,
    },
    {
      path: '/mon-equipe',
      name: 'Team',
      component: TeamPage,
    },
    {
      path: '/mon-equipe/:name',
      name: 'TeamEmbedPage',
      component: TeamEmbedPage,
      beforeEnter: (to, from, next) => {
        const allowedPageNames = [
          'ajouter-des-utilisateurs',
          'supprimer-des-utilisateurs',
        ]
        if (allowedPageNames.includes(to.params.name)) next()
        else next({ name: 'Team' })
      },
    },
  ],
})

export default router
