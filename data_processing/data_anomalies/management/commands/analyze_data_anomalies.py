from django.core.management.base import BaseCommand
from django.utils import timezone

from backend.logs.models.action_logs import UserActionLog
from backend.logs.models.data_quality import DataQualityLog
from backend.person_checks import choices as person_checks_choices
from data_processing.data_anomalies import settings as data_anomalies_settings
from data_processing.data_anomalies.models import DataAnomaly
from data_processing.utils.management_commands import WithRunTask, WithShipData


class Command(WithShipData, WithRunTask, BaseCommand):
    help = "Run an analysis on data anomalies"
    lock_name = data_anomalies_settings.LOCK_NAME_DATA_ANOMALIES
    list_of_log_action_names = data_anomalies_settings.ACTIONS_DATA_ANOMALIES

    def run_analysis(self, cutoff_date):
        """
        Collects all action log entries and makes the corresponding
        analysis entry.
        """
        self.stdout.write("-- Start processing data anomalies.")
        qs = UserActionLog.objects.filter(
            action__in=data_anomalies_settings.ACTIONS_CREW_IMPORT,
            created__gt=cutoff_date,
            ship_data__isnull=False,
        )
        for import_log in qs:
            self.process_anomalies_from_import_actions(
                import_log=import_log,
                person_list_type=person_checks_choices.PERSON_LIST_TYPE_CREW,
            )
        qs = UserActionLog.objects.filter(
            action__in=data_anomalies_settings.ACTIONS_PASSENGER_IMPORT,
            created__gt=cutoff_date,
            ship_data__isnull=False,
        )
        for import_log in qs:
            self.process_anomalies_from_import_actions(
                import_log=import_log,
                person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
            )
        self.stdout.write("-- Finished processing data anomalies.")

    def get_anomalies_from_import_log(self, import_log, person_list_type):
        """
        Get anomalies from action logs.
        We look at the data quality logs and get the anomalies that are created
        right before the import. Why do we look before the import? Because in the
        current implementation, the data quality logs are created right before
        the import log is created.
        """
        if person_list_type == person_checks_choices.PERSON_LIST_TYPE_CREW:
            nca_type = "nca_fal5"
        if person_list_type == person_checks_choices.PERSON_LIST_TYPE_PASSENGER:
            nca_type = "nca_fal6"
        import_date = import_log.created
        a_bit_before_import_date = import_date - timezone.timedelta(seconds=3)
        qs = DataQualityLog.objects.filter(
            action__in=self.list_of_log_action_names,
            created__range=(a_bit_before_import_date, import_date),
            ship_data__ship_call_id=import_log.ship_data.ship_call_id,
            ship_data__way=import_log.ship_data.way,
            object=nca_type,
        )
        return qs

    def preset_anomaly_as_fixed_for_this_import(self, import_log, person_list_type):
        """
        Preset the anomalies as fixed for the given import.
        This is done by setting the anomaly count to 0, and it occurs as
        soon as the import is processed.
        We do this in a presumptive way, because we assume that the import
        could have fixed the anomaly. If that's not the case, even though we
        have set the counters to 0, the anomalies we be raised again and the
        counter will be incremented again, when we will process anomalies
        for that import.
        """
        anomaly = DataAnomaly.objects.filter(
            ship_call_id=import_log.ship_data.ship_call_id,
            way=import_log.ship_data.way,
            person_list_type=person_list_type,
        ).first()
        if not anomaly:
            return None
        for field in data_anomalies_settings.DATA_ANOMALY_FIELDS:
            anomaly_count = getattr(anomaly, field)
            if anomaly_count is not None and anomaly_count > 0:
                self.stdout.write(
                    f"---- Presumptively set '{field}' as fixed for : '{anomaly}'."
                )
                setattr(anomaly, field, 0)
        anomaly.save()

    def process_anomalies_from_import_actions(self, import_log, person_list_type):
        """
        Get anomalies from action logs and process them/
        """
        self.stdout.write(f"---- Looking at import action '{import_log.action}'.")
        self.preset_anomaly_as_fixed_for_this_import(import_log, person_list_type)
        anomalies_qs = self.get_anomalies_from_import_log(import_log, person_list_type)
        for anomaly_log in anomalies_qs:
            self.process_data_anomaly_log(anomaly_log=anomaly_log)
        count = anomalies_qs.count()
        self.stdout.write(f"---- Got {count} anomalies for '{import_log.action}'")

    def process_data_anomaly_log(self, anomaly_log):
        """
        Process the data anomaly logs before updating the tracking entry.
        """
        self.stdout.write(f"------ Found data anomaly '{anomaly_log.action}'.")
        ship_data = self.get_ship_data(anomaly_log)
        data = ship_data.copy()
        self.update_data_anomaly_table(anomaly_log, data)

    def get_number_of_anomaly_occurrences(self, anomaly_log):
        """
        Get the number of anomaly occurrences for the given anomaly log.
        When the target field is a digit, then we consider that it indicated
        the number of persons concerned by the anomaly.
        Otherwise, we consider that the anomaly is a single occurrence, that's
        the case for anomalies that are ship movement related, and not person
        list related.
        """
        number_of_anomaly_occurrences = 1
        if anomaly_log.target.isdigit():
            number_of_anomaly_occurrences = anomaly_log.target
        return number_of_anomaly_occurrences

    def update_data_anomaly_table(self, anomaly_log, data):
        """
        Creates or updates the data anomaly tracking entry for the given ship movement.
        """
        anomaly_field = anomaly_log.action.replace(" ", "_")
        data[anomaly_field] = self.get_number_of_anomaly_occurrences(anomaly_log)
        person_list_type = ""
        if anomaly_log.object == "nca_fal6":
            person_list_type = person_checks_choices.PERSON_LIST_TYPE_PASSENGER
        if anomaly_log.object == "nca_fal5":
            person_list_type = person_checks_choices.PERSON_LIST_TYPE_CREW
        DataAnomaly.objects.update_or_create(
            ship_call_id=anomaly_log.ship_data.ship_call_id,
            way=anomaly_log.ship_data.way,
            person_list_type=person_list_type,
            defaults=data,
        )
