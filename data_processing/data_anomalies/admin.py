from django.conf import settings
from django.contrib import admin

from trackman.admin import TrackingModelAdminMixin

from backend.logs.admin import SHIP_DATA_SEARCH_FIELDS
from data_processing.data_anomalies import settings as data_anomalies_settings
from data_processing.data_anomalies.models import DataAnomaly


class DataAnomalyAdmin(TrackingModelAdminMixin, admin.ModelAdmin):
    list_display = [
        "analysis_period",
        "ship_call_id",
        "person_list_type",
        "way",
    ] + data_anomalies_settings.DATA_ANOMALY_FIELDS
    search_fields = SHIP_DATA_SEARCH_FIELDS + ["analysis_period"]
    list_display_links = ["analysis_period", "ship_call_id", "person_list_type"]
    list_filter = ("analysis_period", "person_list_type")


if settings.TRACKMAN_ENABLED:
    admin.site.register(DataAnomaly, DataAnomalyAdmin)
