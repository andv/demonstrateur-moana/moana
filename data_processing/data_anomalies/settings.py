from backend.logs import settings as logs_settings
from data_processing.person_list import settings as person_list_settings


def get_field_name(action_name):
    """
    Get the field name from the given action name.
    """
    return action_name.replace(" ", "_").lower()


DATA_ANOMALY_FIELDS = [
    get_field_name(action_name) for action_name in logs_settings.DATA_ANOMALY_LIST
]

ACTIONS_DATA_ANOMALIES = logs_settings.DATA_ANOMALY_LIST.copy()
ACTIONS_CREW_IMPORT = person_list_settings.ACTIONS_CREW_IMPORT.copy()
ACTIONS_PASSENGER_IMPORT = person_list_settings.ACTIONS_PASSENGER_IMPORT.copy()

LOCK_NAME_DATA_ANOMALIES = "data-anomalies"
