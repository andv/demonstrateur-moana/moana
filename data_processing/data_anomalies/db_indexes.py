from data_processing.data_anomalies import settings as data_anomalies_settings

DATA_ANOMALIES_INDEX_FIELDS = [
    "analysis_period",
    "person_list_type",
] + data_anomalies_settings.DATA_ANOMALY_FIELDS
