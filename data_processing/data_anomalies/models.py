from django.db import models

from trackman.models import TrackingBaseModel

from backend.logs import db_indexes as logs_db_indexes
from backend.person_checks import choices as person_checks_choices
from backend.ship_types.ship_relation import WithShipTypeInfo
from backend.ships.base_models import ShipMovementBaseModel
from backend.utils.db_indexes import generate_indexes
from data_processing.data_anomalies import db_indexes as data_anomalies_db_indexes
from data_processing.utils.analysis_period import WithAnalysisPeriod


class DataAnomaly(
    TrackingBaseModel, WithAnalysisPeriod, ShipMovementBaseModel, WithShipTypeInfo
):
    """
    Tracks data anomalies for ship movements.
    The fields works as counter :
    - The number n would mean that n anomaly was found.
    - None means there never was any anomaly.
    - 0 means the anomaly was corrected.
    """

    person_list_type = models.CharField(
        "type de liste",
        max_length=256,
        choices=person_checks_choices.PERSON_LIST_TYPE_CHOICES,
    )
    name_formatting = models.PositiveIntegerField(
        "name formatting", null=True, blank=True
    )
    past_date_of_birth = models.PositiveIntegerField(
        "past date of birth", null=True, blank=True
    )
    future_date_of_birth = models.PositiveIntegerField(
        "future date of birth", null=True, blank=True
    )
    bad_encoding = models.PositiveIntegerField("bad encoding", null=True, blank=True)
    empty_name = models.PositiveIntegerField("empty name", null=True, blank=True)
    unknown_name = models.PositiveIntegerField("unknown name", null=True, blank=True)
    past_movement_date = models.PositiveIntegerField(
        "past movement date", null=True, blank=True
    )
    switch_name = models.PositiveIntegerField("switch name", null=True, blank=True)
    missing_nationalities = models.PositiveIntegerField(
        "missing nationalities", null=True, blank=True
    )
    empty_identity_doc = models.PositiveIntegerField(
        "empty identity doc", null=True, blank=True
    )
    unknown_port = models.PositiveIntegerField("unknown port", null=True, blank=True)
    switch_rouen_to_honfleur = models.PositiveIntegerField(
        "switch rouen to honfleur", null=True, blank=True
    )
    only_one_letter_name = models.PositiveIntegerField(
        "only one letter name", null=True, blank=True
    )
    unwanted_numbers = models.PositiveIntegerField(
        "unwanted numbers", null=True, blank=True
    )
    particle_name = models.PositiveIntegerField("particle name", null=True, blank=True)

    class Meta:
        verbose_name = "Anomalie de Données"
        verbose_name_plural = "Anomalies de Données"
        _da_indexes = generate_indexes(
            field_names=data_anomalies_db_indexes.DATA_ANOMALIES_INDEX_FIELDS,
            prefix="i3_",
        )
        _ship_data_indexes = generate_indexes(
            field_names=logs_db_indexes.SHIP_DATA_INDEX_FIELDS, prefix="i3_"
        )
        indexes = _da_indexes + _ship_data_indexes
