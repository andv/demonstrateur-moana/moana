from django.apps import AppConfig


class DataAnomaliesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "data_processing.data_anomalies"
    verbose_name = "2.2 Traitement des données - Anomalies de données"
