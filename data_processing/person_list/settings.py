ACTIONS_CREW_ANALYSIS = [
    "downloaded fpr crew",
    "downloaded annex fpr crew",
    "downloaded roc crew",
    "downloaded annex roc crew",
    "marked fpr crew as checked",
    "marked roc crew as checked",
]

ACTIONS_PASSENGERS_ANALYSIS = [
    "downloaded fpr passenger",
    "downloaded fpr passengers",
    "downloaded annex fpr passenger",
    "downloaded roc passenger",
    "downloaded annex roc passenger",
    "marked fpr passengers as checked",
    "marked roc passengers as checked",
]

ACTIONS_PASSENGER_IMPORT = [
    "email imported fal 6",
    "uploaded fal 6",
]

ACTIONS_CREW_IMPORT = [
    "email imported fal 5",
    "uploaded fal 5",
]

ACTIONS_DELETED_MOVEMENTS = [
    "sip s-wing deleted movement",
    "sip vigiesip deleted movement",
]

LOCK_NAME_PERSON_LIST_ANALYSIS = "person-list-analysis"
