from django.core.management.base import BaseCommand

from backend.logs.models import PersonListAnalyze, UserActionLog
from data_processing.person_list import settings as data_analysis_settings
from data_processing.person_list.management.commands.mixins import (
    PersonListAnalysisCommand,
)


class Command(PersonListAnalysisCommand, BaseCommand):
    help = "Run an analysis on deleted movements"

    def run_analysis(self, cutoff_date):
        """
        Collects all action log entries related to deleted movements
        and mark them as deleted on person list analyze model.
        """
        self.stdout.write("-- Start processing deleted movements.")
        qs = UserActionLog.objects.filter(
            action__in=data_analysis_settings.ACTIONS_DELETED_MOVEMENTS,
            created__gt=cutoff_date,
            ship_data__isnull=False,
        )
        for action_log in qs:
            self.mark_movement_as_deleted(action_log=action_log)
        count = qs.count()
        self.stdout.write(f"-- Processing deleted movements: {count}")

    def mark_movement_as_deleted(self, action_log):
        """
        Updates the deleted status of movements in the person list.
        """
        PersonListAnalyze.objects.filter(
            ship_call_id=action_log.ship_data.ship_call_id,
            way=action_log.ship_data.way,
        ).update(deleted=True)
