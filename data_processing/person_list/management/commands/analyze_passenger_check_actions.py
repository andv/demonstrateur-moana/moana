from django.core.management.base import BaseCommand

from backend.logs.models import UserActionLog
from backend.person_checks import choices as person_checks_choices
from data_processing.person_list import settings as data_analysis_settings
from data_processing.person_list.management.commands.mixins import (
    PersonListAnalysisCommand,
)


class Command(PersonListAnalysisCommand, BaseCommand):
    help = "Run an analysis on passengers check actions"

    def run_analysis(self, cutoff_date):
        """
        Collects all action log entries related to crew check actions and
        make the corresponding entry in analysis model.
        """
        self.stdout.write("-- Start processing passenger check actions.")
        qs = UserActionLog.objects.filter(
            action__in=data_analysis_settings.ACTIONS_PASSENGERS_ANALYSIS,
            created__gt=cutoff_date,
            ship_data__isnull=False,
        )
        for action_log in qs:
            number_of_persons = action_log.ship_data.number_of_passengers_parsed
            self.process_analysis_actions(
                action_log=action_log,
                person_list_type=person_checks_choices.PERSON_LIST_TYPE_PASSENGER,
                number_of_persons=number_of_persons,
            )
        count = qs.count()
        self.stdout.write(f"-- Processing passenger check actions: {count}")
