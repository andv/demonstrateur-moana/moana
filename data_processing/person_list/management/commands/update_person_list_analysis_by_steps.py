import datetime

from django.core.management.base import BaseCommand

from backend.logs.models import PersonListAnalyze
from data_processing.person_list.management.commands.mixins import (
    PersonListAnalysisCommand,
)


class Command(PersonListAnalysisCommand, BaseCommand):
    help = "Update the analysis table by triggering the model's save method. Slipt by steps of 15 days."

    def run_analysis(self, cutoff_date):
        """
        This will update the analysis table.
        """
        self.stdout.write("-- Start updating analysis table.")
        start_date = cutoff_date
        end_date = (
            PersonListAnalyze.objects.order_by("-relevant_time").first().relevant_time
        )
        while start_date.date() <= end_date.date():
            queryset = PersonListAnalyze.objects.filter(
                relevant_time__range=(
                    start_date,
                    start_date + datetime.timedelta(days=14),
                )
            )
            self.stdout.write(
                f"--- Start processing from {start_date.strftime('%Y-%m-%d')} to {(start_date + datetime.timedelta(days=14)).strftime('%Y-%m-%d')}"
            )
            for p in queryset:
                p.save()
            self.stdout.write(
                f"--- Finished processing from {start_date.strftime('%Y-%m-%d')} to {(start_date + datetime.timedelta(days=14)).strftime('%Y-%m-%d')}"
            )
            start_date += datetime.timedelta(days=14)
        self.stdout.write("-- Finished updating analysis table.")
