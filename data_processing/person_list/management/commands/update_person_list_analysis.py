import datetime

from django.core.management.base import BaseCommand

from backend.logs.models import PersonListAnalyze
from data_processing.person_list.management.commands.mixins import (
    PersonListAnalysisCommand,
)


class Command(PersonListAnalysisCommand, BaseCommand):
    help = "Update the analysis table by triggering the model's save method."

    def run_analysis(self, cutoff_date):
        """
        This will update the analysis table.
        """
        self.stdout.write("-- Start updating analysis table.")
        queryset = PersonListAnalyze.objects.filter(relevant_time__gt=cutoff_date)
        for p in queryset:
            p.save()
        self.stdout.write("-- updating analysis table.")
