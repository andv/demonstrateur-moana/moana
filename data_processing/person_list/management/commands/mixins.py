from django.utils import timezone

from backend.accounts.models import Team
from backend.logs.models import PersonListAnalyze
from data_processing.person_list import settings as data_analysis_settings
from data_processing.utils.management_commands import WithRunTask, WithShipData


class PersonListAnalysisCommand(WithShipData, WithRunTask):
    lock_name = data_analysis_settings.LOCK_NAME_PERSON_LIST_ANALYSIS

    def process_import_actions(self, action_log, person_list_type, number_of_persons):
        """
        Generic helper for processing person list import actions.
        """
        if not number_of_persons:
            return
        ship_data = self.get_ship_data(action_log)
        data = ship_data.copy()
        data.update(
            {
                "person_list_type": person_list_type,
                "date_imported": action_log.created,
                "number_of_persons": number_of_persons,
            }
        )
        port_of_call = ship_data.get("port_of_call")
        if not port_of_call:
            return
        team_qs = Team.objects.filter(ports__locode=port_of_call)
        for team in team_qs:
            self.update_tracking_entry(action_log, person_list_type, team, data)

    def process_analysis_actions(self, action_log, person_list_type, number_of_persons):
        """
        Generic helper for processing person list analysis actions.
        """
        if not number_of_persons:
            return
        ship_data = self.get_ship_data(action_log)
        data = ship_data.copy()
        data.update(
            {
                "list_analyzed": True,
                "person_list_type": person_list_type,
                "date_analyzed": action_log.created,
                "number_of_persons": number_of_persons,
            }
        )
        team = action_log.team
        self.update_tracking_entry(action_log, person_list_type, team, data)

    def update_tracking_entry(self, action_log, person_list_type, team, data):
        """
        Creates or updates tracking entry for the given person list.
        """
        PersonListAnalyze.objects.update_or_create(
            ship_call_id=action_log.ship_data.ship_call_id,
            way=action_log.ship_data.way,
            person_list_type=person_list_type,
            team=team,
            defaults=data,
        )
