from django.db import models

from trackman.models import TrackingBaseModel

from backend.logs import db_indexes as logs_db_indexes
from backend.person_checks import choices as person_checks_choices
from backend.ship_types.ship_relation import WithShipTypeInfo
from backend.ships.base_models import ShipMovementBaseModel
from backend.utils.db_indexes import generate_indexes
from data_processing.utils.analysis_period import WithAnalysisPeriod


class PersonListAnalyzeBaseModel(
    TrackingBaseModel, WithAnalysisPeriod, ShipMovementBaseModel, WithShipTypeInfo
):
    """
    This model tracks when person lists are imported, and also when we consider that the were
    analyzed by users when perform actions such as downloading lists or marking lists as checked.
    """

    team = models.CharField("équipe", max_length=256, blank=True)
    person_list_type = models.CharField(
        "type de liste",
        max_length=256,
        choices=person_checks_choices.PERSON_LIST_TYPE_CHOICES,
    )
    list_analyzed = models.BooleanField(
        "Liste analysée ?",
        default=False,
        help_text=(
            "On considère que la liste est analysée lorsqu'elle est téléchargée "
            "ou lorsqu'un un traitement FPR/ROC est fait"
        ),
    )
    number_of_persons = models.PositiveIntegerField(
        "nombre de personnes", blank=True, null=True
    )
    date_imported = models.DateTimeField("date de l'import", blank=True, null=True)
    date_analyzed = models.DateTimeField(
        "date d'analyse de la liste",
        blank=True,
        null=True,
        help_text=(
            "lorsqu'elle est téléchargée ou lorsqu'un un traitement FPR/ROC est fait."
        ),
    )
    deleted = models.BooleanField("mouvement supprimé ?", default=False)

    class Meta:
        abstract = True
        verbose_name = "Liste analysées"
        verbose_name_plural = "Listes analysées"
        _pla_indexes = generate_indexes(
            field_names=logs_db_indexes.PERSON_LIST_ANALYZE_INDEX_FIELDS, prefix="i1_"
        )
        _ship_data_indexes = generate_indexes(
            field_names=logs_db_indexes.SHIP_DATA_INDEX_FIELDS, prefix="i1_"
        )
        indexes = _pla_indexes + _ship_data_indexes
