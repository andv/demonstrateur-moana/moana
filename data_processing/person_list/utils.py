from django.conf import settings
from django.utils.timezone import datetime, make_aware

import pytz


def get_analysis_period(dt):
    if not dt:
        return ""
    for period_name, period_dates in settings.DATA_ANALYSIS_PERIODS.items():
        start_date, end_date = period_dates
        start = make_aware(datetime.strptime(start_date, "%Y-%m-%d"), timezone=pytz.UTC)
        end = make_aware(datetime.strptime(end_date, "%Y-%m-%d"), timezone=pytz.UTC)
        if start.date() <= dt.date() <= end.date():
            return period_name
    return ""
