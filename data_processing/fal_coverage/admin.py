from django.conf import settings
from django.contrib import admin

from trackman.admin import TrackingModelAdminMixin

from backend.logs.admin import SHIP_DATA_SEARCH_FIELDS
from data_processing.fal_coverage.models import FalCoverage


class FalCoverageAdmin(TrackingModelAdminMixin, admin.ModelAdmin):
    list_display = [
        "analysis_period",
        "port_of_call",
        "sip",
        "ship_call_id",
        "agent_name",
        "way",
        "has_fal1",
        "has_fal5",
        "has_fal6",
        "needs_fal6",
        "passenger_list_delay_type",
        "crew_list_delay_type",
        "sip_import",
        "imo_number",
        "ship_name",
        "last_port",
        "next_port",
        "relevant_time",
        "deleted",
        "ship_type_simple",
    ]
    search_fields = SHIP_DATA_SEARCH_FIELDS + ["analysis_period", "sip"]
    list_filter = [
        "analysis_period",
        "deleted",
        "has_fal1",
        "has_fal5",
        "has_fal6",
        "needs_fal6",
        "passenger_list_delay_type",
        "crew_list_delay_type",
        "sip_import",
        "ship_type_simple",
        "sip",
    ]
    list_display_links = ["analysis_period", "port_of_call"]


if settings.TRACKMAN_ENABLED:
    admin.site.register(FalCoverage, FalCoverageAdmin)
