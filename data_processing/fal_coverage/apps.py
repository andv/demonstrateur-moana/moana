from django.apps import AppConfig


class FalCoverageConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "data_processing.fal_coverage"
    verbose_name = "2.3 Traitement des données - Couverture Fal"
