LOCK_NAME_FAL_COVERAGE = "fal-coverage"

ACTIONS_FAL1_IMPORT = [
    "email imported fal 1",
    "uploaded fal 1",
]

ACTIONS_FAL6_IMPORT = [
    "email imported fal 6",
    "uploaded fal 6",
]

ACTIONS_FAL5_IMPORT = [
    "email imported fal 5",
    "uploaded fal 5",
]

ACTIONS_SIP_IMPORT = [
    "sip s-wing added movement",
    "sip s-wing changed movement",
    "sip vigiesip added movement",
    "sip vigiesip changed movement",
]

ACTIONS_DELETED = [
    "sip s-wing deleted movement",
    "sip vigiesip deleted movement",
]

FAL6_NEEDED_FOR_SHIP_TYPES = ["Ferry", "Croisière"]
