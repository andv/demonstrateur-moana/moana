FAL_COVERAGE_INDEX_FIELDS = [
    "analysis_period",
    "sip",
    "agent_name",
    "has_fal1",
    "has_fal5",
    "has_fal6",
    "needs_fal6",
    "sip_import",
    "deleted",
]
