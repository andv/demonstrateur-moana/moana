from django.utils import timezone

from backend.logs.models import UserActionLog
from backend.utils.sentry import sentry_capture
from data_processing.fal_coverage import settings as fal_coverage_settings
from data_processing.fal_coverage.models import FalCoverage
from data_processing.utils.management_commands import WithRunTask, WithShipData


class FalCoverageCommand(WithShipData, WithRunTask):
    help = "Run an analysis on fal coverage"
    lock_name = fal_coverage_settings.LOCK_NAME_FAL_COVERAGE
    fal_coverage_data = None  # Indicate FAL is present, ex: {"has_fal1": True}

    def run_analysis(self, cutoff_date):
        """
        Collects all action log entries and makes the corresponding
        analysis entry.
        """
        self.stdout.write("-- Start processing fal coverage.")
        qs = UserActionLog.objects.filter(
            action__in=self.list_of_log_action_names,
            created__gt=cutoff_date,
            ship_data__isnull=False,
        )
        for action_log in qs:
            self.process_fal_import_action(action_log=action_log)
        count = qs.count()
        self.stdout.write(f"-- Processing fal import actions: {count}")

    def get_fal_coverage_data(self):
        return self.fal_coverage_data or {}

    def process_fal_import_action(self, action_log):
        """
        Process the fal import actions log before updating the tracking entry.
        """
        ship_data = self.get_ship_data(action_log)
        data = ship_data.copy()
        fal_coverage_data = self.get_fal_coverage_data()
        data.update(fal_coverage_data)
        self.update_tracking_entry(action_log, data)

    def update_tracking_entry(self, action_log, data):
        """
        Creates or updates tracking entry for the given ship movement.
        """
        FalCoverage.objects.update_or_create(
            ship_call_id=action_log.ship_data.ship_call_id,
            way=action_log.ship_data.way,
            sip="",
            defaults=data,
        )
