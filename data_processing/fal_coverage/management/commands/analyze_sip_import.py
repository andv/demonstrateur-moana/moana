from django.core.management.base import BaseCommand

from data_processing.fal_coverage import settings as fal_coverage_settings
from data_processing.fal_coverage.management.commands.mixins import FalCoverageCommand


class Command(FalCoverageCommand, BaseCommand):
    list_of_log_action_names = fal_coverage_settings.ACTIONS_SIP_IMPORT
    fal_coverage_data = {"sip_import": True}
