from django.db import models

from trackman.models import TrackingBaseModel

from backend.logs.db_indexes import SHIP_DATA_INDEX_FIELDS
from backend.ship_types.ship_relation import WithShipTypeInfo
from backend.ships.base_models import ShipMovementBaseModel
from backend.utils.db_indexes import generate_indexes
from data_processing.fal_coverage import settings as fal_coverage_settings
from data_processing.fal_coverage.db_indexes import FAL_COVERAGE_INDEX_FIELDS
from data_processing.utils.analysis_period import WithAnalysisPeriod


class FalCoverage(
    TrackingBaseModel, WithAnalysisPeriod, ShipMovementBaseModel, WithShipTypeInfo
):
    """
    This model tracks the FAL coverage on ports.
    """

    sip = models.CharField("sip", max_length=256, blank=True)
    has_fal1 = models.BooleanField("FAL1 présent ?", default=False)
    has_fal5 = models.BooleanField("FAL5 présent ?", default=False)
    has_fal6 = models.BooleanField("FAL6 présent ?", default=False)
    needs_fal6 = models.BooleanField("FAL6 attendu ?", default=False)
    sip_import = models.BooleanField("Import SIP ?", default=False)
    deleted = models.BooleanField("mouvement supprimé ?", default=False)

    class Meta:
        verbose_name = "Couverture FAL"
        verbose_name_plural = "Couverture FAL"
        _pla_indexes = generate_indexes(
            field_names=FAL_COVERAGE_INDEX_FIELDS, prefix="i2_"
        )
        _ship_data_indexes = generate_indexes(
            field_names=SHIP_DATA_INDEX_FIELDS, prefix="i2_"
        )
        indexes = _pla_indexes + _ship_data_indexes

    def update_instance(self, field_name, value):
        """
        Update the value of the given field name on the current
        instance in the database, without triggering signals like pre/post save.
        """
        object_instance = self.__class__.objects.filter(id=self.id)
        object_instance.update(**{field_name: value})

    def update_fal6_needed(self, ship_type_simple):
        """
        Set the needs_fal6 field to True when the ship type is one of the
        types that required FAL6 passenger list - usually Ferry and Cruise ships.
        """
        ship_type_list = fal_coverage_settings.FAL6_NEEDED_FOR_SHIP_TYPES
        needs_fal6 = ship_type_simple in ship_type_list
        self.needs_fal6 = needs_fal6
        self.update_instance("needs_fal6", needs_fal6)

    def save(self, *args, **kwargs):
        # Save the instance first to update status 'needs_fal6'
        result = super().save(*args, **kwargs)
        if self.ship_type_simple:
            self.update_fal6_needed(self.ship_type_simple)
        return result
