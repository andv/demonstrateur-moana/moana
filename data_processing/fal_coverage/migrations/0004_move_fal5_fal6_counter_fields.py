# Generated by Django 4.2.10 on 2024-03-05 16:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("fal_coverage", "0003_fix_typo_on_geographic_area_choices"),
    ]

    operations = [
        migrations.AddField(
            model_name="falcoverage",
            name="fal5_counter",
            field=models.PositiveSmallIntegerField(
                default=0, verbose_name="Nombre de FAL 5 reçus"
            ),
        ),
        migrations.AddField(
            model_name="falcoverage",
            name="fal6_counter",
            field=models.PositiveSmallIntegerField(
                default=0, verbose_name="Nombre de FAL 6 reçus"
            ),
        ),
    ]
