from django.db import models

from data_processing.person_list.utils import get_analysis_period


class WithAnalysisPeriod(models.Model):

    analysis_period = models.CharField("période d'analyse", max_length=256, blank=True)

    class Meta:
        abstract = True

    def update_analysis_period(self, relevant_time):
        """
        Update the analysis_period in the database without triggering the save method again.
        """
        new_analysis_period = get_analysis_period(relevant_time)
        object_instance = self.__class__.objects.filter(id=self.id)
        object_instance.update(analysis_period=new_analysis_period)

    def save(self, *args, **kwargs):
        # Save the instance first to get the relevant_time
        result = super().save(*args, **kwargs)
        if self.relevant_time:
            self.update_analysis_period(self.relevant_time)
        return result
