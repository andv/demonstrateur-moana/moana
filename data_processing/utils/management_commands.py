from django.utils import timezone

from backend.utils.sentry import sentry_capture
from marathon.commands import SingleRunnerCommand


class WithShipData:
    """
    Helper to work with ship data.
    This class is meant to be used as a mixin for a management command.
    """

    def get_ship_data(self, action_log):
        """
        Get ship data from the given action log.
        """
        ship = action_log.ship_data
        ship_data_dict = {
            "imo_number": ship.imo_number,
            "port_of_call": ship.port_of_call,
            "ship_name": ship.ship_name,
            "call_sign": ship.call_sign,
            "flag_state_of_ship": ship.flag_state_of_ship,
            "last_port": ship.last_port,
            "next_port": ship.next_port,
            "eta_to_port_of_call": ship.eta_to_port_of_call,
            "etd_from_port_of_call": ship.etd_from_port_of_call,
            "ata_to_port_of_call": ship.ata_to_port_of_call,
            "atd_from_port_of_call": ship.atd_from_port_of_call,
            "ship_type_reference": ship.ship_type_reference,
            "ship_type_fr": ship.ship_type_fr,
            "ship_type_simple": ship.ship_type_simple,
            "agent_name": ship.agent_name,
            "agent_phone": ship.agent_phone,
            "agent_email": ship.agent_email,
            "position_in_port_of_call": ship.position_in_port_of_call,
            "sip_fal5_available": ship.sip_fal5_available,
            "sip_fal6_available": ship.sip_fal6_available,
            "gross_tonnage": ship.gross_tonnage,
            "crew_list_delay_type": ship.crew_list_delay_type,
            "passenger_list_delay_type": ship.passenger_list_delay_type,
        }
        ship_data_without_empty = {k: v for k, v in ship_data_dict.items() if v}
        return ship_data_without_empty


class WithRunTask(SingleRunnerCommand):
    """
    Helper to run a task.
    This class is meant to be used as a mixin for a management command.
    """

    list_of_log_action_names = None  # Actions names to be consider for analysis

    def add_arguments(self, parser):
        parser.add_argument(
            "-d",
            "--days",
            type=int,
            default=7,
            help="The number of days in the past to look at log entries",
        )

    @sentry_capture
    def run_task(self, *args, **options):
        self.stdout.write(
            f"Start analysis for actions: {self.list_of_log_action_names}"
        )
        days_old = options["days"]
        cutoff_date = timezone.now() - timezone.timedelta(days=days_old)
        self.stdout.write(f"-- Looking at logs {days_old} days old : {cutoff_date}")
        self.run_analysis(cutoff_date=cutoff_date)
        self.stdout.write("End analysis.")
