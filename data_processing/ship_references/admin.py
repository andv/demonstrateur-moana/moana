from django.contrib import admin

from backend.nationalities.admin import NATIONALITY_FIELDSET
from backend.ships.admin import ShipMovementBaseAdmin
from backend.utils.admin import ReadOnlyAdminMixin
from data_processing.ship_references.models import ShipMovementReference


@admin.register(ShipMovementReference)
class ShipMovementReferenceAdmin(ReadOnlyAdminMixin, ShipMovementBaseAdmin):
    fieldsets = (
        ShipMovementBaseAdmin.fieldsets
        + [
            (
                "Type de navire",
                {
                    "fields": [
                        "ship_type_reference",
                        "ship_type_fr",
                        "ship_type_simple",
                    ]
                },
            ),
        ]
        + NATIONALITY_FIELDSET
    )
