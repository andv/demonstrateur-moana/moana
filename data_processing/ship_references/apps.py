from django.apps import AppConfig


class ShipReferencesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "data_processing.ship_references"
    verbose_name = "2.4 Traitement des données - Références mouvements de navires"
