from django.db.models.signals import post_save
from django.dispatch import receiver

from backend.persons import person_counters
from backend.ships.base_models import ShipMovementBaseModel
from backend.ships.models import ShipMovement
from data_processing.ship_references.models import ShipMovementReference


@receiver(post_save, sender=ShipMovement)
def update_ship_reference_from_ship_movement(sender, instance, **kwargs):
    ship = instance
    reference_ship, _ = ShipMovementReference.objects.get_or_create(
        ship_call_id=ship.ship_call_id, way=ship.way
    )
    fields_to_copy = [f.name for f in ShipMovementBaseModel._meta.get_fields()]
    fields_to_copy.pop(fields_to_copy.index("ship_call_id"))
    fields_to_copy.pop(fields_to_copy.index("way"))
    for field_name in fields_to_copy:
        setattr(reference_ship, field_name, getattr(ship, field_name))
    ship_reference_data = {}
    counters_data = reference_ship.get_nationality_counters_from_ship_movement(ship)
    ship_reference_data.update(counters_data)
    person_counters_data = reference_ship.get_person_counters_from_ship_movement(ship)
    ship_reference_data.update(person_counters_data)
    for field_name, field_value in ship_reference_data.items():
        setattr(reference_ship, field_name, field_value)
    reference_ship.save()
