from django.db import models

from model_utils.models import TimeStampedModel
from trackman.models import TrackingBaseModel

from backend.nationalities.nationality_counters import (
    NationalityCounterHelper,
    WithNationalityCounters,
)
from backend.persons.person_counters import (
    PersonCountersHelper,
    WithParsedPersonCounters,
    WithPersonCounters,
)
from backend.ship_types.ship_relation import WithShipTypeInfo
from backend.ships.base_models import ShipMovementBaseModel


class ShipMovementReference(
    TrackingBaseModel,
    ShipMovementBaseModel,
    WithShipTypeInfo,
    WithNationalityCounters,
    NationalityCounterHelper,
    WithPersonCounters,
    WithParsedPersonCounters,
    PersonCountersHelper,
    TimeStampedModel,
):

    class Meta:
        verbose_name = "Référence Mouvement de Navire"
        verbose_name_plural = "Références Mouvements de Navires"
