# Moana 1.0.1

_18 décembre 2023_


## Correctifs

- Mauvais filtre / classement dans les statuts d'escale (embarquants / débarquants / en transit) dans les listes passagers #1078
