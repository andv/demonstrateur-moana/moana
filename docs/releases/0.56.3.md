# Moana 0.56.3

_5 décembre 2023_


## Correctif
- Metabase - Prendre en compte les mouvements supprimés dans la table d'analyse des listes de personnes #1066

