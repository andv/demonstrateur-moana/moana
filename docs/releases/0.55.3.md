# Moana 0.55.3

_23 novembre 2023_


## Fonctionnalités
- Avoir un espace commentaire associé aux traitements des listes #520


## Correctifs
- Ajout de valeurs par défaut pour les variables d'environnement Vue
- Simplification du fichier .env.example en effaçant les VUE_APP non essentielles
- Ajout de la section "Développement et harmonisation du code" sur le Read Me
