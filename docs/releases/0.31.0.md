# Moana 0.31.0

_26 septembre 2022_

## Correctifs
- Certaines données sont présentes sur SWing et ne sont pas présentes sur Moana #551

## Fonctionnalités

- Intégrer Données du SIP SWING via Webservices #550

## Refactoring

- Import SIP : Simplifier le foncitonnement des tâches d'import #637
