# Moana 0.44.3

_10 mai 2023_


## Fonctionnalités

- Afficher une notification quand il y a une nouvelle liste #506
- Mise à jour du référentiel agent maritime #854
