# Moana 0.20.0

_14 juin 2022_


## Correctif
- Il y a un problème avec le champs "team" pour les utilisateurs "Anonymous" #410
- Il y a un bug avec l'object "Site" à l'initialisation de Moana #409
- Mise à jour des librairies python


## Devops
- Déploiement : Mettre en place les app review Scalingo #525
