# Moana 0.17.0

_23 mai 2022_


## Fonctionnalités
- Import des données SIP de Sète et de Port La Nouvelle. #485
- Ajouter les types de navires sur les données importées des SIP. #486


## Refactoring
- Refactoring des utilitaires django. #477
