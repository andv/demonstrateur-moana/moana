# Moana 0.20.1

_17 juin 2022_


## Fonctionnalités

- Mise en place du mode sombre - dark-mode #320
- Ajouter les imports SIP pour Vigiesip #536


## Correctifs
- Problème dans les tests unitaires avec le framework sites et MagicAuth
