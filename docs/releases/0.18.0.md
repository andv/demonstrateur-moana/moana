# Moana 0.18.0

_24 mai 2022_

## Fonctionnalités

- Mise en place du DSFR. #301 #297 #298
- Amélioriation de la zone "Import manuel FAL". #296

## Refactoring

- Enlève dépendance à Vuetify
- Passage de Vue2 à Vue3
